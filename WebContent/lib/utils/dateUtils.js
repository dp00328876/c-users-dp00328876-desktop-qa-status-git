(function() {
	function DateDiff(date1, date2) {
		this.days = null;
		this.hours = null;
		this.minutes = null;
		this.seconds = null;
		this.date1 = date1;
		this.date2 = date2;

		this.init();
	}

	DateDiff.prototype.init = function() {
		var data = new DateMeasure(this.date1 - this.date2);
		this.days = data.days;
		this.hours = data.hours;
		this.minutes = data.minutes;
		this.seconds = data.seconds;
	};

	function DateMeasure(ms) {
		var d, h, m, s;
		s = Math.ceil(ms / 1000);
		m = Math.ceil(s / 60);
		h = Math.ceil(m / 60);
		s = s % 60;
		m = m % 60;
		d = Math.ceil(h / 24);

		this.days = d;
		this.hours = h;
		this.minutes = m;
		this.seconds = s;
	};	

	Date.diff = function(date1, date2) {
		return new DateDiff(date1, date2);
	};

	Date.prototype.diff = function(date2) {
		return new DateDiff(this, date2);
	};
})();

 function convertToDate(dateString){	 
     var year=dateString.slice(6,10);
     var month=dateString.slice(0,2);
     var day=dateString.slice(3,5);
     
     return new Date(year,month,day,0,0,0,0);
 }
/*
  * This SDK is a proprietary of CenturyLink, contains all communication services APIs.
  * Author: Vikash Kumar
  * Copyrights Century Link @ 2016.
 */
var CTL = (function(environment) {
	

	/*
	 * START OF BROWSER SPECIFIC CODE
	 */

	/*
	 * Polyfill for 'endsWith' to support on below browsers 
	 * [IE, Opera, Android, IE Mobile, Opera Mobile, Safari Mobile]
	 */
	if (!String.prototype.endsWith) {
		String.prototype.endsWith = function(searchString, position) {
			var subjectString = this.toString();
			if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
				position = subjectString.length;
			}
			position -= searchString.length;
			var lastIndex = subjectString.indexOf(searchString, position);
			return lastIndex !== -1 && lastIndex === position;
		};
	}

	/*
	 * END OF BROWSER SPECIFIC CODE
	 */
	
	/* Configuration parameters 
	 * It contains Global variables and various constants
	 * 
	 * 
	 * @Param
	 * 	@Name: Config
	 *  @Description: Configuration related to Server

	 * 	@Name: apiUrls
	 *  @Description: URL for APIs
	 * 
	 *	@Name: authHeader, reqHeader
	 *  @Description: Template for Headers
	 *  
	 *  @Name: successResponse, errorResponse
	 *  @Description: Template for Response
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	var _defaults ={			
			enviornmentBaseURIs: {
				'INTR':'https://lab.iaf.centurylink.com:8889',
				'CERT':'https://lab.af.centurylink.com:8889',
				'PROD':'https://live.af.centurylink.com:8889'				
			},
			apiUrls:{
				'oAuth': '/token',
				'userCreds': '/user',
				'userRUDCreds': '/user/:userName',
				'networkUserInvite': '/transactionService/networkUserInvitation/request',
				'invitedUserList':'/account/:accountName/:domain/invited/user/list',
				'updateInvitation':'/transactionService/networkUserInvitation/request',
				'confirmUser': '/transactionService/networkUserInvitation/answer/:seatId/:networkId',
				'quickStartUserCreate' : '/user/quickstart/:seatId',
				'userByNetworkId':'/account/:accountName/subaccount/:networkId/user/list',
				'allNetworkUsers':'/account/:accountName/:domain/user/list',
				'networkIdList':'/account/:accountName/subaccount/list',
				'networkId':'/account/:accountName/subaccount',
				'availablePhoneNumber': '/moniker/v1/available/phoneNumbers',
				'reservePhoneNumber':'/moniker/v1/phoneNumbers/:phoneNumber',
				'readSeat':'/OrderManagement/api/v1/seat',
				'getPhoneNumbers':'/moniker/v1/phoneNumbers',
				'orderManagement': '/OrderManagement/api/v1/account',
				'orderManagementSubAccount': '/OrderManagement/api/v1/account/networkId',
				'orderManagementProduct':'/OrderManagement/api/v1/seat',
				'orderManagementAssignProduct':'/OrderManagement/api/v1/networkId',
				'getProductConfig':'/courier/v1/serviceprofile/:phoneNumber',
				'configureProduct':'/courier/v1/serviceprofile/:phoneNumber',
				'unconfigureProduct':'/courier/v1/serviceprofile/:phoneNumber/:productName',
				'updateProductConfig':'/courier/v1/serviceprofile/:phoneNumber/:productName',
				'sendSMS':'/courier/v1/messages/:phoneNumber',
				'readSMS':'/dumpster/v1/sms',
				'deleteSMS':'/dumpster/v1/sms',
				'smsStatus':'/courier/v1/status/:phoneNumber',
				'feedback':'/feedme/v1/feedback',
				'userList':'/account/:accountName/user/list',
				'getAccountHierarchy':'/ThirdPartyProxy/v2-experimental/subaccounts/:accountName',
				'getAccountInformation':'/ThirdPartyProxy/v2-experimental/accounts/satelliteInfo/:accountName',
				'updateAccount':'/account'
			},
			authHeader:{
			    'Content-Type': 'application/x-www-form-urlencoded',
			    'Accept': 'application/json'
			},
			reqHeader:{
			    'Content-Type': 'application/json',
			    'Accept': 'application/json'
			},
			successResponse:{
			    'ResponseStatus': 'Success',
			    'StatusCode': '200',
			    'StatusText': 'OK'
			},
			errorResponse:{
			    'ResponseStatus': 'Error',
			    'StatusCode': '',
			    'StatusText': ''
			}
	};
	
	/*
	 * Wrapper for all common utility methods
	 * 
	 */
	var _Common = function(){
		
		// Return 5 digit random number
	    var _getRandomFiveDigitNumber = function getRandomFiveDigitNumber(){
	        return Math.floor((Math.random()*89999)+10000);
	    };
	    
	    // Decode Base64 String
		function _decodeData (data, splitChar, position) {
		    var jsonPayload ={};
		    if(data && data != undefined){
		        var jwt_token_array = data.split(splitChar);
		        var decodedPayload = atob(jwt_token_array[position]);
		        jsonPayload = JSON.parse(decodedPayload);
		    }
		    return jsonPayload;
		};

		// Clone any Object, Date, Array
		function _clone(obj) {
		    var copy;

		    // Handle the 3 simple types, and null or undefined
		    if (null == obj || "object" != typeof obj) return obj;

		    // Handle Date
		    if (obj instanceof Date) {
		        copy = new Date();
		        copy.setTime(obj.getTime());
		        return copy;
		    }

		    // Handle Array
		    if (obj instanceof Array) {
		        copy = [];
		        for (var i = 0, len = obj.length; i < len; i++) {
		            copy[i] = _clone(obj[i]);
		        }
		        return copy;
		    }

		    // Handle Object
		    if (obj instanceof Object) {
		        copy = {};
		        for (var attr in obj) {
		            if (obj.hasOwnProperty(attr)) copy[attr] = _clone(obj[attr]);
		        }
		        return copy;
		    }

		    throw new Error("Unable to copy obj! Its type isn't supported.");
		};
		//Populate common headers
		function _populateHeaders(commonHeaders){
			if(sessionStorage.getItem('ctl_auth') != undefined){
				var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));	
				commonHeaders.Authorization = tokenData.Authorization;
				//commonHeaders.AccountName = tokenData.AccountName;
				//commonHeaders.CorrelationId = _Common.getRandomFiveDigitNumber();
			}		
			return commonHeaders;
		}
		
		//Get User Type 
		function _isUserType(userType){
			
			if(userType != undefined && userType != null && sessionStorage.getItem('ctl_auth') != undefined){
				var accountInfoData = JSON.parse(sessionStorage.getItem('ctl_auth'));
				if(accountInfoData != undefined && accountInfoData.groups != undefined && accountInfoData.groups != null){
					var userTypes = accountInfoData.groups;
					if(userTypes !=undefined && userTypes.indexOf(userType) != -1){
						return true;
					}else{
						return false;
					}
				}
			}else{
				return false;
			}
		}
		
		return{
			'getRandomFiveDigitNumber': _getRandomFiveDigitNumber,
			'decodeData': _decodeData,
			'clone':_clone,
			'populateHeaders': _populateHeaders,
			'isUserType':_isUserType
		};
	    
	}();
	
	// Wrapper for all utility methods which are used for Request and Response processing
	var _HTTPHandler = function(){
		
		// Create Query String in format a=b&c=d
		function _createQueryData(data){
			if(data != undefined){
				var str =[];
			    for (var d in data)			    	
			    	str.push(encodeURIComponent(d)+"="+ encodeURIComponent(data[d]));
			    return str.join("&");
			}else{
				return data;
			}		
		};
		
		// Replace parameters in URL with dynamic values
		function _createURIParam(url, data){
			if(data != undefined){
					var result = url.replace(/:(\w+)(\/|\b)/g, function(substring, match, nextMatch){
				    return data[match] + nextMatch;
				});
					return result;
			}else{
				return data;
			}		
		};
		
		// Set Headers into Request
		function _updateHeader(request, headers){
			if(request != undefined && headers != undefined){
			    for (var header in headers)
			    	request.setRequestHeader(header, headers[header]);
			    return request;
			}else{
				return request;
			}		
		};
		
		// Generate Success response in template
		function _generateSuccessResponse(response){
			var res = _Common.clone(_defaults.successResponse);
			res.StatusCode = response.status;
			res.StatusText = response.statusText;
			try{
				res.data = JSON.parse((response.responseText != undefined && response.responseText != '') ? response.responseText : response.response);
			}catch(e){
				res.data = response.statusText;
			}	
			return res;
		};

		// Generate Error response in template
		function _generateErrorResponse(response, requestedURL, reqMethod){
			var res = _Common.clone(_defaults.errorResponse);
			res.StatusCode = response.status;
			res.StatusText = response.statusText;
			try{				
				res.data = JSON.parse((response.responseText != undefined && response.responseText != '') ? response.responseText : response.StatusText);				
				if(res.data.responseMessage != undefined && res.data.responseMessage === ''){
					res.data.responseMessage = response.statusText;
				}
				res.data.StatusCode = response.status;
				res.data.StatusText = response.statusText;
				res.data.requestURL = requestedURL;
				res.data.reqMethod = reqMethod;
			}catch(e){
				res.data = response.statusText;
			}
			
				
			return res;
		};

		// Process HTTP request and return response
		function _processRequest(request) {
			// Return a new promise.
			return new Promise(function(resolve, reject) {
				_sendRequest(request, resolve, reject);
			});
		};

		function _sendRequest(request, resolve, reject, oldRequest,	requestRetry) {
			// Do the usual XHR stuff
			var req = new XMLHttpRequest();
			req.open(request.method, request.url);
			_updateHeader(req, request.headers);

			req.onload = function() {
				// This is called even on 404 etc
				// so check the status
				if (req.status >= 200 && req.status < 300) {
					// Resolve the promise with the response text
					// resolve(JSON.parse(req.response));
					var response = _generateSuccessResponse(req);
					if (request.processResponse != undefined) {
						request.processResponse(response);
					}
					resolve(response);
				} else if (req.status == 401
						&& (oldRequest == undefined || oldRequest == null)
						&& (requestRetry == undefined || !requestRetry)) {
					_refreshTokenAndExecuteRequest(request, resolve, reject, req);
				} else {
					// Otherwise reject with the status text
					// which will hopefully be a meaningful error
					reject(_generateErrorResponse(req, request.url, request.method));
				}
			};

			// Handle network errors
			req.onerror = function() {
				var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));
				var refresh_token = (tokenData != undefined && tokenData != null) ? tokenData.refresh_token : '';
				if ((refresh_token != undefined && refresh_token != '') 
						&& (oldRequest == undefined || oldRequest == null)
						&& (requestRetry == undefined || !requestRetry)) {
					_refreshTokenAndExecuteRequest(request, resolve, reject);
				} else {
					reject(req);
				}
			};

			// Make the request
			if (request.body != undefined) {
				if (request.headers != undefined
						&& request.headers['Content-Type'] == 'application/x-www-form-urlencoded') {
					req.send(request.body);
				} else {
					req.send(JSON.stringify(request.body));
				}
			} else {
				req.send();
			}
		}
			
			// Common method to set all Request variables
			function _process(request){
				if(request.url != undefined && request.urlParam != undefined)
					request.url = _createURIParam(request.url, request.urlParam);
				
				if(request.queryParam != undefined)
					request.url = request.url + '?' + _createQueryData(request.queryParam);
				
				request.url = _defaults.enviornmentBaseURIs[environment] + request.url;
				request.headers = request.headers;
				return _processRequest(request);
			}
			
			// GET method to set all Request variables
			var _processGet = function (inputReq){			
				inputReq.method = 'GET';
				return _process(inputReq);				
			};
			
			// POST method to set all Request variables
			var _processPost = function(inputReq){
				inputReq.method = 'POST';
				if(inputReq.headers != undefined && inputReq.headers['Content-Type'] == 'application/x-www-form-urlencoded')
					inputReq.body = _createQueryData(inputReq.body);

				return _process(inputReq);				
			};
			
			// PUT method to set all Request variables
			var _processPut = function(inputReq){
				inputReq.method = 'PUT';
				if(inputReq.headers != undefined && inputReq.headers['Content-Type'] == 'application/x-www-form-urlencoded')
					inputReq.body = _createQueryData(inputReq.body);

				return _process(inputReq);
			};
			
			// Delete method to set all Request variables
			var _processDelete =function(inputReq){
				inputReq.method = 'DELETE';
				if(inputReq.headers != undefined && inputReq.headers['Content-Type'] == 'application/x-www-form-urlencoded')
					inputReq.body = _createQueryData(inputReq.body);

				return _process(inputReq);
			};
			
			var _refreshTokenAndExecuteRequest = function(oldRequest, resolve,	reject, req) {
			var inputData = {};
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));
			if(tokenData === undefined || tokenData === null || tokenData.refresh_token === undefined || tokenData.refresh_token === null){
				return reject(_generateErrorResponse(req, oldRequest.url, oldRequest.method));
			}
			inputData.refresh_token = tokenData.refresh_token;
			inputData.grant_type = "refresh_token";
			inputData.withJWT = true;

			var request = {};
			request.url = _defaults.apiUrls.oAuth;
			request.headers = _Common.clone(_defaults.authHeader);
			request.body = inputData;
			request.processResponse = function(response) {
				var ctl_auth = JSON.parse(sessionStorage.getItem('ctl_auth'));
				// data = JSON.parse(response.data);
				ctl_auth.Authorization = response.data.token_type + ' '
						+ response.data.access_token;
				ctl_auth.token_type = response.data.token_type;
				ctl_auth.expires_in = response.data.expires_in;
				ctl_auth.refresh_token = response.data.refresh_token;
				// ctl_auth.jwtPayload = _decodeData(response.data.jwt_token);
				ctl_auth.jwtPayload = response.data.jwt_token;
				ctl_auth.AccountName = response.data.account_name != undefined ? response.data.account_name : ctl_auth.AccountName;
				if(response.data.network_ids != undefined) ctl_auth.network_id = response.data.network_ids[0];
				sessionStorage.setItem('ctl_auth', JSON.stringify(ctl_auth));
			};

			if (request.url != undefined && request.urlParam != undefined)
				request.url = _createURIParam(request.url, request.urlParam);

			if (request.queryParam != undefined)
				request.url = request.url + '?'
						+ _createQueryData(request.queryParam);

			request.url = _defaults.enviornmentBaseURIs[environment] + request.url;
			if (request.headers != undefined
					&& request.headers['Content-Type'] == 'application/x-www-form-urlencoded')
				request.body = _createQueryData(request.body);
			request.method = 'POST';

			_sendRequest(request, function(success) {
				var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));
				oldRequest.headers.Authorization = tokenData.Authorization;
				_sendRequest(oldRequest, resolve, reject, null, true);
			}, function(error) {
				reject(error);
			}, oldRequest);

		};
			
			return{
				'processGet': _processGet,
				'processPost': _processPost,
				'processPut': _processPut,
				'processDelete': _processDelete
			};
			
	}();
	
	
	/* 
	 * Container to expose Authentication related Services 
	 * 
	 * 
	 */
	
	var _Oauth2 = function() {
		
		/*
		 * Initialization of SDK
		 * 
		 * Login Method - Invokes OAuth API for login
		 * Parse Response and Store response in Session Storage
		 * 
		 * @Param: username - Username for User
		 * @Param: password - Password for User
		 * 
		 */
		var _login = function(username, password){
		
			var inputData = {};
			inputData.username = username;
			inputData.password = password;
			inputData.grant_type = 'password';
			inputData.withJWT = true;
			
			var request ={};
			request.url = _defaults.apiUrls.oAuth;
			request.headers = _Common.clone(_defaults.authHeader);
			request.body = inputData;
			request.processResponse = function(response){
				var ctl_auth = {};
				//data = response.data;
			  	ctl_auth.Authorization = response.data.token_type + ' ' + response.data.access_token;
				ctl_auth.token_type = response.data.token_type;
				ctl_auth.expires_in = response.data.expires_in;
				ctl_auth.refresh_token = response.data.refresh_token;
				//ctl_auth.jwtPayload = _decodeData(response.data.jwt_token);			
				ctl_auth.jwtPayload = response.data.jwt_token;
				ctl_auth.AccountName = response.data.account_name;
				if(response.data.network_ids != undefined)	ctl_auth.network_id = response.data.network_ids[0];
				sessionStorage.setItem('ctl_auth', JSON.stringify(ctl_auth));
			};
			
			return _HTTPHandler.processPost(request);
						
		};
		

		/*
		 *  
		 * Refresh Token - Invokes OAuth API for Refresh Token and keep session alive
		 * Parse Response and Store response in Session Storage
		 * 
		 * 
		 */
		var _refreshToken = function(){
			var inputData = {};
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));
			inputData.refresh_token = tokenData.refresh_token;
			inputData.grant_type = "refresh_token";
			inputData.withJWT = "true";		
						
			var request ={};
			request.url = _defaults.apiUrls.oAuth;
			request.headers = _Common.clone(_defaults.authHeader);
			request.body = inputData;
			request.processResponse = function(response){
				var ctl_auth = JSON.parse(sessionStorage.getItem('ctl_auth'));
				//data = JSON.parse(response.data);
			  	ctl_auth.Authorization = response.data.token_type + ' ' + response.data.access_token;
				ctl_auth.token_type = response.data.token_type;
				ctl_auth.expires_in = response.data.expires_in;
				ctl_auth.refresh_token = response.data.refresh_token;
				//ctl_auth.jwtPayload = _decodeData(response.data.jwt_token);			
				ctl_auth.jwtPayload = response.data.jwt_token;
				ctl_auth.AccountName = response.data.account_name != undefined ? response.data.account_name : ctl_auth.AccountName;
				if(response.data.network_ids != undefined) ctl_auth.network_id = response.data.network_ids[0];
				sessionStorage.setItem('ctl_auth', JSON.stringify(ctl_auth));
			};
			
			return _HTTPHandler.processPost(request);
			
		};

		/* Service Container to expose User CRUD related services. */
		/*
		 * 
		 * Create User
		 * OAuth API will be invoked to create an user
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 * 	username : User name for the User to be created
		 *  password : Password for the User to be created
		 * 
		 */
		var _createUser = function(inputData){
			var request ={};
			request.url = _defaults.apiUrls.userCreds; 						
			request.headers = _Common.clone(_defaults.reqHeader);
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.headers.Authorization = tokenData.Authorization;
			request.body ={};
			request.body.username = inputData.username;
			request.body.password = inputData.password;
			//request.body.userTypes = (inputData.userTypes == undefined) ? ['network'] : inputData.userTypes;
			
			if (inputData != undefined && 'firstName' in inputData) {
				request.body.firstName = inputData.firstName;
			}
			if (inputData != undefined && 'lastName' in inputData) {
				request.body.lastName = inputData.lastName;
			}
			if (inputData != undefined && 'emailAddress' in inputData) {
				request.body.emailAddress = inputData.emailAddress;
			}
			if (inputData != undefined && 'networkId' in inputData) {
				request.body.networkId = inputData.networkId;
			}			
			request.body.accountName = tokenData.AccountName;
			return _HTTPHandler.processPost(request);
		};
		
		/* Service Container to expose User CRUD related services. */
		/*
		 * 
		 * Create Network User
		 * OAuth API will be invoked to send an invitation email to create a network user
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 * 	username : User name for the User to be created
		 *  emailAddress : email address on which invitation email should be sent
		 * 
		 */
		var _inviteNetworkUser = function(inputData){
			var request ={};
			request.url = _defaults.apiUrls.networkUserInvite; 						
			request.headers = _Common.clone(_defaults.reqHeader);
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.headers.Authorization = tokenData.Authorization;
			request.body ={};
			request.body.groups = (inputData.groups == undefined) ? ['network']:inputData.groups;
			if (inputData != undefined && 'username' in inputData) {
				request.body.username = inputData.username;
			}
			if (inputData != undefined && 'emailAddress' in inputData) {
				request.body.emailAddress = inputData.emailAddress;
			}
			if (inputData != undefined && 'networkId' in inputData) {
				request.body.networkId = inputData.networkId;
			}
			if (inputData != undefined && 'customerLabel' in inputData) {
				request.body.customerLabel = inputData.customerLabel;
			}
			if (inputData != undefined && 'seatId' in inputData) {
				request.body.seatId = inputData.seatId;
			}
			if (inputData != undefined && 'overrideMimeType' in inputData) {
				request.body.overrideMimeType = inputData.overrideMimeType;
			}
			return _HTTPHandler.processPost(request);
		};
		
		var _getInvitedUsers = function(inputData){
			var request ={};
			request.url = _defaults.apiUrls.invitedUserList; 						
			request.headers = _Common.clone(_defaults.reqHeader);
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.headers.Authorization = tokenData.Authorization;
			request.body ={};
			request.urlParam = {};
			if (tokenData != undefined && 'AccountName' in tokenData)
				request.urlParam.accountName = tokenData.AccountName;
			if (inputData != undefined && 'domain' in inputData)
				request.urlParam.domain = inputData.domain;
			return _HTTPHandler.processGet(request);
		};
		
		var _resendInvitation = function(inputData){
			var request ={};
			request.url = _defaults.apiUrls.updateInvitation; 						
			request.headers = _Common.clone(_defaults.reqHeader);
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.headers.Authorization = tokenData.Authorization;
			request.body ={};
			if (inputData != undefined && 'username' in inputData) {
				request.body.username = inputData.username;
			}
			if (inputData != undefined && 'seatId' in inputData) {
				request.body.seatId = inputData.seatId;
			}
			if (inputData != undefined && 'networkId' in inputData) {
				request.body.networkId = inputData.networkId;
			}
			if (inputData != undefined && 'customerLabel' in inputData) {
				request.body.customerLabel = inputData.customerLabel;
			}
			if (inputData != undefined && 'emailAddress' in inputData) {
				request.body.emailAddress = inputData.emailAddress;
			}
			return _HTTPHandler.processPut(request);
		};
		
		/* Service Container to expose User CRUD related services. */
		/*
		 * 
		 * Confirm Network User
		 * OAuth API will be invoked to confirm an invitation email to create a network user
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 * 	username : User name for the User to be created
		 *  emailAddress : email address on which invitation email should be sent
		 * 	firstname
		 * 	lastname
		 * 	password
		 * 	security questions
		 */
		var _confirmNetworkUser = function(inputData){
			var request ={};
			request.url = _defaults.apiUrls.confirmUser; 						
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers.Authorization = "bearer "+inputData.accessToken;
			
			request.urlParam = {};
			if (inputData != undefined && 'seatId' in inputData)
				request.urlParam.seatId = inputData.seatId;		
			if (inputData != undefined && 'networkId' in inputData)
				request.urlParam.networkId = inputData.networkId;	
			
			request.body ={};
			//request.body.userTypes = (inputData.userTypes == undefined) ? ['network']:inputData.userTypes;
			if (inputData != undefined && 'username' in inputData) {
				request.body.username = inputData.username;
			}
			if (inputData != undefined && 'firstName' in inputData) {
				request.body.firstName = inputData.firstName;
			}
			if (inputData != undefined && 'lastName' in inputData) {
				request.body.lastName = inputData.lastName;
			}
			if (inputData != undefined && 'emailAddress' in inputData) {
				request.body.emailAddress = inputData.emailAddress;
			}
			if (inputData != undefined && 'password' in inputData) {
				request.body.password = inputData.password;
			}
			if (inputData != undefined && 'securityQuestions' in inputData) {
				request.body.securityQuestions = inputData.securityQuestions;
			}
			
			return _HTTPHandler.processPut(request);
		};
		
		/*
		 * 
		 * Get User
		 * OAuth API will be invoked to get an user details
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 * 	username : User name for the User whose data needs to be returned
		 * 
		 */
		
		var _getUser = function(inputData){
			
			var request ={};
			var updateCache = false;
			request.url = _defaults.apiUrls.userRUDCreds; 						
			request.headers = _Common.clone(_defaults.reqHeader);
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.headers.Authorization = tokenData.Authorization;
			if (inputData != undefined && 'userName' in inputData)
				request.urlParam = {'userName':inputData.userName};
			if (inputData != undefined && 'updateCache' in inputData)
				updateCache = inputData.updateCache;
			
			request.processResponse = function(response){
				if(sessionStorage.getItem('ctl_auth') != undefined && updateCache){
					var ctl_auth = JSON.parse(sessionStorage.getItem('ctl_auth'));
					ctl_auth.userName = inputData.userName;
					ctl_auth.groups = /*response.data.userTypes*/inputData.userName.endsWith("@consumer") ? ['network'] : ['platform'];
					sessionStorage.setItem('ctl_auth', JSON.stringify(ctl_auth));
				}
				
			};
			
			return _HTTPHandler.processGet(request);
		};
		
		/*
		 * 
		 * Get Network Id list based on Account Name
		 * OAuth API will be invoked to get an network id list for account name
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 * 	networkId : Array of Network ID list needs to be returned
		 * 
		 */
		
		var _getNetworkIdList = function(inputData){
			
			var request ={};
			request.url = _defaults.apiUrls.networkIdList; 						
			request.headers = _Common.clone(_defaults.reqHeader);
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.headers.authorization = tokenData.Authorization;
			request.urlParam = {};
			if (tokenData != undefined && 'AccountName' in tokenData)
				request.urlParam.accountName = tokenData.AccountName;
			return _HTTPHandler.processGet(request);
		};
		
		var _getUserList = function(){
					
			var request ={};
			request.url = _defaults.apiUrls.userList; 						
			request.headers = _Common.clone(_defaults.reqHeader);
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.headers.authorization = tokenData.Authorization;
			request.urlParam = {};
			if (tokenData != undefined && 'AccountName' in tokenData)
				request.urlParam.accountName = tokenData.AccountName;
			
			return _HTTPHandler.processGet(request);
		};
		
		/*
		 * 
		 * Get User
		 * OAuth API will be invoked to get an network user list for network id
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 * 	networkId : Network ID name for user list needs to be returned
		 * 
		 */
		
		var _getUsersByNetworkId = function(inputData){
			
			var request ={};
			request.url = _defaults.apiUrls.userByNetworkId; 						
			request.headers = _Common.clone(_defaults.reqHeader);
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.headers.authorization = tokenData.Authorization;
			request.urlParam = {};
			if (tokenData != undefined && 'AccountName' in tokenData)
				request.urlParam.accountName = tokenData.AccountName;
			if (inputData != undefined && 'networkId' in inputData)
				request.urlParam.networkId = inputData.networkId;
			
			return _HTTPHandler.processGet(request);
		};
		/*
		 * 
		 * Update User
		 * OAuth API will be invoked to update an user
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 *  password : Password for the User to be update
		 * 
		 */
		var _updateUser = function(inputData){
			
			var request ={};
			request.url = _defaults.apiUrls.userRUDCreds; 						
			request.headers = _Common.clone(_defaults.reqHeader);
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.headers.Authorization = tokenData.Authorization;
			if (inputData != undefined && 'userName' in inputData)
				request.urlParam = {'userName':inputData.userName};
			request.body ={};
			if (inputData != undefined && 'oldPassword' in inputData) {
				request.body.oldPassword = inputData.oldPassword;
			}
			if (inputData != undefined && 'password' in inputData) {
				request.body.password = inputData.password;
			}
			if (inputData != undefined && 'firstName' in inputData) {
				request.body.firstName = inputData.firstName;
			}
			if (inputData != undefined && 'lastName' in inputData) {
				request.body.lastName = inputData.lastName;
			}
			if (inputData != undefined && 'emailAddress' in inputData) {
				request.body.emailAddress = inputData.emailAddress;
			}
			if (inputData != undefined && 'securityQuestions' in inputData) {
				request.body.securityQuestions = inputData.securityQuestions;
			}
			//CK-04-Feb Work Arround - Timeing Storing UserName as networkId
			//Reason in OM Seat has to match with NetworkID. Once Oauth2 returns valid networkid on creation of user, 
			//ONBUI has to make GET /user call to return actual user name ../get /user will be implemented in PS 11.
			
			request.body.networkId =  inputData.username;
			
			/*if (inputData != undefined && 'networkId' in inputData) {
				request.body.networkId = inputData.networkId;
			}*/
			request.body.accountName = tokenData.AccountName;
			return _HTTPHandler.processPut(request);
			
		};
		
		/*
		 * 
		 * Get all network Users
		 * OAuth API will be invoked to get a list of all network users
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 * 	username : User name for the User whose data needs to be returned
		 * 
		 */
		var _getAllNetworkIdUsers = function(inputData){
			
			var request ={};
			request.url = _defaults.apiUrls.allNetworkUsers; 						
			request.headers = _Common.clone(_defaults.reqHeader);
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.headers.authorization = tokenData.Authorization;
			request.urlParam = {};
			if (tokenData != undefined && 'AccountName' in tokenData)
				request.urlParam.accountName = tokenData.AccountName;
			
			if (inputData != undefined && 'domain' in inputData)
				request.urlParam.domain = inputData.domain;
			
			return _HTTPHandler.processGet(request);
		};
		
		/*
		 * 
		 * Update Network Users
		 * OAuth API will be invoked to add or remove network users
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 * 	username : User name for the User
		 * 	addUsers : array of username to be added
		 * 	removeUsers : array of username to be removed
		 */
		var _subaccountUserListUpdates = function(inputData){
			
			var request ={};
			request.url = _defaults.apiUrls.userByNetworkId;						
			request.headers = _Common.clone(_defaults.reqHeader);
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.headers.Authorization = tokenData.Authorization;
			
			request.body ={};
			if (inputData != undefined && 'addUsers' in inputData) {
				request.body.addUsers = inputData.addUsers;	
			}
			if (inputData != undefined && 'removeUsers' in inputData) {
				request.body.removeUsers = inputData.removeUsers;
			}
			
			request.urlParam = {'networkId':inputData.networkId, 'accountName':tokenData.AccountName};
			return _HTTPHandler.processPut(request);
		};
		
		/*
		 * 
		 * Delete User
		 * OAuth API will be invoked to delete an user details
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 * 	username : User name for the User which will be deleted
		 * 
		 */
		var _deleteUser = function(inputData){
			
			var request ={};
			request.url = _defaults.apiUrls.userRUDCreds; 						
			request.headers = _Common.clone(_defaults.reqHeader);
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.headers.Authorization = tokenData.Authorization;
			if (inputData != undefined && 'userName' in inputData)
				request.urlParam = {'userName':inputData.userName};
			
			return _HTTPHandler.processDelete(request);
			
		};
		
		/*
		 * 
		 * QuickStart Creating User
		 * OAuth API will be invoked to create a QuickStart User
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 *  seatId : SeatId which will be associated with this newly created User
		 * 	username : User name for the User will be created
		 *  password : Password for this User
		 *  networkId : Network ID or GroupName where this User will belong
		 * 
		 */
		var _createQuickStartUser = function(inputData){
			
			var request ={};
			request.url = _defaults.apiUrls.quickStartUserCreate; 						
			request.headers = _Common.clone(_defaults.reqHeader);
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.headers.Authorization = tokenData.Authorization;
			if (inputData != undefined && 'seatId' in inputData)
				request.urlParam = {'seatId':inputData.seatId};
			
			request.body ={};
			if (inputData != undefined && 'userName' in inputData) {
				request.body.username = inputData.userName;
			}
			if (inputData != undefined && 'customerLabel' in inputData) {
				request.body.customerLabel = inputData.customerLabel;
			}
			if (inputData != undefined && 'password' in inputData) {
				request.body.password = inputData.password;
			}
			return _HTTPHandler.processPost(request);
			
		};
		
		
		/*
		 * QuickStart Login - No updation of ctl_auth sessionStorage
		 * 
		 * Login Method - Invokes OAuth API for login
		 * Parse Response and Store response in Session Storage
		 * 
		 * @Param: username - Username for User
		 * @Param: password - Password for User
		 * 
		 */
		var _quickStartlogin = function(username, password){
		
			var inputData = {};
			inputData.username = username;
			inputData.password = password;
			inputData.grant_type = 'password';
			inputData.withJWT = true;
			
			var request ={};
			request.url = _defaults.apiUrls.oAuth;
			request.headers = _Common.clone(_defaults.authHeader);
			request.body = inputData;
						
			return _HTTPHandler.processPost(request);
						
		};
		
		/*
		 * Create NetworkId - No updation of ctl_auth sessionStorage
		 * 
		 * @Param: seatId - seatId to be associated with SubAccount
		 * @Param: customerLabel - The customerLabel will used to create NetworkId (<AccountName>-<CustomerLabel>)
		 * 
		 */
		var _createNetworkId = function(inputData){
			var request ={};
			request.url = _defaults.apiUrls.networkId; 						
			request.headers = _Common.clone(_defaults.reqHeader);
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.headers.Authorization = tokenData.Authorization;
			request.body = inputData;
			
			request.urlParam = {};
			if (tokenData != undefined && 'AccountName' in tokenData)
				request.urlParam.accountName = tokenData.AccountName;
			
			return _HTTPHandler.processPost(request);
		};
		
		/*
		 * 
		 * Logout
		 * Cleanup of cache variables created by SDK
		 * 
		 * 
		 * 
		 */
		var _logOut = function(){
			
			sessionStorage.removeItem('ctl_auth');
			
		};
		
		return{
			'login':_login,
			'logOut':_logOut,
			'refreshToken': _refreshToken,
			'createUser' : _createUser,
			'inviteNetworkUser': _inviteNetworkUser,
			'getInvitedUsers':_getInvitedUsers,
			'resendInvitation':_resendInvitation,
			'confirmNetworkUser': _confirmNetworkUser,
			'getUser' : _getUser,
			'getNetworkIdList':_getNetworkIdList,
			'getUsersByNetworkId' : _getUsersByNetworkId,
			'updateUser' : _updateUser,
			'deleteUser' : _deleteUser,
			'createQuickStartUser' : _createQuickStartUser,
			'subaccountUserListUpdates' : _subaccountUserListUpdates,
			'getAllNetworkIdUsers' : _getAllNetworkIdUsers,
			'getUserList' : _getUserList,
			'createNetworkId' : _createNetworkId
		};
	}();
	
	var _Configurations = function(){
		
		/*
		 * 
		 * An authorised user will able search Available Phone numbers from Century Link pool which can be assigned to an account.
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 *  state : Search Available Phone numbers by State
		 *  city : Search Available Phone numbers by State and City
		 *  npa : Search Available Phone numbers by NPA
		 *  npanxx : Search Available Phone numbers by NPANPXX
		 *  
		 *  
		 */
		var _getAvailablePhoneNumber = function(inputData){
			var request ={};
			request.url = _defaults.apiUrls.availablePhoneNumber; 
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
			
			request.queryParam ={};
			if (inputData != undefined && 'state' in inputData) {
				request.queryParam.state = inputData.state;
			}
			if (inputData != undefined && 'city' in inputData) {
				request.queryParam.city = inputData.city;
			}
			if (inputData != undefined && 'npa' in inputData) {
				request.queryParam.npa = inputData.npa;
			}
			if (inputData != undefined && 'npanxx' in inputData) {
				request.queryParam.npanxx = inputData.npanxx;
			}
			if (inputData != undefined && 'limit' in inputData) {
				request.queryParam.limit = inputData.limit;
			}

			return _HTTPHandler.processGet(request);
		};
		
		
		/*
		 * 
		 * An authorised user will able fetch all assigned Phone number to an account
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 *  List of Phone Number : Phone numbers(TNs) which are assigned to an account
		 *  
		 *  
		 */
		var _getAssignedPhoneNumber = function(inputData){
			
			var request ={};
			request.url = _defaults.apiUrls.getPhoneNumbers; 
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
								
			return _HTTPHandler.processGet(request);
		};
		
		/*
		 * 
		 * An authorised user will able assign Phone number to an account, which can be used to configure a subscribed product.
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 *  phoneNumber : Phone number(TN) which will be assigned to an account
		 *  
		 *  
		 */
		var _assignPhoneNumber = function(inputData){
			
			var request ={};
			request.url = _defaults.apiUrls.reservePhoneNumber; 
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
			
			if(inputData != undefined && 'seatId' in inputData)
				request.headers.SeatId = inputData.seatId;
						
			if(inputData != undefined && 'phoneNumber' in inputData)
				request.urlParam = {'phoneNumber':inputData.phoneNumber};
						
			return _HTTPHandler.processPost(request);
		};
		
		/*
		 * 
		 * An authorised user will able to configure a subscribed product.
		 * Courier API will be invoked to configure product
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 *  seatId : A subscribed product id for which product will be configured
		 *  productName : Name of Product for which product will be configured
		 *		Supported products: courier-basic
		 *  phoneNumber : Phone number(TN) for which product will be configured
		 *  enableService : true/false, if true a user shall able to consume "send message" service.
		 *  messageSaveStore : true/false, if true the sent messages will be stored in dumpster, and shall be eligible to read/delete the sent messages.
		 */	
		var _configureProduct = function(inputData){
			
			var request ={};
			request.url = _defaults.apiUrls.configureProduct; 
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);

			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			
			if(inputData != undefined && 'seatId' in inputData)
				request.headers.seatId = inputData.seatId;
			if(inputData != undefined && 'productName' in inputData)
				request.headers.ProductName = inputData.productName;
			if(tokenData.network_id != undefined)// Set network_id if received from token
			//request.headers.NetworkID = tokenData.network_id;
			request.body ={};
			if(inputData != undefined && 'enabledService' in inputData)
				request.body.enabled = inputData.enabledService;
			if(inputData != undefined && 'messageSaveStore' in inputData)
				request.body.messageSaveStore = inputData.messageSaveStore;
			if(inputData != undefined && 'productName' in inputData)
				request.body.productName = inputData.productName;
			
			if(inputData != undefined && 'phoneNumber' in inputData)
				request.urlParam = {'phoneNumber':inputData.phoneNumber};
			
			return _HTTPHandler.processPost(request);
		};
		
		/*
		 * 
		 * An authorised user will able to read Product configuration (Service(s), Phone Number) details.
		 * Courier API will be invoked to return product configuration details
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 *  seatId : Subscribed Product Id
		 *  productName : Product Name 
		 * 		Supported products: courier-basic
		 *  phoneNumber : A configured Phone number(TN) of a Subscribed Product Id
		 *  
		 *  
		 */		
		var _getProductConfig = function(inputData){
			
			var request ={};
			request.url = _defaults.apiUrls.getProductConfig; 
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
			
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			
			/*if(tokenData.network_id != undefined)// Set network_id if received from token
				request.headers.NetworkID = tokenData.network_id;*/
			if(inputData != undefined && 'seatId' in inputData)
				request.headers.seatId = inputData.seatId;
			if(inputData != undefined && 'productName' in inputData)
				request.headers.ProductName = inputData.productName;
			
			if(inputData != undefined && 'phoneNumber' in inputData)
				request.urlParam = {'phoneNumber':inputData.phoneNumber};
			
			return _HTTPHandler.processGet(request);
			
		};
		
		
		/*
		 * 
		 *  An authorised user will able to reconfigure a subscribed product.
		 * Courier API will be invoked to configure product
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 *  seatId : A subscribed product id  for which product will be updated/reconfigured
		 *  productName : Product Name for which product will be updated/reconfigured
		 *  phoneNumber : Phone number(TN) for which product will be updated/reconfigured
		 *  enabledService : true/false, if true a user shall able to consume "send message" service.
		 *  messageSaveStore : true/false, if true the sent messages will be stored in dumpster, and shall be eligible to read/delete the sent messages.
		 *   
		 */
		var _reconfigureProduct = function(inputData){
			
			var request ={};
			request.url = _defaults.apiUrls.updateProductConfig; 
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
			
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			
			/*if(tokenData.network_id != undefined)// Set network_id if received from token
				request.headers.NetworkID = tokenData.network_id;*/
			if(inputData != undefined && 'seatId' in inputData)
				request.headers.seatId = inputData.seatId;
			request.body ={};
			if(inputData != undefined && 'enabledService' in inputData)
				request.body.enabled = inputData.enabledService;
			if(inputData != undefined && 'messageSaveStore' in inputData)
				request.body.messageSaveStore = inputData.messageSaveStore;
			
			if(inputData != undefined && 'phoneNumber' in inputData && 'productName' in inputData)
			request.urlParam = {'phoneNumber':inputData.phoneNumber, 'productName':inputData.productName};
			
			return _HTTPHandler.processPut(request);
		};
		
		/*
		 * 
		 * An authorised user will able to remove product configuration for a subscribed product. 
		 * The assigned TN will be auto released and will be available back in search pool.
		 * Courier API will be invoked to configure product
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 *  seatId : A subscribed product id for which product instance will be removed/unconfigured
		 *  productName : Product Name for which product will be removed/unconfigured
		 *  phoneNumber : Phone number(TN) for which product will be removed/unconfigured
		 *   
		 */
		var _removeProductConfig = function(inputData){
			
			var request ={};
			request.url = _defaults.apiUrls.unconfigureProduct; 
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
			
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			
			/*if(tokenData.network_id != undefined)// Set network_id if received from token
				request.headers.NetworkID = tokenData.network_id;*/
			if(inputData != undefined && 'seatId' in inputData)
				request.headers.seatId = inputData.seatId;
			if(inputData != undefined && 'productName' in inputData)
				request.headers.ProductName = inputData.productName;
			
			if(inputData != undefined && 'phoneNumber' in inputData && 'productName' in inputData)
			request.urlParam = {'phoneNumber':inputData.phoneNumber, 'productName':inputData.productName};
			
			return _HTTPHandler.processDelete(request);
		};
		
		return{
			'getAvailablePhoneNumber': _getAvailablePhoneNumber,
			'getAssignedPhoneNumber': _getAssignedPhoneNumber,
			'assignPhoneNumber': _assignPhoneNumber,
			'configureProduct': _configureProduct,
			'getProductConfig': _getProductConfig,
			'reconfigureProduct': _reconfigureProduct,
			'removeProductConfig': _removeProductConfig
		};
		
	}();
	
	
	/* Container to expose Products related services. */
	var _Product = function() {
		
		/*
		 * 
		 * An authorised user will able to Subscribe a Product.
		 * OrderManagement API will be invoked to subscribe product
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 *  customerId : CustomerId for a subscribed Product
		 *  productName : Name of Product for which product will be configured
		 *		Supported products: courier-basic
		 *  legalAckValue : Legal Acknowledgement Value
		 *		Legal Acknowledgment Version which is accepted to subscribe a product.
		 *		TBD: Need to mention where user can read the Terms and Conditions.
		 *  legalAckStatus : true/false, Legal Acknowledgement Status
		 *   
		 */
		var _subscribeProduct = function(inputData){
			var request ={};
			request.url = _defaults.apiUrls.orderManagementProduct; 						
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
			request.headers.UseMock = "false";
			
			request.body = {};
			if (inputData != undefined && 'customerId' in inputData) {
				request.body.customerId = inputData.customerId;
			}
			if (inputData != undefined && 'products' in inputData) {
				request.body.products = inputData.products;
			}
			if (inputData != undefined && 'legalAckValue' in inputData) {
				request.body.legalAckValue = inputData.legalAckValue;
			}
			if (inputData != undefined && 'legalAckStatus' in inputData) {
				request.body.legalAckStatus = inputData.legalAckStatus;
			}
			
			
			return _HTTPHandler.processPost(request);
		};
		
		/*
		 * 
		 * An authorised user will able to retrive all assigned Subscribed Product(s).
		 * OrderManagement API will be invoked to get list of subscribed product
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 *  pageSize : Default Page Size 10 will be considered.
		 *  pageNumber : Default page number 1 will be considered.
		 *   
		 */
		var _getSubscribedProducts = function(inputData){
			var request ={};
			var userType = _Common.isUserType('network');
			if(userType != undefined && userType == true){
				request.url = _defaults.apiUrls.orderManagementSubAccount;
			}
			else{
				request.url = _defaults.apiUrls.orderManagement; 
			}	 						
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
			/*if(inputData != undefined && 'networkId' in inputData)
				request.headers.NetworkId = inputData.networkId;*/
			//request.headers.UseMock = "false";
			//request.queryParam = {};
			
			/*if(inputData != undefined && 'pageSize' in inputData)
				request.queryParam.PageSize = inputData.pageSize;*/
			/*else
				request.queryParam.PageSize = '10';*/
			/*if(inputData != undefined && 'pageNumber' in inputData)
				request.queryParam.PageNumber = inputData.pageNumber;*/
			/*else
				request.queryParam.PageNumber = '1';*/
			
			return _HTTPHandler.processGet(request);
		};
		
		/*
		 * 
		 * An authorised user will able to retrive assigned Subscribed Product details.
		 * OrderManagement API will be invoked to get details of subscribed product
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	
		 */
		var _getSubscribedProduct = function(inputData){
			
			var request ={};
			request.url = _defaults.apiUrls.readSeat; 
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
			
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			
			//if(tokenData.network_id != undefined)// Set network_id if received from token
			//	request.headers.NetworkID = tokenData.network_id;
			
			if(inputData != undefined && 'seatId' in inputData)
				request.headers.seatId = inputData.seatId;
			
			return _HTTPHandler.processGet(request);
			
		};
		
		/*
			Assign Network user to a Product Instance.
		*/
		var _assignProduct = function(inputData){
			var request ={};
			request.url = _defaults.apiUrls.orderManagementAssignProduct; 						
			request.headers = _Common.clone(_defaults.reqHeader);
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.headers.Authorization = tokenData.Authorization;
			request.headers.SeatId = inputData.seatId;
			request.body ={};
			request.body.networkId = [];
			request.body.networkId.push(inputData.networkId);
			return _HTTPHandler.processPost(request);
		};
		
		/*
			Unassign Network user from a Product Instance.
		*/
		var _unassignProduct = function(inputData){
			var request ={};
			request.url = _defaults.apiUrls.orderManagementAssignProduct; 						
			request.headers = _Common.clone(_defaults.reqHeader);
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.headers.Authorization = tokenData.Authorization;
			request.headers.SeatId = inputData.seatId;
			request.headers.NetworkId = inputData.networkId;
			return _HTTPHandler.processDelete(request);
		};
		
		/*
		 * 
		 * An authorised user will able to Unsubscribe a Product
		 * OrderManagement API will be invoked to unsubscribe product
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 *  seatId : A subscribed product id which will be unsubscribed from an account.
		 *   
		 */
		var _unsubscribeProduct = function(inputData){
			
			var request ={};
			request.url = _defaults.apiUrls.orderManagementProduct; 						
			request.headers = {};//_Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
			request.headers.UseMock = "false";
			if(inputData != undefined  && 'seatId' in inputData)
				request.headers.SeatId = inputData.seatId;
			
			return _HTTPHandler.processDelete(request);
		};
		
		
		// Fetch offered products Information from CMS
		var _getAvailableProducts = function(inputData){
			//TODO - Comming soon
		};
		
		
		return{			
			'subscribeProduct':_subscribeProduct,
			'assignProduct':_assignProduct,
			'unassignProduct':_unassignProduct,
			'getSubscribedProducts' : _getSubscribedProducts,
			'getSubscribedProduct': _getSubscribedProduct,
			'unsubscribeProduct':_unsubscribeProduct,
			'Configuration': _Configurations
		};
	}();
	
	/* Container to expose SMS related services. */
	var _SMS = function() {
		
		/*
		 * 
		 * An authorised user will able consume a send SMS service for subscribed product(s).
		 * Courier API will be invoked to send SM
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 *  phoneNumber : Phone Number from which SMS will be sent. Phone Number has to be configured with a subscribed product.
		 *  to : Phone Number to which SMS will be sent
		 *  messageText : Text which will be sent as part of Message
		 *   
		 */
		var _sendMessage = function(inputData) {
			var request ={};
			request.url = _defaults.apiUrls.sendSMS; 
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
			request.headers.SeatID = inputData.SeatID;
			
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			
			/*if(tokenData.network_id != undefined)// Set network_id if received from token
				request.headers.NetworkID = tokenData.network_id;*/
			request.body ={};
			if(inputData != undefined && 'messageText' in inputData)
				request.body.messageText = inputData.messageText;
			if(inputData != undefined && 'to' in inputData)
				request.body.to = inputData.to;
			
			if(inputData != undefined && 'phoneNumber' in inputData)
				request.urlParam = {'phoneNumber':inputData.phoneNumber};
			
			return _HTTPHandler.processPost(request);
		};
		
		/*
		 * 
		 * An authorised user will able to retrive the sent messages from dumpster.
		 * Dumpster API will be invoked to get SMS History
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 *	seatId : A subscribed product id for reading the messages is being requested.
		 *  offset : The offset of the 1st record to return in the response. Example if user have 100 messages then limit=10 and offset=98. Then on response you will see 2 messages.
		 *  limit : The limit of the number of records to return in the response Default limit=10 and max limit=100.
		 *  pretty : Return the JSON response body in pretty format.  Valid values are true/false.
		 *  sort : The field name on which to sort the SMS Messages returned.
		 *  query : The query string, query=anyFieldName:anyFieldValue for the SMS messages is being requested.Example: query=from:vijay or query=from:vijay AND to:bruce or query=from:\"vijay bhaskar\"
		 *  
		 *  		 
		 */
		var _getMessageHistory = function(inputData) {
			
			var request ={};
			request.url = _defaults.apiUrls.readSMS; 
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
			request.headers.SeatId = inputData.seatId;
			
			request.queryParam ={};
			if(inputData != undefined && 'offset' in inputData)
				request.queryParam.offset = inputData.offset;
			if(inputData != undefined && 'limit' in inputData)
				request.queryParam.limit = inputData.limit;
			if(inputData != undefined && 'pretty' in inputData)
				request.queryParam.pretty = inputData.pretty;
			if(inputData != undefined && 'sort' in inputData)
				request.queryParam.sort = inputData.sort;
			if(inputData != undefined && 'query' in inputData)
				request.queryParam.query = inputData.query;
			
			return _HTTPHandler.processGet(request);
		};
		
		/*
		 * 
		 * An authorised user will able to delete the sent messages from dumpster.
		 * Dumpster API will be invoked to delete SMS History
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 *	seatId : A subscribed product id for delete messages is being requested.
		 *  query : The query string, query=anyFieldName:anyFieldValue for the SMS messages is being requested.
		 *  
		 *  		 
		 */
		var _deleteMessageHistory = function(inputData) {
			var request ={};
			request.url = _defaults.apiUrls.readSMS; 
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
			request.headers.seatId = inputData.seatId;
			
			request.queryParam ={};
			if(inputData != undefined && 'query' in inputData)
				request.queryParam.query = inputData.query;
			
			return _HTTPHandler.processDelete(request);
		};
		
		
		/*
		 * 
		 * An authorised user will able consume a get Status service for subscribed product(s).
		 * Courier API will be invoked to get Status
		 * 
		 * @Param
		 * 	inputData - JSON Format
		 * 	It will contains below key Value pair
		 *  phoneNumber (Mandatory): Phone Number for which the status needs to be checked. Phone Number has to be configured with a subscribed product.
		 *  NetworkID (Optional) : Network Id
		 *  CorrelationID (Optional) : CorrelationID
		 *  AccountName (Optional) : AccountName
		 *   
		 */
		var _getStatus = function(inputData) {
			var request ={};
			request.url = _defaults.apiUrls.smsStatus; 
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
			
			request.urlParam = {};
			if(inputData != undefined && 'phoneNumber' in inputData)
				request.urlParam = {'phoneNumber':inputData.phoneNumber};
								
			return _HTTPHandler.processGet(request);
		}
			
		return {
			'sendMessage': _sendMessage,
			'getMessageHistory': _getMessageHistory,
			'deleteMessageHistory': _deleteMessageHistory,
			'getStatus': _getStatus
		};
	}();
	
	var _Helper = function() {		
		var _getBaseApiURL = function() {
			return _defaults.enviornmentBaseURIs[environment];
		}
		
		return {
			'getBaseApiURL' : _getBaseApiURL
		};
	}();

	var _Footer = function() {
		
		var _submitFeedback = function(inputData){
			
			var request ={};
			request.url = _defaults.apiUrls.feedback; 
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
			
			request.body = {};
			
			if (inputData != undefined && 'component' in inputData) {
				request.body.component = inputData.component;
			}
			if (inputData != undefined && 'name' in inputData) {
				request.body.name = inputData.name;
			}
			if (inputData != undefined && 'summary' in inputData) {
				request.body.component = inputData.summary;
			}
			if (inputData != undefined && 'emailAddress' in inputData) {
				request.body.emailID = inputData.emailAddress;
			}
			if (inputData != undefined && 'ratingUI' in inputData) {
				request.body.ratingUI = inputData.ratingUI;
			}
			if (inputData != undefined && 'overallRating' in inputData) {
				request.body.ratingOverall = inputData.overallRating;
			}
			if (inputData != undefined && 'canContact' in inputData) {
				if (inputData.canContact != undefined) {
					request.body.canContact = inputData.canContact;
				} else {
					request.body.canContact = false;
				}
			}

			return _HTTPHandler.processPost(request);
		}
		
		return {
			'submitFeedback' : _submitFeedback
		};
	}();
	
	var _Platform = function() {
		
		var _getAccountHierarchy = function() {
			
			var request = {};
			request.url = _defaults.apiUrls.getAccountHierarchy; 
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
			
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.urlParam = {};
			if (tokenData != undefined && 'AccountName' in tokenData)
				request.urlParam.accountName = tokenData.AccountName;
			
			return _HTTPHandler.processGet(request);
		}
		
		var _getAccount = function() {
			
			var request = {};
			request.url = _defaults.apiUrls.getAccountInformation; 
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
			
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));			
			request.urlParam = {};
			if (tokenData != undefined && 'AccountName' in tokenData)
				request.urlParam.accountName = tokenData.AccountName;
			
			return _HTTPHandler.processGet(request);
		}
		
		var _switchAccount = function(inputData) {
			var request = {};
			request.url = _defaults.apiUrls.updateAccount; 
			request.headers = _Common.clone(_defaults.reqHeader);
			request.headers = _Common.populateHeaders(request.headers);
			
			request.body = {};			
			if (inputData != undefined && 'accountName' in inputData) {
				request.body.accountName = inputData.accountName;
			}
			
			return _HTTPHandler.processPut(request);
		}
		
		return {
			'getAccountHierarchy' : _getAccountHierarchy,
			'getAccount' : _getAccount,
			'switchAccount' : _switchAccount
		};
	}();

	/* Container to expose different services. */
	var _service = function() {
		return {
			"Oauth2": _Oauth2, 
			"Product": _Product,
			"SMS": _SMS,
			"Helper": _Helper,
			"Footer": _Footer,
			"Defaults": _defaults,
			"Platform": _Platform
		};
	}();

	/* Container to expose all communication services. */
	return {"Service":_service};	

}('%CTLENV%'));
'use strict';
var sessionTimer = angular.module("ctl.core.session.timer", []);

sessionTimer.sessionState = function(
    $log,
    $interval,
    OAuthServices
    ) {
    $log;
    $interval;
    OAuthServices;

    var refreshToken;

    var _startSession = function(delay) {
        if(angular.isDefined(refreshToken)){
            // Session Already Initialized
            sessionStorage.setItem("isUserAuthenticated", true);
        }else{
            $log.info("SessionTimer: Session Started");
            sessionStorage.setItem("isUserAuthenticated", true);
            refreshToken = $interval(_refreshToken, delay*1000);
        }
    };


    var _stopSession = function(){
        if(angular.isDefined(refreshToken)){
            $log.info("SessionTimer: Session Stopped");
            try{
                sessionStorage.setItem("isUserAuthenticated", false);
                $interval.cancel(refreshToken);
            }catch(exception){
                $log.error("SessionTimer: Session Stopped Error Occured"+exception);
            }

        }
    }

    var _refreshToken = function(){
        // Execute RefreshToken API here
        $log.info("SessionTimer: Session Refresh Token");
        OAuthServices.refreshAccessToken();
    }


    return {
        startSession: _startSession,
        stopSession: _stopSession
    };
};


sessionTimer.service("SessionTimer",
    ['$log', '$interval', 'OAuthServices', sessionTimer.sessionState]);



'use strict';
var sessionKeeper = angular.module("ctl.core.session.keeper", []);
sessionKeeper.constant("namespace", "ctlSession");
sessionKeeper.constant("sessionObject", "current");
sessionKeeper.constant(
    "sessionKeeperDefaults",
    {
        current: {

        }
    });

sessionKeeper.session = function(
    $rootScope,
    namespace,
    sessionObject,
    sessionKeeperDefaults,
    $log
    ) {
    this.namespace = namespace;
    this.obj = sessionObject;
    this.scope = $rootScope;
    this.log = $log;
    this.defaults = sessionKeeperDefaults;
};


sessionKeeper.service("SessionKeeper",
    ['$rootScope', 'namespace', 'sessionObject', 'sessionKeeperDefaults', '$log',
        sessionKeeper.session
    ]
    );

sessionKeeper.session.prototype.read = function(scope) {
    var _scope = scope || this.scope;
    if (_scope[this.obj]) {
       // this.log.error('SessionKeeper :: read - Reading from scope: '+ angular.toJson(_scope[this.obj]));
        return _scope[this.obj].current;
    }
    if (sessionStorage && sessionStorage.getItem(this.namespace)) {
        var val = sessionStorage.getItem(this.namespace);
        if (val && val != 'undefined') {
            var session = JSON.parse(val);
            if (_scope) {
                //this.log.error('SessionKeeper :: read - Reading from storage and adding to scope: '+ angular.toJson(session));
                return (_scope[this.obj] = session);
            }
            else {
                //this.log.error('SessionKeeper :: read - Reading from storage: '+ angular.toJson(session));
                return session;
            }
        } else {
            if (_scope) {
                //this.log.error('SessionKeeper :: read - Getting defauls and adding to scope: '+ angular.toJson(this.defaults));
                return (_scope[this.obj] = angular.copy(this.defaults));
            }
            else {
                //this.log.error('SessionKeeper :: read - Getting defauls: '+ angular.toJson(this.defaults));
                return angular.copy(this.defaults);
            }
        }
    }
    //this.log.error('SessionKeeper :: read - Getting defauls final: '+ angular.toJson(this.defaults));
    return (_scope[this.obj] = angular.copy(this.defaults));
};

sessionKeeper.session.prototype.save = function(scope) {
    var _scope = scope || this.scope;

    if (sessionStorage) {
        try {
            if (_scope[this.obj]) {
                var txt = JSON.stringify(_scope[this.obj]);
                sessionStorage.setItem(this.namespace, txt);
            }
            this.log.info("SessionKeeper :: save() called! : ");
        } catch (e) {
            this.log.error("SessionKeeper :: save() - Exception occurred: " + e);
        }
    } else {
        this.log.error("SessionKeeper :: save() - Session did not save. sessionStorage not supported!");
    }
};

sessionKeeper.session.prototype.clear = function(scope) {
    var _scope = scope || this.scope;
    if (sessionStorage) {
        try {
            delete _scope[this.obj];
            sessionStorage.removeItem(this.namespace);
            this.log.warn("SessionKeeper :: clear() called!");
        } catch (e) {
            this.log.error("SessionKeeper :: clear() - Exception occurred: " + e);
        }
    }
};


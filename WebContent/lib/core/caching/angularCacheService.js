'use strict';
/**
 *
 * This service wraps the 3rd party angular-cache component
 * Provides configurable caching
 * @type {Module}
 */
var AngularCacheModule = angular.module('ctl.core.services.angularCacheService', ['jmdobry.angular-cache']);

/**
 * Singleton Object for AngularCacheService
 * it returns the different cache for different
 * requirement
 */
AngularCacheModule.factory('AngularCacheService',
    [
        '$log',
        '$angularCacheFactory',
        'cacheConfig',
        function(
            $log,
            $angularCacheFactory ,
            cacheConfig
            ) {
                'use strict';
                return {
                    defaultCache : $angularCacheFactory('defaultCache'),
                    capacityCache :  $angularCacheFactory('capacityCache', {
                        capacity: 10,
                        storageMode: 'localStorage',
                        verifyIntegrity: false,
                        deleteOnExpire: 'none'
                    }),
                    passiveDeleteCache : $angularCacheFactory('passiveDeleteCache', {
                        maxAge: 12000,
                        deleteOnExpire: 'passive',
                        onExpire: function (key, value, done) {
                            $log.debug('AngularCacheService:: '+key + ' expired');
                            $log.debug('AngularCacheService:: Passive mode onExpire callback executed.');
                            done(key, value);
                        }
                    }),
                    aggressiveDeleteCache: $angularCacheFactory('aggressiveDeleteCache', {
                        maxAge: 4000,
                        deleteOnExpire: 'aggressive',
                        onExpire: function (key, value) {
                            $log.debug('AngularCacheService:: '+key + ' expired');
                            $log.debug('AngularCacheService:: Aggressive mode onExpire callback executed.');
                        }
                    }),
                    flushingCache : $angularCacheFactory('flushingCache', {
                        cacheFlushInterval: 15000,
                        verifyIntegrity: false,
                        storageMode: 'sessionStorage'
                        
                    }),
                    contentCache : {
                        english :  $angularCacheFactory('contentCacheEnglish', {
                            capacity: 10,
                            storageMode: cacheConfig.cacheSourceLocal,
                            verifyIntegrity: false,
                            deleteOnExpire: 'aggressive',
                            maxAge: 86400000
                        }),
                        spanish : $angularCacheFactory('contentCacheSpanish', {
                            capacity: 10,
                            storageMode: cacheConfig.cacheSourceLocal,
                            verifyIntegrity: false,
                            deleteOnExpire: 'aggressive',
                            maxAge: 86400000
                        }),
                        language : $angularCacheFactory('contentLanguage',{
                            capacity :1,
                            storageMode: cacheConfig.cacheSourceLocal,
                            verifyIntegrity: false,
                            deleteOnExpire:'aggressive',
                            maxAge: 86400000
                        })
                    },
                    AccountDataCache : $angularCacheFactory('AccountDataCache', {
                        capacity: 3,
                        storageMode: cacheConfig.cacheSourceSession,
                        verifyIntegrity: false,
                        deleteOnExpire: 'aggressive'
                    }),
                    OrderMgmtDataCache : $angularCacheFactory('OrderMgmtDataCache', {
                        capacity: 2,
                        storageMode: cacheConfig.cacheSourceSession,
                        verifyIntegrity: false,
                        deleteOnExpire: 'aggressive'
                    }),
                    MonikerDataCache : $angularCacheFactory('MonikerDataCache', {
                        capacity: 2,
                        storageMode: cacheConfig.cacheSourceSession,
                        verifyIntegrity: false,
                        deleteOnExpire: 'aggressive'
                    }),
                    SplunkDataCache : $angularCacheFactory('SplunkDataCache', {
                        capacity: 2,
                        storageMode: cacheConfig.cacheSourceSession,
                        verifyIntegrity: false,
                        deleteOnExpire: 'aggressive'
                    }),
                    ReportingDataCache : $angularCacheFactory('ReportingDataCache', {
                        capacity: 1,
                        storageMode: cacheConfig.cacheSourceSession,
                        verifyIntegrity: false,
                        deleteOnExpire: 'aggressive'
                    })
                };
}]);
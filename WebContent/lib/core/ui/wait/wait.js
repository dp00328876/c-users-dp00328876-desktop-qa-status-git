'use strict';
var uiWait = angular.module("ctl.core.ui.wait",[]);

uiWait.directive("wait", ['$rootScope', '$log','$timeout','$q', function ($rootScope, $log, $timeout, $q) {
        //style="background-image: url(../images/ajax-loader2.gif);>"
    return {
        restrict: "EAC",
        transclude: true,
        template: 	'<div ng-show="isRouting" class="loader-wait" >' +
            '<div class="spinner-background"><i class="spinner"></i></div>'+
            '<div class="ajax-loader" >' +
            '</div>'+
            '</div>',


        link: function (scope,element) {
            var timers = [],
                cancelling = false;
            /*$rootScope.spinnerPositionCalculation();
                scope.SpinnerTopPos=$rootScope.SpinnerTopPos;
                scope.SpinnerLeftPos=$rootScope.SpinnerLeftPos;*/
            $rootScope.$on("inProgressStart", function () {

                scope.isRouting = true;
              //  $rootScope.loginPageisHide=true;
                /*if (!cancelling) {
                    var timer = $timeout(function(){ },
                        10);
                    timers.push(timer);
                    timer.then(function() {
                        // $rootScope.loginPageisHide=true;
                        scope.isRouting = true;
                    }, function() {
                        scope.isRouting = false;
                        //$rootScope.loginPageisHide=false;
                    });
                }*/
                $log.info("Wait.js :: inProgressStart");
            });


            $rootScope.$on("inProgressEnd", function () {
                scope.isRouting = false;
               // $rootScope.loginPageisHide=false;
                /*if (!cancelling) {
                    cancelling = true;
                    for(var i=0; i<timers.length; i++) {
                        $timeout.cancel(timers[i]); // probably a better way to do this... :)
                    }
                    timers = []; //clear the timers we are watching
                }*/
                $log.info("Wait.js :: inProgressEnd");
                /*$rootScope.loginLoader=true;
                cancelling = false;*/

            });
        }
    }
}]);
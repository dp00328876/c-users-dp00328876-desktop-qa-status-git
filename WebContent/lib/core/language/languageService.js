'use strict';

/**
 *
 * @type {*}
 */
var languageServiceModule = angular.module("ctl.core.language.service", []);

/**
 *
 * @param $log
 * @param $q
 * @param $http
 * @param $resource
 * @param envConfig
 */
languageServiceModule.language = function (
    $log,
    $q,
    $http,
    $resource,
    envConfig
) {
    $log.info("LanguageService::Constructor Called!");
    this.log = $log;
    this.q = $q;
    this.http = $http;
    this.resource = $resource;
    this.config = envConfig;
    //this.check();
};

/**
 *
 */
languageServiceModule.service("languageService",
    [
        '$log',
        '$q',
        '$http',
        '$resource',
        'envConfig',
        languageServiceModule.language]
     );

/**
 *
 * @returns {*|Function|Function|promise|Function|Function|promise|Function|Function|promise|Function|Function}
 */
languageServiceModule.language.prototype.check = function () {

    var self = this;
    var deferred = self.q.defer();
    self.locale = this.config.defaultLocale;
    var success = function (data) {
        self.log.info("languageService::success::retreived response language info : " + angular.toJson(data));
        if (data.data.preferredLanguageCode) {
            self.locale = data.data.preferredLanguageCode;
            self.log.info("languageService::locale is => " + self.locale);
            deferred.resolve(data.data);
        } else {
            deferred.resolve(self.locale);
        }
    };

    var error = function (error) {
        //self.log.error("languageService::error::not received language in response and setting default language");
        deferred.resolve(self.locale);
    };

    if (angular.isString(this.uri) && this.uri.length > 0) {
        self.log.info("languageService::check::calling language service for locale information");
        self.http.get(self.uri).then(success, error);
    } else {
        deferred.resolve({locale: self.locale});
    }
    return deferred.promise;
}

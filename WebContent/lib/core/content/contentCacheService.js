'use strict';
/**
 *
 * @type {Module}
 */
var contentCacheServiceModule = angular.module('ctl.core.content.services.contentCacheService', ['ctl.core.services.angularCacheService']);

/**
 * Singleton Object CmsCacheService
 * responsible for Content Caching
 */
contentCacheServiceModule.factory('ContentCacheService',
    [
        '$cacheFactory',
        '$log',
        'AngularCacheService',
        'languages',
        function(
            $cacheFactory,
            $log,
            AngularCacheService,
            languages) {

            /*var _cache = $cacheFactory('ContentCacheService', {
                capacity: 25 // optional - turns the cache into LRU cache
            });*/
            var englishContent  = AngularCacheService.contentCache.english;
            var spanishContent  = AngularCacheService.contentCache.spanish;
            var contentLanguage = AngularCacheService.contentCache.language;

            var getPageIdArray = function (pageId) {
                var pageIds;
                if(pageId!= undefined){
                    if ($.isArray(pageId)) {
                        pageIds = pageId;
                    }
                    else if (pageId.indexOf(",") == -1) {
                        pageIds = [pageId];
                    }
                    else {
                        pageIds = pageId.split(",");
                    }
                    return pageIds;
                }
            };


            var _getContent = function (pages, language) {

                $log.debug("ContentCacheService::getContent::current language received in parameter " + language.cmsId);

                if (!language) {
                    var appId = contentLanguage.get("userLang");
                    switch (appId) {
                        case languages.ENGLISH.appId:
                            language = languages.ENGLISH;
                            break;
                        case languages.SPANISH.appId:
                            language = languages.SPANISH;
                            break;
                        default:
                            $log.error("Content Service :: Unsupported Language :" + appId);

                    }
                }

                var _cache = language.cmsId == languages.ENGLISH.cmsId || language.cmsId == undefined ? englishContent : spanishContent;
                var content = {};
                var pageIds = getPageIdArray(pages);

                $log.debug("ContentCacheService::current language is " + language.cmsId);

                var dataFoundForAllPages = true;

                if (pageIds.length > 1) {
                    var pageId;
                    for (var key in pageIds) {
                        pageId = pageIds[key];
                        content[pageId] = _cache.get(pageId);
                        if (!content[pageId])
                            dataFoundForAllPages = false;
                    }
                }
                else {
                    content = _cache.get(pageIds[0]);
                    if (!content) {
                        dataFoundForAllPages = false;
                    }
                }
                if (!dataFoundForAllPages)
                    content = undefined;

                return content;
            };

            var _put = function (key, value, language) {

                switch (language.cmsId) {
                    case languages.ENGLISH.cmsId:
                        englishContent.put(key, value);
                        break;
                    case languages.SPANISH.cmsId:
                        spanishContent.put(key, value);
                        break;
                    default:
                        $log.error("Content Service :: Unsupported Language :" + appId);

                }
            };

            /**
             *
             * @returns {*}  => "en" for english and "es" for spanish
             * @private
             */
            var _getLanguage = function () {
                return contentLanguage.get("userLang");
            };

            /**
             * stores language information in sessionStorage
             *
             * @param language => expected value in input : "en" for english, "es" for spanish
             * @private
             */
            var _setLanguage = function (language) {
                contentLanguage.put("userLang", language);
                localStorage.setItem("userLang", language);
            };

            return {
                getContent:_getContent,
                put:_put,
                setLanguage:_setLanguage,
                getLanguage:_getLanguage
            };
}]);


'use strict';

/**
 *
 * @file ContentService.js
 * @overview ContentService is a Singleton Object that fetch the content from
 * backend/stubs
 */


/**
 *
 * @type {Module}
 * Defined all dependencies here.
 */
var contentServiceModule = angular.module('ctl.core.content.services',
    ['ctl.core.services.angularCacheService', 'ngResource', 'ctl.core.language.service']);

/**
 * Constants
 */
contentServiceModule.constant('contentURIs',
    {
        serviceUri      : "mdm/service",    // URL for MDM - Where content will be fetched
        serviceStubUri  : "data/content"
    });

contentServiceModule.constant('languages', {
    ENGLISH: {appId: "en", cmsId: "English" },
    SPANISH: {appId: "es", cmsId: "Spanish" }
});


/**
 * Included response httpInterceptor
 */
contentServiceModule.config(['$httpProvider', function ($httpProvider) {
   // $httpProvider.responseInterceptors.push('myHttpInterceptor');
}]);

/*
    provide Singleton Object ContentService
    responsible to fetch the content from backend
 */
contentServiceModule.factory('ContentService',
    [
        '$http',
        '$rootScope',
        '$log',
        '$resource',
        '$q',
        '$location',
        'envConfig',
        'ContentCacheService',
        'contentURIs',
        '$window',
        'languages',
        '$locale',
        'languageService',
        function (
            $http,
            $rootScope,
            $log,
            $resource,
            $q,
            $location,
            envConfig,
            ContentCacheService,
            contentURIs,
            $window,
            languages,
            $locale,
            languageService
        ) {


        //    var _cmsServiceResource = $resource(CmsURIs.serviceUri, {}, {fetchContent: {method: "POST", headers: bestConfig.headers}});

            var language = languages.ENGLISH;

            /**
             * This is used to change the content language
             * @param lang
             * @private
             */
            var _setLanguage = function (lang) {
                $log.info("ContentService::setLanguage::language is set as "+lang.appId);
                CmsCacheService.setLanguage(lang.appId);
                switch (lang.appId) {
                    case languages.ENGLISH.appId:
                        language = languages.ENGLISH;
                        break;
                    case languages.SPANISH.appId:
                        language = languages.SPANISH;
                        break;
                    default:
                        $log.error("Content Service :: Unsupported Language :" + lang.appId);

                }

            };

            /**
             * returns content's current language
             * @returns {language}
             * @private
             */
            var _currentLanguage = function () {
                return language;
            };


            /**
             * returns Promise Object for content
             * @param pageId
             * @returns {*|Function|Function|promise|Function|Function|promise|Function|Function|promise|Function|Function}
             * @private
             */
            var _fetchContent = function (pageId) {

                /**
                 * Final deferred object which returns promise object as a response
                 * @type {*|Deferred|*|Deferred|*|Deferred}
                 */
                var deferred = $q.defer();

                /**
                 * used to track the missing content pages
                 * which are not present in cache and need service call
                 * to be in cache
                 * @type {Array}
                 */
                var missingPages = [];

                /**
                 * Utility function
                 * @param pageId
                 * @returns {Array}
                 */
                var getArray = function (pageId) {

                    var pageIds = [];
                    if(pageId!== undefined){
                        if ($.isArray(pageId)) {
                            pageIds = pageId;
                        }
                        else if (pageId.indexOf(",") == -1) {
                            pageIds = [pageId];
                        }
                        else {
                            pageIds = pageId.split(",");
                        }
                    }

                    $log.debug("ContentService::getArray::pages are => "+pageIds);
                    return pageIds;
                };

                /**
                 * Different defers for different stubbed page contents
                 * @param pageId
                 * @returns {{}}
                 */
                var getDefers = function (pageId) {
                    var pages = getArray(pageId);
                    var defers = {};
                    angular.forEach(pages, function (value) {
                        defers[value] = $q.defer();
                    });
                    return defers;
                };

                /**
                 * Different Promise objects for different defers
                 * for stubs data
                 * @param defers
                 * @returns {{}}
                 */
                var promises = function (defers) {
                    var promiseObject = {};
                    angular.forEach(defers, function (defer, pageId) {
                        promiseObject[pageId] = defer.promise;
                    });
                    return promiseObject;
                };

                /**
                 * returns request object for content fetching
                 * @param pageId
                 * @param language
                 * @returns {{CommonData: {AppName: string}, ContentProviderRequestDetails: Array}}
                 */
                var requestSerializer = function (
                    pageId,
                    language
                    ) {

                    var requestContainer = {
                        "CommonData": {
                            "AppName": "CTL"
                        },
                        "ContentProviderRequestDetails": []};

                    var requestTemplate = {
                        "PageId": "",
                        "Version": "1",
                        "Language": language.cmsId
                    };

                    var pageIds = getArray(pageId);

                    $(pageIds).each(function (index) {
                        var request = angular.copy(requestTemplate);
                        request.Pageid = pageIds[index];
                        requestContainer.ContentProviderRequestDetails.push(request);
                    });
                    return requestContainer;
                };

                /**
                 * Returns cached data
                 * @param pageIds
                 * @returns {undefined|cachedData}
                 */
                var fetchFromCache = function(pageIds) {

                    var cachedResponse;
                    var pageData;
                    var dataFoundForAll = true;

                    if (pageIds.length > 1) {
                        var value;
                        for(var key=0; key <pageIds.length; key++){
                        //for(var key in pageIds) {
                            value = pageIds[key];
                            if(angular.isDefined(value) && value != null && value != ''){
	                            pageData = ContentCacheService.getContent(value,_currentLanguage());
	                            if (pageData && pageData.PageId === value) {
	                                if (!cachedResponse) {
	                                    cachedResponse = {};
	                                }
	                                cachedResponse[value] = pageData;
	                            } else {
	                                missingPages.push(value);
	                                dataFoundForAll = false;
	                            }
                            }
                        }
                    } else {

                        cachedResponse =  ContentCacheService.getContent(pageIds[0],_currentLanguage());
                        dataFoundForAll = cachedResponse && cachedResponse.PageId === pageIds[0];
                        if(!dataFoundForAll)
                            missingPages.push(pageIds[0]);
                    }

                    //$log.info("ContentCacheService::fetchFromCache::cachedData is => "+ angular.toJson(cachedResponse));

                    return dataFoundForAll ? cachedResponse : undefined;
                };


               /**
                * stores response content in cache
                @param data
                            data contains the response of pagecontent request
                            It uses ContentCacheService to cache the content
                            The expected response formats are as below :
                            case : response have only single page content
                                 {
                                    "PageId": "Common",
                                    "Language": "English",.....
                                 }
                            case : response have multiple page's content
                                {
                                   <PageId> : { <pageContent>},
                                   Common :   {
                                     "PageId": "Common",
                                     "Language": "English",.....
                                   },
                                   <PageId> : {<pageContent>}
                                   ....
                                }
                 */
                var doCaching = function (data) {

                    if (data != undefined && data["PageId"] != undefined) {

                        ContentCacheService.put(data["PageId"], data,_currentLanguage());

                    }else if(data!== undefined) {

                        for(var key in data){
                                if(key.indexOf("\$")===-1)
                                    ContentCacheService.put(key, data[key],_currentLanguage());
                        }
                    }

                };

                var pageIds = getArray(pageId);

                /*
                 Checks for all pageIds are available in cache
                 and
                 return boolean accordingly
                 */
                var cachedResponse = fetchFromCache(pageIds);

                /*
                 return defers objects as below structure
                 {
                 pageId_as_key : json_response,
                 pageId_as_key : json_response,...
                 }
                 */
                var defers = {};


                /*
                 returns promise objects as below structure :

                 {
                 pageId_as_key : promise_object,
                 pageId_as_key : promise_object,...
                 }
                 */
                var getStubbedPromises = function () {
                    var promises = {};
                    angular.forEach(defers, function (value, key) {
                        promises[key] = defers[key].promise;
                    });
                    return promises;
                };

                /**
                 * makes stubbed calls for pages content
                 * @param pageIds
                 * @returns promise Object
                 *
                 */
                var getStubbedData = function (pageIds) {

                    defers = getDefers(pageIds);
                    promises = getStubbedPromises();
                    angular.forEach(pageIds, function (pageId) {
                        var stub = $resource(contentURIs.serviceStubUri + '/' + pageId + '_' + language.cmsId + '.json');
                        stub.get({}, success, error);
                    });

                    var promise = $q.all(promises);

                    promise.then(stubbedSuccess, error);
                };

                /*
                 Returns stubbed data
                 This would be used to return combined result from
                 multiple json files
                 */
                var stubbedSuccess = function (data) {
                    doCaching(data);
                    deferred.resolve(data);
                };

                /**
                 * Handles success response
                 * @param data
                 * @param status
                 */
                var success = function (
                    data,
                    status
                    ) {

                    //$log.debug("ContentService::success:content received => "+angular.toJson(data));

                    var isError             =   false;
                    var fileNotFoundError   =   false;

                    if(data && data.ErrorMessage){
                        isError = true;
                        if(data.ErrorMessage === "File Not Found"){
                            fileNotFoundError = true;
                            $log.error("ContentService::success::File Not found :"+data.PageId);
                        }
                    }
                    else if(data){
                        var value;
                        for(var key in data){
                            value = data[key];
                            if(value["ErrorMessage"]){
                                isError = true;
                                if(value["ErrorMessage"] === "File Not Found"){
                                    fileNotFoundError = true;
                                    $log.error("ContentService::success::File Not found :"+key);
                                }
                            }

                        }
                    }

                    $log.warn("ContentService::Error Status "+ isError);

                    if(!isError){
                        doCaching(data);
                        if(envConfig.useContentStub){
                            defers[data.PageId].resolve(data);
                        }else{
                            var cachedData = ContentCacheService.getContent(pageId,_currentLanguage());

                            /**
                             * In case we recived all pages content , we resolved cached data
                             * otherwise only responded data would be resolved.
                             * The reason is whatever response we received , we already cached.
                             */
                            if(cachedData){
                                deferred.resolve(cachedData);
                            }else{
                                deferred.resolve(data);
                            }
                        }
                    }else{
                        if(!fileNotFoundError && envConfig.contentFallBack){
                            $log.error("ContentService::error::ErrorResponse in success method:As a fallback strategy retrieving data from stub");
                            envConfig.useContentStub = true;
                            getStubbedData(pageIds);
                        }
                    }
                };

                /**
                 * Handles error response
                 * @param data
                 * @param status
                 * @returns {*}
                 */
                var error = function (
                    data,
                    status
                    ) {

                    $log.error("ContentService::error::"
                        + angular.toJson(data) + ' and status: ' + status);
                    if(!envConfig.useContentStub && envConfig.contentFallBack){
                        $log.error("ContentService::error::ErrorResponse in error method:As a fallback strategy retrieving data from stub");
                        envConfig.useContentStub = true;
                        return getStubbedData(pageIds);
                    }
                };

                /**
                 * In case useStubs== true
                 * fetch content from stubbed data
                 */
                if (envConfig.useContentStub) {
                    getStubbedData(pageIds);
                }
                else {

                    /**
                     * In case we have whole data in cache
                     * returns/resolves the data from cachedResponse
                     * otherwise
                     * fetch the missing content from service
                     * and combined it with having ones
                     * and returns/resolves the combined data
                     */
                    if (cachedResponse) {
                        deferred.resolve(cachedResponse);
                    }
                    else {

                        var request = requestSerializer(missingPages, language);

                        if(missingPages.length==0)
                            $log.error("ContentService::fetchContent::If you see this message please inform TechM. There is some issue in caching logic.");

                        //$log.info("ContentService::fetchContent:missingPages are => "+angular.toJson(request));
                        _cmsServiceResource.fetchContent(request, success, error);
                    }
                }
                return deferred.promise;
            };
            
            var _getLanguageBasedOnAppId = function (appId) {

                var lang = languages.ENGLISH;
                switch (appId.toUpperCase()) {
                    case languages.ENGLISH.appId.toUpperCase():
                        lang = languages.ENGLISH;
                        break;
                    case languages.SPANISH.appId.toUpperCase():
                        lang = languages.SPANISH;
                        break;
                    default:
                        $log.info("Content Service::_getLanguageBasedOnAppId:: Unsupported Language => " + appId);
                }

                return lang;
            };
            
            var _fetchContentInterceptor = function(pageId) {
                /**
                 * syncs the current language from session storage
                 */
                var internalDeferred = $q.defer();
                var appId = languages.ENGLISH.appId;
                /*
                 * Case 1: When user refresh page
                 * angularCache will have the selected language
                 * 
                 * Case 2: when user is already logged in and open a new tab. 
                 * In this case we need to maintain the same language which is set by already logged in user.
                 */
                if (ContentCacheService.getLanguage() != undefined || localStorage.getItem("currentLoggedInUser")) {
                    
                    if(ContentCacheService.getLanguage() != undefined){
                        $log.info("ContentService::fetchContentInterceptor::CmsCacheService.getLanguage value is ------------>"+ContentCacheService.getLanguage());
                        appId = ContentCacheService.getLanguage();
                    }else{
                        $log.info("ContentService::fetchContentInterceptor::localStorage.getItem(\"currentLoggedInUserLanguage\") value is ------------>"+localStorage.getItem("currentLoggedInUserLanguage"));
                        appId = localStorage.getItem("currentLoggedInUserLanguage");
                    }

                    language = _getLanguageBasedOnAppId(appId);
                    
                    $log.debug("ContentService::fetchContentInterceptor::language in localStorage => " + appId);
                    _fetchContent(pageId).then(function(data){
                            internalDeferred.resolve(data);
                        },
                        function(error){
                            internalDeferred.resolve(error);
                        });
                }
                /**
                 * Case : When user opens a new window with a new session
                 * i.e.
                 * No logged in user already. No other window opened
                 * 
                 */
                else {
                    $log.debug("ContentService::fetchContent::userLang in session not found");
                    $log.debug("ContentService::fetchContent::language received is => " + languageService.locale);
                   

                     var languagePromise = languageService.check();

                    languagePromise.then(function(data) {
                            language = _getLanguageBasedOnAppId(languageService.locale);
                            $log.debug("ContentService::fetchContent::language is going to be set as :" + language.appId);

                            ContentCacheService.setLanguage(language.appId);
                            _fetchContent(pageId).then(function(data){
                                    internalDeferred.resolve(data);
                                },
                                function(error){
                                    internalDeferred.resolve(error);
                                });
                        },
                        function (error) {
                            $log.error("ContentService::fetchContentInterceptor::errorFunction =>" + error);
                            $log.error("ContentService::fetchContentInterceptor::language not found. going with default one and i.e. " + languages.ENGLISH.appId);
                            ContentCacheService.setLanguage(languages.English.appId);
                            _fetchContent(pageId).then(function(data){
                                    internalDeferred.resolve(data);
                                },
                                function(error){
                                    internalDeferred.resolve(error);
                                });
                        });

                }
                return internalDeferred.promise;
            };

            var _getPageTitle = function(pageTitlePickerField,pageId){

                var configureMessage    = "Configure Page Title Please !";
                var pageTitleSource     = null;
                var pageTitle           = null;

                $log.debug("ContentService::getPageTitle::pageId for pageTitle => "+pageId);
                $log.debug("ContentService::getPageTitle::pageTitlePickerField => "+pageTitlePickerField);

                if(pageId!=undefined ){

                    pageTitleSource = CmsCacheService.getContent(pageId,_currentLanguage());

                    if(pageTitleSource != undefined){
                        if( pageTitleSource.PageId === pageId ){

                            if(pageTitlePickerField!=undefined){

                                var fieldHeirarchy = pageTitlePickerField.split(".");
                                var value;
                                for(var key in fieldHeirarchy){
                                    value = fieldHeirarchy[key];
                                    if (key == 0)
                                        pageTitle = pageTitleSource[value];
                                    else
                                        pageTitle = pageTitle[value];

                                }
                                if(pageTitle==undefined){
                                    pageTitle = configureMessage;
                                }
                            }else{
                                pageTitle = configureMessage;
                            }


                        }else{
                            $log.warn("ContentService::content is not available");
                            pageTitle = configureMessage;
                        }
                    }else{
                        pageTitle = configureMessage;
                    }
                }else{
                    pageTitle = configureMessage;
                }
                $rootScope.title = pageTitle;
                $log.info("ContentService::getPageTitle::pageTitle => "+pageTitle);
                return pageTitle;
            };

            return {
                fetchContent: _fetchContentInterceptor,
                setLanguage: _setLanguage,
                currentLanguage: _currentLanguage,
                getPageTitle : _getPageTitle
            };

        }]);

// register the interceptor as a service
contentServiceModule.factory('myHttpInterceptor', [
    '$q',
    function (
    $q
    ) {
    return function (promise) {
        return promise.then(
            function (response) {

                if (response.data.Content != undefined && $.isArray(response.data.Content)) {

                    if (response.data.Content.length == 1) {

                        response.data = response.data.Content[0];

                    }
                    else if (response.data.Content.length > 1) {

                        var jsonResponse = {};

                        $(response.data.Content).each(function (index) {
                            var pageId = response.data.Content[index].PageId;
                            jsonResponse[pageId] = response.data.Content[index];
                        });

                        response.data = jsonResponse;
                    }
                }

                return response;
            },
            function (response) {
                return $q.reject(response);
            });
    }
}]);

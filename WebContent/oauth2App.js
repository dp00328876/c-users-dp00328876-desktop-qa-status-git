/* File - oauth2App.js
*/

'use strict';

var oauth2App = angular.module('oauth2App', ['ngResource']);

oauth2App.service('oauth2Service',[
	'$log', '$q', '$resource',
	function( $log, $q, $resource ) {

		var _oauth2Service = $resource("https://auth.veuxdu.centurylink.com/token", {}, {
			login: {
				method : "POST", 
				headers : {"Content-Type": "application/x-www-form-urlencoded"} 
			}
		});
		//grant_type=password&username=test01&password=password1234

		var _login = function(inputData) {
			alert('oauth2Service._login');
			var postData = "grant_type=" + inputData.grant_type + "&username=" + inputData.username + "&password=" + inputData.password;
			$log.info("oauth2Service: _login:" + postData);
			var deferred = $q.defer();
			
			var success = function(successResponse){
				$log.info("oauth2Service _login: " + angular.toJson(successResponse));
				deferred.resolve(successResponse);
			};
			var error = function(errorResponse){
				$log.info("oauth2Service _login: Error" + errorResponse);
				deferred.reject(errorResponse);
			};
			
			$log.info("oauth2Service: _login API invoked : ");
			_oauth2Service.login(postData, success, error);
			return deferred.promise;
		}
	    
		return {
			login : _login
		};
	}
]);

oauth2App.controller('oauth2Controller', [
    '$scope', '$log', 'oauth2Service',
    function ( $scope, $log, oauth2Service ) {
		
		//Authentication
        $scope.login = function () {
			$log.info('oauth2Controller.login');
			var inputData = {};
			inputData.grant_type = $scope.grant_type;
			inputData.username = $scope.username;
			inputData.password = $scope.password;
			var loginResponse = oauth2Service.login(inputData);
			loginResponse.then(
				function(successData) {
					$scope.SearchResults = true;
					$log.info('oauth2Controller::login method:: Success '+successData);
				},
				function(errorData) {
					$scope.SearchResults = false;
					$log.info('oauth2Controller::login method:: Error ' + errorData);
				}
			)
		};
		
	}
]);
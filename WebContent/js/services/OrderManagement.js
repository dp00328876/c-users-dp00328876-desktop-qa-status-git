'use strict';

var ordermgmt = angular.module('ctl.ordermanagement.service', []);


ordermgmt.constant('orderManagementURIs',
	{
		uris: {
			readSeatStubUri: "stubs/ordermanagement/readSeatSuccess.json",
			createSeatStubUri: "stubs/ordermanagement/createSeatSuccess.json",
			updateSeatStubUri: "stubs/ordermanagement/updateSeatSuccess.json",
			readNetworkIDStubUri: "",
			createNetworkIDStubUri: "",
			deleteNetworkIDstubUri: "",
			deleteSeatStubUri: "",
			readAccountStubUri: "stubs/ordermanagement/readAccountSuccess.json",
			readSubAccountStubUri: "stubs/ordermanagement/readSubAccountSuccess.json"
		}
	});


ordermgmt.factory('OrderManagementService',
		[
			'$log',
			'$q',
			'$rootScope',
			'$route',
			'$resource',
			'$http',
			'orderManagementURIs',
			'apiConfig',
			'envConfig',
			'AngularCacheService',
			'ctlUtils',
			function(
				$log,
				$q,
				$rootScope,
				$route,
				$resource,
				$http,
				orderManagementURIs,
				apiConfig,
				envConfig,
				AngularCacheService,
				ctlUtils
			) {
				
				//Stubs are missing for _createSeatStubService, _deleteSeatStubService
				
				//var _readAccountService = $resource(orderManagementURIs.uris.readAccountUri,{}, {readAccount:{method:"GET",headers:apiConfig.headers}});
				var _readAccountStubService = $resource(orderManagementURIs.uris.readAccountStubUri);
				var _readSubAccountStubService = $resource(orderManagementURIs.uris.readSubAccountStubUri);
				var _readNetworkIdStubService = $resource(orderManagementURIs.uris.readNetworkIDStubUri);
				var _createNetworkIdStubService = $resource(orderManagementURIs.uris.createNetworkIDStubUri);
				var _deleteNetworkIdStubService = $resource(orderManagementURIs.uris.deleteNetworkIDstubUri);
				var _readSeatStubService = $resource(orderManagementURIs.uris.readSeatStubUri);
				var orderMgmtDataCache = AngularCacheService.OrderMgmtDataCache;    //Initializing cache service
				
				/* Place holder to store the reference of SDK's Product Service. */
				var _productService = CTL.Service.Product;

				/**
				 * Set cache
				 * @param {String} key
				 * @param {Object|String} val
				 * @returns {Boolean}
				 */
				var _setOrderMgmtDataCache=function(key,val) {
					if(angular.isDefined(key) && angular.isDefined(val) && key!=="") {
						orderMgmtDataCache.put(key,val);
						return(true);
					}
					return(false);
				};
				
				/**
				 * Set cache
				 * @param {String} key
				 * @param {Object|String} val
				 * @returns {Boolean}
				 */
				var _clearOrderMgmtDataCache = function(key) {
					if(angular.isDefined(key)) {
						orderMgmtDataCache.put(key, null);
						return(true);
					}
					return(false);
				};

				/**
				 * Getting Cache data
				 * @param {String} key
				 * @returns {Boolean|Object|String}
				 */
				var _getOrderMgmtDataCache=function(key) {
					return(orderMgmtDataCache.get(key) || false);
				};

				var _createSeat = function(inputData) {

					$log.info("orderManagementService: _createSeat");
				
					var deferred = $q.defer();
					var postData = {};
					//inputData.headers.AccountName = apiConfig.headers.AccountName;
					//inputData.headers.CorrelationId = apiConfig.headers.CorrelationId;
					//$log.info("orderManagementService: CorrelationId"+inputData.headers.CorrelationId);
					/*var createSeatHeaders = {};
					angular.copy(apiConfig.headers, createSeatHeaders);
					createSeatHeaders.UseMock = inputData.headers.UseMock;*/
					
					postData.customerId = inputData.customerId;
					postData.products = inputData.products;
					postData.legalAckValue = inputData.legalAckValue;
					postData.legalAckStatus = inputData.legalAckStatus;

					/*var _createSeatService = $resource(orderManagementURIs.uris.createSeatUri,{},
						{createSeat:{method:"POST",headers:createSeatHeaders, params: {phoneNumber:inputData.phoneNumber }}});*/
					var success = function(successResponse){
						$log.info("orderManagementService: _createSeat: "+ angular.toJson(successResponse));
						deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);						
					};
					var error = function(errorResponse){
						$log.info("orderManagementService: _createSeat: Error"+angular.toJson(errorResponse));
						deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
					};
					if (envConfig.useOrderManagementStub) {
						$log.info("OrderManagementService: _createSeat Stub  API invoked : ");
						_createSeatStubService.get({}, success, error);
					} else{
						$log.info("OrderManagementService: _createSeat API invoked : ");
						_productService.subscribeProduct(postData).then(success, error);
					}
					return deferred.promise;
				}

				var _deleteSeat = function(inputData) {

					$log.info("OrderManagementService: _deleteSeat");
					
					var deferred = $q.defer();
					var postData = {};
					/*var deleteSeatHeaders = {};
					angular.copy(apiConfig.headers, deleteSeatHeaders);
					deleteSeatHeaders.SeatId = inputData.headers.SeatId; 
					deleteSeatHeaders.UseMock = inputData.headers.UseMock; */
					postData.seatId = inputData.headers.SeatId;
				
					/*var _deleteSeatService = $resource(orderManagementURIs.uris.deleteSeatUri,{},
						{deleteSeat:{method:"DELETE",headers:deleteSeatHeaders, params: inputData.params}});*/

					var success = function(successResponse){
						$log.info("OrderManagementService: _deleteSeat: "+ angular.toJson(successResponse));
						deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
					};
					var error = function(errorResponse){
						$log.info("OrderManagementService: _deleteSeat: Error"+angular.toJson(errorResponse));
						deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
					};


					if (envConfig.useOrderManagementStub) {
						$log.info("OrderManagementService: _deleteSeat Stub  API invoked : ");
						_deleteSeatStubService.get({}, success, error);
					} else{
						$log.info("OrderManagementService: _deleteSeat API invoked : ");
						_productService.unsubscribeProduct(postData).then(success, error);
					}
					return deferred.promise;
				};
				
				var _readSeat = function(inputData) {

					$log.info("OrderManagementService: _readSeat");
					
					var deferred = $q.defer();
					var postData = {};
					
					postData.seatId = inputData.seatId;
				
					var success = function(successResponse){
						$log.info("OrderManagementService: _readSeat: "+ angular.toJson(successResponse));
						deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
					};
					var error = function(errorResponse){
						$log.info("OrderManagementService: _readSeat: Error"+angular.toJson(errorResponse));
						deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
					};


					if (envConfig.useOrderManagementStub) {
						$log.info("OrderManagementService: _readSeat Stub  API invoked : ");
						_readSeatStubService.get({}, success, error);
					} else{
						$log.info("OrderManagementService: _readSeat API invoked : ");
						_productService.getSubscribedProduct(postData).then(success, error);
					}
					return deferred.promise;
				};

				var _readAccount = function(inputData) {

					$log.info("OrderManagementService: _readAccount ");

					var deferred = $q.defer();
					var postData = {};
					//postData.networkId = inputData.networkId;
					var success = function(successResponse){
						$log.info("OrderManagementService: _readAccount: success");
						deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : _populateAccountResponse(successResponse.data));
					};
					var error = function(errorResponse){
						$log.info("OrderManagementService: _readAccount: Error"+angular.toJson(errorResponse));
						if(angular.isDefined(errorResponse) && angular.isDefined(errorResponse.StatusCode) && errorResponse.StatusCode === 404){
							deferred.resolve(angular.isUndefined(errorResponse) ? errorResponse : _populateNoSeatsResponse(errorResponse));
						}else{
							deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
						}			
					};
					
					if (envConfig.useOrderManagementStub) {
						$log.info("OrderManagementService: _readAccount Stub  API invoked : ");
						_readAccountStubService.get({}, success, error);
					} else{
						$log.info("OrderManagementService: _readAccount API invoked : ");
						_productService.getSubscribedProducts(postData).then(success, error);
	                }
					
					return deferred.promise;
				};
				
				var _populateNoSeatsResponse = function(accountSummaryResponse){
					var accountSummaryData =[];
					var totalCount = 0;
					var pendingCount = 0;
					var monikerCount = 0;					
					accountSummaryResponse.totalCount = totalCount;
					accountSummaryResponse.totalPending = pendingCount;
					accountSummaryResponse.monikerCount = monikerCount;
					accountSummaryData.push(accountSummaryResponse);
					_setOrderMgmtDataCache("accountsummarydata", accountSummaryData);
					return accountSummaryData;
				};
				
				var _populateAccountResponse = function(accountSummaryResponse){
					var accountSummaryData =[];
					var totalCount = 0;
					var pendingCount = 0;
					var monikerCount = 0;
					angular.forEach(accountSummaryResponse.seats, function(seat){
						totalCount = totalCount+1;
						if(seat.seatStatus == 'reserved'){
							pendingCount = pendingCount+1;
						}
						if(angular.isUndefined(seat.moniker) ||  seat.moniker== ''){
							seat.moniker = 'Unassigned';
						}
						else{
							monikerCount = monikerCount + 1;
						}
						if(angular.isUndefined(seat.customerId) ||  seat.customerId == ''){
							seat.customerId = '--';
						}
					})
					accountSummaryResponse.totalCount = totalCount;
					accountSummaryResponse.totalPending = pendingCount;
					accountSummaryResponse.monikerCount = monikerCount;
					accountSummaryResponse.collapsed = true;
					accountSummaryData.push(accountSummaryResponse);
					_setOrderMgmtDataCache("accountsummarydata", accountSummaryData);
					return accountSummaryData;
				};

				var _createNetworkId = function(inputData) {

					$log.info("orderManagementService: _createNetworkId");				
					var deferred = $q.defer();
					var postData = {};
					postData.seatId = inputData.seatId;
					postData.networkId = inputData.networkId;
					$log.info("orderManagementService: _createNetworkId: seatId:"+ postData.seatId + " networkId:" +postData.networkId);
					
					var success = function(successResponse){
						$log.info("orderManagementService: _createNetworkId: Success" + angular.toJson(successResponse));
						deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
					};
					var error = function(errorResponse){
						$log.info("orderManagementService: _createNetworkId: Error"+angular.toJson(errorResponse));
						deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
					};
					if (envConfig.useOrderManagementStub) {
						$log.info("OrderManagementService: _createNetworkId Stub  API invoked : ");
						_createNetworkIdStubService.get({}, success, error);
					} else{
						$log.info("OrderManagementService: _createNetworkId API invoked : ");
						_productService.assignProduct(postData).then(success, error);
					}
					return deferred.promise;
				};	
				
				var _deleteNetworkId = function(inputData) {
					$log.info("OrderManagementService: _deleteNetworkId");
					var deferred = $q.defer();
					var postData = {};
					postData.seatId = inputData.seatId;
					postData.networkId = inputData.networkId;
					var success = function(successResponse){
						$log.info("OrderManagementService: _deleteNetworkId: Success"+ angular.toJson(successResponse));
						deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
					};
					var error = function(errorResponse){
						$log.info("OrderManagementService: _deleteNetworkId: Error"+angular.toJson(errorResponse));
						deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
					};
					if (envConfig.useOrderManagementStub) {
						$log.info("OrderManagementService: _deleteNetworkId Stub  API invoked : ");
						_deleteNetworkIdStubService.get({}, success, error);
					} else{
						$log.info("OrderManagementService: _deleteNetworkId API invoked : ");
						_productService.unassignProduct(postData).then(success, error);
					}
					return deferred.promise;
				};
				
				var _updateNetworkId = function(inputData) {

					$log.info("orderManagementService: _updateNetworkId");				
					var deferred = $q.defer();
					
					var success = function(successResponse){
						$log.info("OrderManagementService: _updateNetworkId: Success"+ angular.toJson(successResponse));
						deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
					};
					var error = function(errorResponse){
						$log.info("OrderManagementService: _updateNetworkId: Error"+angular.toJson(errorResponse));
						deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
					};
					
					var newNetworkId = inputData.networkId;
					inputData.networkId = inputData.oldNetworkId;
					
					_productService.unassignProduct(inputData)
						.then(function (successResponse) {
							inputData.networkId = newNetworkId;
							_productService.assignProduct(inputData)
								.then(success, error);
						}, error);
					
					return deferred.promise;
				};
				
				return {
					createSeat : _createSeat,
					deleteSeat : _deleteSeat,
					readAccount : _readAccount,
					createNetworkId:_createNetworkId,
					deleteNetworkId:_deleteNetworkId,
					setOrderMgmtDataCache: _setOrderMgmtDataCache,
					getOrderMgmtDataCache: _getOrderMgmtDataCache,
					clearOrderMgmtDataCache: _clearOrderMgmtDataCache,
					readSeat : _readSeat,
					updateNetworkId : _updateNetworkId
				};
				
			}
		]);


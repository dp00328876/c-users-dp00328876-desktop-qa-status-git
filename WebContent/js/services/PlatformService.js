'use strict';

var platform = angular.module('ctl.platform.service', []);


courier.factory('PlatformService',
	[
		'$log',
		'$q',
		'$rootScope',
		'$route',
		'$resource',
		'$http',
		'apiConfig',
		'envConfig',
		'AngularCacheService',
		function(
			$log,
			$q,
			$rootScope,
			$route,
			$resource,
			$http,
			apiConfig,
			envConfig,
			AngularCacheService
		) {
			
			/* place holder to store the reference of SDK's Platform service */
			var _platformService = CTL.Service.Platform;			
					
			var _getAccount = function() {

				$log.info("PlatformService: _getAccount");
				
				var deferred = $q.defer();
				
				var success = function(successResponse){
					$log.info("PlatformService: _getAccount: " + angular.toJson(successResponse));
					var platformAccountInfo = angular.isUndefined(successResponse.data) ? successResponse : successResponse.data;
					/* Temp Code unless we get this information from platform ---- START */
					var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));
					if (angular.isDefined(tokenData)) {
						platformAccountInfo.branding.loginLogoUrl = "/Theme/LoginLogoNoCache/" + tokenData.AccountName;
					}
					/* Temp Code unless we get this information from platform ---- END */
					localStorage.setItem('platformAccountInfo', JSON.stringify(platformAccountInfo));
					deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
				};
				var error = function(errorResponse){
					$log.info("PlatformService: _getAccount: Error" + angular.toJson(errorResponse));
					deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
				};
			
				$log.info("PlatformService: _getAccount API invoked : ");
				_platformService.getAccount().then(success, error);
				
				return deferred.promise;
			}
			
			var _switchAccount = function(inputData) {
				$log.info("PlatformService: _switchAccount");
				
				var deferred = $q.defer();
				
				var success = function(successResponse){
					$log.info("PlatformService: _switchAccount: " + angular.toJson(successResponse));
					deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
				};
				var error = function(errorResponse){
					$log.info("PlatformService: _switchAccount: Error" + angular.toJson(errorResponse));
					deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
				};
			
				$log.info("PlatformService: _switchAccount API invoked : ");
				_platformService.switchAccount(inputData).then(success, error);
				
				return deferred.promise;
			}
			
			
			return {
				getAccount : _getAccount,
				switchAccount : _switchAccount
			};

		}
	]);

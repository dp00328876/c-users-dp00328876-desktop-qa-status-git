/* File - DumpsterService.js
 * Author - PB0083784
 * Date - 10/30/2015
 * Description - Dumpster service to read and delete SMS.
 * 
 */
'use strict';

var dumpster = angular.module('ctl.dumpster.service', []);

dumpster.constant('dumpsterURIs',
		{
			uris: {
				
				readMessageUri: "http://64.15.189.209/v1/sms/:key",
				readMessageStubUri: "stubs/dumpster/readMessageSuccess.json",

				deleteMessageUri: "http://64.15.189.209/v1/sms/:key",
				deleteMessageStubUri: "stubs/dumpster/deleteMessageSuccess.json",
			}
		});

dumpster.factory('DumpsterService', 
	[
		'$log',
		'$q',
		'$rootScope',
		'$route',
		'$resource',
		'$http',
		'dumpsterURIs',
		'apiConfig',
		'envConfig',
		function(
			$log,
			$q,
			$rootScope,
			$route,
			$resource,
			$http,
			dumpsterURIs,
			apiConfig,
			envConfig
		) {
	
		//var _readMessageService = $resource(dumpsterURIs.uris.readMessageUri,{}, {getMessages:{method:"GET",headers:apiConfig.headers}});
			var _readMessageStubService = $resource(dumpsterURIs.uris.readMessageStubUri);
			var _deleteMessageStubService = $resource(dumpsterURIs.uris.deleteMessageStubUri);

			/* place holder to store the reference of SDK's SMS service */
			var _smsService = CTL.Service.SMS;
			
			var _readMessage = function(inputData) {

				$log.info("DumpsterService: _readMessage");
				//GET v1/sms/:messageId
				//Required headers: AccountName, CorrelationID, SeatID, NetworkID
				//Request body {"enabled": true,"messageSaveStore": true}
				
				//inputData.headers.AccountName = apiConfig.headers.AccountName;
				var deferred = $q.defer();
				
				/*var _readMessageService = $resource(inputData.readMessageUri,{},
					{readMessage:{method:"GET",headers:inputData.headers
						}});*/

				var success = function(successResponse){
					$log.info("DumpsterService: _readMessage: "+ angular.toJson(successResponse));
					deferred.resolve(/*angular.isUndefined(successResponse.data) ?*/ successResponse /*: successResponse.data*/);
				};
				var error = function(errorResponse){
					$log.info("DumpsterService: _readMessage: Error"+angular.toJson(errorResponse));
					deferred.reject(/*angular.isUndefined(errorResponse.data) ?*/ errorResponse /*: errorResponse.data*/);
				};


				if (envConfig.useDumpsterStub) {
					$log.info("DumpsterService: _readMessage Stub  API invoked : ");
					_readMessageStubService.get({}, success, error);
				} else{
					$log.info("DumpsterService: _readMessage API invoked : ");
					_smsService.getMessageHistory(inputData).then(success, error);
				}
				return deferred.promise;
			}

	
			var _deleteMessage = function(inputData) {

				$log.info("DumpsterService: _deleteMessage");
				//GET v1/sms/:messageId
				//Required headers: AccountName, CorrelationID, SeatID, NetworkID
				//Request body {"enabled": true,"messageSaveStore": true}

				var deferred = $q.defer();
				/*var postData = {};
				inputData.headers.AccountName = apiConfig.headers.AccountName;
				var _deleteMessageService = $resource(inputData.deleteMessageUri,{},
					{deleteMessage:{method:"DELETE",headers:inputData.headers}});*/

				var success = function(successResponse){
					$log.info("DumpsterService: _deleteMessage: "+ angular.toJson(successResponse));
					deferred.resolve(/*angular.isUndefined(successResponse.data) ?*/ successResponse /*: successResponse.data*/);
				};
				var error = function(errorResponse){
					$log.info("DumpsterService: _deleteMessage: Error"+angular.toJson(errorResponse));
					deferred.reject(/*angular.isUndefined(errorResponse.data) ?*/ errorResponse /*: errorResponse.data*/);
				};


				if (envConfig.useDumpsterStub) {
					$log.info("DumpsterService: _deleteMessage Stub  API invoked : ");
					_deleteMessageStubService.get({}, success, error);
				} else{
					$log.info("DumpsterService: _deleteMessage API invoked : ");
					_smsService.deleteMessageHistory(inputData).then(success, error);
				}
				return deferred.promise;
			}
			
	    
	    
	    return {
	    	readMessage : _readMessage,
	    	deleteMessage : _deleteMessage
	    };
	}]
);
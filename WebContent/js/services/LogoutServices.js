'use strict';

var logoutServices = angular.module('ctl.login.service', ['ngResource']);

logoutServices.factory('LogoutServices',
    [
        '$rootScope',
        '$log',
        'envConfig',
        'SessionKeeper',
        '$angularCacheFactory',
        'SessionTimer',
        function(
            $rootScope,
            $log,
            envConfig,
            SessionKeeper,
            $angularCacheFactory,
            SessionTimer
            ) {

        	var _oauthServices = CTL.Service.Oauth2;
        	
            var _logOut = function() {
                $log.info("LogoutServices :: In _logOut");
                // Call backend API

                //clear all cache
                _logoutMethodCall();


            };

            var _sessionTimelogOut = function() {
                // Call backend API

                //clear all cache
                _logoutMethodCall();


            };

            var _logoutMethodCall = function() {
                $log.info("LogoutServices :: In _logoutMethodCall");
                // Remove entries from LocalStorage
                //localStorage.removeItem('');
                
                _oauthServices.logOut();	// Cleaning up SDK cache
                _stopSessionTimer();

                SessionKeeper.clear();

                //Clear the cache after logout
                $angularCacheFactory.clearAll();  //This can be moved in SessionKeeper
                
                // clearing session storage
                sessionStorage.clear();

                // Check if values like language needs to be stored into LocalStorage
            };

            var _startSessionTimer = function() {
                $log.info("LogoutServices :: startSessionTimer - Starting session monitoring");
                var current = SessionKeeper.read();
                current.isUserLoggedIn = true;
                sessionStorage.setItem("isUserAuthenticated", true);
                SessionKeeper.save();
            };
            var _stopSessionTimer = function() {
                $log.info("LogoutServices :: stopSessionTimer - Stopping session monitoring");
                var current = SessionKeeper.read();
                current.isUserLoggedIn = false;
                sessionStorage.setItem("isUserAuthenticated", false);
                SessionKeeper.save();
                SessionTimer.stopSession(); // Stop Session
                $rootScope.$broadcast("endDisplayMenu");
            };

            var _isSessionMonitored = function() {
                var current = SessionKeeper.read();
                $log.info("LogoutServices :: isSessionMonitored - " + current.isUserLoggedIn);
                return current.isUserLoggedIn;
            };

            /** **********************************Private Method and Business Login End******************************* */
            return {
                startSessionTimer: _startSessionTimer,
                stopSessionTimer: _stopSessionTimer,
                isSessionMonitored: _isSessionMonitored,
                logOut: _logOut,
                logoutMethodCall : _logoutMethodCall,
                sessionTimelogOut: _sessionTimelogOut
            };
        }]);

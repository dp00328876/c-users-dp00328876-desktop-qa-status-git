/* File - CourierService.js
 * Author - PB0083784
 * Date - 10/30/2015
 * Description - Moniker service file to read, search and reserve Monikers.
 *
 */

'use strict';

var courier = angular.module('ctl.courier.service', []);


courier.constant('courierURIs',
	{
		uris: {

			readCourierUri: "http://64.15.188.20/courier/v1/serviceprofile/:phoneNumber",
			readCourierStubUri: "stubs/moniker/updateCourierSuccess.json",

			createCourierUri: "http://64.15.188.20/courier/v1/serviceprofile/:phoneNumber",
			createCourierStubUri: "stubs/moniker/updateCourierSuccess.json",

			updateCourierUri: "http://64.15.188.20/courier/v1/serviceprofile/:phoneNumber/:productName",
			updateCourierStubUri: "stubs/moniker/updateCourierSuccess.json",
			
			deleteCourierUri: "http://64.15.188.20/courier/v1/serviceprofile/:phoneNumber/:productName",
			deleteCourierStubUri: "stubs/moniker/deleteCourierSuccess.json",
			
			sendSMSUri:	"http://64.15.188.20/courier/v1/messages/:fromPhoneNumber",
			sendSMSStubUri: "",
			
			testCourierUri: "http://64.15.188.20/courier/v1/status/:phoneNumber",
			testCourierStubUri : "",
				
			readCountryCallingCodesStubUri:"stubs/courier/countryCallingCodes.json"
			
		}
	});


courier.factory('CourierService',
	[
		'$log',
		'$q',
		'$rootScope',
		'$route',
		'$resource',
		'$http',
		'courierURIs',
		'apiConfig',
		'envConfig',
		'AngularCacheService',
		function(
			$log,
			$q,
			$rootScope,
			$route,
			$resource,
			$http,
			courierURIs,
			apiConfig,
			envConfig,
			AngularCacheService
		) {


			//var _updateCourierService = $resource(courierURIs.uris.getMonikerUri,{}, {getMonikers:{method:"PUT",headers:apiConfig.headers}});
			var _updateCourierStubService = $resource(courierURIs.uris.updateCourierStubUri);

			var _createCourierStubService = $resource(courierURIs.uris.createCourierStubUri);
			var _readCourierStubService = $resource(courierURIs.uris.readCourierStubUri);
			var _deleteCourierStubService = $resource(courierURIs.uris.deleteCourierStubUri);
			var _sendSMSStubService = $resource(courierURIs.uris.sendSMSStubUri);
			var _testCourierStubService = $resource(courierURIs.uris.testCourierStubUri);
			var _readCountryCallingCodesStubService = $resource(courierURIs.uris.readCountryCallingCodesStubUri);
			
			/* place holder to store the reference of SDK's SMS service */
			var _smsService = CTL.Service.SMS;
			
			/* place holder to store the reference of SDK's Product Configuration service */
			var _productConfigurationService = CTL.Service.Product.Configuration;
			
			var _updateCourier = function(inputData) {

				$log.info("CourierService: _updateCourier");
				//PUT /courier/v1/serviceprofile/{phoneNumber}/{productName}
				//Required headers: AccountName, CorrelationID, SeatID, NetworkID
				//Request body {"enabled": true,"messageSaveStore": true}

				var deferred = $q.defer();
				/*var postData = {};
				
				var updateCourierHeaders = {};
				angular.copy(apiConfig.headers, updateCourierHeaders);
				updateCourierHeaders.SeatID = inputData.SeatID;
				updateCourierHeaders.NetworkID = inputData.NetworkID;
				postData.enabled = inputData.enabledService;
				postData.messageSaveStore = inputData.messageSaveStore;*/
				
				/*var _updateCourierService = $resource(courierURIs.uris.updateCourierUri,{},
					{updateCourier:{method:"PUT",headers:updateCourierHeaders, params: {phoneNumber:inputData.phoneNumber, productName:inputData.productName }}});*/

				var success = function(successResponse){
					$log.info("CourierService: _updateCourier: "+ angular.toJson(successResponse));
					deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
				};
				var error = function(errorResponse){
					$log.info("CourierService: _updateCourier: Error"+angular.toJson(errorResponse));
					deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
				};


				if (envConfig.useCourierStub) {
					$log.info("CourierService: _updateCourier Stub  API invoked : ");
					_updateCourierStubService.get({}, success, error);
				} else{
					$log.info("CourierService: _updateCourier API invoked : ");
					_productConfigurationService.reconfigureProduct(inputData).then(success, error);
				}
				return deferred.promise;
			}

			var _createCourier = function(inputData) {

				$log.info("CourierService: _createCourier");
				//POST /courier/v1/serviceprofile/{phoneNumber}
				//Required headers: AccountName, CorrelationID, SeatID, NetworkID, ProductName
				//Request body {"enabled": true,"messageSaveStore": true,"productName": "string"}

				var deferred = $q.defer();
				/*var postData = {};
				var createCourieHeaders = {};
				angular.copy(apiConfig.headers, createCourieHeaders);
				createCourieHeaders.SeatID = inputData.SeatID;
				createCourieHeaders.NetworkID = inputData.NetworkID;
				createCourieHeaders.ProductName = inputData.productName;
				postData.enabled = inputData.enabledService;
				postData.messageSaveStore = inputData.messageSaveStore;
				postData.productName = inputData.productName;*/

				/*var _createCourierService = $resource(courierURIs.uris.createCourierUri,{},
					{createCourier:{method:"POST",headers:createCourieHeaders, params: {phoneNumber:inputData.phoneNumber }}});*/

				var success = function(successResponse){
					$log.info("CourierService: _createCourier: "+ angular.toJson(successResponse));
					deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
				};
				var error = function(errorResponse){
					$log.info("CourierService: _createCourier: Error"+angular.toJson(errorResponse));
					deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
				};


				if (envConfig.useCourierStub) {
					$log.info("CourierService: _createCourier Stub  API invoked : ");
					_createCourierStubService.get({}, success, error);
				} else{
					$log.info("CourierService: _createCourier API invoked : ");
					_productConfigurationService.configureProduct(inputData).then(success, error);
				}
				return deferred.promise;
			}


			var _readCourier = function(inputData) {

				$log.info("CourierService: _readCourier");
				//POST /courier/v1/serviceprofile/{phoneNumber}
				//Required headers: AccountName, CorrelationID, SeatID, NetworkID, ProductName
				//Request body {"enabled": true,"messageSaveStore": true,"productName": "string"}

				var deferred = $q.defer();
				/*var postData = {};*/
				
				/*var _readCourierService = $resource(courierURIs.uris.readCourierUri,{},
					{readCourier:{method:"GET",headers:apiConfig.headers, params: {phoneNumber:inputData.phoneNumber }}});*/

				var success = function(successResponse){
					$log.info("CourierService: _readCourier: "+ angular.toJson(successResponse));
					deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
				};
				var error = function(errorResponse){
					$log.info("CourierService: _readCourier: Error"+angular.toJson(errorResponse));
					deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
				};

				if (envConfig.useCourierStub) {
					$log.info("CourierService: _readCourier Stub  API invoked : ");
					_readCourierStubService.get({}, success, error);
				} else{
					$log.info("CourierService: _readCourier API invoked : ");
					_productConfigurationService.getProductConfig(inputData).then(success, error);
				}
				return deferred.promise;
			}


			var _deleteCourier = function(inputData) {

				$log.info("CourierService: _readCourier");
				//POST /courier/v1/serviceprofile/{phoneNumber}
				//Required headers: AccountName, CorrelationID, SeatID, NetworkID, ProductName
				//Request body {"enabled": true,"messageSaveStore": true,"productName": "string"}

				var deferred = $q.defer();
				/*var postData = {};
				var deleteCourierHeaders = {};
				angular.copy(apiConfig.headers, deleteCourierHeaders);
				deleteCourierHeaders.seatID = inputData.SeatID;
				deleteCourierHeaders.networkID = inputData.NetworkID;
				deleteCourierHeaders.productName = inputData.productName;*/

				/*var _deleteCourierService = $resource(courierURIs.uris.deleteCourierUri,{},
					{deleteCourier:{method:"DELETE",headers:deleteCourierHeaders, params: {phoneNumber:inputData.phoneNumber, productName:inputData.productName}}});*/

				var success = function(successResponse){
					$log.info("CourierService: _deleteCourier: "+ angular.toJson(successResponse));
					deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
				};
				var error = function(errorResponse){
					$log.info("CourierService: _deleteCourier: Error"+angular.toJson(errorResponse));
					deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
				};


				if (envConfig.useCourierStub) {
					$log.info("CourierService: _deleteCourier Stub  API invoked : ");
					_deleteCourierStubService.get({}, success, error);
				} else{
					$log.info("CourierService: _deleteCourier API invoked : ");
					_productConfigurationService.removeProductConfig(inputData).then(success, error);
				}
				return deferred.promise;
			}
			
			var _sendSMS = function(smsData) {

				$log.info("CourierService: _createCourier");
				//POST /courier/v1/serviceprofile/{phoneNumber}
				//Required headers: AccountName, CorrelationID, SeatID, NetworkID, ProductName
				//Request body {"enabled": true,"messageSaveStore": true,"productName": "string"}

				var deferred = $q.defer();
				var postData = {};
				/*var sendSMSHeaders = {};
				angular.copy(apiConfig.headers, sendSMSHeaders);
				sendSMSHeaders.TransactionID = smsData.headers.TransactionID;
				sendSMSHeaders.SeatID = smsData.headers.SeatID;*/
				postData.phoneNumber = smsData.fromPhoneNumber;
				postData.messageText = smsData.messageText;
				postData.to = smsData.to;
				postData.SeatID = smsData.headers.SeatID;

				/*var _sendSMSService = $resource(courierURIs.uris.sendSMSUri,{},
					{sendSMS:{method:"POST",headers:sendSMSHeaders, params: {fromPhoneNumber:smsData.fromPhoneNumber }}});*/

				var success = function(successResponse){
					$log.info("CourierService: _sendSMS: "+ angular.toJson(successResponse));
					deferred.resolve(/*angular.isUndefined(successResponse.data) ?*/ successResponse /*: successResponse.data*/);
				};
				var error = function(errorResponse){
					$log.info("CourierService: _sendSMS: Error"+angular.toJson(errorResponse));
					deferred.reject(/*angular.isUndefined(errorResponse.data) ?*/ errorResponse /*: errorResponse.data*/);
				};


				if (envConfig.useCourierStub) {
					$log.info("CourierService: _sendSMS Stub  API invoked : ");
					_sendSMSStubService.get({}, success, error);
				} else{
					$log.info("CourierService: _sendSMS API invoked : ");
					_smsService.sendMessage(postData).then(success, error);
				}
				return deferred.promise;
			}

			var _testCourier = function(testData) {

				$log.info("CourierService: _testCourier");
				//POST /courier/v1/serviceprofile/{phoneNumber}
				//Required headers: AccountName, CorrelationID, SeatID, NetworkID, ProductName
				//Request body {"enabled": true,"messageSaveStore": true,"productName": "string"}

				var deferred = $q.defer();
				var postData = {};
				//apiConfig.headers.TransactionID = testData.headers.TransactionID;
				postData.phoneNumber = testData.phoneNumber;

				/*var _testCourierService = $resource(courierURIs.uris.testCourierUri,{},
					{testCourier:{method:"GET",headers:testData.headers, params: {phoneNumber:testData.phoneNumber }}});*/

				var success = function(successResponse){
					$log.info("CourierService: _testCourier: "+ angular.toJson(successResponse));
					deferred.resolve(/*angular.isUndefined(successResponse.data) ?*/ successResponse /*: successResponse.data*/);
				};
				var error = function(errorResponse){
					$log.info("CourierService: _testCourier: Error"+angular.toJson(errorResponse));
					deferred.reject(/*angular.isUndefined(errorResponse.data) ?*/ errorResponse /*: errorResponse.data*/);
				};


				if (envConfig.useCourierStub) {
					$log.info("CourierService: _testCourier Stub  API invoked : ");
					_testCourierStubService.get({}, success, error);
				} else{
					$log.info("CourierService: _testCourier API invoked : ");
					_smsService.getStatus(postData).then(success, error);
				}
				return deferred.promise;
			}
			
			var _readCountryCallingCodes = function() {
				var deferred = $q.defer();
				var success = function(successResponse){
					$log.info("CourierService: "+ successResponse);
					deferred.resolve(successResponse);
				};
				var error = function(errorResponse){
					$log.info("CourierService: Error"+errorResponse);
					deferred.reject(errorResponse);
				};
					
				$log.info("CourierService: readCountryCallingCodes Stub  API invoked : ");
				_readCountryCallingCodesStubService.get({}, success, error);
				
				return deferred.promise;
			}
			
			var _getBillingRates = function() {

				$log.info("CourierService: _getBillingRates");
				
				var deferred = $q.defer();
				
				var success = function(successResponse){
					$log.info("CourierService: _getBillingRates: "+ angular.toJson(successResponse));
					deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
				};
				var error = function(errorResponse){
					$log.info("CourierService: _testCourier: Error"+angular.toJson(errorResponse));
					deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
				};
				
				$log.info("CourierService: _getBillingRates API invoked : ");
				_smsService.getBillingRates().then(success, error);
				
				return deferred.promise;
			}
			
			
			return {
				createCourier : _createCourier,
				readCourier : _readCourier,
				updateCourier : _updateCourier,
				deleteCourier : _deleteCourier,
				sendSMS : _sendSMS,
				getCourierServiceStatus : _testCourier,
				readCountryCallingCodes : _readCountryCallingCodes,
				getBillingRates : _getBillingRates
			};

		}
	]);

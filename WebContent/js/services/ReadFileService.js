/* File - ReadFileService.js
 * Description - Read JSON File.
 *
 */

'use strict';

var readFile = angular.module('ctl.readfile.service', []);


readFile.constant('fileURIs',
	{
		uris: {

			reportingJSONStubUri: "data/config/reporting.json",
			apiDocsJSONSrUri: "data/swagger/"
			
		}
	});


readFile.factory('ReadFileService',
	[
		'$log',
		'$q',
		'$resource',
		'fileURIs',
		'AngularCacheService',
		function(
			$log,
			$q,
			$resource,
			fileURIs,
			AngularCacheService
		) {

			var _readReportingJSONStubService = $resource(fileURIs.uris.reportingJSONStubUri);
			
			var reportingDataCache = AngularCacheService.ReportingDataCache;    //Initializing cache service
			
			/**
             * Set cache
             * @param {String} key
             * @param {Object|String} val
             * @returns {Boolean}
             */
            var _setReportingDataCache=function(key,val) {
                if(angular.isDefined(key) && angular.isDefined(val) && key!=="") {
                	reportingDataCache.put(key,val);
                    return(true);
                }
                return(false);
            };

            /**
             * Getting Cache data
             * @param {String} key
             * @returns {Boolean|Object|String}
             */
            var _getReportingDataCache=function(key) {
                return(reportingDataCache.get(key) || false);
            };
            
			/*
			 * Read Reporting JSON
			 */
			var _loadReportingJSON = function() {
				var deferred = $q.defer();
				var success = function(successResponse){
					$log.info("ReadFileService: "+ angular.toJson(successResponse));
					_setReportingDataCache('reportingConfig', successResponse);
					deferred.resolve(successResponse);
				};
				var error = function(errorResponse){
					$log.info("ReadFileService: Error"+errorResponse);
					deferred.reject(errorResponse);
				};
					
				$log.info("ReadFileService: _readReportingJSONStubService Stub  API invoked : ");
				_readReportingJSONStubService.get({}, success, error);
				
				return deferred.promise;
			};
			
			var _loadApiDocsJSON = function(apiJsonName) {
				var deferred = $q.defer();
				var success = function(successResponse){

					deferred.resolve(successResponse);
				};
				var error = function(errorResponse){
					deferred.reject(errorResponse);
				};
					
				$log.info("ReadFileService: _readApiDocsJSONStubService Stub  API invoked : ");
				$resource(fileURIs.uris.apiDocsJSONSrUri + apiJsonName).get({}, success, error);				
				
				return deferred.promise;
			};
			
			return {
				loadReportingJSON : _loadReportingJSON,
				getReportingDataCache : _getReportingDataCache,
				loadApiDocsJSON : _loadApiDocsJSON
			};

		}
	]);

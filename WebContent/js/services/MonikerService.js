/* File - MonikerService.js
 * Author - PB0083784
 * Date - 10/30/2015
 * Description - Moniker service file to read, search and reserve Monikers.
 * 
 */

'use strict';

var moniker = angular.module('ctl.moniker.service', []);

moniker.constant('monikerURIs',
	{
		uris: {
			stateStubUri: "stubs/moniker/usStates.json",
			cityStubUri: "stubs/moniker/cities/:state",
			searchMonikerStubUri: "stubs/moniker/searchMonikerSuccess.json",
			reserveMonikerStubUri: "stubs/moniker/reserveMonikerSuccess.json",
			getMonikerStubUri: "stubs/moniker/getMonikerSuccess.json"
		}
	});

moniker.factory('MonikerService',
	[
		'$log',
		'$q',
		'$rootScope',
		'$route',
		'$resource',
		'$http',
		'monikerURIs',
		'apiConfig',
		'envConfig',
		'AngularCacheService',
		function(
			$log,
			$q,
			$rootScope,
			$route,
			$resource,
			$http,
			monikerURIs,
			apiConfig,
			envConfig,
			AngularCacheService
		) {

			var _productConfigServices = CTL.Service.Product.Configuration;

			//$rootScope.reservedMoniker ="";
			var _cityAndStateService = $resource(monikerURIs.uris.stateAndCityUri,{}, {getStateAndCity:{method:"GET",headers:""}});
			var _stateStubService = $resource(monikerURIs.uris.stateStubUri);
			var _cityStubService = $resource(monikerURIs.uris.cityStubUri);
			var _searchMonikerStubService = $resource(monikerURIs.uris.searchMonikerStubUri);
			var _reserveMonikerStubService = $resource(monikerURIs.uris.reserveMonikerStubUri);
			var _getAllocatedMonikerStubService = $resource(monikerURIs.uris.getMonikerStubUri);
			var monikerDataCache = AngularCacheService.MonikerDataCache;    //Initializing cache service
			/**
			 * Set cache
			 * @param {String} key
			 * @param {Object|String} val
			 * @returns {Boolean}
			 */
			var _setMonikerDataInCache=function(key,val) {
				if(angular.isDefined(key) && angular.isDefined(val) && key!=="") {
					monikerDataCache.put(key,val);
					return(true);
				}
				return(false);
			};

			/**
			 * Getting Cache data
			 * @param {String} key
			 * @returns {Boolean|Object|String}
			 */
			var _getMonikerDataFromCache=function(key) {
				return(monikerDataCache.get(key) || false);
			};

			var _searchMoniker = function(inputData) {

				$log.info("MonikerService: _searchMoniker: State: "+ inputData.state + ", City: "+inputData.city + ",NPA:" + inputData.NPA + ",NXX:" + inputData.NXX);
				var deferred = $q.defer();
				var postData = {};
				if (postData != undefined && 'state' in inputData) {
					postData.state = inputData.state;
				}
				if (postData != undefined && 'city' in inputData) {
					postData.city = inputData.city;
				}
				if (postData != undefined && 'NPA' in inputData) {
					postData.npa = inputData.NPA;
				}
				if (postData != undefined && 'NPA' in inputData && 'NXX' in inputData) {
					postData.npanxx = inputData.NPA+inputData.NXX;
				}
				if (postData != undefined && 'limit' in inputData) {
					postData.limit = inputData.limit;
				}
				var success = function(successResponse){
					$log.info("MonikerService _searchMoniker: "+ angular.toJson(successResponse));
					deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
				};
				var error = function(errorResponse){
					$log.info("MonikerService _searchMoniker: Error"+errorResponse);
					deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
				};
				if (envConfig.useMonikerStub) {
					$log.info("MonikerService: _reserveMoniker Stub  API invoked : ");
					_searchMonikerStubService.get({}, success, error);
				} else{
					$log.info("MonikerService: SDK _getAvailablePhoneNumber API invoked : ");
					_productConfigServices.getAvailablePhoneNumber(postData).then(success, error);
				}
				return deferred.promise;
	    	}
	    
	    	var _reserveMoniker = function(inputData) {
				$log.info("MonikerService: _reserveMoniker: Moniker: "+inputData.moniker );
				var deferred = $q.defer();
				var postData ={};
				postData.phoneNumber = inputData.moniker;
				postData.seatId = inputData.seatId;
				var success = function(successResponse){
					$log.info("MonikerService: _reserveMoniker: Success"+ angular.toJson(successResponse));            	                
					deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
				};
				var error = function(errorResponse){
					$log.info("MonikerService: _reserveMoniker: Error"+angular.toJson(errorResponse));
					deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);              
				};
				if (envConfig.useMonikerStub) {
					$log.info("MonikerService: _reserveMoniker Stub  API invoked : ");
					_getAllocatedMonikerStubService.get({}, success, error);
				} else{
					$log.info("MonikerService: SDK _reserveMoniker API invoked : ");
					_productConfigServices.assignPhoneNumber(postData).then(success, error);
				}
			return deferred.promise;
		}
	  
		var _getAllocatedMonikers = function(inputData) {
			$log.info("MonikerService: _getAllocatedMonikers: Moniker: ");
			var deferred = $q.defer();
			var postData = {};
			var success = function(successResponse){
				$log.info("MonikerService: _getAllocatedMonikers: Success"+ angular.toJson(successResponse));
				if(angular.isDefined(successResponse.data)){
					_setMonikerDataInCache("monikersData", successResponse.data);	// Put Allocated Monikers in cache  
				}               	                
				deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
			};
			var error = function(errorResponse){
				$log.info("MonikerService: _getAllocatedMonikers: Error"+angular.toJson(errorResponse));
				deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);              
			};
			if (envConfig.useMonikerStub) {
				$log.info("MonikerService: _getAllocatedMonikers Stub  API invoked : ");
				_getAllocatedMonikerStubService.get({}, success, error);
            } else{
				$log.info("MonikerService: SDK _getAllocatedMonikers API invoked : ");
				_productConfigServices.getAssignedPhoneNumber(postData).then(success, error);
            }
			return deferred.promise;
	    }

		var _readState = function() {
			var deferred = $q.defer();
			var success = function(successResponse){
				$log.info("MonikerService: "+ successResponse);
				deferred.resolve(successResponse);
			};
			var error = function(errorResponse){
				$log.info("MonikerService: Error"+errorResponse);
				deferred.reject(errorResponse);
			};
			if (true) {
				$log.info("MonikerService: _readState Stub  API invoked : ");
				_stateStubService.get({}, success, error);
			} else{
				$log.info("MonikerService: _readState API invoked : ");
				_cityAndStateService.getStateAndCity(postData, success, error);
			}
			return deferred.promise;
		}
		
		var _readCity = function(stateName) {
			var deferred = $q.defer();
			var success = function(successResponse){
				$log.info("MonikerService: "+ successResponse);
				deferred.resolve(successResponse);
			};
			var error = function(errorResponse){
				$log.info("MonikerService: Error"+errorResponse);
				deferred.reject(errorResponse);
			};
			if (true) {
				$log.info("MonikerService: _readCity Stub  API invoked : ");
				_cityStubService.get({state:(stateName + '.json')}, success, error);
			} else{
				$log.info("MonikerService: _readCity API invoked : ");
				_cityAndStateService.getStateAndCity(postData, success, error);
			}
			return deferred.promise;
		}

	    return {
			searchMoniker : _searchMoniker,
            reserveMoniker : _reserveMoniker,
			getAllocatedMonikers : _getAllocatedMonikers,
			readState : _readState,
			readCity : _readCity,
			getMonikerDataFromCache: _getMonikerDataFromCache,
			setMonikerDataInCache : _setMonikerDataInCache
        };
	
	}
]);
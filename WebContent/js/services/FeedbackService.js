'use strict';

var feedbackService = angular.module('ctl.feedback.service', []);

feedbackService.factory('FeedbackService',
	[
		'$log',
		'$q',
		'$rootScope',
		'$route',
		'$resource',
		'$http',
		'apiConfig',
		'envConfig',
		'ctlUtils',
		function(
			$log,
			$q,
			$rootScope,
			$route,
			$resource,
			$http,
			apiConfig,
			envConfig,
			ctlUtils
		) {

			var _feebackService = CTL.Service.Footer;
			
			var _submitFeedback = function(inputData) {
				
				$log.info("FeedbackService: _submitFeedback");				
				var deferred = $q.defer();
				
				var success = function(successResponse){
					$log.info("FeedbackService: _submitFeedback: Success" + angular.toJson(successResponse));
					deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
				};
				var error = function(errorResponse){
					$log.info("FeedbackService: _submitFeedback: Error"+angular.toJson(errorResponse));
					deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
				};
				if (envConfig.useFeedbackStub) {
					$log.info("FeedbackService: _submitFeedback Stub  API invoked : ");
					_submitFeedbackStubService.post({}, success, error);
				} else{
					$log.info("FeedbackService: _submitFeedback API invoked : ");
					_feebackService.submitFeedback(inputData).then(success, error);
				}
				return deferred.promise;
			};
			
			return {
				submitFeedback : _submitFeedback
			};

		}
	]);

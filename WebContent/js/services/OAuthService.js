/**
 * Created by vk0014941 on 11/8/2015.
 */

'use strict';

var oAuth = angular.module('ctl.oauth.service', [

    ]);

oAuth.constant('oAuthURIs',
    {
        uris: {
            signInUri: "http://auth.veuxdu.centurylink.com/token",
            signInStubUri: "stubs/login/oAuthSuccess.json",
            
            createUserUri: "http://auth.veuxdu.centurylink.com/userCredentials",
        	createUserStubUri: "stubs/login/userCreateSuccess.json",

            readUserUri: "http://auth.veuxdu.centurylink.com/userCredentials/:userName",
            readUserStubUri: "stubs/login/readUserSuccess.json",
            
            updateUserUri: "http://auth.veuxdu.centurylink.com/userCredentials/:userName",
            updateUserStubUri: "stubs/login/userUpdateSuccess.json",
            
            deleteUserUri: "http://auth.veuxdu.centurylink.com/userCredentials/:userName",
            deleteUserStubUri: "stubs/login/userDeleteSuccess.json",

            menusUri: "",
            menuStubUri: "data/content/MenuItem_English.json",
            
            getNetworkIdListStubUri: "stubs/login/getNetworkListSuccess_Chaitra.json",
            getUserByNetworkIdStubUri:"stubs/login/getUserByNetworkIdSuccess_ntuser001.json",
            getPendingUserStubUri:"stubs/login/getPendingUsers.json"
        }
    });

oAuth.factory('OAuthServices',
    [
        '$log',
        '$q',
        '$rootScope',
        '$route',
        '$resource',
        'envConfig',
        'oAuthURIs',
        'AngularCacheService',
        'apiConfig',
        'SessionKeeper',
        'ContentService',
        'ctlUtils',
        function (
            $log,
            $q,
            $rootScope,
            $route,
            $resource,
            envConfig,
            oAuthURIs,
            AngularCacheService,
            apiConfig,
            SessionKeeper,
            ContentService,
            ctlUtils
        ) {

            var accountDataCache = AngularCacheService.AccountDataCache;    //Initializing cache service

            /**
             * Set cache
             * @param {String} key
             * @param {Object|String} val
             * @returns {Boolean}
             */
            var _setAccountDataInCache=function(key,val) {
                if(angular.isDefined(key) && angular.isDefined(val) && key!=="") {
                    accountDataCache.put(key,val);
                    return(true);
                }
                return(false);
            };

            /**
             * Getting Cache data
             * @param {String} key
             * @returns {Boolean|Object|String}
             */
            var _getAccountDataFromCache=function(key) {
                return(accountDataCache.get(key) || false);
            };


            /*var _signInService = $resource(oAuthURIs.uris.signInUri,{}, {loginAuth:{method:"POST",headers:apiConfig.tokenHeaders,
                                    transformRequest: function(data, headersGetter){
                                        var str =[];
                                        for (var d in data)
                                        str.push(encodeURIComponent(d)+ "="+ encodeURIComponent(data[d]));
                                        return str.join("&");
                                    }}});*/
            
            var _oAuth2Service = CTL.Service.Oauth2;
            var _signInStubService = $resource(oAuthURIs.uris.signInStubUri);


           // var _createUserService = $resource(oAuthURIs.uris.createUserUri,{}, {createUser:{method:"POST",headers:apiConfig.tokenHeaders}});
            var _createUserStubService = $resource(oAuthURIs.uris.createUserStubUri);

            // var _readUserService = $resource(oAuthURIs.uris.readUserUri,{}, {readUser:{method:"GET",headers:apiConfig.tokenHeaders}});
            var _readUserStubService = $resource(oAuthURIs.uris.readUserStubUri);
            var _getNetworkIdListStubService = $resource(oAuthURIs.uris.getNetworkIdListStubUri);
            var _getUserByNetworkIdStubService = $resource(oAuthURIs.uris.getUserByNetworkIdStubUri);
            var _getPendingUserStubService = $resource(oAuthURIs.uris.getPendingUserStubUri);
            

            var _refreshAccessToken = function(){

                $log.info("OAuthService: _refreshAccessToken :");

                var deferred = $q.defer();
                var postData = {};

                var success = function(successResponse){
                    $log.info("AuthenticationServices :: _refreshAccessToken - Authentication Response Received : SUCCESS");
                    deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : _updateTokenResponse(successResponse.data));
                };
                var error = function(errorResponse){
                    $log.error("AuthenticationServices :: _refreshAccessToken - Authentication Error Response Received :-"+JSON.stringify(loginErrorResponse));
                    deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
                };

                if (envConfig.useLoginStub) {
                    $log.info("OAuthService: _refreshAccessToken Stub  OAuth API invoked");
                    _signInStubService.get({}, success, error);
                } else{
                    $log.info("OAuthService: SDK _refreshAccessToken OAuth API invoked ");
                    _oAuth2Service.refreshToken(postData).then(success, error);
                }
                              
                return deferred.promise;
            }


            var _updateTokenResponse = function(loginResponse){
                var authResponse = _getAccountDataFromCache("accountInfoData");
                authResponse.refreshToken = loginResponse.refresh_token;
                authResponse.accessToken = loginResponse.access_token;
                _setAccountDataInCache("accountInfoData", authResponse);
            }
            /***
             *
             * @param userId
             * @param password
             *  OAuth Service call to authenticate the user
             *  return the promise.
             */
            var _authenticateUser = function(userDetails){
                $log.info("OAuthService: _authenticateUser UserId: "+userDetails.userId);
                
                var deferred = $q.defer();

                var postData = {};
                postData.username = userDetails.userId;
                postData.password = userDetails.password;
                
                var success = function(successResponse){
                    $log.info("OAuthService: _authenticateUser: "+ angular.toJson(successResponse));
                    deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : _populateTokenResponse(successResponse.data,userDetails.userId));
	             };
	             var error = function(errorResponse){
	                    $log.info("OAuthService: _authenticateUser: Error"+angular.toJson(errorResponse));
	                    deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
	             };
	
	
	             if (envConfig.useLoginStub) {
	                    $log.info("OAuthService: _authenticateUser Stub  OAuth API invoked UserId: "+userDetails.userId);
	                    _signInStubService.get({}, success, error);
	             } else{
	                    $log.info("OAuthService: SDK login() OAuth API invoked UserId: "+userDetails.userId);
	                    _oAuth2Service.login(userDetails.userId, userDetails.password).then(success, error);
	             }

                return deferred.promise;
            };


            /***
             *
             * @param loginResponse
             * @param userId
             * @private
             */
            var _populateTokenResponse = function (loginResponse, userId){
                $log.info("OAuthService: _populateTokenResponse UserId: "+userId);
                $log.info("OAuthService: _populateTokenResponse Response: "+angular.toJson(loginResponse));
                var userTokenResponse = {};
                userTokenResponse.userName = userId;
                userTokenResponse.accessToken = loginResponse.access_token;
                userTokenResponse.tokenType = loginResponse.token_type;
                userTokenResponse.expiresIn = loginResponse.expires_in;
                userTokenResponse.refreshToken = loginResponse.refresh_token;
                userTokenResponse.accountName = loginResponse.account_name;
                userTokenResponse.network_id = loginResponse.network_id;
                var jwtPayloadJson = _decodeJWTandReturnPayload(loginResponse.jwt_token);
                userTokenResponse.issuer = jwtPayloadJson.issuer;
                userTokenResponse.subject = jwtPayloadJson.subject;
                userTokenResponse.audience = jwtPayloadJson.audience;
                //userTokenResponse.accountName = jwtPayloadJson.audience;
                
                userTokenResponse.expirationTime = jwtPayloadJson.expirationTime;
                userTokenResponse.issuedAt = jwtPayloadJson.issuedAt;
                userTokenResponse.jwdId = jwtPayloadJson.jwdId;


                if(angular.isUndefined(userTokenResponse.accountName) || userTokenResponse.accountName ==''){
                	userTokenResponse.accountName = userId;
                }
                var current = SessionKeeper.read();
                current.accessToken = userTokenResponse.accessToken;
                SessionKeeper.save();
                // GA Reporting Setting Common Parameters
				ga('set', 'userId', userTokenResponse.userName);	// User ID
				//ga('set', 'language', 'en-us');	// Language
				ga('set', 'dimension1', userTokenResponse.accountName);	// Account NAme
				if(angular.isDefined(userTokenResponse.network_id)){
					ga('set', 'dimension2', userTokenResponse.network_id);	// Network ID
				}				
                return userTokenResponse;
            }

            var _getOrderManagementPermissions = function(){
            	var permissionArray = _getAccountDataFromCache("accountInfoData");
            	$log.info("_getOrderManagementPermissions: permissionArray : "+angular.toJson(permissionArray.API.userCredentials));
            	return permissionArray.API.userCredentials;
            }


            var _decodeJWTandReturnPayload = function (jwt_token) {
            	try{
            		return ctlUtils.decodeJWT(jwt_token, 1);
            	}catch (e){
            		$log.error('Error in decoding JWT Token');
            	}
            	
                
            };

            var _setMenuDisplay = function(userTokenResponse){

            	var deferred = $q.defer();
                var menuItemsInfo ={};

                var success = function(successData) {
                    $log.debug("Fetch Menu: "+angular.toJson(successData));
                    menuItemsInfo.menuData = successData.MenuItem.Menus;
                   menuItemsInfo.displayName="" ;
                   if((angular.isDefined(userTokenResponse.firstName) && userTokenResponse.firstName !== '')  || (angular.isDefined(userTokenResponse.lastName) && userTokenResponse.lastName !== '')){
                	   menuItemsInfo.displayName=(angular.isDefined(userTokenResponse.firstName) ? userTokenResponse.firstName : '') + " " + (angular.isDefined(userTokenResponse.lastName) ? userTokenResponse.lastName : '');
                   }else if(angular.isDefined(userTokenResponse.username)){
                	   menuItemsInfo.displayName= (userTokenResponse.username.substr((userTokenResponse.username.length - '@consumer'.length), userTokenResponse.username.length) === '@consumer') 
                	   								? userTokenResponse.username.substr(0, (userTokenResponse.username.length - '@consumer'.length)) 
                	   								: userTokenResponse.username.substr(0, (userTokenResponse.username.length - '@admin'.length));
                   }
                    $log.debug("Menus: "+angular.toJson(menuItemsInfo.menuData));
                   
                    menuItemsInfo.showAcctSummary = true;	// OrderManagement GET                   
                    menuItemsInfo.showUsageStats = true;	// Courier GET                    	
                    menuItemsInfo.showProducts = true;	// Display To ALL                    
                    menuItemsInfo.showManageNwId = true;	//ToDo  Based on OAUTH - Check what needs to be done here
                    _setAccountDataInCache("menuItemsInfo", menuItemsInfo);
                    var current = SessionKeeper.read();
                    current.menuItemsInfo = menuItemsInfo;
                    SessionKeeper.save();
                    deferred.resolve(menuItemsInfo);
                };
                var error = function(errorData) {
                    menuItemsInfo.userName = userTokenResponse.audience;
                    
                    menuItemsInfo.showAcctSummary = true;	// OrderManagement GET                   
                    menuItemsInfo.showUsageStats = true;	// Courier GET                    
                    menuItemsInfo.showProducts = true;	// Display To ALL                    
                    menuItemsInfo.showManageNwId = true;	//ToDo  Based on OAUTH - Check what needs to be done here

                    var current = SessionKeeper.read();
                    current.menuItemsInfo = menuItemsInfo;
                    SessionKeeper.save();
                    deferred.resolve(menuItemsInfo);
                };
                var menuData = ContentService.fetchContent("MenuItem");
                menuData.then(success, error);
                return deferred.promise;
            }


            var _createUser = function(inputData) {

				$log.info("OAuthService: _createUser" +inputData.username +":"+ inputData.password);				
				var deferred = $q.defer();
				var postData = {};
				postData.username = inputData.username;
				postData.password = inputData.password;
				postData.firstName = inputData.firstName;
				postData.lastName = inputData.lastName;
				postData.emailAddress = inputData.emailAddress;
				postData.networkId = inputData.networkId;
				postData.userTypes = inputData.userTypes;
				
				var success = function(successResponse){
					$log.info("OAuthService: _createUser: Success"+ angular.toJson(successResponse));
					deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
				};
				var error = function(errorResponse){
					$log.info("OAuthService: _createUser: Error"+angular.toJson(errorResponse));
					deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);					
				};
				
				if (envConfig.useLoginStub) {
					$log.info("OAuthService: _createUser Stub  API invoked : ");
					_createUserStubService.get({}, success, error);
				} else{
					$log.info("OAuthService: _createUser API invoked : ");
					_oAuth2Service.createUser(postData).then(success, error);
				}
				
				return deferred.promise;
			};

            var _readUser = function(inputData, updateCache) {

                $log.info("OAuthService: _readUser " +inputData.userName );
                var deferred = $q.defer();
                var postData = {};
                postData.userName = inputData.userName;
                updateCache = angular.isDefined(updateCache) ? updateCache : true;
                postData.updateCache = updateCache; 
                
                var success = function(successResponse){
                    $log.info("OAuthService: _readUser: Success"+ angular.toJson(successResponse));
                    // Setting AccountData in Cache
                    if(angular.isDefined(successResponse.data) && updateCache){
                    	inputData.groups = /*[successResponse.data.userTypes]*/ inputData.userName.endsWith("@consumer") ? ['network'] : ['platform'];
                        inputData.firstName = successResponse.data.firstName;
                        inputData.lastName = successResponse.data.lastName;
                        inputData.emailAddress = successResponse.data.emailAddress;
                        inputData.securityQuestions = successResponse.data.securityQuestions;
                        if(angular.isDefined(successResponse.data.networkIds)) inputData.network_id = successResponse.data.networkIds[0];
                        //$log.info("UserTokenResponse : "+angular.toJson(inputData));
                        _setAccountDataInCache("accountInfoData", inputData);
                    }                   
                    deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
                };
                var error = function(errorResponse){
                    $log.info("OAuthService: _readUser: Error"+angular.toJson(errorResponse));
                    //deferred.reject(errorResponse);
                    //inputData.userType = 'platform';
                    //inputData.groups = ['network'];
                    //_setAccountDataInCache("accountInfoData", inputData);

                    deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);                
                };

                if (envConfig.useLoginStub) {
                    $log.info("OAuthService: _readUser Stub  API invoked : ");
                    _readUserStubService.get({}, success, error);
                } else{
                    $log.info("OAuthService: _readUser API invoked : ");
                    _oAuth2Service.getUser(postData).then(success, error);
                }
                
                
                return deferred.promise;
            };
            
            var _updateUser = function(inputData) {

                $log.info("OAuthService: _updateUser " +angular.toJson(inputData));
                var deferred = $q.defer();
                var postData = {};
                postData.userName = inputData.userName;
                postData.oldPassword = inputData.oldPassword;
                postData.password = inputData.password;
                postData.firstName = inputData.firstName;
                postData.lastName = inputData.lastName;
                postData.emailAddress = inputData.email;
                postData.securityQuestions = inputData.securityQuestions;
                var success = function(successResponse){
                    $log.info("OAuthService: _updateUser: Success"+ angular.toJson(successResponse));
                    /*// Setting AccountData in Cache
                    inputData.userType = successResponse.userType;
                    $log.info("UserTokenResponse : "+angular.toJson(inputData));*/
                    
                    //update cache
                    var updateAccountInfoData = _getAccountDataFromCache("accountInfoData");
                    updateAccountInfoData.firstName = inputData.firstName;
                    updateAccountInfoData.lastName = inputData.lastName;
                    updateAccountInfoData.emailAddress = inputData.email;
                    _setAccountDataInCache("accountInfoData", updateAccountInfoData);
                    deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
                };
                var error = function(errorResponse){
                    $log.info("OAuthService: _updateUser: Error"+angular.toJson(errorResponse));
                    deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);        
                };

                if (envConfig.useLoginStub) {
                    $log.info("OAuthService: _updateUser Stub  API invoked : ");
                    _updateUserStubService.get({}, success, error);
                } else{
                    $log.info("OAuthService: SDK _updateUser API invoked : ");
                    _oAuth2Service.updateUser(postData).then(success, error);
                }
               
                return deferred.promise;
            };
            
            var _deleteUser = function(inputData) {
                $log.info("OAuthService: _deleteUser " +inputData.username);
                var deferred = $q.defer();
                var postData = {};
                postData.userName = inputData.username;

                var success = function(successResponse){
                    $log.info("OAuthService: _deleteUser: Success"+ angular.toJson(successResponse));                 
                    deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
                };
                var error = function(errorResponse){
                    $log.info("OAuthService: _deleteUser: Error"+angular.toJson(errorResponse));
                    deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);       
                };

                if (envConfig.useLoginStub) {
                    $log.info("OAuthService: _deleteUser Stub  API invoked : ");
                    _deleteUserStubService.get({}, success, error);
                } else{
                    $log.info("OAuthService: _deleteUser API invoked : ");
                    _oAuth2Service.deleteUser(postData).then(success, error);
                }
                
                return deferred.promise;
            };
            
            var _createNetworkUser = function(inputData) {
				$log.info("OAuthService: _createNetworkUser method invoked");	
				var deferred = $q.defer();
				var postData = {};
				postData.username = inputData.username;
				postData.emailAddress = inputData.emailAddress;
				postData.networkId = inputData.networkId;
				postData.customerLabel = inputData.customerLabel;
				postData.seatId = inputData.seatId;
				//postData.overrideMimeType = false;
				
				var success = function(successResponse){
					$log.info("OAuthService: _createNetworkUser: Success"+ angular.toJson(successResponse));
					deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
				};
				var error = function(errorResponse){
					$log.info("OAuthService: _createNetworkUser: Error"+angular.toJson(errorResponse));
					deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);					
				};
				
				if (envConfig.useLoginStub) {
					$log.info("OAuthService: _createNetworkUser Stub  API invoked : ");
					_createUserStubService.get({}, success, error);
				} else{
					$log.info("OAuthService: _createNetworkUser API invoked : ");
					_oAuth2Service.inviteNetworkUser(postData).then(success, error);
				}
				
				return deferred.promise;
			};
			
			var _confirmNetworkUser = function(inputData) {
				$log.info("OAuthService: _createNetworkUser method invoked");	
				var deferred = $q.defer();
				var postData = {};
				postData.accessToken = inputData.accessToken;
				postData.seatId = inputData.seatId;
				postData.networkId = inputData.networkId;
                postData.userName = inputData.userName;
                postData.password = inputData.password;
                postData.firstName = inputData.firstName;
                postData.lastName = inputData.lastName;
                postData.emailAddress = inputData.emailAddress;
                postData.securityQuestions = inputData.securityQuestions;
                $log.info("OAuthService: _createNetworkUser PostData"+ angular.toJson(postData));
				var success = function(successResponse){
					$log.info("OAuthService: _createNetworkUser: Success"+ angular.toJson(successResponse));
					deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
				};
				var error = function(errorResponse){
					$log.info("OAuthService: _createNetworkUser: Error"+angular.toJson(errorResponse));
					deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);					
				};
				
				if (envConfig.useLoginStub) {
					$log.info("OAuthService: _createNetworkUser Stub  API invoked : ");
					_createUserStubService.get({}, success, error);
				} else{
					$log.info("OAuthService: _createNetworkUser API invoked : ");
					_oAuth2Service.confirmNetworkUser(postData).then(success, error);
				}
				
				return deferred.promise;
			};
			
			var _getPendingInvitedUsers = function(inputData) {
				$log.info("OAuthService: _updateInvitedUser method invoked");	
				var deferred = $q.defer();
				var postData = {};
				postData.domain = inputData.domain;
				var success = function(successResponse){
					$log.info("OAuthService: _getPendingInvitedUsers: Success"+ angular.toJson(successResponse));
					deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
				};
				var error = function(errorResponse){
					$log.info("OAuthService: _getPendingInvitedUsers: Error"+angular.toJson(errorResponse));
					deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);					
				};
				
				if (envConfig.useLoginStub) {
					$log.info("OAuthService: _getPendingInvitedUsers Stub  API invoked : ");
					_getPendingUserStubService.get({}, success, error);
				} else{
					$log.info("OAuthService: _getPendingInvitedUsers API invoked : ");
					_oAuth2Service.getInvitedUsers(postData).then(success, error);
				}
				
				return deferred.promise;
			};
			
			var _updateEmailResendInvite = function(inputData) {
				$log.info("OAuthService: _updateEmailResendInvite method invoked");	
				var deferred = $q.defer();
				var postData = {};
				postData.username = inputData.username;
				postData.accountName = inputData.accountName;
				postData.platformUsername = inputData.platformUsername;
				postData.emailAddress = inputData.emailAddress;
				//postData.networkId = inputData.networkId;
				//postData.customerLabel = inputData.customerLabel;
				//postData.seatId = inputData.seatId;
				//postData.overrideMimeType = false;
				
				var success = function(successResponse){
					$log.info("OAuthService: _updateEmailResendInvite: Success"+ angular.toJson(successResponse));
					deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
				};
				var error = function(errorResponse){
					$log.info("OAuthService: _updateEmailResendInvite: Error"+angular.toJson(errorResponse));
					deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);					
				};
				
				if (envConfig.useLoginStub) {
					$log.info("OAuthService: _updateEmailResendInvite Stub  API invoked : ");
					_createUserStubService.get({}, success, error);
				} else{
					$log.info("OAuthService: _updateEmailResendInvite API invoked : ");
					_oAuth2Service.resendInvitation(postData).then(success, error);
				}
				
				return deferred.promise;
			};
			
            var _getUsersByNetworkId = function(inputData){
            	
           	 $log.info("OAuthService: _getUsersByNetworkId " +inputData.networkId );
                var deferred = $q.defer();
                var postData = {};
                postData.networkId = inputData.networkId;
                var networkId = inputData.networkId;
                var success = function(successResponse){
                    $log.info("OAuthService: _getUsersByNetworkId: 	"+ angular.toJson(successResponse));
                    deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
                };
                var error = function(errorResponse){
                    $log.info("OAuthService: _getUsersByNetworkId: Error"+angular.toJson(errorResponse));
                    deferred.reject(errorResponse);
                };

                if (envConfig.useLoginStub) {
                    $log.info("OAuthService: _getUsersByNetworkId Stub  API invoked : ");
                    _getUserByNetworkIdStubService.get({}, success, error);
                } else{
                    $log.info("OAuthService: _getUsersByNetworkId API invoked : ");
                    _oAuth2Service.getUsersByNetworkId(postData).then(success, error);
                }
                
                
                return deferred.promise;
            }
            
            var _getNetworkIdList = function(){
            	
                 var deferred = $q.defer();
                 var postData = {};
                 var success = function(successResponse){
                     $log.info("OAuthService: _getNetworkIdList: 	"+ angular.toJson(successResponse));
                     
                     deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
                 };
                 var error = function(errorResponse){
                     $log.info("OAuthService: _getNetworkIdList: Error"+angular.toJson(errorResponse));
                     deferred.reject(errorResponse);
                 };

                 if (envConfig.useLoginStub) {
                     $log.info("OAuthService: _getNetworkIdList Stub  API invoked : ");
                     _getNetworkIdListStubService.get({}, success, error);
                 } else{
                     $log.info("OAuthService: _getNetworkIdList API invoked : ");
                     _oAuth2Service.getNetworkIdList(postData).then(success, error);
                 }
                 
                 
                 return deferred.promise;
            }
            
            var _createQuickStartUser = function(inputData){
            	
                var deferred = $q.defer();
                var postData = {};
                postData.seatId = inputData.seatId;
                postData.userName = inputData.userName;
                postData.password = inputData.password;
                postData.customerLabel = inputData.networkId;
                var success = function(successResponse){
                    $log.info("OAuthService: _createQuickStartUser: 	"+ angular.toJson(successResponse));
                    
                    deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
                };
                var error = function(errorResponse){
                    $log.info("OAuthService: _createQuickStartUser: Error"+angular.toJson(errorResponse));
                    deferred.reject(errorResponse);
                };

                if (envConfig.useLoginStub) {
                    $log.info("OAuthService: _createQuickStartUser Stub  API invoked : ");
                    _createQuickStartUserStubService.get({}, success, error);
                } else{
                    $log.info("OAuthService: _createQuickStartUser API invoked : ");
                    _oAuth2Service.createQuickStartUser(postData).then(success, error);
                }
                
                
                return deferred.promise;
           };
           
           var _getAllNetworkIdUsers = function(inputdata){
           	
				$log.info("OAuthService: _getAllNetworkIdUsers ");
				var deferred = $q.defer();
				var success = function(successResponse){
					$log.info("OAuthService: _getAllNetworkIdUsers: 	"+ angular.toJson(successResponse));
					deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
				};
				var error = function(errorResponse){
					$log.info("OAuthService: _getAllNetworkIdUsers: Error "+angular.toJson(errorResponse));
					deferred.reject(errorResponse);
				};
				
				if (envConfig.useLoginStub) {
					$log.info("OAuthService: _getAllNetworkIdUsers Stub  API invoked : ");
					_getUserByNetworkIdStubService.get({}, success, error);
				} else{
					$log.info("OAuthService: _getAllNetworkIdUsers API invoked : ");
					_oAuth2Service.getAllNetworkIdUsers(inputdata).then(success, error);
				}

				return deferred.promise;
           };

           var _subaccountUserListUpdates = function(inputData) {
	           $log.info("OAuthService: _subaccountUserListUpdates " +inputData.username);
	           var deferred = $q.defer();
	           var postData = {};
	           postData.addUsers = inputData.addUsers;
	           postData.removeUsers = inputData.removeUsers;
	           postData.networkId = inputData.networkId;
	               
	           var success = function(successResponse){
	           $log.info("OAuthService: _subaccountUserListUpdates: Success"+ angular.toJson(successResponse));                 
	           deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
	           };
	           var error = function(errorResponse){
	           $log.info("OAuthService: _subaccountUserListUpdates: Error"+angular.toJson(errorResponse));
	           deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);       
	           };
	
	           if (envConfig.useLoginStub) {
	           $log.info("OAuthService: _subaccountUserListUpdates Stub  API invoked : ");
	           _deleteUserStubService.get({}, success, error);
	           } else{
	           $log.info("OAuthService: _subaccountUserListUpdates API invoked : ");
	           _oAuth2Service.subaccountUserListUpdates(postData).then(success, error);
	           }
	               
	           return deferred.promise;
           };
           var _getUserList = function(){
           	
               var deferred = $q.defer();
               var postData = {};
               var success = function(successResponse){
                   $log.info("OAuthService: _getUserList: 	"+ angular.toJson(successResponse));
                   
                   deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
               };
               var error = function(errorResponse){
                   $log.info("OAuthService: _getUserList: Error"+angular.toJson(errorResponse));
                   deferred.reject(errorResponse);
               };

               if (envConfig.useLoginStub) {
                   $log.info("OAuthService: _getUserList Stub  API invoked : ");
                  // _getNetworkIdListStubService.get({}, success, error);
               } else{
                   $log.info("OAuthService: _getUserList API invoked : ");
                   _oAuth2Service.getUserList(postData).then(success, error);
               }                
               
               return deferred.promise;
          };
          
          var _createNetworkId = function(inputData) {
             	
              var deferred = $q.defer();
              var success = function(successResponse){
                  $log.info("OAuthService: _createNetworkId: 	" + angular.toJson(successResponse));
                  
                  deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
              };
              var error = function(errorResponse){
                  $log.info("OAuthService: _createNetworkId: Error" + angular.toJson(errorResponse));
                  deferred.reject(errorResponse);
              };

              if (envConfig.useLoginStub) {
                  $log.info("OAuthService: _createNetworkId Stub  API invoked : ");
                 // _getNetworkIdListStubService.get({}, success, error);
              } else{
                  $log.info("OAuthService: _createNetworkId API invoked : ");
                  _oAuth2Service.createNetworkId(inputData).then(success, error);
              }                
              
              return deferred.promise;
         };
           

            return {

                authUser: function(user){
                	 var deferred = $q.defer();
                     _authenticateUser(user).then(
                        function(authData){
                             _readUser(authData).then(
                                function(authData){
                                     _setMenuDisplay(authData).then(function(successResponse){
								           $log.info("OAuthService: _setMenuDisplay: Success"+ angular.toJson(successResponse));                 
								           deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
								           }, function(errorResponse){
								           $log.info("OAuthService: _setMenuDisplay: Error"+angular.toJson(errorResponse));
								           deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);       
								           })
	                            }, function(errorResponse) {
                                    authData.userType = 'platform';
                                    authData.groups = ['network'];
                                    authData.username = authData.userName;
                                    
                					var ctl_auth = JSON.parse(sessionStorage.getItem('ctl_auth'));
                					ctl_auth.userName = authData.userName;
                					ctl_auth.groups = ['network'];
                					sessionStorage.setItem('ctl_auth', JSON.stringify(ctl_auth));
                                                                       
                                    _setMenuDisplay(authData).then(function(successResponse){
								           $log.info("OAuthService: _readUser Error _setMenuDisplay: Success"+ angular.toJson(successResponse));                 
								           deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
								           }, function(errorResponse){
								           $log.info("OAuthService: _readUser Error _setMenuDisplay: Error"+angular.toJson(errorResponse));
								           deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);       
								           })
                             })}, function(errorResponse) {
                        	$log.info("OAuthService: _authenticateUser: Error"+angular.toJson(errorResponse));
                        	deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);
                         	}
	                    );
                    return deferred.promise;
                },
                authPlatformUser: function(authData){
               	 	var deferred = $q.defer();                    
	                _readUser(authData).then(
	                   function(authData){
	                        _setMenuDisplay(authData).then(function(successResponse){
						           $log.info("OAuthService: _setMenuDisplay: Success"+ angular.toJson(successResponse));                 
						           deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
						           }, function(errorResponse){
						           $log.info("OAuthService: _setMenuDisplay: Error"+angular.toJson(errorResponse));
						           deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);       
						           })
	                    }, function(errorResponse) {
	                    	authData.userType = 'platform';
                            authData.groups = ['platform'];
                            authData.username = authData.userName;
                            
        					var ctl_auth = JSON.parse(sessionStorage.getItem('ctl_auth'));
        					ctl_auth.userName = authData.userName;
        					ctl_auth.groups = ['platform'];
        					sessionStorage.setItem('ctl_auth', JSON.stringify(ctl_auth));
                                                               
                            _setMenuDisplay(authData).then(function(successResponse){
						           $log.info("OAuthService: _readUser Error _setMenuDisplay: Success"+ angular.toJson(successResponse));                 
						           deferred.resolve(angular.isUndefined(successResponse.data) ? successResponse : successResponse.data);
						           }, function(errorResponse){
						           $log.info("OAuthService: _readUser Error _setMenuDisplay: Error"+angular.toJson(errorResponse));
						           deferred.reject(angular.isUndefined(errorResponse.data) ? errorResponse : errorResponse.data);       
						           })
	                });
                   return deferred.promise;
               },
                authenticateUser: _authenticateUser,
                getAccountDataFromCache: _getAccountDataFromCache,
                setAccountDataInCache: _setAccountDataInCache,
                refreshAccessToken: _refreshAccessToken,
                createUser: _createUser,
                readUser: _readUser,
                createNetworkUser: _createNetworkUser,
                confirmNetworkUser:_confirmNetworkUser,
                getPendingInvitedUsers:_getPendingInvitedUsers,
                updateEmailResendInvite:_updateEmailResendInvite,
                getNetworkIdList:_getNetworkIdList,
                getUsersByNetworkId: _getUsersByNetworkId,
                updateUser: _updateUser,
                deleteUser: _deleteUser,
                getOrderManagementPermissions: _getOrderManagementPermissions,
                createQuickStartUser: _createQuickStartUser,
                subaccountUserListUpdates: _subaccountUserListUpdates,
                getAllNetworkIdUsers: _getAllNetworkIdUsers,
                getUserList: _getUserList,
                createNetworkId: _createNetworkId
            };
        }
    ]);
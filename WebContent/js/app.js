/**
 * Created by vk0014941 on 11/8/2015.
 */

'use strict';

// The hack to avoid issues in IE starts
window.console = window.console || {};
window.console.log = window.console.log || function() {
    };
// The hack to avoid issues in IE ends

var onBoardingApp = angular.module('onBoardingApp', [
    'ngRoute',
    'ngResource',
    'ngSanitize',
    'ui.bootstrap',
    'ctl.core.content.services',
    'ctl.core.content.services.contentCacheService',    
    'ctl.oauth.service',
    'ctl.moniker.service',
    'ctl.courier.service',
	'ctl.dumpster.service',
	'ctl.ordermanagement.service',
    'onboarding.configuration.service',
    'ctl.core.services.angularCacheService',
    'ctl.common.utils.service',
    'ctl.core.session.timer',
    'ctl.core.session.keeper',
    'ctl.login.service',
    'ctl.readfile.service',
    'ctl.feedback.service',
    'ctl.splunk.service',
    'ngPasswordStrength',
    '720kb.tooltips',
    'ctl.platform.service',
    'ngMaterial'
    ]);

onBoardingApp.run(
    [
        '$rootScope',
        '$location',
        '$interval',
        '$log',
        function(
        $rootScope,
        $location,
        $interval,
        $log
        )
    {
    	$log.debug("app.js:: Entering inside app.js");
    	$log.debug("app.js:: $location.path = " + $location.path());

        $rootScope.$on('$routeChangeStart',
            function(event, next, current) {
                $rootScope.$broadcast("inProgressStart");
            });

    }
]);


onBoardingApp.factory(
    'apiInterceptor', [
        '$q',
        '$log',
        '$rootScope',
        '$templateCache',
        'apiConfig',
        'AngularCacheService',
        'ctlUtils',

        function(
            $q,
            $log,
            $rootScope,
            $templateCache,
            apiConfig,
            AngularCacheService,
            ctlUtils

        ){
            return {
                // On request sending
                request: function(config){
                    //ToDo - This should execute only for API Calls
                    try{
                        //$log.info("Headers: "+apiConfig.headers);
                        //Update apiHeaders here before sending request

                    	/*var url = config.url;
                        if(AngularCacheService.AccountDataCache
                            && AngularCacheService.AccountDataCache.get('accountInfoData')
                            && AngularCacheService.AccountDataCache.get('accountInfoData').accountName){
                            var accountName = AngularCacheService.AccountDataCache.get('accountInfoData').accountName;
                            $log.debug("Setting AccountName in Header:"+accountName);
                            apiConfig.headers.AccountName = accountName;
                            apiConfig.headers.CorrelationId = ctlUtils.getRandomFiveDigitNumber();
                        }
                        var oAuthUrl = url.indexOf('/userCredentials');
                        if( oAuthUrl != -1 && AngularCacheService.AccountDataCache
                            && AngularCacheService.AccountDataCache.get('accountInfoData')
                            && AngularCacheService.AccountDataCache.get('accountInfoData').accessToken){
                            var accessToken = AngularCacheService.AccountDataCache.get('accountInfoData').accessToken;
                            apiConfig.headers.access_token = accessToken;
                        }*/
                    }catch(e){
                        $log.error("app.js :: apiInterceptor - Error while setting AccountName in Header");
                    }

                    // Return the config or wrap it in a promise if blank.
                    return config || $q.when(config);
                },
                // On request failure
                requestError: function(rejection){
                    // Return the promise rejection.
                    return $q.reject(rejection);
                },
                // On response success
                response: function(response) {
                    if(response && response.data){
                        //$log.info("Response received successfully"+angular.toJson(response));
                        $rootScope.apiResponse ={};
                        $rootScope.apiResponse.StatusCode = response.status;
                        $rootScope.apiResponse.StatusText = response.statusText;
                        if(response.data.error){
                            $rootScope.apiResponse.ErrorText = response.data.error;
                            $rootScope.apiResponse.ErrorDesc = response.data.error_description;
                        }
                    }
                    return response;
                },
                // On response error
                responseError: function(response) {
                    // Return the promise response.
                    $log.info("AppJS Error "+angular.toJson(response));
                    if(response){
                        $rootScope.apiResponse ={};
                        $rootScope.apiResponse.StatusCode = response.status;
                        $rootScope.apiResponse.StatusText = response.statusText;
                        if(response.data && response.data.error){
                            $rootScope.apiResponse.ErrorText = response.data.error;
                            $rootScope.apiResponse.ErrorDesc = response.data.error_description;
                        }
                    }
                    return $q.reject(response);
                }
            }
        }
    ]);

onBoardingApp.config(['$httpProvider', function($httpProvider) {

    $httpProvider.interceptors.push('apiInterceptor');
    // Enable cross domain calls
    $httpProvider.defaults.useXDomain = true;

    //Remove the header used to identify ajax call  that would prevent CORS from working
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]);

onBoardingApp.config(['$provide',function($provide) {
	$provide.decorator( '$log', [ '$delegate', function( $delegate ) {
          // Save the original $log.debug()
          var debugFn = $delegate.debug;
          var infoFn = $delegate.info;
   
          $delegate.debug = function() {
        	// do nothing, digest messages  
          };
          
          $delegate.info = function() {
        	// do nothing, digest messages
          }
          
          return $delegate;
	 }]);
}]);

onBoardingApp.config(['$provide',function($provide) {
	$provide.decorator('$q', ['$delegate', function($delegate) {
	// Extend promises with non-returning handlers
    function decoratePromise(promise) {
      promise._then = promise.then;
      promise.then = function(thenFn, errFn, notifyFn) {
        var p = promise._then(thenFn, function (value) {
        	if (angular.isDefined(value) && angular.isDefined(value.requestURL)) {
            	console.log("sending error info to GA ----> " + JSON.stringify(value));
            	ga('send', 'event', ('ErrorCode_' + value.StatusCode),value.reqMethod+" : "+ value.requestURL, value.StatusText);
        	}
        	if (angular.isDefined(errFn) && errFn instanceof Function) {
        		errFn(value);
        	}
        }, notifyFn);
        return decoratePromise(p);
      };

      promise.success = function (fn) {
        promise.then(function (value) {
          fn(value);
        });
        return promise;
      };
      promise.reject = function (fn) {
          promise.then(null, function (value) {
            fn(value);
          });
          return promise;
        };
      promise.error = function (fn) {	        
    	  promise.then(null, function (value) {
          fn(value);
        });
        return promise;
      };
      return promise;
    }

    var defer = $delegate.defer, when = $delegate.when, reject = $delegate.reject, all = $delegate.all;
    
    $delegate.defer = function() {
      var deferred = defer();
      decoratePromise(deferred.promise);
      return deferred;
    };
    
    $delegate.when = function() {
    	var p = when.apply(this, arguments);
	    return decoratePromise(p);
    };
    
    $delegate.reject = function() {	      
    	var p = reject.apply(this, arguments);
    	return decoratePromise(p);
    };
    
    $delegate.all = function() {
      var p = all.apply(this, arguments);
      return decoratePromise(p);
    };

    return $delegate;
  }]);
}]);

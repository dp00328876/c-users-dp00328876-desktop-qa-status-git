var PlatformSSOCtrl = [
        '$route',
        '$rootScope',
        '$scope',
        '$log',
        '$location',
        'OAuthServices',
        'PlatformService',
        'oauthDomainConfig',
        'SessionTimer',
        'envConfig',
        '$window',
        function(
            $route,
            $rootScope,
            $scope,
            $log,
            $location,
            OAuthServices,
            PlatformService,
            oauthDomainConfig,
            SessionTimer,
            envConfig,
            $window
        ){
            $log.info("PlatformSSOCtrl.js:: Entering inside");
            
            var parseLocation = function(location) {
            	var obj = {};
            	if (location.indexOf("?") !== -1) {
	                var queryParams = location.split("?");
	                if (queryParams[1].indexOf("&") !== -1) {
		                var pairs = queryParams[1].split("&");
	                	var pair;
		                var i;
		
		                for (i = 0; i < pairs.length; i++) {
		                  if ( pairs[i] === "" ) continue;	
		                  pair = pairs[i].split("=");
		                  obj[ decodeURIComponent( pair[0] ) ] = decodeURIComponent(pair[1]);
		                }
	                }
            	} 
            	return obj;
              };
            
            var parseLocation = parseLocation($location.absUrl());              
            $scope.accessToken = parseLocation['accessToken'];
            $scope.refreshToken = parseLocation['refreshToken'];
            $scope.accountName = parseLocation['accountName'];
            $scope.bearerToken = parseLocation['bearerToken'];
            $scope.userName = parseLocation['userName'];
            
            if (angular.isDefined($scope.accessToken)) {
            	$scope.loginErrorText ='';
            	$scope.user = {};
            	            	
                $rootScope.$broadcast("inProgressStart");
                              
                var userDetails = {};
                userDetails.userName = $scope.userName;
                userDetails.accountName = $scope.accountName;
              
                var ctl_auth = {};
   			  	ctl_auth.Authorization = 'Bearer' + ' ' + $scope.accessToken;
   				ctl_auth.token_type = 'Bearer';
   				ctl_auth.expires_in = 120000;
   				ctl_auth.refresh_token = $scope.refreshToken;
   				ctl_auth.jwtPayload = $scope.bearerToken;
   				ctl_auth.AccountName = $scope.accountName;
   				sessionStorage.setItem('ctl_auth', JSON.stringify(ctl_auth));
                
                var authResponse = OAuthServices.authPlatformUser(userDetails);
                authResponse.then(
                   function(successData) {                       
	       				$log.info('PlatformSSOCtrl:: auto login method:: Read User details Success ' + successData);
	       				SessionTimer.startSession(envConfig.sessionTimeOut);       				
	       				
	       				// fetch the account information
	       				PlatformService.getAccount()
	       					.then(
	       						function(successData) {
	       							$log.info("Account Information loaded from platform.");
	       							// loading Branding information
	       				            $rootScope.loadBrandingInfo();
	       							$location.path('/accountSummary');
	       							/* reload to update the branding information */
	       							//setTimeout( function() {$window.location.reload()}, 500);
	       						},
	       						function(errorData) {
	       							$log.info("Failed to load the Account Information from platform.");       		       					
	       							$location.path('/accountSummary');
	       						});
                    },
                    function(errorData) {
                        $scope.displayloginError = true;
                        if(angular.isDefined(errorData.responseMessage)){
                         	  $scope.loginErrorText = errorData.responseMessage;
                        } else if(angular.isDefined($rootScope.apiResponse.ErrorText) && angular.isDefined($rootScope.apiResponse.ErrorDesc)){
                        	$scope.loginErrorText =  $rootScope.apiResponse.ErrorText+" : "+  $rootScope.apiResponse.ErrorDesc;
                        }else{
                        	$scope.loginErrorText = "Failed to login";
                        }
                        
                        $log.info('PlatformSSOCtrl:: auto login method :: Authentication Error '+angular.toJson(errorData));
                        $log.info('PlatformSSOCtrl:: auto login method :: Authentication Error ');
                    }
                )['finally'](function(){
                	$rootScope.$broadcast("inProgressEnd");
                });
            }
        }
    ];
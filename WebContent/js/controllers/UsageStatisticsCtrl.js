/**
 * Created by vk0014941 on 11/12/2015.
 */

//Controller for UserStatistics Section

var UsageStatisticsCtrl = [
    '$scope',
    '$rootScope',
    '$log',
    '$q',
    '$timeout',
    'AngularCacheService',
    'apiConfig',
    'ctlUtils',
    'OAuthServices',
    'SplunkService',
    'OrderManagementService',
    'splunkConfig',
    'envConfig',
    'content',
    'pricingConfig',
    function ($scope, $rootScope, $log, $q, $timeout, AngularCacheService,apiConfig,ctlUtils,OAuthServices, SplunkService, OrderManagementService, splunkConfig, envConfig , content, pricingConfig) {
    	       
        $log.info(" Inside UsageStatisticsCtrl");
        $scope.page = 'mainPage';
        $scope.useSplunk= envConfig.useSplunk;
        $scope.content = content;
        // Pagination	
       	$scope.mainPage = {
           		'pagination':{
           			'enabled': false,
           			'currentPage': 1,
           			'startPage': 1,
           			'itemPerPage': 3,
           			'totalPages': 0
           		}        		
           };      
       	$scope.processCount = 0;
    	$scope.showReportCriteria=true;
    	$scope.StartTime = new Date();
        $scope.StartTime.setHours(0);
        $scope.StartTime.setMinutes(0);
        
        $scope.EndTime = new Date();
        $scope.EndTime.setHours(0);
        $scope.EndTime.setMinutes(0);
        $scope.usageStatistics = {};
        $scope.graphsLoaded = false;
        
        $scope.errorDiv = [];
    	// Login to Splunk Service
        
        if($scope.useSplunk){
    	var loginRes = SplunkService.login();
        }
        
    	$log.info("Splunk Login "+angular.toJson(loginRes));    	
    	var ntIDData = OrderManagementService.getOrderMgmtDataCache('networkidsummarydata');
    	/*var numbering=0;
    	for (var i=0 ; i<ntIDData.length; i++){
    		numbering+=1;
    		var ArrayLength=angular.toJson(ntIDData[i].seats.length);
        	$log.info(numbering +") the array length : "+ ntIDData[i].networkId+"  "+ArrayLength);      	
    		
    	}
    	*/
    	
        var splunkDataCache = AngularCacheService.SplunkDataCache;
    	$scope.chartCache = splunkDataCache.get("usageChartData");
	 	   if(angular.isUndefined($scope.chartCache)){
	 		  $scope.chartCache = [];
	 	   }
		 if ($rootScope.isNetworkUser) {			 
			 $scope.showDropDownNetworkId=false;					
			//$scope.networkIds = AngularCacheService.AccountDataCache.get('accountInfoData').network_id;
			$scope.usageStatistics.networkID=$scope.networkIds;
				
			$log.info("the ntdata data is "+angular.toJson(ntIDData[0].networkId));
			$scope.networkIds=ntIDData[0].networkId;
			 
			
		 } else if (!$rootScope.isNetworkUser){				

			 $scope.showDropDownNetworkId=true;
			 $log.info("Fetching the list of network Ids for platform user" );
			 $scope.networkIds = [];


			 var jsonObj = {};
			 for (var i=0 ; i<ntIDData.length; i++){				
				 jsonObj.networkId = ntIDData[i].networkId;
				 jsonObj.seatCount = ntIDData[i].seats.length;
				 var monikercount = 0;
				 for(var j=0; j<ntIDData[i].seats.length; j++){
					 if(angular.isDefined(ntIDData[i].seats[j].moniker) && ntIDData[i].seats[j].moniker != 'Unassigned'){
						 monikercount = monikercount+1;
					 }
				 }
				 jsonObj.monikercount = monikercount;
				 $scope.networkIds.push(jsonObj);
				 jsonObj = {};
				 $scope.OneTimeCharge=($scope.networkIds[i].seatCount*(pricingConfig.seatRental)) + ($scope.networkIds[i].monikercount*(pricingConfig.phoneNumberRental));
			 }
			 $log.info("The networkId array is "+angular.toJson($scope.networkIds));

			}
        
        
        $scope.ChartType = [ { name: 'COLUMN'}, { name: 'AREA'}, { name: 'LINE'}];
        $scope.ReportByData = [
			{name: 'month', value:'date_month'},
			{name: 'day', value:'date_mday'},
			{name: 'hour', value:'date_hour'}
		];
        
        $scope.update = function() {
			var chartType=$scope.charttype.name;
			$log.info('selected chart type:'+chartType);
			$scope.drawGraph(chartType);     	    
		}
       
        var startDateChange=function () {
        	$log.info("inside start date change");
            var newMininumDate = $scope.usageStatistics.startDate;
            $('#datepicker1').datepicker('option', 'minDate', newMininumDate);
        };
        $scope.outputProducts  = [];
        $scope.outputInstances = [];
        $scope.outputMonikers  = [];
        $scope.outputServices = [];
        $scope.outputNetworkId = [];
          

        
        $scope.showStatistics=function(){
			$scope.showReportCriteria=false;        	
			var usageStatisticsData={};
			var querystring='';
			var dateRange=''; 
        	 
			var content=document.getElementById("displayChart");
			content.innerHTML="";
			var content=document.getElementById("displayError");
			content.innerHTML="";
			
			//**CK** TBD - filter data belongs to account
			querystring = "AccountName="+$rootScope.accountName; //read account name from cache
			/***************	from phone number ***************/
            if(angular.isUndefined($scope.usageStatistics.fromPhoneNumber) || $scope.usageStatistics.fromPhoneNumber==""){
            	usageStatisticsData.fromPhoneNumber=""; 
            }
            else{
            	usageStatisticsData.fromPhoneNumber=$scope.usageStatistics.fromPhoneNumber;
            	querystring=' PhoneNumber='+usageStatisticsData.fromPhoneNumber;
            }
            /*************** To phone number ***************/
            if(angular.isUndefined($scope.usageStatistics.toPhoneNumber) || $scope.usageStatistics.toPhoneNumber==""){
            	usageStatisticsData.toPhoneNumber="";
            }
            else{
            	usageStatisticsData.toPhoneNumber=$scope.usageStatistics.toPhoneNumber;
            	querystring=querystring + ' To='+usageStatisticsData.toPhoneNumber;
            }
            /*************** Platform User***************/
            if(!$rootScope.isNetworkUser){            
            if(angular.isUndefined($scope.usageStatistics.networkID) || $scope.usageStatistics.networkID==""){
            	usageStatisticsData.networkUser=""; 
            }
            else{
            	$scope.selectedNetworkId=angular.toJson($scope.usageStatistics.networkID.networkId);            	
            	$scope.selectedNetworkId=($scope.selectedNetworkId).replace(/"/g,"");
            	$log.info("The selected network Id :"+$scope.selectedNetworkId);
            	usageStatisticsData.networkUser= $scope.selectedNetworkId;
            	usageStatisticsData.networkUser=usageStatisticsData.networkUser;
            	querystring=querystring+' NetworkId='+usageStatisticsData.networkUser;
            	$scope.selectedNetworkId = angular.toJson($scope.usageStatistics.networkID.networkId);
            	$log.info("The selected ntID "+$scope.selectedNetworkId);
            }
            }else if( $rootScope.isNetworkUser){
            	  /*************** Network User***************/
            	usageStatisticsData.networkUser=$scope.networkIds;
            	querystring=querystring+' NetworkId='+usageStatisticsData.networkUser;
            	$scope.selectedNetworkID = usageStatisticsData.networkUser;
            	$scope.selectedNetworkId = $scope.networkIds;
            	$log.info("The selected ntID "+$scope.selectedNetworkId);
            }
            /*************** Start Date***************/
            if(angular.isUndefined($scope.usageStatistics.startDate) || $scope.usageStatistics.startDate==""){
            	usageStatisticsData.startDate=""; 
            }
            else{
            	usageStatisticsData.startDate=$scope.usageStatistics.startDate;            	
            	usageStatisticsData.startDate=$scope.usageStatistics.startDate;            	
				var Sdate=$scope.usageStatistics.startDate;
				$log.info("The start date fom calender is:-"+$scope.usageStatistics.startDate);
				/*var startTime=$scope.StartTime;
				var hours=$scope.StartTime.getHours();
				var min=$scope.StartTime.getHours();*/
				
				var newCreateDate=$scope.usageStatistics.startDate+':00:00:00';
				usageStatisticsData.crateDateMs =newCreateDate;
				$log.info("The Start date is :"+ usageStatisticsData.crateDateMs ); 
				dateRange=dateRange +" earliest=\""+usageStatisticsData.crateDateMs+"\"";             	
            }
            /*************** End Date***************/
            if(angular.isUndefined($scope.usageStatistics.endDate) || $scope.usageStatistics.endDate==""){
            	usageStatisticsData.endDate=""; 
            }
            else{
            	usageStatisticsData.endDate=$scope.usageStatistics.endDate;
				//querystring=querystring+' AND '+usageStatisticsData.endDate;
				var Edate=$scope.usageStatistics.endDate;
				$log.info("The end date from form:"+$scope.usageStatistics.endDate);     
				var startTime=$scope.EndTime;
				/* var hours=$scope.StartTime.getHours();
				var min=$scope.StartTime.getHours();*/
				
				var newEndDate=Edate+':00:00:00';
				usageStatisticsData.newEndDate = newEndDate;
				$log.info("The end date is :"+usageStatisticsData.newEndDate );
				dateRange=dateRange +" latest=\""+usageStatisticsData.newEndDate+"\"";    	
            }
            
            querystring=querystring.replace(/^ AND\s+/i,'');
            $log.info('The query is: '+querystring);
                  
           /****** building of actual query********/
		    //+ determineReportBy()
            var splunkQuery="search index=sdf source= \"" + splunkConfig.dashBoardLogFile + "\" " + dateRange 
				+ " Endpoint=SendMessage " + querystring + " | " + determineReportBy() + "| where count  > 0 | rename count as \"SMS Count\" | rename _time as \""+showXLabel()+"\" | eval "+showXLabel()+"=strftime("+showXLabel()+", \"%d-%b\") ";
            
            if(usageStatisticsData.networkUser === ""){
            	usageStatisticsData.networkUser = "NA";
            }
            $scope.fetchData(splunkQuery, usageStatisticsData.networkUser, "#displayChart");
         //   $scope.drawChart(splunkQuery,"#displayChart");
            $log.info("buildAndDisplayCharts called");
            $log.info(splunkQuery);     
          
        }
        

        var determineReportBy = function() {
			var spanPeriod = "span=1day";
        	if (!angular.isUndefined($scope.ReportBy)) {
				if ($scope.ReportBy.value == 'date_month') {
					spanPeriod='span=1month';
				}else if ($scope.ReportBy.value == 'date_mday') {
					spanPeriod='span=1day';
				}else if ($scope.ReportBy.value == 'date_hour') {
					spanPeriod='span=1hour';
				}
        	}
			//** Account Name and Network Id filter is missing
			//var graphBy = "timechart" + spanPeriod + " count by NetworkId";
			var graphBy = "timechart " + spanPeriod + " count";
			return graphBy;
        }
        
        var showXLabel = function() {
			var xAxisLabel = "Days";
        	if (!angular.isUndefined($scope.ReportBy)) {
				if ($scope.ReportBy.value == 'date_month') {
					xAxisLabel='Months';
				}else if ($scope.ReportBy.value == 'date_mday') {
					xAxisLabel='Days';
				}else if ($scope.ReportBy.value == 'date_hour') {
					xAxisLabel='Hours';
				}
        	}
			
			return xAxisLabel;
        }
        
        // Redraw Chart if Chart Type is changed
        $scope.drawGraph=function(chartType){ 
        	if( angular.isDefined($scope.selectedNetworkId)){
        		var networkID = ($scope.selectedNetworkId).replace(/"/g,"");
        	}
        	
        	$log.info("The networkId inside drawgraph function is "+networkID);
        	if(angular.isUndefined(networkID)){
        		networkID = "NA";
        	}
        	var results = splunkDataCache.get("usageChartData");
        	if($rootScope.isNetworkUser){
        		var networkID=results[0].networkId;
        	}       	
        	
        	$log.info("The data from the cache :"+ angular.toJson(results));        	     	
        		for(var k = 0 ; k<results.length; k++ ){
        			$log.info('networkId = ' + results[k].networkId);
        			$log.info('networkId passed = ' + networkID);        			
        			$log.info('networkId matched = ' + (results[k].networkId== networkID ));
        			
        			if(results[k].networkId== networkID ){
        				$log.info('Inside if ');
        				var chartData = results[k].response;
        				$log.info('response = ' + results[k].response);
        			}
        			
        		}
          
        	/*var chartData = results[networkID];*/
        	$log.info("The chartdata :"+angular.toJson(chartData));        	
        	console.log("splunkservice.js.results:" + JSON.stringify(chartData));
   			var chartUR;
   			var chart = null; 
   			
   			var chartToken = splunkjs.UI.loadCharting(splunkConfig.chartJSPath, function() {
   				// Once we have the charting code, create a chart and update it.
   				if(chartType == "AREA") {
   					$log.info("AREA graph selected");
   					chart = new splunkjs.UI.Charting.Chart($("#displayChart"), splunkjs.UI.Charting.ChartType.AREA , false);
   				} 
   				else if (chartType == "LINE") {
   					$log.info("LINE graph selected");
   					chart = new splunkjs.UI.Charting.Chart($("#displayChart"), splunkjs.UI.Charting.ChartType.LINE , false);
   				}
   				else if (chartType == "COLUMN") {
   					$log.info("COLUMN graph selected");
   					chart = new splunkjs.UI.Charting.Chart($("#displayChart"), splunkjs.UI.Charting.ChartType.COLUMN , false);
   				}
   				
   			});
   			splunkjs.UI.ready(chartToken, function() {
   				chart.setData(chartData, { "chart.stackMode": "stacked" });  				
   				chart.draw();
   			});
        }
      
        var displayMessage=function(message){        	
        	$scope.setMessages([{'type':'2', 'message':"An error occurred:" + JSON.stringify(message)}]);
        }
               
        function openModal() {
            document.getElementById('modal').style.display = 'block';
	    }
	
	    function closeModal() {
	        document.getElementById('modal').style.display = 'none';
	    }         
       
       $scope.ShowNetworkIDGraphs=true;
       $scope.toggleDisplayGraph=function(){
    	   if( $rootScope.isNetworkUser === false){
    	   $scope.ShowNetworkIDGraphs = $scope.ShowNetworkIDGraphs === false ? true: false;
    	   }
       }
   
       
		//function to fetch Billing Summary
	     function fetchBillingSummary (networkID,spanPeriod){
	    	if($scope.useSplunk){
	    	 try{
	         var networkQuery = "";
	         if(angular.isDefined(networkID) && networkID != ""){
	        	 networkQuery = " NetworkId="+networkID;
	         }
	         var query="search index=sdf source= "+splunkConfig.dashBoardLogFile 
	        	 +" Endpoint=SendMessage earliest="+spanPeriod+" AccountName="+$rootScope.accountName+networkQuery+" | stats count ";
	         $log.info("The query for fetchbill"+query);
	         var results = SplunkService.getResult(query, networkID+spanPeriod);
	         results.then(
			   function(success){  	
				   
				// claculate one time charge
   				for(var i=0; i<$scope.networkIds.length; i++){
   					if($scope.networkIds[i].networkId === networkID){
   						$scope.networkIds[i].oneTimeCharges = $scope.networkIds[i].seatCount*(pricingConfig.seatRental) + $scope.networkIds[i].monikercount*(pricingConfig.phoneNumberRental);
	    					$log.info("monthEstimatesfor :"+networkID+" "+$scope.networkIds[i].networkId+":"+ success.response.columns[0]+ ":"+$scope.networkIds[i].monthEstimates);
   					}
   				}
				   
				// Calculate amount for Span of 1HR
	    			if(spanPeriod === "-1h@h"){
	    				for(var i=0; i<$scope.networkIds.length; i++){
	    					if($scope.networkIds[i].networkId === networkID){
	    						$scope.networkIds[i].currentHrsEstimates =  ""+ (success.response.columns[0])*(pricingConfig.domesticSMSRental) ;
		    					$log.info("currentHrsEstimates for :"+networkID+" "+$scope.networkIds[i].networkId+ " "+success.response.columns[0]+ ":"+$scope.networkIds[i].currentHrsEstimates);
	    					}
	    				}	    					
	    			}
	    			// Calculate amount for span of 1Month
	    			if(spanPeriod === "-7d@d"){
	    				for(var i=0; i<$scope.networkIds.length; i++){
	    					if($scope.networkIds[i].networkId === networkID){
	    						$scope.networkIds[i].monthEstimatesSMS =  (success.response.columns[0]) * (pricingConfig.domesticSMSRental) * 4;
	    						$scope.networkIds[i].monthEstimatesTotal= $scope.networkIds[i].monthEstimatesSMS + $scope.networkIds[i].oneTimeCharges;
	    						$scope.networkIds[i].monthEstimatesDisplayHTMLTotal = ""+$scope.networkIds[i].monthEstimatesTotal;
		    					$log.info("monthEstimatesfor :"+networkID+" "+$scope.networkIds[i].networkId+":"+ success.response.columns[0]+ ":"+$scope.networkIds[i].monthEstimates);
	    					}
	    				}
	    				
	    			}
			   },
			   function(error){
				// Calculate amount for Span of 1HR
	    			if(spanPeriod === "-1h@h"){
	    				$scope.$apply(function() {
	    					$scope.networkIds[networkID].currentHrsEstimates = "Error";
	    				});
	    			}
	    			// Calculate amount for span of 1Month
	    			if(spanPeriod === "-7d@d"){
	    				$scope.$apply(function() {
	    					$scope.networkIds[networkID].monthEstimates = "Error";
	    					$scope.networkIds[networkID].monthEstimatesDisplayHTMLTotal="Error";
	    					    
	    				});
	    			}	    			
	    			$scope.networkIds[networkID].oneTimeCharges="Error";
			   });
	    	 } catch(err){
	    		 $scope.networkIds[networkID].currentHrsEstimates = "Error";
	    		 $scope.networkIds[networkID].monthEstimates = "Error";
	    		 $scope.networkIds[networkID].oneTimeCharges="Error";
	    		 $scope.networkIds[networkID].monthEstimatesDisplayHTMLTotal="Error";
	    	 }
	    	}
	         
		 };
       
       
       /* Draw Chart */
       $scope.fetchData = function(query, networkId, divId, service){    
    	   $rootScope.$broadcast("inProgressStart");
    	   
    	   $scope.processCount = $scope.processCount +1;
			var results = SplunkService.getResult(query, networkId, service);    	   
			results.then(
  			   function(success){  					 
  					if(success.response.fields.length>0 ){
  						$scope.chartCache.push({"networkId" :success.networkId, "response":success.response});
  						$scope.drawChart(success.response, divId);
  					}else{  
  						$scope.chartCache.push({"networkId" :success.networkId, "response":"Error"});
  	   					$(divId).html('<div class="error-list-view-items"> <svg class="cyclops-icon md danger"><use xlink:href="#icon-exclamation-circle"></svg> No Data Found. </div>');   
  	   					//callback(null, job);
  	   				}
  			   },
  			   function(error){
  				 $rootScope.$broadcast("inProgressEnd");
  				 $scope.setMessages([{"type":2, "message":error.response}]);
  			   })['finally'](function(){
  				   splunkDataCache.put("usageChartData", $scope.chartCache);
  					$rootScope.$broadcast("inProgressEnd");
  				  
  			   });	    	   
       };
       
       $scope.drawChart = function(chartData, divId){
    	   var chart = null; 
    	   var chartToken = splunkjs.UI.loadCharting(splunkConfig.chartJSPath, function() {
		   // Once we have the charting code, create a chart and update it.      	  	   					
		    		chart = new splunkjs.UI.Charting.Chart($(divId), splunkjs.UI.Charting.ChartType.COLUMN , false);  	  	   			    		
		    		splunkjs.UI.ready(chartToken, function() {  	  	   			    			
          			chart.setData(chartData, { "chart.stackMode": "stacked", }); 
          			chart.draw();  	          				
     			});
   			});
       }
             
       /* code to geneate the graphs of all networkIDs*/
       $scope.generateDashBoardGraphs=function(){
    	  if($scope.useSplunk){
    	   if(!$rootScope.isNetworkUser){
    		   var startFrom = ($scope.mainPage.pagination.currentPage -1) * $scope.mainPage.pagination.itemPerPage;
        	   var endTo = startFrom + $scope.mainPage.pagination.itemPerPage;
        	   var chartIndex = 0;        	          	   
        	   for(var i=startFrom; i<endTo ;i++){
        		   // Generate and Display CHart Data
        		   var cacheFound = false; 
       	     	 	if(splunkDataCache.get("usageChartData")){
       	     	 		var cache = splunkDataCache.get("usageChartData");
       	     	 		for(var l=0; l<cache.length; l++){
       	     	 			if(cache[l].networkId === $scope.networkIds[i].networkId){
       	     	 				cacheFound = true;
       	     	 				if(cache[l].response != "Error"){
       	     	 					$scope.errorDiv[chartIndex] = "";
       	     	 					$scope.drawChart(cache[l].response, "#displayChart"+chartIndex);      	     	 					
       	     	 				}
       	     	 				else{
       	     	 					$log.info("#displayChart"+chartIndex);
       	     	 					var dId = "#displayChart"+chartIndex;
       	     	 					$(dId).html('<div class="error-list-view-items"> <svg class="cyclops-icon md danger"><use xlink:href="#icon-exclamation-circle"></svg> No Data Found. </div>');
       	     	 					//$(dId).text("No Data Found");
       	     	 					$scope.errorDiv[chartIndex] = "Error";
       	     	 					//$(dId).html('<div class="error-list-view-items"> <svg class="cyclops-icon md danger"><use xlink:href="#icon-exclamation-circle"></svg> No Data Found. </div>');
       	     	 				}
       	     	 			}
       	     	 		}
       	     	 	}
       	     	 	if(!cacheFound){
	       	     	 	var splunkQuery="search index=sdf source= "+splunkConfig.dashBoardLogFile
		   	     	 		+" Endpoint=SendMessage earliest="+splunkConfig.spanPeriod+" AccountName="+$rootScope.accountName
		   	     	 		+" NetworkId="+$scope.networkIds[i].networkId+" | timechart span="+splunkConfig.span+" count | where count > 0 | rename count as \"SMS Count\" | rename _time as Days | eval Days=strftime(Days, \"%d-%b\") ";
		   	     	 	$log.info("Creating splunk job for NetworkId [" + $scope.networkIds[i].networkId + "].")	
		   	     	 	$scope.errorDiv[chartIndex] = "";
		   	     	 	$scope.fetchData(splunkQuery, $scope.networkIds[i].networkId, "#displayChart"+chartIndex); 		   	     	 	
       	     	 	}
       	     	 	
       	     	 	// Generate Billing Summary
       	     	 	
       				fetchBillingSummary($scope.networkIds[i].networkId,"-7d@d");
       				fetchBillingSummary($scope.networkIds[i].networkId,"-1h@h");
       	     	 	
       	     	 	chartIndex++;
       	       }
    	   }
    	   else if ($rootScope.isNetworkUser){    		 
	   		    $scope.ShowNetworkIDGraphs=false;
	   		    $log.info("The networkId of network user is"+$scope.networkIds);
	   		    var splunkQuery="search index=sdf source= "+splunkConfig.dashBoardLogFile
	   		    	+" Endpoint=SendMessage earliest="+splunkConfig.spanPeriod+" AccountName="+$rootScope.accountName
	   		    	+" NetworkId="+$scope.networkIds+" | timechart span="+splunkConfig.span+" count | where count  > 0 | rename count as \"SMS Count\" | rename _time as Days | eval Days=strftime(Days,\"%d-%b\")";
	   		    	$scope.fetchData(splunkQuery, $scope.networkIds, "#displayChart");
    	   }
    	  }
       };
     
       if($scope.networkIds  && !$scope.graphsLoaded){
       		$scope.generateDashBoardGraphs();
       		//$scope.graphsLoaded = true;
			$log.info('Loading Graphs');
       };
       
       $scope.clickNext = function(){
    	   $scope.mainPage.pagination.startPage=$scope.mainPage.pagination.startPage + $scope.mainPage.pagination.itemPerPage;
    	   $scope.mainPage.pagination.currentPage = $scope.mainPage.pagination.startPage;
    	   $scope.generateDashBoardGraphs();
       };
       
       $scope.clickPrevious = function(){    	   
    	   $scope.mainPage.pagination.startPage=$scope.mainPage.pagination.startPage - $scope.mainPage.pagination.itemPerPage;
    	   $scope.mainPage.pagination.currentPage = $scope.mainPage.pagination.startPage;
    	   $scope.generateDashBoardGraphs();
       };
       
    }
];

UsageStatisticsCtrl.resolve ={
		  content:['ContentService',
			        function(ContentService){
			            return ContentService.fetchContent("Common,Usage_Statistics");
			        }]
		
	};



/**
 * Created by pb0083784 on 3/14/2016.
 */

//Controller for Invite and Create Network User

var UserInvitationCtrl = [
    '$scope',
    '$rootScope',
    '$log',
    '$location',
    '$window',
    'MonikerService',
    'OAuthServices',
    'OrderManagementService',
    'envConfig',
    'PlatformService',
    'SessionTimer',
    'content',
    function (
        $scope,
        $rootScope,
        $log,
        $location,
        $window,
        MonikerService,
        OAuthServices,
        OrderManagementService,
        envConfig,
        PlatformService,
        SessionTimer,
        content
    ) {
    	
    	/*$log.info(" confirmUserCtrl Content"+angular.toJson(content));
        $scope.content = content;*/
    	$scope.errLbl=true;
       $log.info("UserInvitationCtrl invoked");
       $scope.content = content;
       $scope.username = $location.search()['username'];
       $scope.emailAddress = $location.search()['emailAddress'];
       $scope.seatId = $location.search()['seatId'];
       $scope.networkId = $location.search()['networkId'];
       $scope.accessToken = $location.search()['bearerToken'];
       
       $scope.confirmUserSuccess = false;
       $scope.showConfirmuserform = true;
       
       $scope.securityQuestions = $scope.content.Security_Questions.Questions;
       
       $scope.confirmUser = function(){
        	var inputData = {};
        	var securityQuestions1 = {};
        	var securityQuestions2 = {};
        	//inputData = $scope.confirmUser;
        	inputData.accessToken = $scope.accessToken
        	inputData.seatId = $scope.seatId;
        	inputData.networkId = $scope.networkId;
        	inputData.userName = $scope.username;
        	inputData.securityQuestions = [];
        	
        	var passwordVerified = false;
        	var securityAnswerProvided = true;
        	
        	if (angular.isDefined($scope.confirmUser.firstName)){
        		inputData.firstName = $scope.confirmUser.firstName;
            }
        	if (angular.isDefined($scope.confirmUser.lastName)){
        		inputData.lastName = $scope.confirmUser.lastName;
            }
           if (angular.isDefined($scope.emailAddress)){ 
        	   inputData.emailAddress = $scope.emailAddress;
            }
           
        	if (angular.isDefined($scope.confirmUser.securityQue1) && $scope.confirmUser.securityQue1!=''){
        		securityAnswerProvided = false;
        		securityQuestions1.securityQuestion = $scope.confirmUser.securityQue1.value;
        		if (angular.isDefined($scope.confirmUser.securityAnswer1)){
        			securityQuestions1.securityAnswer = $scope.confirmUser.securityAnswer1;
        			securityAnswerProvided = true;
            		inputData.securityQuestions.push(securityQuestions1);
                }else{
                	$scope.securityQuestion1Error = true;
                }
        		
        	}
        	
        	if (angular.isDefined($scope.confirmUser.securityQue2) && $scope.confirmUser.securityQue2!=''){
        		securityAnswerProvided = false;
        		securityQuestions2.securityQuestion = $scope.confirmUser.securityQue2.value;
        		if (angular.isDefined($scope.confirmUser.securityAnswer2)){
        			securityQuestions2.securityAnswer = $scope.confirmUser.securityAnswer2;
        			securityAnswerProvided = true;
            		inputData.securityQuestions.push(securityQuestions2);
                }else{
                	$scope.securityQuestion2Error = true;
                }
            }
        	
            
        	if (angular.isDefined($scope.confirmUser.password)){
        		inputData.password = $scope.confirmUser.password;
        		if(angular.isDefined($scope.confirmUser.verifyPassword) && 
        			$scope.confirmUser.password == $scope.confirmUser.verifyPassword){
        			passwordVerified = true;
        			$scope.passwordVerificationError = false;
        		}else{
        			passwordVerified = false;
        			$scope.passwordVerificationError = true;
        		}
        		
            }
        	
        	$log.info("updateAccountData invoked : "+angular.toJson(inputData));

        	if(passwordVerified && securityAnswerProvided){
        		 $rootScope.$broadcast("inProgressStart");
	        	 updateUserResponse = OAuthServices.confirmNetworkUser(inputData);
	        	 updateUserResponse.then(
							function(successData) {
								$log.info('confirmUserCtrl::updateAccountData method:: Success '+angular.toJson(successData));
								//$scope.setMessages([{"type":0, "message": successData.status }]);
								//$scope.confirmUserSuccess = true;
							    //$scope.confirmuserform = false;
							    
							    /* Auto Login ---- START */
							    var userDetails = {};
					            userDetails.userId = inputData.userName;
					            userDetails.password = inputData.password;

				                var authResponse = OAuthServices.authUser(userDetails);
				                	authResponse.then(
					                   function(successData) {
					                       $log.info('Auto Login after registration :: Authentication Success '+successData);
					                       SessionTimer.startSession(envConfig.sessionTimeOut);                      
					                       
					                       // fetch the account information
						       			   PlatformService.getAccount()
						       					.then(
						       						function(successData) {
						       							$log.info("Account Information loaded from platform.");
//						       						 	loading Branding information
						       				            $rootScope.loadBrandingInfo();
						       							$rootScope.$broadcast("inProgressEnd");
						       							$window.location.href = '#/accountSummary';
						       							/* reload to update the branding information */
						       							//setTimeout( function() {$rootScope.$broadcast("inProgressEnd"); $window.location.reload()}, 500);
						       						},
						       						function(errorData) {
						       							$log.info("Failed to load the Account Information from platform.");	       							
						       							$rootScope.$broadcast("inProgressEnd");
						       							$window.location.href = '#/accountSummary';	       							
						       						});
					                    },
					                    function(errorData) {
					                        if(angular.isDefined(errorData.responseMessage)){
					                         	  $scope.loginErrorText = errorData.responseMessage;
					                        } else if(angular.isDefined($rootScope.apiResponse.ErrorText) && angular.isDefined($rootScope.apiResponse.ErrorDesc)){
					                        	$scope.loginErrorText =  $rootScope.apiResponse.ErrorText+" : "+  $rootScope.apiResponse.ErrorDesc;
					                        }else{
					                        	$scope.loginErrorText = "Failed to login";
					                        }
					                        
					                        $scope.confirmUserSuccess = false;
										    $scope.showConfirmuserform = true;
										    $scope.setMessages([{"type":2, "message": $scope.loginErrorText }]);
										    
					                        $log.info('LoginCtrl::login method:: Authentication Error '+angular.toJson(errorData));
					                        $log.info('LoginCtrl::login method:: Authentication Error ');
					                        $rootScope.$broadcast("inProgressEnd");
					                    }
				                )['finally'](function(){				                	                	
				                    $scope.user.adminLogin = false;				                    
				                });							    
							    /* Auto Login ---- END */
							    
			                },
			                function(errData) {
			                	$scope.errLabel = false;
			                    $log.info('confirmUserCtrl::updateAccountData method:: Error '+angular.toJson(errData));
			                    $scope.confirmUserSuccess = false;
							    $scope.showConfirmuserform = true;
							 	if (angular.isDefined(errData.responseMessage)
     									&& (errData.responseMessage != null || errData.responseMessage != "")) {
       	        		   			$scope.setMessages([ {"type" : 2,"message" :errData.responseMessage} ]);
     							} else {
     								$scope.setMessages([ {"type" : 2,"message" : "Failed to update profile"} ]);
     							}			                    
							 	$rootScope.$broadcast("inProgressEnd");
			                }
	             );
	        }
        }
        
    }
];

UserInvitationCtrl.resolve = {
	    content: ['ContentService', function(ContentService) {
	            return ContentService.fetchContent("Common,Security_Questions");
	    }],
	};

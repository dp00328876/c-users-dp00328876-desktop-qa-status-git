/**
 * Created by ms0089635 on 04/11/2016.
 */

// Controller for QuickStart Section
var QuickStartCtrl = [
		'$scope',
		'$rootScope',
		'$log',
		'$location',
		'$q',
		'$timeout',
		'quickStartConfig',
		'OAuthServices',
		'MonikerService',
		'CourierService',
		'OrderManagementService',
	    'oauthDomainConfig',
	    'content',
		function($scope, $rootScope, $log, $location, $q, $timeout,
				quickStartConfig, OAuthServices, MonikerService,
				CourierService, OrderManagementService, oauthDomainConfig,content) {
			$log.info("Inside quickstart controller");
			$scope.content=content;
			$scope.confirmationModalContent=false;
			$scope.quickStartMainPage=true;
			// Get account info data
			var accountInfoData = OAuthServices.getAccountDataFromCache("accountInfoData");
			var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));
			$scope.isNetworkUser = (angular.isDefined(accountInfoData.groups) && accountInfoData.groups.indexOf('network') != -1);
			
			$scope.quickStartDetails = {};
			$scope.quickStartDetails.legalAckStatus = true;
			$scope.quickStartDetails.userName = quickStartConfig.consumerName + '_' + tokenData.AccountName;
			$scope.quickStartDetails.password = quickStartConfig.consumerPassword;
			$scope.quickStartDetails.networkId = quickStartConfig.consumerNetworkId;
			$scope.quickStartDetails.srchMnkr = {};
			$scope.quickStartDetails.srchMnkr.State = quickStartConfig.searchMoniker_State;
			$scope.quickStartDetails.srchMnkr.City = quickStartConfig.searchMoniker_City;
			$scope.quickStartDetails.failureReason = '';
			
			var lastSuccessStepInformation = {'stepId':0, 'successData':''};
				
			var productMetaData = [ {
				"id" : "1",
				"name" : "Courier-Basic",
				"version" : "1.0",
				"legalText" : ""
			} ];
			
			$scope.goToSendSMS=function(){
				$location.path('/products/products_sendmessage');
			} 
			
			$scope.closeRedirectSection=function(){
				$scope.quickStartMainPage=true;
				$scope.confirmationModalContent=false;
				$location.path('/accountSummary');
				for (i=0; i< $scope.quickStartData.length ; i++ ){					
					$scope.quickStartData[i].status = "notStarted";
					$scope.quickStartData[i].statusText = "";
				}
			}
			 
			$scope.quickStartData = [ {
				step : 1,
				description : "Subscribe seat",
				statusText : "",
				status : "notStarted"
			}, {
				step : 2,
				description : "Create a new or use an existing consumer",
				statusText : "",
				status : "notStarted"
			}, {
				step : 3,
				description : "Login as Consumer",
				statusText : "",
				status : "notStarted"
			}, {
				step : 4,
				description : "Select one of the available phone number based on criteria",
				statusText : "",
				status : "notStarted"
			}, {
				step : 5,
				description : "Assign phone number to this seat",
				statusText : "",
				status : "notStarted"
			}, {
				step : 6,
				description : "Activate service to send SMS",
				statusText : "",
				status : "notStarted"
			} ];

			$scope.showSendSMSForm=false;
			$scope.doQuickSetup = function() {				
				if (validate()) {									
					if (lastSuccessStepInformation.stepId === 0) {
						$scope.quickStartData[0].status = "inProgress";
						step1_subscribeSeat(0); 
					}
					else if (lastSuccessStepInformation.stepId === 1) {
						$scope.quickStartData[1].status = "inProgress";
						return step2_createConsumer(1, lastSuccessStepInformation.successData); 
					}
					else if (lastSuccessStepInformation.stepId === 2) {
						$scope.quickStartData[2].status = "inProgress";
						lastSuccessStepInformation.successData.userName = $scope.quickStartDetails.userName + oauthDomainConfig.network;
						lastSuccessStepInformation.successData.password = $scope.quickStartDetails.password;						
						return step3_loginAsConsumer(2, lastSuccessStepInformation.successData); 
					}
					else if (lastSuccessStepInformation.stepId === 3) {
						$scope.quickStartData[3].status = "inProgress";
						return step4_searchPhoneNumber(3, lastSuccessStepInformation.successData); 
					}
					else if (lastSuccessStepInformation.stepId === 4) {
						$scope.quickStartData[4].status = "inProgress";
						return step5_assignPhoneNumber(4, lastSuccessStepInformation.successData); 
					}
					else if (lastSuccessStepInformation.stepId === 5) {
						$scope.quickStartData[5].status = "inProgress";
						$timeout(function() {
							return step6_activateServices(5, lastSuccessStepInformation.successData);
						}, 10000);
					}
				}
			};
			
			function updateLastSuccessStep (stepId, successData) {
				lastSuccessStepInformation.stepId = stepId;
				lastSuccessStepInformation.successData = successData;
			}
			
			$scope.selectedMonikerSearchCriteria = {'selectedOption':'stateCity'};
			$scope.monikerSearchCriteriaFound = true;
			
			function validate () {				
				var valid = true;
				$scope.monikerSearchCriteriaFound = true;
				
				if (angular.isUndefined($scope.quickStartDetails.srchMnkr) || (angular.isUndefined($scope.quickStartDetails.srchMnkr.State) 
						&& angular.isUndefined($scope.quickStartDetails.srchMnkr.City)
    					&& angular.isUndefined($scope.quickStartDetails.srchMnkr.NPA)
    					&& angular.isUndefined($scope.quickStartDetails.srchMnkr.NXX))) {
					valid = false;
					$scope.monikerSearchCriteriaFound = false;
				}
				
				return (valid && $scope.LegalTermsForm.$valid && $scope.QuickUserName.$valid && $scope.monikerSearchForm.$valid);
			}
			
			$scope.statesArr = [];
			$scope.citiesArr = [];
			
			// Search Moniker - Load States
    		$scope.loadStateData = function() {
    			$log.info("Loading State and City");

    			var statesArrResponse = MonikerService.readState();
    			statesArrResponse
    					.then(
    							function(successData) {
    								$log
    										.info('AccountSummaryCtrl::loadStateData method:: Success '
    												+ angular
    														.toJson(successData));
    								$scope.statesArr = successData.states;
    								$log.info(angular.toJson($scope.statesArr));

    							},
    							function(errorData) {
    								$log
    										.info('AccountSummaryCtrl::loadStateData method:: Error ');
    							})
    		};

    		// Search Moniker - Load Cities
    		var loadCityData = function(state) {
    			$log.info("Loading State and City");

    			var statesArrResponse = MonikerService.readCity(state);
    			statesArrResponse
    					.then(
    							function(successData) {
    								$log
    										.info('AccountSummaryCtrl::loadCityData method:: Success '
    												+ angular
    														.toJson(successData));
    								$scope.citiesArr = successData.cities;
    								$log.info(angular.toJson($scope.citiesArr));

    							},
    							function(errorData) {
    								$scope.citiesArr = [];
    								$log
    										.info('AccountSummaryCtrl::loadCityData method:: Error ');
    							})
    		};
    		
    		$scope.setMonikerSrchState = function(item) {
    			loadCityData(item);
    		};

    		
    		$scope.loadStateData();
    		loadCityData(quickStartConfig.searchMoniker_State);
    		
    		$scope.resetMonikerSearchCriteria = function () {
    			$scope.quickStartDetails.srchMnkr = {};
    		}
    		
			/*
			 * Step 1 - Subscribe Seat - Invoke OM API to create a Seat
			 * 
			 */
			var step1_subscribeSeat = function(step) {				
				var inputData = {};
				inputData.customerId = quickStartConfig.customerId
						+ accountInfoData.userName;
				inputData.products = [{'productName':quickStartConfig.productName}];
				inputData.legalAckValue = productMetaData[0].name + "_"
						+ productMetaData[0].version + "_"
						+ accountInfoData.userName;
				inputData.legalAckStatus = true;
				
				if (!$scope.quickStartDetails.legalAckStatus) {
					$scope.quickStartData[step].status = "error";
					$scope.quickStartData[step].statusText = " (Please accept Legal terms to proceed.)";
					$scope.quickStartDetails.failureReason = $scope.quickStartData[step].statusText;
				} else {
					OrderManagementService
							.createSeat(inputData)
							.then(
									function(successData) {
										var sData = {};
										sData.seatId = successData.seatId;
										$scope.quickStartData[step].status = "complete";
										$scope.quickStartData[step + 1].status = "inProgress";
										$scope.quickStartData[step].statusText = " (Created seat id is " + successData.seatId + ")";
										$scope.SeatID=successData.seatId;
										updateLastSuccessStep (1, sData);
										step2_createConsumer(1, sData);
									},
									function(errorData) {
										$scope.quickStartData[step].status = "error";
										$scope.quickStartData[step].statusText = " (Failed to create the seat.)";
										$scope.quickStartDetails.failureReason = errorData.responseMessage;
										$scope.setMessages([ {
											"type" : 2,
											"message" : errorData.responseMessage
										} ]);
									})['finally'](function() {
	
					});
				}
			};

			/*
			 * Step 2 - Create Consumer - Check if QuickStart User exist, invoke
			 * API to assign Seat to Group - if User does not exist, invoke API
			 * to create Consumer, Group and assign to Seat
			 * 
			 */
			var step2_createConsumer = function(step, req) {
				$log.info("The succesData from step1 is" + angular.toJson(req));
				var inputData = {};
				inputData.seatId = req.seatId;
				inputData.userName = $scope.quickStartDetails.userName + oauthDomainConfig.network;
				inputData.password = $scope.quickStartDetails.password;
				inputData.networkId = quickStartConfig.consumerNetworkId;

				OAuthServices
						.createQuickStartUser(inputData)
						.then(
								function(successData) {
									$scope.quickStartData[step].statusText = " (User "
											+ inputData.userName
											+ "/"
											+ inputData.password
											+ " created successfully under Group [" + inputData.networkId + "].)";
									$scope.quickStartData[step].status = "complete";
									$scope.quickStartData[step + 1].status = "inProgress";
									inputData.networkId = tokenData.AccountName + "-" + inputData.networkId;
									updateLastSuccessStep (2, inputData);
									step3_loginAsConsumer(2, inputData);
								},
								function(errorData) {									
									// Response Status === 409 [ User Exists ]
									if (errorData.StatusCode === 409) {
										$scope.quickStartData[step].statusText = " (User "
												+ inputData.userName
												+ " already exists, adding seat to group.)";
										var seatAssociationInput = {};
										seatAssociationInput.seatId = inputData.seatId;
																				
										OAuthServices.readUser(inputData, false)
											.then(function(successData) {
												if(angular.isDefined(successData) && angular.isDefined(successData.networkIds) && successData.networkIds.length >0){
													seatAssociationInput.networkId = successData.networkIds[0];
													OrderManagementService.createNetworkId(seatAssociationInput)
													.then(
														function(success) {
															$scope.quickStartData[step].status = "complete";
															$scope.quickStartData[step + 1].status = "inProgress";
															inputData.networkId = seatAssociationInput.networkId;
															updateLastSuccessStep (2, inputData);
															step3_loginAsConsumer(2, inputData);
														},
														function(errorData) {
															//$rootScope.$broadcast("inProgressEnd");
															$scope.quickStartData[step].status = "error";
															$scope.quickStartDetails.failureReason = errorData.responseMessage;
															$scope.setMessages([ {
																		"type" : 2,
																		"message" : errorData.responseMessage
																	} ]);
														});
													
												}
												else{													
													$scope.quickStartData[step].status = "error";
													$scope.quickStartDetails.failureReason = "User is not assigned to any group";
													$scope.setMessages([ {
																"type" : 2,
																"message" : "User is not assigned to any group"
															} ]);
												}
												
											}, function(errorData) {
												$scope.quickStartData[step].status = "error";
												$scope.quickStartDetails.failureReason = errorData.responseMessage;
												$scope.setMessages([ {
															"type" : 2,
															"message" : errorData.responseMessage
														} ]);
											})										
									} else {
										$scope.quickStartData[step].status = "error";
										$scope.quickStartDetails.failureReason = errorData.responseMessage;
										$scope.setMessages([ {
													"type" : 2,
													"message" : errorData.responseMessage
												} ]);
									}

								})['finally'](function() {

				});
			};

			/*
			 * Step3 - Do Consumer Login - Authentication information will be
			 * kept in scope for Consumer and only available for this screen
			 * only - This information will be used in next steps
			 */
			var step3_loginAsConsumer = function(step, req) {
				$log.info("the successdata from step 2 is"	+ angular.toJson(req));
				var userDetails = {};
				
				userDetails.userId = req.userName;
				userDetails.password = req.password;

				OAuthServices
						.authUser(userDetails)
						.then(
								function(successData) {
									$log.info("success data from step2 " + angular.toJson(successData));
									var sData = {};
									sData.seatId = req.seatId;
									sData.networkId = req.networkId;
									$scope.quickStartData[step].statusText = " (Now logged in as "
											+ req.userName
											+ "/"
											+ req.password
											+ " Group Name [" + req.networkId + "])";
									$scope.setMessages([ {
										"type" : 3,
										"message" : 'You have logged in as consumer.'
									} ]);
									$scope.quickStartData[step].status = "complete";
									$scope.quickStartData[step + 1].status = "inProgress";
									OrderManagementService.clearOrderMgmtDataCache("accountsummarydata");
									$rootScope.$broadcast("startDisplayMenu");
									$rootScope.$broadcast("refreshUserTypeData");
									accountInfoData = OAuthServices.getAccountDataFromCache("accountInfoData");
									$scope.isNetworkUser = (angular.isDefined(accountInfoData.groups) && accountInfoData.groups.indexOf('network') != -1);
									$log.info("isNetwork User after Login : " + $scope.isNetworkUser);
									
									/* reloading accounts data for Account Switcher ---- START */									
									sessionStorage.setItem('platform_account_hierarchy', 
												JSON.stringify([{"accountAlias":tokenData.AccountName,"businessName":tokenData.AccountName,
														"accountStatus":"active","primaryDataCenter":"","subAccounts":[]}]));
									reloadAccountSwitcherData();
									/* reloading accounts data for Account Switcher ---- START */
									updateLastSuccessStep (3, sData);
									step4_searchPhoneNumber(3, sData); 
								},
								function(errorData) {
									$scope.quickStartData[step].status = "error";
									$scope.quickStartData[step].statusText = " (Failed to login as "
											+ req.userName
											+ "/"
											+ req.password
											+ ")";
									$rootScope.$broadcast("endDisplayMenu");
									$scope.quickStartDetails.failureReason = errorData.responseMessage;
									$scope.setMessages([ {
										"type" : 2,
										"message" : errorData.responseMessage
									} ]);
								})['finally'](function() {

				});				
			};

			/*
			 * Step 4 - Search a Phone Number - Fetch Phone Number and pass to
			 * next step - COnfigurable index value will be used to fetch phone
			 * number from result array
			 */
			var step4_searchPhoneNumber = function(step, req) {
				var searchInputData = {};
				if ($scope.quickStartDetails.srchMnkr.State != undefined)
					searchInputData.state = $scope.quickStartDetails.srchMnkr.State;
				if ($scope.quickStartDetails.srchMnkr.City != undefined)
					searchInputData.city = $scope.quickStartDetails.srchMnkr.City;
				if ($scope.quickStartDetails.srchMnkr.NPA != undefined)
					searchInputData.NPA = $scope.quickStartDetails.srchMnkr.NPA;
				if ($scope.quickStartDetails.srchMnkr.NXX != undefined)
					searchInputData.NXX = $scope.quickStartDetails.srchMnkr.NXX;
				searchInputData.limit = 10;

				MonikerService
						.searchMoniker(searchInputData)
						.then(
								function(successData) {
									if (angular.isDefined(successData.availablePhoneNumbers) && angular.isArray(successData.availablePhoneNumbers)
											&& successData.availablePhoneNumbers.length > 0) {
										var sData = {};
										sData.moniker = successData.availablePhoneNumbers[quickStartConfig.monikerIndex];
										sData.seatId = req.seatId;
										sData.networkId = req.networkId;
										sData.Authorization = req.Authorization;
										$scope.quickStartData[step].statusText = " (Selected phone number is " + sData.moniker + ")";
										$scope.quickStartData[step].status = "complete";
										$scope.quickStartData[step + 1].status = "inProgress";
										updateLastSuccessStep (4, sData);
										step5_assignPhoneNumber(4, sData);
									} else {
										$scope.quickStartData[step].statusText = " (No phone number found for given criteria.)";
										$scope.quickStartData[step].status = "error";
										$scope.quickStartDetails.failureReason = errorData.responseMessage;
										$scope.setMessages([ {
											"type" : 2,
											"message" : errorData.responseMessage
										} ]);
									}
								},
								function(errorData) {
									$scope.quickStartData[step].statusText = " (Failed to select a number.)";
									$scope.quickStartData[step].status = "error";
									$scope.quickStartDetails.failureReason = errorData.responseMessage;
									$scope.setMessages([ {
										"type" : 2,
										"message" : errorData.responseMessage
									} ]);
								})['finally'](function() {

				});				
			};

			/*
			 * Step 5 - Assign this phone number to seat - Assign phone number
			 * received from previous step to Seat
			 */
			var step5_assignPhoneNumber = function(step, req) {
				$log.info("the successdata from step 4 is " + angular.toJson(req));

				var monikerReserveData = {};
				monikerReserveData.moniker = req.moniker;
				monikerReserveData.seatId = req.seatId;

				MonikerService
						.reserveMoniker(monikerReserveData)
						.then(
								function(successData) {
									var sData = {};
									sData.moniker = req.moniker;
									sData.seatId = req.seatId;
									sData.networkId = req.networkId;
									sData.Authorization = req.Authorization;
									$scope.phoneNumber=req.moniker;
									$scope.quickStartData[step].statusText = " (" + sData.moniker
											+ " phone number is assigned to the seat.)";
									$scope.quickStartData[step].status = "complete";
									$scope.quickStartData[step + 1].status = "inProgress";
									updateLastSuccessStep (5, sData);
									$timeout(function() {
										return step6_activateServices(5, sData);
									}, 10000);
								},
								function(errorData) {
									$scope.quickStartData[step].statusText = " (Failed to assign "
											+ req.moniker
											+ " phone number to the seat.)";
									$scope.quickStartData[step].status = "error";
									$scope.quickStartDetails.failureReason = errorData.responseMessage;
									$scope.setMessages([ {
										"type" : 2,
										"message" : errorData.responseMessage
									} ]);
								})['finally'](function() {

				});				
			};

			/*
			 * Step 6 - Activate Services for this Phone Number
			 */
			var step6_activateServices = function(step, req) {
				var serviceData = {};
				serviceData.phoneNumber = req.moniker;
				serviceData.productName = quickStartConfig.productName;
				serviceData.seatId = req.seatId;
				serviceData.networkId = req.networkId;
				serviceData.enabledService = true;
				serviceData.messageSaveStore = true;

				CourierService
						.createCourier(serviceData)
						.then(
								function(successData) {
									$scope.showSendSMSForm=true;
									$scope.quickStartData[step].statusText = " (Couier Basic activated.)";
									$scope.quickStartData[step].status = "complete";
									$scope.confirmationModalContent=true;
									$scope.quickStartMainPage=false;
									lastSuccessStepInformation = {'stepId':0, 'successData':''};						 
								},
								function(errorData) {
									//$rootScope.$broadcast("inProgressEnd");
									$scope.quickStartData[step].statusText = " (Failed to activate Couier Basic.)";
									$scope.quickStartData[step].status = "error";
									$scope.quickStartDetails.failureReason = errorData.responseMessage;
									$scope.setMessages([ {
										"type" : 2,
										"message" : errorData.responseMessage
									} ]);
								})['finally'](function() {

				});				
			};
			
			$scope.isRetry = function() {
				return lastSuccessStepInformation.stepId != 0;
			}
			
			$scope.isStepComplted = function(stepId) {
				return lastSuccessStepInformation.stepId > stepId;
			}
		} ];

QuickStartCtrl.resolve = {
    content: ['ContentService', function(ContentService) {
        return ContentService.fetchContent("QuickStart,Common,AddSeat");
    }],
};
/**
 * Created by vk0014941 on 11/12/2015.
 */

//Controller for Manage Account Section

var ManageAccountCtrl = [
    '$scope',
    '$rootScope',
    '$log',
    'MonikerService',
    'OAuthServices',
    'OrderManagementService',
    'ContentService',
    'content',
    '$window',
    function (
        $scope,
        $rootScope,
        $log,
        MonikerService,
        OAuthServices,
        OrderManagementService,
        ContentService,
        content,
        $window
    ) {
    	$scope.errLbl=true;
    	/*$log.info(" ManageAccountCtrl Content"+angular.toJson(content));
        $scope.content = content;*/
    	$scope.content=content;
        $log.info("ManageAccountCtrl invoked");
        $scope.manageAccount = {};
        $scope.ManageAccountNetworkUser = false;
        $scope.ManageAccountPlatformUser = true;
        $scope.securityQuestions = $scope.content.Security_Questions.Questions;
      //Check create network user permission
       var accountInfoData = OAuthServices.getAccountDataFromCache("accountInfoData");
       $log.info("ManageAccountCtrl: accountInfoData: "+angular.toJson(accountInfoData));
        if(angular.isDefined(accountInfoData.groups) && accountInfoData.groups.indexOf('network') != -1){
            $scope.ManageAccountNetworkUser = true;
            $scope.ManageAccountPlatformUser = false;
            
            //populate profile values
            $scope.manageAccount.firstName = accountInfoData.firstName;
            $scope.manageAccount.lastName = accountInfoData.lastName;
            $scope.manageAccount.email = accountInfoData.emailAddress;
            if(angular.isUndefined(accountInfoData.securityQuestions)){
            	$scope.secQuesMustUpdate = true;
            }
        }
      
        $scope.updatePassword = false;
        $scope.secQuesChanged1 = false;
        $scope.secQuesChanged2 = false;
        $scope.showPassword = function(){
            $scope.updatePassword = !$scope.updatePassword;
        }

        $scope.updateSecurityQuestions = false;
        $scope.showSecurityQuestions = function(){
        	$scope.updateSecurityQuestions = !$scope.updateSecurityQuestions;
        	if($scope.updateSecurityQuestions){
	        	if(angular.isDefined(accountInfoData.securityQuestions)){
		            if(angular.isDefined(accountInfoData.securityQuestions[0])){
		            	$scope.manageAccount.securityQue1 = accountInfoData.securityQuestions[0].securityQuestion;
		            }
		            if(angular.isDefined(accountInfoData.securityQuestions[1])){
		            	$scope.manageAccount.securityQue2 = accountInfoData.securityQuestions[1].securityQuestion;
		            }
	            }
        	}
        }
        
        $scope.goToPlatformForProfileChange = function() {
        	if (angular.isDefined($rootScope.brandingInfo.controlUrl) && angular.isDefined($rootScope.brandingInfo.controlUserProfileUrl)) {
        		$window.open($rootScope.brandingInfo.controlUrl + $rootScope.brandingInfo.controlUserProfileUrl, '_blank');
        	}
        }
        
        $scope.secQuesChanged = function(id){
        	var securityQ = id ==='1' ? $scope.manageAccount.securityQue1: id === '2' ?$scope.manageAccount.securityQue2:'';
        	
        	if(angular.isDefined(accountInfoData.securityQuestions) && accountInfoData.securityQuestions[id-1].securityQuestion){
        		if(accountInfoData.securityQuestions[id-1].securityQuestion === securityQ){
        			$scope.secQuesChanged1 = id==='1' ? false:$scope.secQuesChanged1;
        			$scope.secQuesChanged2 = id==='2' ? false:$scope.secQuesChanged2;
        		}else{
        			$scope.secQuesChanged1 = id==='1' ? true:$scope.secQuesChanged1;
        			$scope.secQuesChanged2 = id==='2' ? true:$scope.secQuesChanged2;
        		}
        	}else{
    			
        	}
        }
        
        $scope.updateAccountData = function(formValid){
        	var inputData = {};
        	var securityQuestions1 = {};
        	var securityQuestions2 = {};
        	inputData.accessToken = accountInfoData.accessToken;
        	inputData.userName = accountInfoData.userName;
        	
        	if (angular.isDefined($scope.manageAccount.firstName)){
        		inputData.firstName = $scope.manageAccount.firstName;
            }
        	if (angular.isDefined($scope.manageAccount.lastName)){
        		inputData.lastName = $scope.manageAccount.lastName;
            }
        	
        	if (angular.isDefined($scope.manageAccount.email)){
        		inputData.email = $scope.manageAccount.email;
            }
        	//Check if security questions section is expanded and if yes, set the input 
        	if($scope.updateSecurityQuestions && ($scope.secQuesChanged1 || $scope.secQuesChanged2)){
        		$scope.submitted = true;
        		if (angular.isDefined($scope.manageAccount.securityQue1)){
                	inputData.securityQuestions = [];
            		securityQuestions1.securityQuestion = $scope.manageAccount.securityQue1;
            		if ($scope.manageAccount.securityQue1 !='' && angular.isDefined($scope.manageAccount.securityAnswer1) && ($scope.secQuesChanged1 || $scope.secQuesChanged2)){
            			securityQuestions1.securityAnswer = $scope.manageAccount.securityAnswer1;
                		inputData.securityQuestions.push(securityQuestions1);
                    }
            		
            	}
            	
            	if (angular.isDefined($scope.manageAccount.securityQue2)){
            		if(angular.isUndefined(inputData.securityQuestions) || !inputData.securityQuestions){
            			inputData.securityQuestions = [];
            		}
            		securityQuestions2.securityQuestion = $scope.manageAccount.securityQue2;
            		if ($scope.manageAccount.securityQue2 !='' && angular.isDefined($scope.manageAccount.securityAnswer2) && ($scope.secQuesChanged1 || $scope.secQuesChanged2)){
            			securityQuestions2.securityAnswer = $scope.manageAccount.securityAnswer2;
                		inputData.securityQuestions.push(securityQuestions2);
                    }
                }
        	}
        	//check if password section is expanded and if yes, set the input
        	if($scope.updatePassword){ 
        	$scope.submitted = true;
        		if (angular.isDefined($scope.manageAccount.oldPassword)){
            		inputData.oldPassword = $scope.manageAccount.oldPassword;
                }
            	
            	if (angular.isDefined($scope.manageAccount.password)){
            		inputData.password = $scope.manageAccount.password;
            		if(angular.isDefined($scope.manageAccount.verifyPassword) && 
            			$scope.manageAccount.password == $scope.manageAccount.verifyPassword){
            		}
                }
        	}
        	
        	if(formValid){
            	$log.info("updateAccountData invoked : "+angular.toJson(inputData));
        		$rootScope.$broadcast("inProgressStart");
        		//Reset the $dirty to original (true) state to disable the update button
        		$scope.manageaccountform.$setPristine();	
	        	 updateUserResponse = OAuthServices.updateUser(inputData);
	        	 updateUserResponse.then(
							function(successData) {
								$scope.errLabel=false;
								$log.info('ManageAccountCtrl::updateAccountData method:: Success '+angular.toJson(successData));
								// Updating displayName in UI
								var updatedName ="";
								if((angular.isDefined(inputData.firstName) && inputData.firstName !=='') || (angular.isDefined(inputData.lastName) && inputData.lastName !=='')){
									updatedName = (angular.isDefined(inputData.firstName) ? inputData.firstName : '') + " " + (angular.isDefined(inputData.lastName) ? inputData.lastName : '');
								}
								else{
									updatedName = (inputData.userName.substr((inputData.userName.length - '@consumer'.length), inputData.userName.length) === '@consumer') 
	   													? inputData.userName.substr(0, (inputData.userName.length - '@consumer'.length)) 
	   													: inputData.userName.substr(0, (inputData.userName.length - '@admin'.length));
								}
								var menuItemsInfo = OAuthServices.getAccountDataFromCache("menuItemsInfo");
								menuItemsInfo.displayName = updatedName;
								$rootScope.menuItemsInfo = menuItemsInfo;
								OAuthServices.setAccountDataInCache("menuItemsInfo", menuItemsInfo);
								$scope.setMessages([{"type":0, "message": successData.message }]);
								// Update accountInfoData Cache
								OAuthServices.readUser(inputData, true).then(function(successData) {
									accountInfoData = OAuthServices.getAccountDataFromCache("accountInfoData");
								});
			                },
			                function(errData) {
			                    $log.info('ManageAccountCtrl::updateAccountData method:: Error ');			                    
			                    if(angular.isDefined(errData)){
									$scope.monikerReservedFailureText = "Failed to upadate profile: [" + errData.responseMessage +"]";
								}
								else{
									$scope.monikerReservedFailureText = "Failed to upadate profile.";
								}
			                    $scope.setMessages([{"type":2, "message": $scope.monikerReservedFailureText }]);
			                }
	             )['finally'](function(){
                 	$rootScope.$broadcast("inProgressEnd");
                 });
	        }
        }
    }
];

ManageAccountCtrl.resolve ={
	    content:['ContentService',
	        function(ContentService){
	            return ContentService.fetchContent("Common,My_Profile,Security_Questions");
	        }]
	    
	};


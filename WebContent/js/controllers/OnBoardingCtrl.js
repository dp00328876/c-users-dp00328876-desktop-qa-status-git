'use strict';
/**
 * Created by vk0014941 on 11/8/2015.
 */

onBoardingApp.controller(
    'OnBoardingCtrl',
    [
        '$route',
        '$rootScope',
        '$scope',
        '$log',
        '$location',
        'LogoutServices',
        'envConfig',
        'OAuthServices',
        '$window',
        'errorMessageConfig',
        'ContentService',
        'pricingConfig',
        'oauthDomainConfig',
        'SessionTimer',
        'platformConfig',
        'platformBrandingConfig',
        'platformSupportConfig',
        '$mdDialog', 
        '$mdMedia',
        function(
            $route,
            $rootScope,
            $scope,
            $log,
            $location,
            LogoutServices,
            envConfig,
            OAuthServices,
            $window,
            errorMessageConfig,
            ContentService,
            pricingConfig,
            oauthDomainConfig,
            SessionTimer,
            platformConfig,
            platformBrandingConfig,
            platformSupportConfig,
            $mdDialog, 
            $mdMedia
        ){
            $log.info("OnBoardingCtrl.js:: Entering inside");
            $scope.pricingConfig=pricingConfig;
            var buildNumber = (envConfig.build === '${build.number}') ? '' :envConfig.build ;
            
            var sdkHelper = CTL.Service.Helper;
            
            $scope.buildVersion=envConfig.appVersion+" "+ buildNumber;
            $scope.buildDate = envConfig.date;
           // RouteChangeStart, RouteChangeSuccess, RouteChangeError
           // LocationChangeStart, LocationChangeSuccess
           // Browser specific scenarios and generic error scenario can be handled here            
            
            
            $rootScope.loadBrandingInfo = function() {
	            // Branding Information            
	            var platformAcctInfo = localStorage.getItem('platformAccountInfo');
	            if(angular.isDefined(platformAcctInfo) && platformAcctInfo != null && platformAcctInfo != '' && JSON.parse(platformAcctInfo).branding){
	            	$rootScope.brandingInfo = JSON.parse(platformAcctInfo).branding;
	            	//Temp Code
	            	$rootScope.brandingInfo.controlUrl = platformConfig.platformURL;
	            	$rootScope.brandingInfo.controlName = (angular.isDefined($rootScope.brandingInfo.controlName) && $rootScope.brandingInfo.controlName === 'Control Portal') 
	            												? '' : $rootScope.brandingInfo.controlName;
	           	 	$log.info("Branding info:"+angular.toJson($rootScope.brandingInfo));
	           	 	$rootScope.supportInfo = JSON.parse(platformAcctInfo).support;
	            } else {
	            	$rootScope.brandingInfo = platformBrandingConfig;
	            	$rootScope.supportInfo = platformSupportConfig;
	            }
            }
            
            // loading Branding information
            $rootScope.loadBrandingInfo();
            
            $scope.logOutExpanded = false;
            var messages = [];
            $scope.appVersion = envConfig.appVersion;
            $scope.expandLogOut = function (){
            	$scope.logOutExpanded = !$scope.logOutExpanded;            	
            };
            $rootScope.genericErrorMessage = errorMessageConfig.defaultErrorMessage.English;           
            $rootScope.$on('$locationChangeStart', function (event, next, current) {
                try {
                    

                }catch(err){
                    $log.info('OnBoardingCtrl:: ERROR :' + err);
                }
            });

            var getBoolean = function(value) {
                if (value === true || value === 'true') {
                    return true;
                }
                else {
                    return false;
                }
            };


            $scope.redirectToLogin = function(pageId){
            	$location.url($location.path());
            	if(angular.isDefined(pageId)){
            		$location.path('/login/'+pageId);
            	}else{
            		$location.path('/login');
            	}
            	
            };
            
            $scope.changeMenu = function(menuUrl){
            	//$location.url($location.path());
            	$location.path(menuUrl);
            	$route.reload();
            };
            
            $scope.resetAlertMessage = function(){
            	$scope.setMessages('');
            };
            
            $rootScope.$on("refreshUserTypeData", function() {
            	// function to display the Account name on account summary page
                var accountInfoData = OAuthServices.getAccountDataFromCache("accountInfoData");
                
            	if(angular.isDefined(accountInfoData) && angular.isDefined(accountInfoData.groups) 
         			   && accountInfoData.groups.indexOf('network') != -1){
         		   $rootScope.isNetworkUser = true;
         		   $rootScope.isPlatformUser = false;
         	   	}
         	   	else{
         		   $rootScope.isPlatformUser = true;
         		   $rootScope.isNetworkUser = false;
         	   	}
            });
	    
            $rootScope.$on("$routeChangeStart", function(event, next,current) {
                $log.info('OnBoardingCtrl:: In $routeChangeStart Next isAuth:'+next.isAuthRequired);
                $rootScope.$broadcast("inProgressStart");
                $scope.setMessages('');	// Setting Error messages blank
                $rootScope.breadcrumbs = []; // Setting BreadCrumbs blank
                $scope.logOutExpanded = false;
                try {
                    var isAuthRequired = next.isAuthRequired;
                    var isUserAuthenticated = sessionStorage.getItem('isUserAuthenticated');
                    isUserAuthenticated = isUserAuthenticated ? getBoolean(isUserAuthenticated) : false;
                    $rootScope.isUserAuthenticated = isUserAuthenticated;
                    if(isAuthRequired){

                        $log.info('OnBoardingCtrl.js:::'+isUserAuthenticated);
                        if(isUserAuthenticated){
                            //$log.info('OnBoardingCtrl.js:: User is Authenticated');
                        }else{
                            $log.info('OnBoardingCtrl:: User is not Authenticated Redirecting to Login');
                            LogoutServices.logOut();
                            $location.path('/');
                        }
                    }else{
                        // No Auth Required
                    }
                    
                  // function to display the Account name on account summary page
                    var accountInfoData = OAuthServices.getAccountDataFromCache("accountInfoData");
                    
                    if(angular.isDefined(accountInfoData) && angular.isDefined(accountInfoData.groups) 
                			   && accountInfoData.groups.indexOf('network') != -1){
                		   $rootScope.isNetworkUser = true;
                		   $rootScope.isPlatformUser = false;
                	   }
                	   else{
                		   $rootScope.isPlatformUser = true;
                		   $rootScope.isNetworkUser = false;
                	   }
                    
                    $scope.displayAccountName="";
               	   if($location.path()=='/accountSummary'){
                       	if(angular.isDefined(accountInfoData)){
                       		$scope.displayAccountName = accountInfoData.accountName; 
                       		if($rootScope.isNetworkUser && angular.isDefined(accountInfoData.network_id)){
                       			$scope.displayAccountName = accountInfoData.network_id;
                       		}
                       	}
                       else{
                       	$scope.displayAccountName="";
                       }
               	   }
               	   
               	   if(angular.isDefined(accountInfoData) && angular.isDefined(accountInfoData.accountName)){
               		   $rootScope.accountName = accountInfoData.accountName;
               	   }
               	   
               	   var tokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));
               	   if (tokenData != undefined && tokenData != null) {
               		   $rootScope.accountNameForSplunk = tokenData.AccountName;
               	   }
 
                }catch(err){
                    $log.info('OnBoardingCtrl:: $routeChangeStart ERROR :' + err);
                }

            	//Fetch Content for Title and Description
                if(angular.isDefined(next.contentPageId) && next.contentPageId != ''){
                	ContentService.fetchContent(next.contentPageId).then(
                    		function success(successData){
                    			$rootScope.pageTitle = successData[next.contentPageId].title;
                                $rootScope.pageDescription = successData[next.contentPageId].description;
                    		},
                    		function error(errorData){
                    			
                    		}
                    	);
                }else{
                	$rootScope.pageTitle = '';
                    $rootScope.pageDescription = '';
                }
            	
                
            });
            $rootScope.$on("$routeChangeSuccess", function(event, next, previous) {

                $log.info('OnBoardingCtrl:: In $routeChangeSuccess ');
                var isUserAuthenticated = sessionStorage.getItem('isUserAuthenticated');
                isUserAuthenticated = isUserAuthenticated ? getBoolean(isUserAuthenticated) : false;
                if(isUserAuthenticated){
                    if(OAuthServices.getAccountDataFromCache("menuItemsInfo")){
                        $rootScope.$broadcast("startDisplayMenu");
                    }else{
                        $rootScope.$broadcast("endDisplayMenu");
                    }
                    //$log.info('OnBoardingCtrl.js:: User is Authenticated');
                }
                $rootScope.$broadcast("inProgressEnd");

                /*var millisecondsToWait = 10000;
                setTimeout(function() {
                    // Whatever you want to do after the wait

                }, millisecondsToWait);*/
                
            });

            $rootScope.$on("$routeChangeError", function() {

                $log.info('OnBoardingCtrl:: In $routeChangeError ');
                $rootScope.$broadcast("inProgressEnd");
            });
            
            /* Method to get the messages. */
        	$scope.getMessages = function() {
        		return messages;
        	};
        	
        	$scope.setMessages = function(msgs) {
        		messages = msgs;
        		if(angular.isDefined(msgs) && msgs.length > 0){
        			$window.scrollTo(0, 0);
        		}
        	};        	

        	$scope.logout=function(){
        		$location.path('/logout');
        	}
        	$scope.goToCTL=function(){
        		$window.location.href= (angular.isDefined($rootScope.brandingInfo.controlUrl) && $rootScope.brandingInfo.controlUrl !== '') ? $rootScope.brandingInfo.controlUrl : platformConfig.platformURL;
        	}
        	$scope.goToSSO=function(){
        		$window.location.href= platformConfig.platformURL + "/auth/sso?ReturnUrl=" + encodeURIComponent(sdkHelper.getBaseApiURL() + "/token?localRedirectUrl=" + $location.absUrl().substr(0, $location.absUrl().indexOf('#')) +"&domain=admin&appLocalRedirect=/platform/sso");
        	}
        	
        	$scope.isSupportChatAvailable = function() {
        		return (angular.isDefined($rootScope.supportInfo.chatServiceUrl) && $rootScope.supportInfo.chatServiceUrl != '');
        	}
        	
        	$scope.goToSupportChat = function() {
        		$window.open($rootScope.supportInfo.chatServiceUrl, '_blank');
        	}
        	
        	$scope.isKnowledgeBaseAvailable = function() {
        		return (angular.isDefined($rootScope.supportInfo.knowledgebaseUrl) && $rootScope.supportInfo.knowledgebaseUrl != '');
        	}
        	
        	$scope.goToKnowledgeBase = function() {
        		$window.open($rootScope.supportInfo.knowledgebaseUrl, '_blank');
        	}
        	
        	$scope.isSupportRequestAvailable = function() {
        		return (angular.isDefined($rootScope.supportInfo.supportRequestUrl) && $rootScope.supportInfo.supportRequestUrl != '');
        	}
        	
        	$scope.goToSupportRequest = function() {
        		$window.open($rootScope.supportInfo.supportRequestUrl, '_blank');
        	}
        	
        	$scope.gotoAPIDocument = function() {        	   	
            	$location.path('/apiDocs/restAPIDocs');
            };
            
            $scope.showSignInHelp = function(ev) {
            	var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
                $mdDialog.show({
                  controller: ['$scope', '$mdDialog', function($scope, $mdDialog) {
                	  $scope.answer = function(answer) {
                		    $mdDialog.hide(answer);
                		  };
                		}],
                  templateUrl: 'partials/fragmentTemplates/login/signInHelp.tmpl.html',
                  parent: angular.element(document.body),
                  targetEvent: ev,
                  clickOutsideToClose:false,
                  fullscreen: useFullScreen
                })
                .then(function(answer) {

                }, function() {

                });
                $scope.$watch(function() {
                  return $mdMedia('xs') || $mdMedia('sm');
                }, function(wantsFullScreen) {
                  $scope.customFullscreen = (wantsFullScreen === true);
                });
              };
        }
    ]);

var AddSeatCtrl = [
    '$scope',
    '$q',
    '$rootScope',
    '$log',
    '$location',
    '$route',
    '$window',
    'content',
    'OAuthServices',
    'OrderManagementService',
    'AngularCacheService',
    '$mdDialog', 
    '$mdMedia',
    function (
    		$scope,
            $q,
            $rootScope,
            $log,
            $location,
            $route,
            $window,
            content,
            OAuthServices,
            OrderManagementService,
            AngularCacheService,
            $mdDialog, 
            $mdMedia
    ) {
    	
    	 /**
         * Set cache
         * @param {String} key
         * @param {Object|String} val
         * @returns {Boolean}
         */
        var setOrderMgmtDataCache=function(key,val) {
            if(angular.isDefined(key) && angular.isDefined(val) && key!=="") {
            	OrderMgmtDataCache.put(key,val);
                return(true);
            }
            return(false);
        };
          $scope.errLbl=false;
    	  $scope.content = content;
    	  $scope.SMSRate=0.0065;
          $scope.IneternationSMSRate=0.0065;
          $scope.productRate=0.99;
          $scope.TNRate=0.99;
          $scope.NationalSMSSent=0;
          $scope.InternationalSMSSent=0;
          $scope.buyProductInput = {};
          $scope.buyProductInput.prodQuantity = 1;
          $scope.successOrders=[];
          $scope.addToCard=true;
//        $scope.thankYouSection=false;
          var productMetaData = [{
		      		"id" : "1",
		      		"name" : "Courier-Basic",
		      		"version" : "1.0",
		      		"legalText" : ""
		      	}];
          var accountInfoData = OAuthServices.getAccountDataFromCache("accountInfoData");
          var OrderMgmtDataCache = AngularCacheService.OrderMgmtDataCache;    //Initializing cache service
          var customerIdList = [];
          var requestList = [];
       //   var customerIdList = [];
          var productList = [];
         
          $rootScope.breadcrumbs = [{ label:'Services', url: '#/accountSummary'}, { label:'Courier SMS', url: '#/seatDetails'}]; // Setting Breadcrumbs
          
          $scope.addSeat = function(productName){ 
        	  if($scope.buyProductInput.legalAckStatus === false || $scope.buyProductInput.legalAckStatus === undefined){
        		  $scope.checkValue=true;
        	  }else{
        		  $scope.checkValue=false;
               $rootScope.$broadcast("inProgressStart");         	  
        	  var inputData = {};
                	inputData.customerId = $scope.buyProductInput.customerId;
                    inputData.products = [{'productName':'courier-basic'}];
                    inputData.legalAckValue = productMetaData[0].name + "_" + productMetaData[0].version + "_" + accountInfoData.userName;    
                    inputData.legalAckStatus = $scope.buyProductInput.legalAckStatus;
                    $log.debug("inputData:" +angular.toJson(inputData));
                    requestList.push(OrderManagementService.createSeat(inputData));
                    customerIdList.push(inputData.customerId);
                    if($scope.content.Common.Products[0].Product.Key === inputData.productName){
                        productList.push($scope.content.Common.Products[0].Product.Value);
                    }else{
                        productList.push(inputData.productName);
                    }
                    asyncCall(requestList,
                            function error(results) {
                                 $log.debug('Some error occurred', angular.toJson(results));
								 if (angular.isDefined(results)) {
     		                    	$scope.setMessages([{"type":2, "message":results.responseMessage}]);
     		                     }
                                 requestList = [];
                                 customerIdList = [];
                                 productList = [];
                                 $scope.successOrders=[];
                             },
                             function success(results) {
                            	 $log.debug('Results (final):', angular.toJson(requestList));
                                 $log.debug('Results (final):', angular.toJson(results));
                                 responseList = results;
                                // var successCount = 0;
								if(results[0] != null && results[0] != undefined){
									for(var i=0; i< customerIdList.length; i++){
										 successResponse = {};
									 successResponse.customerId = customerIdList[i];
									 successResponse.productName = productList[i];
									 successResponse.response = responseList[i];
									 $scope.successOrders.push(successResponse);
									 $log.debug("succesorders:" +angular.toJson($scope.successOrders))
									 $log.debug("successOrders:" + $scope.successOrders.customerId);
									 $log.debug("successOrders:" + $scope.successOrders.response);
									}
										 $scope.addToCard=false;
										// $scope.thankYouSection=true;
										// $rootScope.pageTitle ="";
										 $location.path('/accountSummary/' + $scope.successOrders[0].response.seatId);
								}
                	        		 
                             }
                            )['finally'](function(){
                            	$rootScope.$broadcast("inProgressEnd");
                            });
        	  }   
                    
            }
        
          function asyncCall(listOfPromises, onErrorCallback, finalCallback) {

              listOfPromises  = listOfPromises  || [];
              onErrorCallback = onErrorCallback || angular.noop;
              finalCallback   = finalCallback   || angular.noop;

              // Create a new list of promises
              // that can "recover" from rejection
              var newListOfPromises = listOfPromises.map(function (promise) {
                return promise['catch'](function (reason) {

                  // First call the `onErrroCallback`
                  onErrorCallback(reason);
                  return reason;

                });
              });

              return $q.all(newListOfPromises).then(finalCallback);
            };
            
          $scope.clickCancel = function(){
        	  $window.history.back();
          }; 
          
          $scope.gotoPricing = function(){
        	  $location.path('/pricing');
          };
          
          $scope.showTermsAndCondition = function(ev) {
          	var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
              $mdDialog.show({
                controller: ['$scope', '$mdDialog', function($scope, $mdDialog) {
              	  $scope.answer = function(answer) {
              		    $mdDialog.hide(answer);
              		  };
              		}],
                templateUrl: 'partials/fragmentTemplates/products/termsAndConditions.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:false,
                fullscreen: useFullScreen
              })
              .then(function(answer) {

              }, function() {

              });
              $scope.$watch(function() {
                return $mdMedia('xs') || $mdMedia('sm');
              }, function(wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
              });
            };
        }	
  
];

AddSeatCtrl.resolve ={
     	content:['ContentService',
     	        function(ContentService){
     	            return ContentService.fetchContent("Common,Products,AddSeat");
     	        }]
	
};
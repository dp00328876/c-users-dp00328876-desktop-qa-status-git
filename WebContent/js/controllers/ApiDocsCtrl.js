/**
 * Created by vk0014941 on 11/12/2015.
 */

//Controller for Account Summary Section

var ApiDocsCtrl = [
    '$scope',
    '$rootScope',
    '$log',
    '$route',
    '$location',
    '$window',
    '$http',
    'MonikerService',
    'OAuthServices',
    'ReadFileService',
    'ContentService',
    'content',
    function (
        $scope,
        $rootScope,
        $log,
        $route,
        $location,
        $window,
        $http,
        MonikerService,
        OAuthServices,
        ReadFileService,
        ContentService,
        content
    ) {

        $log.info(" Inside ApiDocsCtrl");
        $scope.page = 'mainPage';
        $scope.selectedAPIId = '';
        $scope.selectedAPI = {};
        $scope.activeMethodIndex = 0;
        $scope.methodDetails ={};
        $scope.isExpandedId = '';
        $scope.content = content;
        if($route.current.params.backLink == 'products'){
        	$scope.backLink = false;
        } else if($route.current.params.backLink == 'restAPIDocs'){
        	$scope.page = 'restAPIDocs';
        }else if($route.current.params.backLink == 'sdkAPIDocs'){
        	$scope.page = 'sdkAPIDocs';
        }else {
        	$scope.backLink = true;
        }
        
        $scope.setPage = function(pageId, apiJsonName){
        	if (pageId == "mainPage" || pageId == "restAPIDocs" || pageId == "sdkAPIDocs" || pageId == "faq"){
        		$scope.page = pageId;
        	} else {
        		$scope.page = "oauthAPIDocs";
        		$scope.selectedAPIId = '';
        		$scope.isExpandedId = '';
        		$rootScope.$broadcast("inProgressStart");
                ReadFileService.loadApiDocsJSON(apiJsonName).then(
                	function(successData) {
  	        		   $log.info('ApiDocsCtrl::setPage method:: Success ');
 	        		   $scope.OauthSwaggerData = successData;
 	        		   var i = 0;
 	        		   angular.forEach(successData.paths, function(item, index){
 	        			  $scope.ButtonText[i] = "C";
 	        			  i = i + 1;
 	        		   });
                	},
  	        	   	function(errorData) {
   	        		   $log.info('ApiDocsCtrl::setPage method:: Error ');
   	        		   $scope.errors = {};
                	}	
                )['finally'](function() {
    				$rootScope.$broadcast("inProgressEnd");
    			});
        	}
        };
        
        $scope.ButtonText = [];
        $scope.toggleAPIDetails=function(selectedAPIId, selectedAPI){
      	  $scope.selectedAPIId=selectedAPIId;
      	  $scope.selectedAPI = selectedAPI;
      	  if($scope.isExpandedId === selectedAPIId){
      		$scope.isExpandedId = '';
      		$scope.ButtonText[selectedAPIId] = "C";
      	  }else{
      		$scope.isExpandedId = selectedAPIId;
      		$scope.ButtonText[selectedAPIId] = "E";
      	  }
      	  
      	  var assignDefaultMethod = false;
      	  angular.forEach($scope.selectedAPI, function(method, index){
        	if(!assignDefaultMethod){
              	$scope.activeMethodIndex = 0;
            	$scope.methodDetails = method;
            	assignDefaultMethod = true;
        	}
      	  });
        };
        
        $scope.go = function (page) {
        	$log.info("page = " + page);
        	$location.path('/' + page);
        };
        
        if($route.current.params.backLink == 'products'){
        	$scope.backLink = false;
        } else {
        	$scope.backLink = true;
        }

        $scope.setActiveMethod = function(methodIndex, methodDetails){
        	$scope.activeMethodIndex = methodIndex;
        	$scope.methodDetails = methodDetails;
        }
        
        $scope.changePage = function(page){
        	$scope.page = page;
        }
        
        $scope.toggleDetail=false;
      $scope.toggleDisplayDetails=function(selectedId){
    	  $log.info(selectedId);
    	  $scope.selectedId=selectedId;
    	  
      }
              
        $scope.showDashboard=true;
        $scope.container_class='PopularProducts_cointainer';
        if($location.path()==='/'){
    		$scope.showDashboard=false;
    		$scope.container_class='';
    	}else{
    		$scope.showDashboard=true;
    		$scope.container_class='PopularProducts_cointainer';
    	}
        
        $scope.showCourierSection = function(){
            $scope.displayCourierSection = true;
            $scope.displayAPIList = false;
        };

        $scope.showAPIList = function(){ 
        	if($location.path()==='/apiDocs'){
        		 $scope.displayCourierSection = false;
                 $scope.displayAPIList = true;
        	}else{
        		$window.history.back();
        	}
        	
        };
        $scope.tabId = 1;
        $scope.showTab = function(tabId){
            $scope.tabId = tabId;
        };

        $scope.isSet = function (tabId) {
            return $scope.tabId === tabId;
        };
        
        


    }
];

ApiDocsCtrl.resolve = {
	    content: ['ContentService', function(ContentService) {
	            return ContentService.fetchContent("Common,API_Docs,FAQ");
	    }],
	};
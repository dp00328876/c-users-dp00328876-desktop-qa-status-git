/**
 * Created by vk0014941 on 11/8/2015.
 */

'use strict';

var LoginCtrl = [
    '$scope',
    '$rootScope',
    '$log',
    '$location',
    '$route',
    'OAuthServices',
    'PlatformService',
    '$timeout',
    'LogoutServices',
    'SessionTimer',
    'envConfig',
    'content',
    'oauthDomainConfig',
    '$window',
    function(
        $scope,
        $rootScope,
        $log,
        $location,
        $route,
        OAuthServices,
        PlatformService,
        $timeout,
        LogoutServices,
        SessionTimer,
        envConfig,
        content,
        oauthDomainConfig,
        $window
    ){
        $log.info("LoginCtrl Started");
        $scope.content = content;
        $scope.forwardpath = '/';
        $scope.page = 'landingPage';
        $scope.user ={};
        $scope.errLbl=true;
        if($location.path() === "" || $location.path()==='/'){        	
        	if(sessionStorage.getItem("isUserAuthenticated")  && sessionStorage.getItem("isUserAuthenticated") === 'true'){
                //LogoutServices.logOut();   
                $location.path('/accountSummary');
            }
        	if($location.path() === ""){
        		$log.info("Redirecting to Base path /");
                $location.path('/');
        	}           
        }
        
        if(angular.isDefined($route.current.params.pageId)){
        	$scope.page = $route.current.params.pageId;
        }
        
        $scope.changePage = function(page){
        	$scope.loginErrorText = '';
        	$scope.signUpErrorText = '';
        	$scope.page = page;
        }
        $scope.showLandingPage = function(){
        	$scope.page = 'landingPage';
        };
        
        $scope.goTo = function(url){
        	 $location.path(url);
        };
    	
        $scope.buttonText = 'more';

    	$scope.showMoreLess = function(){
        	$scope.showMore = !$scope.showMore;
        	if($scope.showMore == true){
        		$scope.buttonText = 'less';
        	}else{
        		$scope.buttonText = 'more';
        	}
        };
        
        $scope.redirectToProduct = function(pageId, redirectTo){
        	$log.info("redirectTo = "+redirectTo);
        	$rootScope.redirectTo = redirectTo;
        	$location.url($location.path());
        	$location.path('/login/'+pageId);
        };
        
        $scope.signInUser = function() {
        	$scope.loginErrorText ='';
            $rootScope.$broadcast("inProgressStart");
            if (angular.isUndefined($scope.user.userId) || $scope.user.userId=='' || $scope.user.userId == null){
                $scope.showErrorForSignIn = true;
            }
            if (angular.isUndefined($scope.user.password) || $scope.user.password=='' || $scope.user.password == null){
                $scope.showErrorForSignIn = true;
            }
            
            var userDetails = {};
            userDetails.userId = $scope.user.userId;
            userDetails.password = $scope.user.password;
            if (angular.isDefined($scope.user.adminLogin) && $scope.user.adminLogin){
            	userDetails.userId = userDetails.userId + oauthDomainConfig.platform;
            }else{
            	userDetails.userId = userDetails.userId + oauthDomainConfig.network;
            }
            $log.info("UserId: "+userDetails.userId);
            if(angular.isDefined(userDetails.userId) && angular.isDefined(userDetails.password)){

                var authResponse = OAuthServices.authUser(userDetails);
                authResponse.then(
                   function(successData) {
                       $log.info('LoginCtrl::login method:: Authentication Success '+successData);
                       SessionTimer.startSession(envConfig.sessionTimeOut);                      
                       
                       // fetch the account information
	       			   PlatformService.getAccount()
	       					.then(
	       						function(successData) {
	       							$log.info("Account Information loaded from platform.");
	       							// 	loading Branding information
	       				            $rootScope.loadBrandingInfo();
	       							$location.path('/accountSummary');
	       							/* reload to update the branding information 
	       							setTimeout( function() {$window.location.reload()}, 500);*/
	       						},
	       						function(errorData) {
	       							$log.info("Failed to load the Account Information from platform.");
	       							$location.path('/accountSummary');	       							
	       						});
                    },
                    function(errorData) {
                        $scope.displayloginError = true;
                        $scope.errLbl=false;
                        if(angular.isDefined(errorData.responseMessage)){
                         	  $scope.loginErrorText = errorData.responseMessage;
                        } else if(angular.isDefined($rootScope.apiResponse.ErrorText) && angular.isDefined($rootScope.apiResponse.ErrorDesc)){
                        	$scope.loginErrorText =  $rootScope.apiResponse.ErrorText+" : "+  $rootScope.apiResponse.ErrorDesc;
                        }else{
                        	$scope.loginErrorText = "Failed to login";
                        }
                        
                        $log.info('LoginCtrl::login method:: Authentication Error '+angular.toJson(errorData));
                        $log.info('LoginCtrl::login method:: Authentication Error ');
                    }
                )['finally'](function(){
                	$rootScope.$broadcast("inProgressEnd");                	
                    $scope.user.adminLogin = false;
                });
            }
        };
        
        // Sign Up User
        /*$scope.signUpUser = function(){
          $rootScope.$broadcast("inProgressStart");
          
          if (angular.isUndefined($scope.user.userId) || $scope.user.userId=='' || $scope.user.userId == null){
              $scope.showErrorForSignIn = true;
          }
          if (angular.isUndefined($scope.user.password) || $scope.user.password=='' || $scope.user.password == null){
              $scope.showErrorForSignIn = true;
          }
          
          var userDetails = {};
          userDetails.userId = $scope.user.userId+$scope.domain.platform;
          userDetails.password = $scope.user.password;
          
          var tokenCredentials = {}
          tokenCredentials.userId = "onbuiapp"+$scope.domain.webapplication;
          tokenCredentials.password = "SenturyLink$df";
          var authResponse = OAuthServices.authenticateUser(tokenCredentials);         
          authResponse.then(
             function(successData) {
              
               var createUserInput = {}
               createUserInput.username = userDetails.userId;
               createUserInput.password = userDetails.password;
               createUserInput.userTypes = ["platform"];

               var createUserResponse = OAuthServices.createUser(createUserInput);
               createUserResponse.then(
                    function(successData) {                                
                   
                     authResponse = OAuthServices.authUser(userDetails);
                        authResponse.then(
                           function(successData) {
                               $log.info('LoginCtrl::login method:: Authentication Success '+successData);
                               SessionTimer.startSession(envConfig.sessionTimeOut);
                               $location.path('/accountSummary');
                          
                            },
                            function(errorData) {
                            	if(angular.isDefined(errorData.responseMessage)){
                                    $scope.signUpErrorText = errorData.responseMessage;
                                 }
                          	  		else if(angular.isDefined($rootScope.apiResponse.ErrorText) && angular.isDefined($rootScope.apiResponse.ErrorDesc)){
                                   $scope.signUpErrorText =  $rootScope.apiResponse.ErrorText+" : "+  $rootScope.apiResponse.ErrorDesc;
                          	  	  } else{
                                   $scope.signUpErrorText = "Some error detected while Signup. Please retry later or contact administrator";
                                 }
                                 $log.info('LoginCtrl::signup method:: User Creation succesful but login failed');
                            }
                        )                   
                     },
                     function(errorData) {
                    	 if(angular.isDefined(errorData.responseMessage)){
                             $scope.signUpErrorText = errorData.responseMessage;
                          }
                   	  		else if(angular.isDefined($rootScope.apiResponse.ErrorText) && angular.isDefined($rootScope.apiResponse.ErrorDesc)){
                            $scope.signUpErrorText =  $rootScope.apiResponse.ErrorText+" : "+  $rootScope.apiResponse.ErrorDesc;
                   	  	  } else{
                            $scope.signUpErrorText = "Some error detected while Signup. Please retry later or contact administrator";
                          }
                          $log.info('LoginCtrl::signup method:: User Creation failed');
                     }
                 )
              },
              function(errorData) {
            	  if(angular.isDefined(errorData.responseMessage)){
                      $scope.signUpErrorText = errorData.responseMessage;
                   }
            	  else if(angular.isDefined($rootScope.apiResponse.ErrorText) && angular.isDefined($rootScope.apiResponse.ErrorDesc)){
                     $scope.signUpErrorText =  $rootScope.apiResponse.ErrorText+" : "+  $rootScope.apiResponse.ErrorDesc;
                  } else{
                     $scope.signUpErrorText = "Some error detected while Signup. Please retry later or contact administrator";
                  }
                  $log.info('LoginCtrl::signup method:: Authentication Error for admin account');
              }
          )['finally'](function(){
          	$rootScope.$broadcast("inProgressEnd");
            $scope.user.userId = "";
            $scope.user.password = "";
          });          
      };*/
      
      	//Pricing details section on landing page
	  	$scope.displayPricingLanding = true;
		$scope.displayPricingDetails = false;
		 
        $scope.showPricingLanding = function(){
            $scope.displayPricingLanding = true;
            $scope.displayPricingDetails = false;
        };
		$scope.showPricingDetails = function(){
			$scope.displayPricingDetails = true;
			$scope.displayPricingLanding = false;
		}
		
		//Product section on landing page
		$scope.displayProductDetails = false;
        $scope.displayProductLanding = true;
		$scope.showProductDetails = function(){
			$scope.displayProductDetails = true;
			$scope.displayProductLanding = false;
		}
		
		$scope.showProductLanding = function(){
            $scope.displayProductDetails = false;
            $scope.displayProductLanding = true;
        };
    }
];

LoginCtrl.resolve = {
    content: ['ContentService', function(ContentService) {
            return ContentService.fetchContent("Common,FAQ,Login,Pricing");
    }],
    reporting: ['ReadFileService', function(ReadFileService){
    	return ReadFileService.loadReportingJSON();
    }]
};
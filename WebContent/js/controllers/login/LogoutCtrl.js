'use strict';
var LogoutCtrl =
    [
        '$log',
        '$rootScope',
        '$location',
        '$window',
        'LogoutServices',
        function(
            $log,
            $rootScope,
            $location,
            $window,
            LogoutServices
            ) {

        	if ($rootScope.isPlatformUser) {
    			$rootScope.$broadcast("inProgressStart");
    			LogoutServices.logOut();
    			$window.location.href = $rootScope.brandingInfo.controlUrl + $rootScope.brandingInfo.controlLogoutUrl;
    			setTimeout(function() {
    				$rootScope.$broadcast("inProgressEnd");
                }, 2000);        			
    		} else {
    			$log.warn('LogoutCtrl.resolve :: logout');
                $rootScope.$broadcast("endDisplayMenu");
                LogoutServices.logOut();
    			$location.path('/');
    		}
        }];

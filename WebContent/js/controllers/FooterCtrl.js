var FooterCtrl = [
    '$scope',
    '$rootScope',
    '$log',
    '$route',
    '$location',
    'OAuthServices',
    'FeedbackService',
    'oauthDomainConfig',
    'content',
    function (
        $scope,
        $rootScope,
        $log,
        $route,
        $location,
        OAuthServices,
        FeedbackService,
        oauthDomainConfig,
        content
    ) {
    	$scope.content = content;  
        $log.info(" Inside Footer");

        $scope.goBack = function(){
        	if(angular.isDefined(history.back()))
        		$location.path(history.back());
        	else
        		$location.path('/accountSummary');
        }
        
        $scope.changePage = function(page){
        	$location.path('/landingPage');
        }
        
        $scope.initialize=function(){
    	  $log.info('inside initialization');
        	  var mapProp = {
        	    center:new google.maps.LatLng(39.746176,-104.995962),
        	    zoom:5,
        	    mapTypeId:google.maps.MapTypeId.ROADMAP
        	  };
        	 // var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
        	  
         }
        var currentpath = $location.path();
        if(angular.isDefined(currentpath) && currentpath === '/Feedback'){
			$rootScope.pageTitle = $scope.content.Feedback.feedbackLabel;
            $rootScope.pageDescription = $scope.content.Feedback.feedbackFormTitleMessage;
        }
       $scope.starRating1 = 0;
       $scope.starRating2 = 0;    
       //google.maps.event.addDomListener(window, 'resize',$scope.initialize);
       
		$scope.SubmitFeedback = function() {
			$log.info('inside SubmitFeedback');
			
			var inputData = {};
			inputData.component = $scope.feedback.component;
			inputData.name = $scope.feedback.name;
			inputData.summary = $scope.feedback.summary;
			inputData.emailAddress = $scope.feedback.email;
			inputData.ratingUI = $scope.starRating1; 
			inputData.overallRating = $scope.starRating2;
			inputData.canContact = $scope.feedback.contactYou;
			$log.info('input value = ' + angular.toJson(inputData));
			//Check if token is available. If not then get the token
			 var accountInfoData = OAuthServices.getAccountDataFromCache("accountInfoData");		
			if(angular.isDefined(accountInfoData) && angular.isDefined(accountInfoData.accessToken) && accountInfoData.accessToken != ''){
				postFeedback(inputData);
				
			}else{
				var tokenCredentials = {}
		          tokenCredentials.userId = "onbuiapp" + oauthDomainConfig.webapplication;
		          tokenCredentials.password = "SenturyLink$df";

		          $rootScope.$broadcast("inProgressStart");			

		          var authResponse = OAuthServices.authenticateUser(tokenCredentials);         
		          authResponse.then(
		             function(successData) {
		            	 $log.info('FooterCtrl::get token success');
		            	 postFeedback(inputData);		              
		              },
		              function(errorData) {
		                  $log.info('FooterCtrl::get token error');

		            	  if(angular.isDefined(errorData.responseMessage)){
		                      $scope.feedbackErrorText = errorData.responseMessage;
		                   }
		            	  else if(angular.isDefined($rootScope.apiResponse.ErrorText) && angular.isDefined($rootScope.apiResponse.ErrorDesc)){
		                     $scope.feedbackErrorText =  $rootScope.apiResponse.ErrorText+" : "+  $rootScope.apiResponse.ErrorDesc;
		                  } else{
		                     $scope.feedbackErrorText = "Failed to Submit feedback";
		                  }
		              }
		          )['finally'](function(){
		          	$rootScope.$broadcast("inProgressEnd");
		          });          
			}
						
		};
		
		$rootScope.displayFeedbackLanding = true;
		$rootScope.feedbackMessage = "";
		
		function postFeedback(inputData){
			$log.info('input value = ' + angular.toJson(inputData));
			$rootScope.$broadcast("inProgressStart");
			
			$rootScope.displayFeedbackLanding = false;
			FeedbackService.submitFeedback(inputData).then(
				function(success) {
					$log.info("FooterCtrl: submit feedback success");
					//$scope.setMessages([{"type":0, "message": success }]);
				    $rootScope.feedbackMessage = "success";
				},
				function(error) {
					$log.info("FooterCtrl: submit feedback error :  ");
				    $rootScope.feedbackMessage = "error";
					if(angular.isDefined(error.responseMessage)){
						$scope.feedbackFailureText = "Failed to submit feedback: [" + error.responseMessage +"]";
					}
					else{
						$scope.feedbackFailureText = "Failed to submit feedback";
					}
               //     $scope.setMessages([{"type":2, "message": $scope.feedbackFailureText }]);
				}
			)['finally'](function() {
				$rootScope.$broadcast("inProgressEnd");
			});
		}
    }
];

FooterCtrl.resolve ={
	    content:['ContentService',
	        function(ContentService){
	            return ContentService.fetchContent("Common,Contact_US,LEGAL,Feedback,Login");
	        }]	    
	};
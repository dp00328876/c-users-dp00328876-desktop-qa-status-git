/**
 * Created by vk0014941 on 11/12/2015.
 */

//Controller for Account Summary Section

var AccountSummaryCtrl = [
    '$scope',
    '$rootScope',
    '$log',
    '$route',
    '$q',
    'MonikerService',
    'OAuthServices',
    'CourierService',
    'OrderManagementService',
    'AngularCacheService',
    'apiConfig',
    'SessionKeeper',
    'content',
    '$routeParams',
    '$location',
    'SplunkService',
    'splunkConfig',
    'envConfig',
    'oauthDomainConfig',
    'pricingConfig',
    'accountSummaryConfig',
    '$filter',
    '$timeout',
    function (
        $scope,
        $rootScope,
        $log,
        $route,
        $q,
        MonikerService,
        OAuthServices,
        CourierService,
        OrderManagementService,
        AngularCacheService,
        apiConfig,
	    SessionKeeper,
        content,
        $routeParams,
        $location,
        SplunkService,
        splunkConfig,
        envConfig,
        oauthDomainConfig,
        pricingConfig,
        accountSummaryConfig,
        $filter,
        $timeout
    ) {
    	$scope.errLbl=true;
    	$scope.passThruSeatId = $routeParams.seatId;
        //$scope.pendingInviteCount = 0;
    	$scope.apiCount = 0;
        $scope.content = content;
        $scope.pricingConfig=pricingConfig;
        $scope.networkUser = {};  // Code to reset the create network form
        $scope.page = 'mainPage';
        $scope.pageSection = {
        		'viewEditPage': '',
        		'searchReserveTN': '',
        		'manageService': '',
        		'activateService': '',
        		'assignGroup': false,
        		'assignTN':false,
        		'inviteUser':false
        };
        $scope.selectedProduct = {};
        $scope.mainPage = {
        		'filter':{
        			'seatType':'all'
        		},
        		'pagination':{
        			'enabled': false,
        			'currentPage': 1,
        			'startPage': 1,
        			'itemPerPage': 10,
        			'totalPages': 0,
        			'navPageCount': 5
        		}        		
        };
        
        $scope.viewEditUserPage = {
        		'pagination':{
        			'enabled': false,
        			'currentPage': 1,
        			'itemPerPage': 10
        		}        		
        };
               
        $scope.searchReserveTNPage = {
        		'pagination':{
        			'enabled': false,
        			'currentPage': 1,
        			'startPage': 1,
        			'itemPerPage': 5,
        			'totalPages': 0
        		}        		
        };
    	$scope.sortData ={
        		sortType: "moniker",
        		sortReverse: false
        	};
        $scope.stopSpinner = function(){  
        	$scope.apiCount = $scope.apiCount -1;
        	if($scope.apiCount <= 0) {
        		$scope.apiCount = 0;
        		$rootScope.$broadcast("inProgressEnd");
        	}
        };
        $scope.startSpinner = function(){ 
        	$rootScope.$broadcast("inProgressStart");
          	$scope.apiCount = $scope.apiCount +1;
          };
        $scope.selectedNetworkIdToAssign = {"value":"", "selectedOption":"existingGroup", "newNetworkId":""};
        $scope.createNW = {"selectedOption":"existingGroup"};
                
        // code to display tootip text
        /*if(!$rootScope.isNetworkUser){
        	$scope.toolTipText="Delete seat, Assign Seat, Add new user";
        }else if ($rootScope.isNetworkUser){
        	$scope.toolTipText="Configure Phone Number, Enable SMS Service";	
        }
        
        $scope.pendingInviteTooTipText = "Manage pending invitations";*/
        
     // Get account info data
		var accountInfoData = OAuthServices.getAccountDataFromCache("accountInfoData");
		
        $scope.useSplunk=envConfig.useSplunk;
        $scope.billingSummary ={};	// Billing Summary Section Data
        //var splunkDataCache = AngularCacheService.SplunkDataCache;
		
		//function to fetch Billing Summary
	     function fetchBillingSummary (networkID,spanPeriod){
	
	         var networkQuery = "";
	         if(angular.isDefined(networkID) && networkID != ""){
	        	 networkQuery = " NetworkId="+networkID;
	         }
	         var query="search index=sdf source= "+splunkConfig.dashBoardLogFile 
	        	 +" Endpoint=Billing earliest="+spanPeriod+" AccountName="+$rootScope.accountNameForSplunk+networkQuery+" | stats count";
	         try{	        	 
		         var results = SplunkService.getResult(query, spanPeriod);
		         results.then(
				   function(success){  					 
					// Calculate amount for Span of 1HR
		    			if(spanPeriod === "-1h@h"){
		    				//$scope.$apply(function() {
		    					$scope.billingSummary.currentHrsEstimates = success.response.columns[0] * (pricingConfig.domesticSMSRental);
		    					$log.info("currentHrsEstimates"+$scope.billingSummary.currentHrsEstimates);
		    				//});
		    			}
		    			// Calculate amount for span of 1Month
		    			if(spanPeriod === "-7d@d"){
		    				//$scope.$apply(function() {
		    					$scope.billingSummary.monthEstimatesForTN = success.response.columns[0] * (pricingConfig.domesticSMSRental) * 4;
		    					$log.info("monthEstimatesforTN"+$scope.billingSummary.monthEstimatesForTN);
		    				//});
		    			}
				   },
				   function(error){
					// Calculate amount for Span of 1HR
		    			if(spanPeriod === "-1h@h"){
		    				//$scope.$apply(function() {
		    					$scope.billingSummary.currentHrsEstimates = "Error";
		    				//});
		    			}
		    			// Calculate amount for span of 1Month
		    			if(spanPeriod === "-7d@d"){
		    				//$scope.$apply(function() {
		    					$scope.billingSummary.monthEstimates = "Error";
		    				//});
		    			}
				   });
	         } catch(err) {
	        	 	if(spanPeriod === "-1h@h"){
	    				$scope.billingSummary.currentHrsEstimates = "Error";
	    			}
	    			if(spanPeriod === "-7d@d"){
	    				$scope.billingSummary.monthEstimates = "Error";
	    			}
	          }
		 }
     
     
     
     /*$scope.showBillingSummaryData = false;
     $scope.showBillingSummarySection = true;
     if($rootScope.previousPage === 'manageSeat'){
    	 $scope.showBillingSummarySection = false;
    	 $rootScope.previousPage = '';
     }
     $scope.toggleBillingSummaryData = function() {
         $scope.showBillingSummaryData = !$scope.showBillingSummaryData;
     }
     $scope.showAcctSummarySection = false;
     $scope.toggleAcctSummarySection = function() {    	 
         $scope.showAcctSummarySection = !$scope.showAcctSummarySection;         
     }*/
/*     $scope.showFilteredProductData = function(filterValue) {
    	 $scope.filterAccountData(filterValue);
    	 $scope.showAcctSummarySection = !$scope.showAcctSummarySection ? !$scope.showAcctSummarySection : true;
    	 // code to reset all the chevron arrows to their default position
    	 for(var j=0; j<$scope.networkIdSummaryData.length; j++){
 			$scope.ButtonText[j] = "C";
			}
    	 $scope.selectedNetworkId="";
     }*/
     
	
        
        $scope.changePage = function(pageValue, seatIdValue, networkIdValue){
        	$scope.changePageSection('searchReserveTN','');
        	$scope.resetAlertMessage();
        	$scope.page = pageValue;
        	if(angular.isDefined(seatIdValue)){
        		reloadSeatData(seatIdValue);	// Set Selected Product
        	}
        };
        // Go to Manage Seat Page
        $scope.gotoManageSeat = function(seatId){        	   	
        	$location.path('/accountSummary/'+seatId);
        };
        // Go to Quick Start page
        $scope.goToQuickStart=function(){        	  		
	      	$location.path('/quickStart');      	
	    };
        
        /*$scope.gotoPendingInvitations = function(){
        	$log.info("gotoPendingInvitations invoked");
        	$location.path('/accountSummary/pendingInvites');
        };*/
	 // Go to Seat Details page
        $scope.goToSeatDetails = function(){
        	$log.info("goToSeatDetails invoked");        	
        	$location.path('/seatDetails');
        };
        $scope.gotoAcctSummary = function(previousPage){
        	//if(angular.isUndefined(previousPage) || previousPage == 'manageSeat')
        	//	$rootScope.previousPage = 'manageSeat';
        	$scope.selectedProduct = {};
        	$location.path('/accountSummary');
        };
	    // Go to Add Seat Page
        $scope.goToBuyProducts=function(){
        	$location.path('/addSeat');
        };
        
        $scope.changePageSection = function(pageId, pageSection){        	
        	if(angular.isDefined(pageId)){
        		$scope.pageSection[pageId] = pageSection;
        	}
        	if(angular.isDefined(pageSection) && pageSection == 'addUserSection'){
        		$scope.createNW.username = '';
        		$scope.createNW.email = '';
        		$scope.createNW.subAccountName = '';
        	}
 
        };
        var currentpath = $location.path();
        if(angular.isDefined(currentpath) && currentpath === '/seatDetails'){
        	$scope.page = 'seatDetails';
        }

        // Fetching Account Data
        $scope.accountSummaryData = false;
        $scope.seatsData = false;
        $scope.networkIdSummaryData = OrderManagementService.getOrderMgmtDataCache("networkidsummarydata");
        
        if($scope.page === 'mainPage' || $scope.page === 'seatDetails'){
        	$scope.showBillingSummarySection = true;
        }
    	// Passthrough from Products page here - AccountSummaryData and Network Data will be available before this
        if($scope.passThruSeatId && $scope.page != 'pendingInvites' && $scope.page != 'seatDetails'){
        	$scope.accountSummaryData = OrderManagementService.getOrderMgmtDataCache("accountsummarydata");
        	$scope.showBillingSummarySection = false;
        	$log.info("Manage Seat page for SeatID " + $scope.passThruSeatId);
        	$scope.page = 'configureSeat';
        	$rootScope.breadcrumbs = [{ label:'SMS Seats', url: '#/seatDetails'}]; // Setting Breadcrumbs
        	$scope.pageSection.activateService = '';
        	if(angular.isDefined($scope.passThruSeatId)){
        		reloadSeatData($scope.passThruSeatId);	// Set Selected Product
        		getNetworkIdList();              		
        	}
        	//getNetworkIdUserData();
        	//$location.url($location.path());
        };
        
        
        
       
		// Generate Billing Summary  
		if(angular.isDefined(accountInfoData) && 
				angular.isDefined(envConfig.useSplunk) && envConfig.useSplunk){
			try {
                SplunkService.login();
                var networkId = "";
                if($rootScope.isNetworkUser){                   
                       networkId = accountInfoData.network_id;  
                       $log.info("The network Id is "+networkId);
                }
                fetchBillingSummary(networkId,"-7d@d");
                $log.info("Inside Account Summary Controller (network User) and after build display Charts for Week");
                fetchBillingSummary(networkId,"-1h@h");
                $log.info("Inside Account Summary Controller (network User) and after build display Charts for one day");
	          } catch(err) {
	                $scope.billingSummary.currentHrsEstimates = "Error";
	                $scope.billingSummary.monthEstimates = "Error";
	          }
		};
        

        
        //Arrange the seats with Network Ids
    	//consideration is one seat will have only one network id associated(seat --> networkid (one to one)). 
        //Going forward, if multiple association of 
        //network ids are supported by seat, then this code need to modify to include many to many relationship.
    	
        //$scope.ButtonText = [];
        $scope.getNetworkIdSummaryData = function(accountSummaryData){
    		$scope.networkIdSummaryData = [];
    		var networkIdKey;
    		var networkIdArray = [];
    		var valueArray = [];
    		var jsonObj = {};
    		angular.forEach(accountSummaryData[0].seats, function(seat, index){
    			networkIdArray = seat.networkId;
	    			if(angular.isDefined(networkIdArray)){
		    			networkIdKey = networkIdArray[0];
	    			}else{
	    				networkIdKey = '-NA_NWID';
		    		}
					jsonObj.networkId = networkIdKey;
					jsonObj.seats = [];
	    			var entryExists = false;
	    			for(var i=0; i<$scope.networkIdSummaryData.length; i++){
	    				if(angular.isDefined($scope.networkIdSummaryData[i].networkId) &&
	    					$scope.networkIdSummaryData[i].networkId == networkIdKey){
	    					$scope.networkIdSummaryData[i].seats.push(seat);
	    	    			entryExists = true;
	    				}
	    			}
	    			if(entryExists == false) {
	    				jsonObj.seats.push(seat); 
		    			$scope.networkIdSummaryData.push(jsonObj);
	    			}
	    			jsonObj = {};
	    			jsonObj.seats = [];
    		});	

    		//set in cache
    		OrderManagementService.setOrderMgmtDataCache("networkidsummarydata", $scope.networkIdSummaryData); 		
    		
    	} 
        // Get Account Information
        if((angular.isUndefined($scope.accountSummaryData) || !($scope.accountSummaryData))){
        	var inputData = {};
        	//inputData.networkId = accountInfoData.network_id; 
        	$scope.startSpinner();
    		OrderManagementService.readAccount(inputData).then(
    				function(successData) {
 	        		   $log.info('AccountSummaryCtrl::accountSummaryData method:: Success ');
 	        		   $scope.accountSummaryData = successData;
 	        		   $scope.seatsData = successData;
 	        		   $scope.getNetworkIdSummaryData(successData);
 	        		   if ($rootScope.isPlatformUser) {
 	        			   getNetworkIdUserData();
 	        		   }
 	        		   if($rootScope.isNetworkUser){
 	        			  getUsersForNetworkId();
 	        		   }
 	        		   checkSeatsCount();
 	        		  OrderManagementService.setOrderMgmtDataCache("accountsummarydata", $scope.seatsData);
 	        	   },
 	        	   function(errorData) {
 	        		   $log.info('AccountSummaryCtrl::accountSummaryData method:: Error ');
 	        		   $scope.noSeatsAvailable = true;
 	        		   $scope.billingSummary.oneTimeCharges ="Error";	// Calcualte Billing Summary One Time Charges
 	        		   $scope.errors = {};
 	        		   if(angular.isUndefined(errorData.StatusCode) || (angular.isDefined(errorData.StatusCode) && errorData.StatusCode !='404')){
 	        			  if(angular.isDefined(errorData.responseMessage) && errorData.responseMessage != null && errorData.responseMessage != ""){
 	 	        			   $scope.errors.errorMsg = errorData.responseMessage;
 	 	        		   }else{
 	 	        			   $scope.errors.errorMsg = "Failed to retrieve product summary.";   
 	 	        		   }
 	 	        		  $scope.setMessages([{"type":2, "message":$scope.errors.errorMsg}]);
 	        		   } 	        		   
 	        	   }		       		
    		)['finally'](function() {
    			$scope.stopSpinner();
			});
    	} else{
    		$scope.accountSummaryData = OrderManagementService.getOrderMgmtDataCache("accountsummarydata");
    		$scope.seatsData = $scope.accountSummaryData;
    		//$scope.getNetworkIdSummaryData($scope.accountSummaryData);
    		if ($rootScope.isPlatformUser) {
  			   getNetworkIdUserData();
  		    }
    		if($rootScope.isNetworkUser){
   			  getUsersForNetworkId();
   		   }
    		checkSeatsCount();
    	}
        //pending user invitations
        /*$scope.pendingInvitations = [];
        //Get pending user invitations
        if(angular.isUndefined($scope.pendingInvitation) || !($scope.pendingInvitation) || $scope.pendingInvitations == []){
        	if(!$rootScope.isNetworkUser && $scope.page === 'mainPage'){
	        	var inputData = {};
	        	inputData.domain = "network"; 
	    		$rootScope.$broadcast("inProgressStart");
	    		$scope.apiCount = $scope.apiCount +1;
	    		OAuthServices.getPendingInvitedUsers(inputData).then(
	    				function(successData) {
	 	        		   $log.info('AccountSummaryCtrl::accountSummaryData method:: Success '+angular.toJson(successData));
	 	        		   $scope.pendingInvitations = successData;
	 	        		   $scope.pendingInviteCount = successData.length;
	 	        	   },
	 	        	   function(errorData) {
	 	        		   $log.info('AccountSummaryCtrl::accountSummaryData method:: Error ');
	 	        		   $scope.errors = {};
	 	        		   if(angular.isDefined(errorData.responseMessage) && errorData.responseMessage != null && errorData.responseMessage != ""){
	 	        			   $scope.errors.errorMsg = errorData.responseMessage;
	 	        		   }else{
	 	        			   $scope.errors.errorMsg = "Failed to retrieve pending invitations.";   
	 	        		   }
	 	        		  $scope.setMessages([{"type":2, "message":$scope.errors.errorMsg}]);
	 	        	   }		       		
	    		)['finally'](function() {
					//$rootScope.$broadcast("inProgressEnd");
	    			$scope.stopSpinner();
				});
        	}
        }*/
        	// CHeck if No Seats are available
        	function checkSeatsCount(){
    			if (angular.isUndefined($scope.accountSummaryData)
    					|| (angular.isDefined($scope.accountSummaryData) && angular
    							.isUndefined($scope.accountSummaryData.length))
    					|| (angular.isDefined($scope.accountSummaryData)
    							&& $scope.accountSummaryData.length > 0
    							&& angular
    									.isDefined($scope.accountSummaryData[0].seats) && $scope.accountSummaryData[0].seats.length == 0)) {
    				$scope.noSeatsAvailable = true;
    				$scope.billingSummary.oneTimeCharges ="";	// Calcualte Billing Summary One Time Charges
    			}
    			else{
    				if($scope.accountSummaryData[0].totalCount > $scope.mainPage.pagination.itemPerPage){
    					$scope.mainPage.pagination.enabled = true;
    					$scope.mainPage.pagination.totalPages = Math.ceil($scope.accountSummaryData[0].totalCount/$scope.mainPage.pagination.itemPerPage);
    					// Calcualte Billing Summary One Time Charges    					
    					
    				}
    				$scope.billingSummary.oneTimeCharges = ($scope.accountSummaryData[0].totalCount * (pricingConfig.seatRental)) + ($scope.accountSummaryData[0].monikerCount * (pricingConfig.phoneNumberRental));
    				//$scope.billingSummary.monthEstimates=  ($scope.billingSummary.monthEstimatesForTN) + ($scope.billingSummary.oneTimeCharges);
    				
    				 $scope.$watch('billingSummary.monthEstimatesForTN', function() {
    					 if(angular.isDefined($scope.billingSummary.monthEstimatesForTN)){
    						 $scope.billingSummary.monthEstimates=  (angular.isDefined($scope.billingSummary.monthEstimatesForTN) ? $scope.billingSummary.monthEstimatesForTN : 0 )  + ($scope.billingSummary.oneTimeCharges);
    					 }    					 
    			    }, true);
    			}   			
    		};
        		    
        	//$scope.showseatdetails = false;
        	
        	//
        	/*$scope.expandDetails = function(networkId, index){
        		$scope.selectedNetworkId = networkId;
        		if($scope.ButtonText[index] == 'E'){
	        		//$scope.showseatdetails = false;
	        		$scope.ButtonText[index] = "C";
	        		//$scope.selectedNwIdIndex = undefined;
        		}else{
	        		//$scope.showseatdetails = true;
	        		$scope.ButtonText[index] = "E";
	        		//if(angular.isDefined($scope.selectedNwIdIndex)) $scope.ButtonText[$scope.selectedNwIdIndex] = "C";
	        		//$scope.selectedNwIdIndex = index;
	        		//if(!$rootScope.isNetworkUser) $scope.getNetworkUsersList(networkId);
	        		$scope.getNetworkUsersList(networkId);
        		}
        	}*/
        	
        	/*$scope.hideDetails = function(){
        		//$scope.showseatdetails = false;
        		for(var i=0; i<$scope.ButtonText.length; i++){
        			$scope.ButtonText[i] = "C";
        		}
        		
        		//$scope.selectedNwIdIndex = undefined;
        	}*/
        	//Get network id list from cache
        	//$scope.networkIdsList = OAuthServices.getAccountDataFromCache("networkIdsUserList");
        	//
        	 /*if(angular.isUndefined($scope.networkIdsList) || !($scope.networkIdsList) || $scope.networkIdsList.length <= 0){
        		$scope.networkIdsList = [];
        		if(!$rootScope.isNetworkUser){
	          		$rootScope.$broadcast("inProgressStart");
	          		$scope.apiCount = $scope.apiCount +1;
	          		OAuthServices.getNetworkIdList().then(
	          				function(successData) {
	       	        		   $log.info('AccountSummaryCtrl::networkIds :: Success '+angular.toJson(successData));
	       	        		   var jsonObj = {};
	       	        		   for(var i=0; i<successData.length; i++){
	     		  	           		jsonObj.name = 	successData[i];
	     		  	           		jsonObj.value = [];
	     		  	           		$scope.networkIdsList.push(jsonObj);
	     		  	           		$scope.ButtonText[i] = "C";
	     		  	           		jsonObj = {};
	     	  	        		 }
	       	        		  OAuthServices.setAccountDataInCache("networkIdsUserList", $scope.networkIdsList);
	
	       	        	   },
	       	        	   function(error) {
	       	        		   $log.info('AccountSummaryCtrl::networkIds :: Error '+angular.toJson(error.data));
	       	        		   	if (angular.isDefined(error.data.responseMessage)
	     									&& (error.data.responseMessage != null || error.data.responseMessage != "")) {
	       	        		   		$scope.setMessages([ {"type" : 2,"message" : error.data.responseMessage} ]);
	     							} else {
	     								$scope.setMessages([ {"type" : 2,"message" : "Authorization Denied"} ]);
	     							}
	       	        	   }		       		
	          		)['finally'](function(){
	     					//$rootScope.$broadcast("inProgressEnd");
	          				$scope.stopSpinner();
	     				});
        		}
        		if($rootScope.isNetworkUser){
        			// Setting networkIdsUserList in Cache
        			var jsonObj = {};
        			jsonObj.name = 	accountInfoData.network_id;
	  	           	jsonObj.value = [];
	  	           	$scope.networkIdsList.push(jsonObj);
        			OAuthServices.setAccountDataInCache("networkIdsUserList", $scope.networkIdsList);
        		}
          	}*/
        	
        	//Get the network users list for the selected network id
        	/*$scope.networkUsersList = [];
        	$scope.getNetworkUsersList = function(networkId){
        		$scope.networkIdsList = OAuthServices.getAccountDataFromCache("networkIdsUserList");
        		$scope.networkUsersList = [];
        		$scope.index = 0;
        		if(angular.isDefined($scope.networkIdsList) && $scope.networkIdsList.length > 0 
        				&& angular.isDefined(networkId) && networkId != '-NA_NWID'){
        		//Check if network user list is available in cache
        			for(var i=0;i<$scope.networkIdsList.length;i++){
        				if($scope.networkIdsList[i].name == networkId){
        					$scope.index = i;
        					$scope.networkUsersList = $scope.networkIdsList[i].value;

        				}
        			}	
        			
        			if(angular.isUndefined($scope.networkUsersList) || $scope.networkUsersList.length <= 0 || $scope.networkUsersList ==''){
        				$log.info('AccountSummaryCtrl::getUsersByNetworkId'+angular.toJson($scope.networkUsersList));
        				$rootScope.$broadcast("inProgressStart");
		     	      	var inputData = {};
		     	      	inputData.networkId = networkId;
		     	      	nwUserResponse = OAuthServices.getUsersByNetworkId(inputData);
		     	      	nwUserResponse.then(
		     				function(successData) {
			     				$log.info('AccountSummaryCtrl::getNetworkUsersList method:: Success '+angular.toJson(successData));
			     				$scope.networkUsersList = successData;
			     				$scope.networkIdsList[$scope.index].value = successData;
			     				OAuthServices.setAccountDataInCache("networkIdsUserList", $scope.networkIdsList);
     		                },
     		                function(error) {
     		                    $log.info('AccountSummaryCtrl::getNetworkUsersList method:: Error '+angular.toJson(error));
     		                    $scope.networkUsersList = [];
     		                    if (angular.isDefined(error.data)) {
     		                    	$scope.setMessages([{"type":2, "message":error.data.error_description}]);
     		                    } else {
     		                    	$scope.setMessages([{"type":2, "message":"Failed to get user for selected Network ID."}]);
     		                    }
     		                }
		     	      		)['finally'](function(){
		     	      			$rootScope.$broadcast("inProgressEnd");
		     	      		});
        			}
        		}else{
        			$scope.networkUsersList = [];
        		}
             };*/
    		
             // Filter Seat Data
             $scope.filterSeatData = function(filterValue){
            	// Resetting Pagination
        		 $scope.mainPage.pagination ={
        	        			'enabled': false,
        	        			'currentPage': 1,
        	        			'startPage': 1,
        	        			'itemPerPage': 10,
        	        			'totalPages': 0,
        	        			'navPageCount': 5
        	        			};
        		 $scope.mainPage.filter.seatType = filterValue;
        		 $scope.filter.displayValue = filterValue;
        		 var filterAccountSummaryData = OrderManagementService.getOrderMgmtDataCache("accountsummarydata");
     			 var dataToFilter = [];
    			 angular.copy(filterAccountSummaryData, dataToFilter);
    			
    			 if(filterValue == "working"){				
     				
     				var temp = [];
 					var workingCount = 0;
 					var jsonObj = {};
     				angular.forEach(dataToFilter, function(item, index){
     					jsonObj.networkId = item.networkId;
     					jsonObj.seats = [];
     					angular.forEach(item.seats, function(seat, seatIndex){
     						if(seat.seatStatus == 'working'){
     							workingCount = workingCount+1;
     							jsonObj.seats.push(seat);
     						}
     					})
     					if(jsonObj.seats.length > 0) temp.push(jsonObj);
     					jsonObj = {};
     				})
     				$scope.accountSummaryData[0].totalWorking = workingCount;
     				$scope.seatsData = temp;
     			
     			}else if(filterValue == "reserved"){
     				var temp = [];
 					var reservedCount = 0;
 					var jsonObj = {};
     				angular.forEach(dataToFilter, function(item, index){
     					jsonObj.networkId = item.networkId;
     					jsonObj.seats = [];
     					angular.forEach(item.seats, function(seat, seatIndex){
     						if(seat.seatStatus == 'reserved'){
     							reservedCount = reservedCount+1;
     							jsonObj.seats.push(seat);
     						}
     					})
     					if(jsonObj.seats.length > 0) temp.push(jsonObj);
     					jsonObj = {};
     				})
     				$scope.seatsData = temp;
     				
     			}else{
     				$scope.seatsData = filterAccountSummaryData;
     			}
    			 
    			 if($scope.seatsData[0].seats.length > $scope.mainPage.pagination.itemPerPage){
 					$scope.mainPage.pagination.enabled = true;
 					$scope.mainPage.pagination.totalPages = Math.ceil($scope.seatsData[0].seats.length/$scope.mainPage.pagination.itemPerPage);
 				}
             };
             
             
        	/*// Set Seat Filter
        	$scope.filterAccountData = function(filterValue){
        		// Resetting Pagination
        		 $scope.mainPage.pagination ={
        	        			'enabled': false,
        	        			'currentPage': 1,
        	        			'startPage': 1,
        	        			'itemPerPage': 10,
        	        			'totalPages': 0
        	        			};
    			$log.info("AccountSummaryCtrl:filterAccountSummaryData invoked");
    			$scope.mainPage.filter.seatType = filterValue;
    			var filterAccountSummaryData = OrderManagementService.getOrderMgmtDataCache("networkidsummarydata");
    			var dataToFilter = [];
    			angular.copy(filterAccountSummaryData, dataToFilter);
    			
    			if(filterValue == "working"){				
    				
    				var temp = [];
					var workingCount = 0;
					var jsonObj = {};
    				angular.forEach(dataToFilter, function(item, index){
    					jsonObj.networkId = item.networkId;
    					jsonObj.seats = [];
    					angular.forEach(item.seats, function(seat, seatIndex){
    						if(seat.seatStatus == 'working'){
    							workingCount = workingCount+1;
    							jsonObj.seats.push(seat);
    						}
    					})
    					if(jsonObj.seats.length > 0) temp.push(jsonObj);
    					jsonObj = {};
    				})
    				$scope.accountSummaryData[0].totalWorking = workingCount;
    				$scope.networkIdSummaryData = temp;
    			
    			}else if(filterValue == "reserved"){
    				var temp = [];
					var reservedCount = 0;
					var jsonObj = {};
    				angular.forEach(dataToFilter, function(item, index){
    					jsonObj.networkId = item.networkId;
    					jsonObj.seats = [];
    					angular.forEach(item.seats, function(seat, seatIndex){
    						if(seat.seatStatus == 'reserved'){
    							reservedCount = reservedCount+1;
    							jsonObj.seats.push(seat);
    						}
    					})
    					if(jsonObj.seats.length > 0) temp.push(jsonObj);
    					jsonObj = {};
    				})
    				$scope.networkIdSummaryData = temp;
    				
    			}else{
    				$scope.networkIdSummaryData = filterAccountSummaryData;
    			}
    			
    			if($scope.networkIdSummaryData.length > $scope.mainPage.pagination.itemPerPage){
					$scope.mainPage.pagination.enabled = true;
					$scope.mainPage.pagination.totalPages = Math.ceil($scope.networkIdSummaryData.length/$scope.mainPage.pagination.itemPerPage);
				}
    		
        	};*/
        	
        	// Fetch Seat Data and update in Cache
        	//Reload Seat data when seat gets modified
        	function reloadSeatData(seatId){
        		$scope.startSpinner();
    			
    			var inputData = {};
    			inputData.seatId = seatId;
    			OrderManagementService.readSeat(inputData).then(
    					function(success) {
    						$log.info("AccountSummaryCtrl: reload seatData : "+ angular.toJson(success));
    						angular.copy(success, $scope.selectedProduct);
    						//$scope.selectedProduct = success;
    						if(!angular.isDefined($scope.selectedProduct.moniker)){
    							$scope.selectedProduct.moniker = 'Unassigned';
    						}
    						//$scope.updateAccountSummaryCache($scope.selectedProduct);
    						//$scope.updateNetworkIdSummaryCache($scope.selectedProduct);
    						if(angular.isDefined($scope.selectedProduct.moniker) && 
    			        			$scope.selectedProduct.moniker != '' && $scope.selectedProduct.moniker != 'Unassigned'){
    			        			//$scope.pageSection.searchReserveTN = 'tnReservedSection';
    			        			$scope.availableTNSection = false;
    			        			// TN available to configure
    			        			$scope.pageSection.manageService = 'configService';
    			        			$scope.readCourier();
			        		}else{
			            		$scope.loadStateData();
			            		//$scope.pageSection.searchReserveTN = 'searchSection';
			            		$scope.availableTNSection = false;
			            		// No TN Available to configure
			        			$scope.pageSection.manageService = 'configServiceNA';
			        		}
    						if(angular.isDefined($scope.selectedProduct.networkId)){
    						//set selected network id
	    						$scope.selectedNetworkId = $scope.selectedProduct.networkId[0];
	    						$scope.selectedNetworkIdToAssign.value = $scope.selectedProduct.networkId[0];
	    						getCurrentNetworkIdUsers();
    						}
    						
    					},
    					function(error) {							
    						$log.info("AccountSummaryCtrl: reload seatData Error: "+angular.toJson(error));
    						if (angular.isDefined(error.data.responseMessage)
									&& (error.data.responseMessage != null || error.data.responseMessage != "")) {
    							$scope.setMessages([ {"type" : 2,"message" : error.data.responseMessage} ]);
							} else {
								$scope.setMessages([ {"type" : 2,"message" : "Failed to retrieve Seat Information"} ]);
							}
    						
		            		$scope.loadStateData();
		            		//$scope.pageSection.searchReserveTN = 'searchSection';
		            		$scope.availableTNSection = false;
		            		// No TN Available to configure
		        			$scope.pageSection.manageService = 'configServiceNA';
    						
    					})['finally'](function() {
    						$scope.stopSpinner();
    					});
    		};
    		
    		
    		//Update AccountSummaryCache
    		$scope.updateAccountSummaryCache = function(inputData){
    			if(angular.isDefined($scope.accountSummaryData[0]) && angular.isDefined($scope.accountSummaryData[0].seats)){
        			angular.forEach($scope.accountSummaryData[0].seats, function(item, index){
        				if(item.seatId == inputData.seatId){
        					$scope.accountSummaryData[0].seats[index] = inputData;
        				}
                    }) 
    			}
    			
    		};
    		
    		//Update Seat Network Id Summary Cache
    		/*$scope.updateNetworkIdSummaryCache = function(inputData){
            	var updated = false;
            	for (var index = 0; index < $scope.networkIdSummaryData.length; index++) {
	            	for (var index1 = 0; index1 < $scope.networkIdSummaryData[index].seats.length; index1++) {
	            		if ($scope.networkIdSummaryData[index].seats[index1].seatId === inputData.seatId) {
	            			$scope.networkIdSummaryData[index].seats[index1] = inputData;
	            			updated = true;
	            			break;
	            		}
	            	}
	            	if (updated) {
	            		break;
	            	}
            	}
            	
            	OrderManagementService.setOrderMgmtDataCache("networkidsummarydata", $scope.networkIdSummaryData);
    		};*/
    		
    		// Create Network User
    		
    		
    		
    		$scope.isNetworkIdSelected = function() {
    			return (angular.isDefined($scope.selectedNetworkId) && $scope.selectedNetworkId != '-NA_NWID');
    		};
    		
    		$scope.getNetworkIdName = function() {
    			return $scope.isNetworkIdSelected() ? $scope.selectedNetworkId : 'This is the Group Name for the Seat';
    		};

    		$scope.createNetworkUser = function() {
    			$log.info("AccountSummaryCtrl createNetworkUser invoked"+ $scope.createNW.username);
    			$scope.startSpinner();
    			var inputData = {};
    			inputData.username = $scope.createNW.username + oauthDomainConfig.network;
    			inputData.emailAddress = $scope.createNW.email;
    			inputData.seatId = $scope.selectedProduct.seatId;
    			// Already NetworkId associated with Seat
    			if(angular.isDefined($scope.selectedProduct.networkId)){
    				inputData.networkId = angular.isArray($scope.selectedProduct.networkId) ? $scope.selectedProduct.networkId[0] : $scope.selectedProduct.networkId;
    			}
    			else{
    				if(angular.isDefined($scope.createNW.selectedOption) && $scope.createNW.selectedOption === "existingGroup"){
    					inputData.networkId = $scope.createNW.selectedNetworkId;
    				}
    				else if (angular.isDefined($scope.createNW.selectedOption) && $scope.createNW.selectedOption === "newGroup"){
    					inputData.customerLabel = $scope.createNW.newNetworkId;
    				}
    			}
    			if(angular.isDefined(inputData.emailAddress) && angular.isDefined(inputData.username) && angular.isDefined(inputData.networkId) || angular.isDefined(inputData.customerLabel)){
    			 var networkIDResponse = OAuthServices.createNetworkUser(inputData);
    			networkIDResponse.then(
					function(successData) {
						editEmailIndex = [];
						$log.info('AccountSummaryCtrl::createNetworkID method:: Success '
										+ angular.toJson(successData));
						$scope.changePageSection('viewEditPage','');
						reloadSeatData($scope.selectedProduct.seatId);
						//OAuthServices.setAccountDataInCache("networkIdsUserList", false);
						getNetworkIdUserData();
						$scope.hideSection('inviteUser');
						$scope.setMessages([ {"type" : 0,"message" : 'Email invitation sent to '+ $scope.createNW.email+'. Please inform user to check and confirm.'} ]);
					},
					function(errorData) {
						$scope.errLbl=false;
						$scope.showNetworkIDList = false;
						$scope.showCreateNetworkIDform = false;
						$scope.CreateNetworkIDSuccessMsgflag = false;
						$scope.CreateNetworkIDFailureMsgflag = true;
						if (angular.isDefined(errorData.responseMessage)
								&& errorData.responseMessage != null && errorData.responseMessage != "") {
							$scope.setMessages([ {"type" : 2,"message" : errorData.responseMessage} ]);
						} else {
							$scope.setMessages([ {"type" : 2,"message" : $rootScope.genericErrorMessage} ]);
						}
						$log.info('AccountSummaryCtrl::createNetworkID method:: Error '+angular.toJson(errorData));
					})['finally'](function() {
						$scope.stopSpinner();
						$log.info("Inside finally block :createNetworkUser() method");
						// Code to reset the create network form
						$scope.createNW={};
						$scope.networkUser.form.$setPristine();
						$scope.networkUser.form.$setUntouched();
					});
    			}
    		};
    		
    		var editEmailIndex = [];    		
    		$scope.editEmail = function(index) {
    			editEmailIndex.push(index);
    		}
    		
    		$scope.enableEmailEdit = function(index) {
    			return (editEmailIndex.indexOf(index) != -1);
    		}
    		
    		$scope.cancelEditEmail = function(invitationIndex) {
    			var index = editEmailIndex.indexOf(invitationIndex);
				if (index > -1) {
					editEmailIndex.splice(index, 1);
				}	
    		}
    		
    		//resend invite
    		$scope.resendUserInvite = function(user, invitationIndex) {
			$log.info("AccountSummaryCtrl resendUserInvite invoked");
			$scope.startSpinner();
			var inputData = {};
			inputData.username = user.username;
			inputData.accountName = user.accountName;
			inputData.platformUsername = user.platformUsername;
			inputData.emailAddress = user.emailAddress;
			
			var networkIDResponse = OAuthServices.updateEmailResendInvite(inputData);
			networkIDResponse.then(
				function(successData) {
					var index = editEmailIndex.indexOf(invitationIndex);
					if (index > -1) {
						editEmailIndex.splice(index, 1);
					}					
					$log.info('AccountSummaryCtrl::createNetworkID method:: Success '
									+ angular.toJson(successData));
					$scope.setMessages([ {"type" : 0,"message" : 'Email invitation sent to '+ inputData.emailAddress +'. Please inform user to check and confirm.'} ]);
				},
				function(errorData) {
					if (angular.isDefined(errorData.responseMessage)
							&& errorData.responseMessage != null && errorData.responseMessage != "") {
						$scope.setMessages([ {"type" : 2,"message" : errorData.responseMessage} ]);
					} else {
						$scope.setMessages([ {"type" : 2,"message" : $rootScope.genericErrorMessage} ]);
					}
					$log.info('AccountSummaryCtrl::createNetworkID method:: Error '+angular.toJson(errorData));
				})['finally'](function() {
					$scope.stopSpinner();
				});
		};
    		
    		// Search Moniker Based on State, City / NPA / NPANXX
    		$scope.srchMnkrSelected = {};
    		$scope.srchMnkr = {};
    		$scope.setMonikerSrchState = function(item) {
    			$scope.srchMnkrSelected.State = item;
    			loadCityData($scope.srchMnkrSelected.State);
    			$log.info("AccountSummaryCtrl Item " + item);
    		};
    		$scope.setMonikerSrchCity = function(item) {
    			$scope.srchMnkrSelected.City = item;
    			$log.info("AccountSummaryCtrl Item " + item);
    		};
    		
    		// Search Moniker - Load States
    		$scope.loadStateData = function() {
    			$log.info("Loading State and City");

    			var statesArrResponse = MonikerService.readState();
    			statesArrResponse
    					.then(
    							function(successData) {
    								$log
    										.info('AccountSummaryCtrl::loadStateData method:: Success '
    												+ angular
    														.toJson(successData));
    								$scope.statesArr = successData.states;
    								$log.info(angular.toJson($scope.statesArr));

    							},
    							function(errorData) {
    								$log
    										.info('AccountSummaryCtrl::loadStateData method:: Error ');
    							})
    		};

    		// Search Moniker - Load Cities
    		var loadCityData = function(state) {
    			$log.info("Loading State and City");

    			var statesArrResponse = MonikerService.readCity(state);
    			statesArrResponse
    					.then(
    							function(successData) {
    								$log
    										.info('AccountSummaryCtrl::loadCityData method:: Success '
    												+ angular
    														.toJson(successData));
    								$scope.citiesArr = successData.cities;
    								$log.info(angular.toJson($scope.citiesArr));

    							},
    							function(errorData) {
    								$scope.citiesArr = [];
    								$log
    										.info('AccountSummaryCtrl::loadCityData method:: Error ');
    							})
    		};
    		
    		
    		$scope.criteriaFound = true;
    		$scope.searchMoniker = function() {
    			
    			if ((angular.isDefined($scope.srchMnkrSelected.State) && angular
    					.isDefined($scope.srchMnkrSelected.City))
    					|| angular.isDefined($scope.srchMnkr.NPA)
    					|| angular.isDefined($scope.srchMnkr.NXX)) {
    				$scope.criteriaFound = true;
    				$scope.startSpinner();
    				$scope.selectedMonikerToReserve.value = '';
    				$scope.searchReserveTNPage = {
    		        		'pagination':{
    		        			'enabled': false,
    		        			'currentPage': 1,
    		        			'startPage': 1,
    		        			'itemPerPage': 5,
    		        			'totalPages': 0
    		        		}        		
    		        };
    				$log.info("AccountSummaryCtrl searchMoniker() invoked for State "
    								+ angular.toJson($scope.srchMnkrSelected.State)
    								+ " ,City: "+ $scope.srchMnkrSelected.City
    								+ " ,NPA: "	+ $scope.srchMnkr.NPA
    								+ " ,NXX: " + $scope.srchMnkr.NXX);
    				// $log.info("MonikerService");
    				var searchInputData = {};
    				if ($scope.srchMnkrSelected.State != undefined)
    					searchInputData.state = $scope.srchMnkrSelected.State;
    				if ($scope.srchMnkrSelected.City != undefined)
    					searchInputData.city = $scope.srchMnkrSelected.City;
    				if ($scope.srchMnkr.NPA != undefined)
    					searchInputData.NPA = $scope.srchMnkr.NPA;
    				if ($scope.srchMnkr.NXX != undefined)
    					searchInputData.NXX = $scope.srchMnkr.NXX;
    				
    				searchInputData.limit = accountSummaryConfig.monikerSearchCount;

    				var monikrSrchResponse = MonikerService.searchMoniker(searchInputData);
    				monikrSrchResponse.then(
    								function(successData) {
    									$scope.availableTNSection = true;
    									$log.info('AccountSummaryCtrl::searchMoniker method:: Success '+ successData);
    									$scope.monikrSrchResponse = successData.availablePhoneNumbers;
    									if ($scope.monikrSrchResponse == undefined || $scope.monikrSrchResponse.length <= 0) {
    								  	//	ga('send', 'event', 'Phone Number Search', 'No Phone Number Found', JSON.stringify(searchInputData));
    										$scope.setMessages([ {
												"type" : 0,
												"message" : 'No Phone Number Found ' + JSON.stringify(searchInputData)
											} ]);
    										$scope.availableTNSection = false;
    									}
    									if($scope.monikrSrchResponse && $scope.monikrSrchResponse.length > $scope.searchReserveTNPage.pagination.itemPerPage){
    				    					$scope.searchReserveTNPage.pagination.enabled = true;
    				    					$scope.searchReserveTNPage.pagination.totalPages = Math.ceil($scope.monikrSrchResponse.length/$scope.searchReserveTNPage.pagination.itemPerPage);
    				    				}
    								},
    								function(errorData) {
    									$scope.availableTNSection = false;
    									if (angular.isDefined(errorData.responseMessage)
    											&& errorData.responseMessage != null && errorData.responseMessage != "") {
    		    							$scope.setMessages([ {"type" : 2,"message" : errorData.responseMessage} ]);
    									} else {
    										$scope.setMessages([ {"type" : 2,"message" : $rootScope.genericErrorMessage} ]);
    									}
    									$log.info('AccountSummaryCtrl::searchMoniker method:: Error ');
    								})['finally'](function() {
    									$scope.stopSpinner();
    		    					});
    			} else {
    				$scope.criteriaFound = false;
    			}
    		};
    		
    		// Moniker Reserve Button
    		$scope.selectedMonikerToReserve = {'value':''};
    		/*$scope.onResMnkrSelect = function(resMoniker) {
    			$scope.selectedMonikerToReserve = resMoniker;
    			$log.info("Reserved Moniker1: " + resMoniker);

    		};*/

    		$scope.reserveMoniker = function() {
    			$log.info("Moniker to be reserved: "+ $scope.selectedMonikerToReserve.value);

    			if (angular.isUndefined($scope.selectedMonikerToReserve.value)
    					|| $scope.selectedMonikerToReserve.value == ''
    					|| $scope.selectedMonikerToReserve.value == null) {
    				$scope.showErrorForReserveMoniker = true;
    			}

    			if (angular.isDefined($scope.selectedMonikerToReserve.value)) {
    				$log.info("AccountSummaryCtrl ReserveMoniker() invoked for Moniker "
    								+ angular.toJson($scope.selectedMonikerToReserve.value));
    				$log.info("AccountSummaryCtrl ReserveMoniker() invoked for Moniker: SeatId: "
    								+ angular.toJson($scope.selectedProduct.seatId));
    				$scope.startSpinner();
    				var monikerReserveData = {};
    				monikerReserveData.moniker = $scope.selectedMonikerToReserve.value;
    				monikerReserveData.seatId = $scope.selectedProduct.seatId;
    				var monikerReserveResponse = MonikerService.reserveMoniker(monikerReserveData);
    				monikerReserveResponse.then(
    								function(successData) {
    									$log.info('AccountSummaryCtrl::reserveMoniker method:: Success '+ angular.toJson(successData));
    									$scope.monikerReserveResponse = successData;
    									$scope.selectedProduct.moniker = successData.phoneNumber;
    									$scope.pageSection.manageService = 'configService';
    									$scope.pageSection.searchReserveTN = '';
    									$scope.selectedMonikerToReserve.value = '';
    									$scope.pageSection.assignTN = false;
    									//$scope.selectedProduct.seatStatus = "reserved";
    									reloadSeatData($scope.selectedProduct.seatId);
    										$scope.setMessages([ {
												"type" : 0,
												"message" : 'TN is reserved succesfully for this seat'
											} ]);
    								},
    								function(errorData) {
    									if (angular.isDefined(errorData.responseMessage)
    	    									&& errorData.responseMessage != null && errorData.responseMessage != "") {
    										$scope.monikerReservedFailureText = "Failed to reserve moniker ["+ errorData.responseMessage + "]";
    									} else {
    										$scope.monikerReservedFailureText = "Failed to reserve moniker.";
    									}
    									$scope.setMessages([ {
    												"type" : 2,
    												"message" : $scope.monikerReservedFailureText
    											} ]);
    									$log.info('AccountSummaryCtrl::reserveMoniker method:: Error ');
    								})['finally'](function() {
    									$scope.stopSpinner();
    		    					});
    			}

    			$scope.searchMonikerScreen = false;
    			$scope.searchResultScreen = false;
    		};
    		
    		$scope.listOfServices = [ 'SMS Service' ];
    		
    		$scope.selectService = function(selectedServRowId, selectedService) {
    			$scope.selectedServRowId = selectedServRowId;
    			$scope.selectedService = selectedService;
    		};
    		
    		// Read Courier
    		$scope.mngService = {};
    		$scope.readCourier = function() {
    			$scope.mngService = {};
    			$scope.mngService.servEnabledString = '...';
    			$scope.mngService.msgStoreString = '...';
    			$scope.mngService.readCouriesSuccess = false;
    			
    			if($scope.selectedProduct.seatStatus == 'working' || $scope.selectedProduct.seatStatus == 'reserved'){
 	    			if ($scope.selectedProduct.moniker	&& $scope.selectedProduct.moniker != 'Unassigned') {
 	    				$scope.startSpinner();
	    				var inputData = {};
	    				inputData.phoneNumber = $scope.selectedProduct.moniker;
	    				inputData.productName = 'courier-basic';
	    				inputData.seatId = $scope.selectedProduct.seatId;
	    				inputData.networkId = accountInfoData.network_id;
	    				var readCourierResponse = CourierService.readCourier(inputData);
	    				readCourierResponse.then(
	    						function(successData) {
	    							$log.info('AccountSummaryCtrl::readCourier method API Call:: Success '+ angular.toJson(successData));
	    							$scope.mngService.servEnabled = successData.serviceDetails.enabled;
	    							$scope.mngService.servEnabledOriginal = successData.serviceDetails.enabled;
	    							$scope.mngService.msgStore = successData.serviceDetails.messageSaveStore;
	    							$scope.mngService.msgStoreOriginal = successData.serviceDetails.messageSaveStore;
	    							$scope.mngService.servEnabledString = $scope.mngService.servEnabled ? 'Enabled' : 'Disabled';
	    			    			$scope.mngService.msgStoreString = $scope.mngService.msgStore ? 'Enabled' : 'Disabled';
	    			    			$scope.mngService.readCouriesSuccess = true;
	    						},
	    						function(errorData) {
	    							$log.info('AccountSummaryCtrl::readCourier method API Call:: Error ');
	    							$scope.mngService.servEnabled = false;
	    							$scope.mngService.servEnabledOriginal = false;
	    							$scope.mngService.msgStore = false;
	    							$scope.mngService.msgStoreOriginal = false;
	    							$scope.mngService.servEnabledString = '--';
	    			    			$scope.mngService.msgStoreString = '--';
	    						})['finally'](function() {
	    							$scope.stopSpinner();
		    					});
	
	    			}
    			}

    		};
    		
    		// Delete Courier/Service
    		$scope.deleteCourier = function() {
    			$log.info(" deleteCourier - Moniker: "+ $scope.selectedProduct.moniker);
    			if ($scope.selectedProduct.moniker) {
    				$scope.startSpinner();
    				var inputData = {};
    				inputData.phoneNumber = $scope.selectedProduct.moniker;
    				inputData.productName = 'courier-basic';
    				$log.info("deleteCourier:"+ $scope.selectedProduct.productName);
    				inputData.seatId = $scope.selectedProduct.seatId;
    				inputData.networkId = accountInfoData.network_id;
    				var deleteCourierResponse = CourierService.deleteCourier(inputData);
    				deleteCourierResponse.then(
    								function(successData) {
    									$log.info('AccountSummaryCtrl::deleteCourier method API Call:: Success '+ angular.toJson(successData));
    									$scope.pageSection.configureService = false;
    									$scope.setMessages([ {"type" : 0,"message" : 'Courier has been deleted successfully'} ]);
    									//$scope.reloadAccountSummaryData();
    									reloadSeatData($scope.selectedProduct.seatId);
    									// $scope.mngService.msgStore =
    									// successData.serviceDetails.messageSaveStore;
    								},
    								function(errorData) {
    									$log.info('AccountSummaryCtrl::deleteCourier method API Call:: Error ');
    									
    									if (angular.isDefined(errorData.responseMessage)
    	    									&& errorData.responseMessage != null && errorData.responseMessage != "") {
    		    							$scope.setMessages([ {"type" : 2,"message" : errorData.responseMessage} ]);
    									} else {
    										$scope.setMessages([ {"type" : 2,"message" : $rootScope.genericErrorMessage} ]);
    									}
    									// $log.info(errorData.data.responseMessage.errorMessage);
    								})['finally'](function() {
    									$scope.stopSpinner();
    		    					});
    			}

    		};
    		
    		
    		// Save or Update Courier Service
    		$scope.saveCourierServices = function() {
    			$log.info("Selected Moniker "+ $scope.selectedProduct.moniker+ "ServiceEnabled: "
    					+ $scope.mngService.servEnabled + " MsgStore: "+ $scope.mngService.msgStore);
    			var inputData = {};
    			inputData.phoneNumber = $scope.selectedProduct.moniker;
    			inputData.productName = 'courier-basic';
    			inputData.seatId = $scope.selectedProduct.seatId;
    			inputData.networkId = accountInfoData.network_id;
    			inputData.enabledService = $scope.mngService.servEnabled;
    			inputData.messageSaveStore = $scope.mngService.msgStore;
    			
    			if ($scope.selectedProduct.seatStatus == 'working') {
    				$scope.startSpinner();
    				var updateCourierResponse = CourierService.updateCourier(inputData);
    				updateCourierResponse.then(
    					function(successData) {
    						$log.info('AccountSummaryCtrl::updateCourier method API Call:: Success '+ angular.toJson(successData));
    						$scope.mngServiceSuccessMesssage = true;
    						$scope.pageSection.activateService = '';
    						$scope.configureServiceSection = false;
    						$scope.pageSection.configureService = false;
    						reloadSeatData($scope.selectedProduct.seatId);
    						$scope.setMessages([ {
								"type" : 0,
								"message" : 'Configuration changes are updated succesfully.'
							} ]);
    					},
    					function(errorData) {
    						$scope.mngServiceFailureMesssage = true;
    						$scope.configureServiceSection = false;
    						$scope.mngService.servEnabled = $scope.mngService.servEnabledOriginal;
    						$scope.mngService.msgStore = $scope.mngService.msgStoreOriginal;
    						if (angular.isDefined(errorData.responseMessage)
    								&& errorData.responseMessage != null && errorData.responseMessage != "") {
    							$scope.mngServiceFailureMesssageText = errorData.responseMessage;
    						} else {
    							$scope.mngServiceFailureMesssageText = $rootScope.genericErrorMessage;
    						}
							$scope.setMessages([ {
										"type" : 2,
										"message" : $scope.mngServiceFailureMesssageText
									} ]);
    						$log.info('AccountSummaryCtrl::updateCourier method API Call:: Error ');
    					})['finally'](function() {
    						$scope.stopSpinner();
    					});

    			} else {
    				$scope.startSpinner();
    				var createCourierResponse = CourierService.createCourier(inputData);
    				createCourierResponse.then(
    						function(successData) {
    							$log.info('AccountSummaryCtrl::createCourier method API Call:: Success '
    											+ angular.toJson(successData));

    							reloadSeatData($scope.selectedProduct.seatId);
    							$scope.configureServiceSection = false;
    							$scope.pageSection.configureService = false;
    							$scope.setMessages([ {
    								"type" : 0,
    								"message" : 'Configuration changes are added succesfully.'
    							} ]);
    						},
    						function(errorData) {
    							$scope.mngServiceFailureMesssage = true;
    							$scope.configureServiceSection = false;
        						$scope.mngService.servEnabled = $scope.mngService.servEnabledOriginal;
        						$scope.mngService.msgStore = $scope.mngService.msgStoreOriginal;
    							if (angular.isDefined(errorData.responseMessage) 
    									&& errorData.responseMessage != "" && errorData.responseMessage != null) {
    								$scope.mngServiceFailureMesssageText = errorData.responseMessage;
    							} else {
    								$scope.mngServiceFailureMesssageText = $rootScope.genericErrorMessage;
    							}
    							$scope.setMessages([ {
									"type" : 2,
									"message" : $scope.mngServiceFailureMesssageText
								} ]);
    							$log.info('AccountSummaryCtrl::createCourier method API Call:: Error ');
    						})['finally'](function() {
    							$scope.stopSpinner();
	    					});
    			}
    		};
    		$scope.seatDeleted = false;
    		// Delete a Seat
    		$scope.deleteSeat = function() {    			
    			var inputData = {};
    			inputData.headers = {};
    			inputData.params = {};
    			$log.info("AccountSummaryCtrl deleteSeat: invoked");
    			inputData.headers.SeatId = $scope.selectedProduct.seatId;
    			inputData.headers.UseMock = "false";
    			$scope.startSpinner();
    			var deleteSeatResponse = OrderManagementService.deleteSeat(inputData);
    			deleteSeatResponse.then(
    							function(successData) {
    								$log.info('AccountSummaryCtrl::deleteSeat method:: Success '+ angular.toJson(successData));
    								$scope.reloadAccountSummaryData();
    								//$scope.deleteSeatFromNetworkSummary();
    								
    								$scope.setMessages([{"type" : 0,"message" : "Seat Deleted successfully."} ]);
    								$scope.seatDeleted = true;
    								/*$timeout(function(){
    									$location.path("/seatDetails");
    								},3000);*/
    								
    							},
    							function(errorData) {
    								$log.info('AccountSummaryCtrl::deleteSeat method:: Error ');
    								if (angular.isDefined(errorData.responseMessage)
        									&& errorData.responseMessage != "" && errorData.responseMessage != null) {
    									$scope.setMessages([ {"type" : 2,"message" : errorData.responseMessage} ]);
    								} else {
    									$scope.setMessages([ {"type" : 2,"message" : "Failed to delete Seat"} ]);
    								}
    							})['finally'](function() {
    								$scope.stopSpinner();
    							});

    		};
    		// Reload Account Summary
    		$scope.reloadAccountSummaryData = function() {
    			$scope.startSpinner();
    			var inputData = {};
    			if($scope.selectedNetworkId != '-NA_NWID')	inputData.networkId = $scope.selectedNetworkId;
    			OrderManagementService.readAccount(inputData).then(
    					function(success) {
    						$scope.accountSummaryData = success;
    						$scope.accountSummaryData = OrderManagementService.setOrderMgmtDataCache("accountsummarydata", success);
    						$log.info("AccountSummaryCtrl: reload accountSummaryData : "+ angular.toJson($scope.accountSummaryData));
    					})['finally'](function() {
    						$scope.stopSpinner();
						});

    			};
    			// Remove Seat from Cache
    			/*$scope.deleteSeatFromNetworkSummary = function(){
    				angular.forEach($scope.networkIdSummaryData, function(item, index){
    					if(item.networkId == $scope.selectedNetworkId){
    						angular.forEach(item.seats, function(seat, seatIndex){
    							if(seat.seatId == $scope.selectedProduct.seatId){
    								item.seats.splice(seatIndex, 1);
    							}
    						})
    					}    							
    				})
    			};*/
    			//When network id assigned to seat, move seat from Unassigned to network id
    			//update network summary cache
    			/*$scope.updateSeatInNetworkSummaryCache = function(networkId, seat){
        			if(angular.isDefined($scope.networkIdSummaryData)){
        				for(var i=0; i<$scope.networkIdSummaryData.length; i++){
    	    				if(angular.isDefined($scope.networkIdSummaryData[i].networkId) &&
    	    					$scope.networkIdSummaryData[i].networkId == networkId){
    	    					$scope.networkIdSummaryData[i].seats.push(seat);
    	    					$log.info("seat updated in network summary cache");
    	    				}else if(angular.isDefined($scope.networkIdSummaryData[i].networkId) &&
    	    					$scope.networkIdSummaryData[i].networkId == '-NA_NWID'){
    	    					for(var j=0; j<$scope.networkIdSummaryData[i].seats.length; j++){
    	    						if($scope.networkIdSummaryData[i].seats[j].seatId == seat.seatId){
    	    							$scope.networkIdSummaryData[i].seats.splice(j, 1);
    	    							$log.info("seat removed from unassinged netword id");
    	    						}
    	    					}
    	    				}
    	    			}
        			}else{
        				//$scope.getNetworkIdSummaryData($scope.accountSummaryData);
        			}
        		}*/
    		
    		// Assign a Seat to selected Network ID    			
    		$scope.assignNetworkIdToSeat = function() {
    			if ($scope.selectedNetworkIdToAssign.selectedOption === 'existingGroup') {
	    			if (angular.isUndefined($scope.selectedNetworkIdToAssign.value)
	    					|| $scope.selectedNetworkIdToAssign.value == ''
	    					|| $scope.selectedNetworkIdToAssign.value == null) {
	    				$scope.setMessages([ {
							"type" : 2,
							"message" : 'Please Select a Network Id.'
						} ]);
	    			}
    			} else if ($scope.selectedNetworkIdToAssign.selectedOption === 'newGroup') {
    				if (angular.isUndefined($scope.selectedNetworkIdToAssign.newNetworkId)
	    					|| $scope.selectedNetworkIdToAssign.newNetworkId == ''
	    					|| $scope.selectedNetworkIdToAssign.newNetworkId == null) {
	    				$scope.setMessages([ {
							"type" : 2,
							"message" : 'Please Provide a Network Id.'
						} ]);
	    			}
    			}

    			if (($scope.selectedNetworkIdToAssign.selectedOption === 'existingGroup' && angular.isDefined($scope.selectedNetworkIdToAssign.value))
    				|| ($scope.selectedNetworkIdToAssign.selectedOption === 'newGroup' && angular.isDefined($scope.selectedNetworkIdToAssign.newNetworkId))) {
    				//$scope.startSpinner();
    				$rootScope.$broadcast("inProgressStart"); 
	    			var seatAssociationInput = {};
	    			seatAssociationInput.seatId = $scope.selectedProduct.seatId;
	    			seatAssociationInput.oldNetworkId = angular.isDefined($scope.selectedProduct.networkId) ? $scope.selectedProduct.networkId : ''; 
	    			
	    			if ($scope.selectedNetworkIdToAssign.selectedOption === 'existingGroup') {
	    				seatAssociationInput.networkId = $scope.selectedNetworkIdToAssign.value;
	    			} else if ($scope.selectedNetworkIdToAssign.selectedOption === 'newGroup') {
	    				seatAssociationInput.customerLabel = $scope.selectedNetworkIdToAssign.newNetworkId;
	    			}
	    			
	    			var serviceCall = ($scope.selectedNetworkIdToAssign.selectedOption === 'existingGroup') ? 
	    								(angular.isDefined($scope.selectedProduct.networkId) ? OrderManagementService.updateNetworkId(seatAssociationInput)  : OrderManagementService.createNetworkId(seatAssociationInput)) 
	    												: OAuthServices.createNetworkId(seatAssociationInput); 
	    			serviceCall.then(
	    				function(success) {
	    					//$scope.updateSeatInNetworkSummaryCache((($scope.selectedNetworkIdToAssign.selectedOption === 'existingGroup') ? $scope.selectedNetworkIdToAssign.value : $scope.selectedNetworkIdToAssign.newNetworkId), $scope.selectedProduct);
	    					//$scope.selectedNetworkIdToAssign.value = '';
	    					reloadSeatData($scope.selectedProduct.seatId);
	    					$scope.pageSection.viewEditPage = '';
/*	    					if(!$rootScope.isNetworkUser) { 
	    						$scope.getNetworkUsersList(seatAssociationInput.networkId);
	    					}
*/	    					$scope.selectedNetworkId = seatAssociationInput.networkId;
	    					$scope.selectedNwIdIndex = undefined;
							$scope.setMessages([ {
								"type" : 0,
								"message" : 'Network Id assigned successfully.'
							} ]);
							$rootScope.$broadcast("inProgressEnd");
	    				}, function(error) {
	    					$scope.setMessages([ {
	    						"type" : 2,
	    						"message" : 'Failed to assign the Network Id.'
	    					} ]);
	    					$rootScope.$broadcast("inProgressEnd");
	    				}
	    			)['finally'](function() {
	    				//$scope.stopSpinner();
						$scope.hideSection('assignGroup');
						$rootScope.$broadcast("inProgressEnd");
					});
    			}
    		};
    		
    		// Pagination - Next Button for Account Summary page
    		$scope.clickNext = function(){
    			//$scope.hideDetails();
    			$scope.mainPage.pagination.startPage=$scope.mainPage.pagination.startPage + $scope.mainPage.pagination.navPageCount;
    			$scope.mainPage.pagination.currentPage = $scope.mainPage.pagination.startPage;
    			
    		};
    		
    		//Pagination - Previous Button for Account Summary page
    		$scope.clickPrevious = function(){
    			$scope.mainPage.pagination.startPage=$scope.mainPage.pagination.startPage - $scope.mainPage.pagination.navPageCount;
    			$scope.mainPage.pagination.currentPage = $scope.mainPage.pagination.startPage;
    			
    		};
    		
    		//Pagination - Next Button for Search Moniker page
    		$scope.clickNextMoniker = function(){
    			$scope.searchReserveTNPage.pagination.startPage = $scope.searchReserveTNPage.pagination.startPage+5;
    			$scope.searchReserveTNPage.pagination.currentPage = $scope.searchReserveTNPage.pagination.startPage;
    			
    			//$scope.mainPage.pagination.startPage=$scope.mainPage.pagination.startPage+ 5;
    			//$scope.mainPage.pagination.currentPage = $scope.mainPage.pagination.startPage;
    			
    		};
    		
    		// Pagination - Previous Button for Search Moniker page
    		$scope.clickPreviousMoniker = function(){
    			$scope.searchReserveTNPage.pagination.startPage = $scope.searchReserveTNPage.pagination.startPage-5;
    			$scope.searchReserveTNPage.pagination.currentPage = $scope.searchReserveTNPage.pagination.startPage;
    			//$scope.mainPage.pagination.startPage=$scope.mainPage.pagination.startPage- 5;
    			//$scope.mainPage.pagination.currentPage = $scope.mainPage.pagination.startPage;
    			
    		};
    		
    		// Get List of Available Network ID/Group Name
    		/*$scope.availableNetworkIds = [];
    		$scope.searchAvailbleNetworkIds = function() {
    			$rootScope.$broadcast("inProgressStart");
    			OAuthServices.getNetworkIdList().then(
         				function(successData) {
      	        		   $log.info('AccountSummaryCtrl::networkIds :: Success '+angular.toJson(successData));
      	        		   $scope.availableNetworkIds = successData;
      	        		   $scope.changePageSection('viewEditPage','assignNetworkIdSection');
      	        	   },
      	        	   function(error) {
      	        		   $log.info('AccountSummaryCtrl::networkIds method:: Error '+angular.toJson(error.data));
      	        		   if (angular.isDefined(error.data.responseMessage)
									&& (error.data.responseMessage != null || error.data.responseMessage != "")) {
  	        		   		$scope.setMessages([ {"type" : 2,"message" : error.data.responseMessage} ]);
							} else {
								$scope.setMessages([ {"type" : 2,"message" : "Authorization Denied"} ]);
							}
      	        		  	$scope.availableNetworkIds = [];
      	        	   }		       		
         		)['finally'](function(){
    					$rootScope.$broadcast("inProgressEnd");
    				});   			
    		}*/
    		
    		//Set NetworkId List
    		function getNetworkIdList (){
    			$scope.availableNetworkIds = [];
    			$scope.networkIdUserData = OrderManagementService.getOrderMgmtDataCache("networkidUserData");
    			if(angular.isDefined($scope.networkIdUserData)){
    				for(var i=0; i<$scope.networkIdUserData.length; i++) {
    					if ($scope.networkIdUserData[i].status === 'active')
    					$scope.availableNetworkIds.push($scope.networkIdUserData[i].networkId);
        			}
    			} 
    			if($scope.availableNetworkIds.length <= 0){
    				$scope.selectedNetworkIdToAssign.selectedOption = "newGroup";
    				$scope.createNW.selectedOption = "newGroup";
    			}
    		};
    		
    		
    		// Fetch UserList based on NetworkId
    		$scope.getUserList = function(networkId){
    			if(angular.isDefined(networkId) && angular.isDefined($scope.networkIdUserData) && angular.isArray($scope.networkIdUserData)) {
    				var userArr = $filter('filter')($scope.networkIdUserData, {networkId: networkId}, true);
    				if(angular.isDefined(userArr) && userArr.length >0){
    					return userArr[0].Users;
    				}
    				return [];
    				//return $filter('filter')($scope.networkIdUserData, {networkId: networkId});
    			}
    			else{
    				return [];
    			}    			
    		};
    		
    		
    		// Fetch UserList based on NetworkId
    		$scope.getPendingUserList = function(seatId){
    			if(angular.isDefined(seatId) && angular.isDefined($scope.networkIdUserData) && angular.isArray($scope.networkIdUserData)) {
    				var userArr = $filter('filter')($scope.networkIdUserData, {seatId: seatId}, true);
    				if(angular.isDefined(userArr) && userArr.length >0){
    					return userArr[0].Users;
    				}
    				return [];
    				//return $filter('filter')($scope.networkIdUserData, {networkId: networkId});
    			}
    			else{
    				return [];
    			}    			
    		};
    		
    		//Fetch UserList for Current Seat-NetworkId
    		function getCurrentNetworkIdUsers(){
    			var networkId = $scope.selectedProduct.networkId[0];
    			$scope.currentSeatUserList =[];
    			if(angular.isDefined(networkId) && angular.isDefined($scope.networkIdUserData) && angular.isArray($scope.networkIdUserData)) {
    				var userArr = $filter('filter')($scope.networkIdUserData, {networkId: networkId}, true);
    				if(angular.isDefined(userArr) && userArr.length >0){
    					$scope.currentSeatUserList =  userArr[0].Users;
    				}
    			}   			
    		};
    		
    		// Fetch UserList for a NetworkId
    		function getUsersForNetworkId(){
    			/*if(OrderManagementService.getOrderMgmtDataCache("networkidUserData")){
    				$scope.networkIdUserData = OrderManagementService.getOrderMgmtDataCache("networkidUserData");
    				return;
    			}*/
    			$scope.networkIdUserData =[];
    			$scope.startSpinner();
     	      	var inputData = {};
     	      	inputData.networkId = accountInfoData.network_id;
     	      	nwUserResponse = OAuthServices.getUsersByNetworkId(inputData);
     	      	nwUserResponse.then(
     				function(successData) {
	     					$log.info('AccountSummaryCtrl::getNetworkUsersList method:: Success '+angular.toJson(successData));
	     					$scope.networkIdUserData.push({"networkId": accountInfoData.network_id, "Users" :successData});  
	     					OrderManagementService.setOrderMgmtDataCache("networkidUserData", $scope.networkIdUserData);	     				
		                },
		                function(error) {
		                    $log.info('AccountSummaryCtrl::getNetworkUsersList method:: Error '+angular.toJson(error));
		                }
     	      		)['finally'](function(){
     	      			$scope.stopSpinner();
     	      		});       		
        		
        		
    		};
    		
    		// Fetch NetworkId List - Fetch UserList for each NetworkId
    		// Create a Map [NetworkId -UserList- UserDetails]
    		function getNetworkIdUserData (){
    				$scope.startSpinner();
        			// Fetch NetworkId List
            		$scope.networkIdUserData =[];
            		 var requestList = [];
            		 // Get Pending Users List 
            		 var inputData = {};
            		 inputData.domain = "consumer";
            		 requestList.push(OAuthServices.getNetworkIdList());        		 
            		 requestList.push(OAuthServices.getAllNetworkIdUsers(inputData));
            		 requestList.push(OAuthServices.getPendingInvitedUsers(inputData));
                    		 
            		 asyncCall(requestList,
     	       				function error(results) {
     	       					$log.info('AccountSummaryCtrl getUsersByNetworkId Some error occurred', angular.toJson(results));
     	       					$scope.stopSpinner();
     	       				},
     	                    function success(results) {
     	       					$log.info('AccountSummaryCtrl getUsersByNetworkId Success', angular.toJson(results));
     	       					var networkIdList = results[0];
     	       					var allUserList = results[1];
     	       					var pendingUserList = results[2];
     	       					var combinedUserList = [];
     	       					if (angular.isDefined(networkIdList) && angular.isArray(networkIdList)) {
	     	       					for(var i=0; i<networkIdList.length; i++){
	     	       						var networkId = networkIdList[i];
	     	       						var users =[];
	     	       						
	     	       						if (angular.isDefined(allUserList) && angular.isArray(allUserList)) {
		     	       						for(var j=0; j < allUserList.length; j++){
		     	       							if(angular.isDefined(allUserList[j].networkIds) && networkId === allUserList[j].networkIds[0]){
		     	       								allUserList[j].status="Active";
		     	       								users.push(allUserList[j]); 	       								
		     	       							}
		     	       						}
	     	       						}
	     	       						
	     	       						if (angular.isDefined(pendingUserList) && angular.isArray(pendingUserList)) {
		     	       						for(var k=0; k < pendingUserList.length; k++){
		    	       							if(angular.isDefined(pendingUserList[k].networkIds) && networkId === pendingUserList[k].networkIds[0]){
		    	       								pendingUserList[k].status="Pending";
		    	       								users.push(pendingUserList[k]); 	       								
		    	       							}
		    	       						}
	     	       						}
	     	       						
	     	       						combinedUserList.push({"networkId": networkId, "Users" :users, "status":'active', "seatId":[]});
	     	       					}
     	       					}
     	       					
     	       				if (angular.isDefined(pendingUserList) && angular.isArray(pendingUserList)) {
 	       						for(var k=0; k < pendingUserList.length; k++) {
 	       							var matchFound = false;
 	       							for(var l = 0; l < combinedUserList.length; l++) {
 	       								if(angular.isDefined(pendingUserList[k].networkIds) && combinedUserList[l].status === 'pending' && combinedUserList[l].networkId === pendingUserList[k].networkIds[0]) {
 	       									pendingUserList[k].status="Pending";
 	       									combinedUserList[l].Users.push(pendingUserList[k]);
 	       									if (combinedUserList[l].seatId.indexOf(pendingUserList[k].seatId) != -1) {
 	       										combinedUserList[l].seatId.push(pendingUserList[k].seatId)
 	       									}
 	       									matchFound = true;
 	       									break;
 	       								}
 	       							}
	       							
 	       							if (!matchFound) {
 	       								pendingUserList[k].status="Pending";
 	       								combinedUserList.push({"networkId": pendingUserList[k].networkIds[0], "Users" :[pendingUserList[k]], "status":'pending', "seatId":[pendingUserList[k].seatId]});       								
 	       							}
	       						}
	       					}
     	       					
		     	       			
     	       					
     	       					$scope.networkIdUserData = combinedUserList;
     	       					OrderManagementService.setOrderMgmtDataCache("networkidUserData", $scope.networkIdUserData);
     	       					
	     	       				// populating users data in seat
		     	       			if (angular.isDefined($scope.seatsData) && angular.isArray($scope.seatsData) && angular.isDefined($scope.seatsData[0]) && angular.isDefined($scope.seatsData[0].seats) && angular.isArray($scope.seatsData[0].seats) && $scope.seatsData[0].seats.length > 0) {
		     	       				for (var index = 0; index < $scope.seatsData[0].seats.length; index++) {
		     	       					if (angular.isDefined($scope.seatsData[0].seats[index].networkId) && angular.isArray($scope.seatsData[0].seats[index].networkId)) {
		     	       						$scope.seatsData[0].seats[index].users = $scope.getUserList($scope.seatsData[0].seats[index].networkId[0]);
		     	       					}
		     	       				}
		     	       			}	
     	       					
     	       					$scope.stopSpinner();
     	       				}
     	                 )

    		};
    		
    		function asyncCall(listOfPromises, onErrorCallback, finalCallback) {

                listOfPromises  = listOfPromises  || [];
                onErrorCallback = onErrorCallback || angular.noop;
                finalCallback   = finalCallback   || angular.noop;

                // Create a new list of promises
                // that can "recover" from rejection
                var newListOfPromises = listOfPromises.map(function (promise) {
                  return promise['catch'](function (reason) {

                    // First call the `onErrroCallback`
                    onErrorCallback(reason);
                    return reason;

                  });
                });

                return $q.all(newListOfPromises).then(finalCallback);
            };
    		
    		$scope.showSection = function(section) {
    			$scope.pageSection[section] = true;
    		};
    		
    		$scope.hideSection = function(section) {
    			$scope.pageSection[section] = false;
    		};
    		
    		$scope.filter = function() {
    		    $timeout(function() { //wait for 'filtered' to be changed
    		        $scope.mainPage.pagination.totalPages = Math.ceil($scope.filtered.length/$scope.mainPage.pagination.itemPerPage);
    		        if($scope.mainPage.pagination.currentPage > $scope.mainPage.pagination.totalPages){
    		        	$scope.mainPage.pagination.currentPage = 1;
    		        	$scope.mainPage.pagination.startPage = 1;
    		        }
    		    }, 10);
    		};
    		
    		$scope.deleteNetworkUser = function(networkId){
    			$scope.startSpinner();
    	      	var inputData = {};
    	      	inputData.username = networkId.username;
    	      	deleteResponse = OAuthServices.deleteUser(inputData);
    	      	deleteResponse.then(
    						function(successData) {
    							$log.info('ManageUsersCtrl::deleteNetworkId method:: Success ');
    							$scope.deleteNetworkIdSuccessMsg = successData;
    							$scope.setMessages([{"type":0, "message":"Network User Deleted successfully."}]);    							
    							getNetworkIdUserData();
    		                },
    		                function(errorData) {
    		                    $log.info('ManageUsersCtrl::deleteNetworkId method:: Error ');
    		                    if (angular.isDefined(errorData)) {
    		                    	$scope.setMessages([{"type":2, "message":errorData.responseMessage}]);
    		                    } else {
    		                    	$scope.setMessages([{"type":2, "message":"Failed to delete Network User."}]);
    		                    }
    		                }
    	      		)['finally'](function(){
    	      			$scope.stopSpinner();
    	      		});
          	};
 
        $scope.productServiceConfig = 
        	[	
        	 	{	productName: "courier-basic",
        			services: [{label: 'Send SMS', url: '#/product_services/products_sendmessage'}, {label: 'Read SMS', url: '#/product_services/products_readmessage'}, {label:'Delete SMS', url:'#/product_services/products_deletemessage'}]
        		},
        		{	productName: "clinkbox",
        			services: [{label: 'Send Message', url: ''}, {label: 'Read Message', url: ''}, {label: 'Delete Message', url: ''}]
        		},
        		{	productName: "SpeakEasy",
        			services: [{label: 'SpeakEasy Message', url: ''}, {label: 'SpeakEasy Message', url: ''}, {label: 'SpeakEasy Message', url: ''}]
        		}
        	];
        	
        	$scope.showServiceMenu = false;
          	
        	$scope.getDisplayList=function(productName){
          		//$log.info("productName:" +angular.toJson(selectedProduct));
          		//$scope.selectedProduct = [{productName: "courier-basic"},{productName: "SpeakEasy"},{productName: "clinkbox"}];
          		/*$scope.serviceArr =[];
          		$log.info("$scope.selectedProduct:" +$scope.selectedProduct.length);
          		for(i=0; i<$scope.selectedProduct.length; i++){
          			for(j=0; j< $scope.productServiceConfig.length;j++){
          				if($scope.productServiceConfig[j].productName == $scope.selectedProduct[i].productName){
          					for(k=0; k<$scope.productServiceConfig[j].services.length;k++ )
          						$scope.serviceArr.push($scope.productServiceConfig[j].services[k]);
          			    //$scope.serviceArr = angular.toJson($scope.serviceArr);
          				}
          			}
          			
          		}
          		*/
          		$scope.serviceArr =[];
          		for(j=0; j< $scope.productServiceConfig.length;j++){ 
          			if($scope.productServiceConfig[j].productName == productName){
          				for(k=0; k<$scope.productServiceConfig[j].services.length;k++ ){
          					$scope.serviceArr.push($scope.productServiceConfig[j].services[k]);
          				}
          			}
          		}
          		$log.info("$scope.serviceArr:" +angular.toJson($scope.serviceArr));
          		$scope.showServiceMenu = true;
          	}
        	
        	$scope.displayDeleteUserToolTip = true;
    		
    		$scope.showDeleteUserToolTip = function() {
    			$scope.displayDeleteUserToolTip = true;
    		}
    		
    		$scope.hideDeleteUserToolTip = function() {
    			$scope.displayDeleteUserToolTip = false;
    		}
    		
    		$scope.getServiceStatus = function (selectedProduct, productName) {
    			if (angular.isDefined(productName) && angular.isDefined(selectedProduct.products) && angular.isArray(selectedProduct.products)) {
    				for (var index = 0; index < selectedProduct.products.length; index++) {
    					if (selectedProduct.products[index].productName === productName) {
    						return (selectedProduct.products[index].productStatus === 'working') ? 'active' : 'pending';
    					}
    				}
    			}
    			
    			return "unconfigured";
    		}
    		
    		$scope.resetMonikerSearchCriteria = function () {
    			$scope.srchMnkr = {};
    		}
    		
    		$scope.searchSeat = function (row) {
    	        return (angular.lowercase(row.moniker).indexOf(angular.lowercase($scope.filterSeats) || '') !== -1 ||
    	                angular.lowercase(row.seatId).indexOf(angular.lowercase($scope.filterSeats) || '') !== -1 ||
    	                angular.lowercase(row.customerId).indexOf(angular.lowercase($scope.filterSeats) || '') !== -1 ||
    	                searchInArray(row.networkId, $scope.filterSeats) ||
    	                searchInArray(row.users, $scope.filterSeats, 'username'));
    	    };
    	    
    	    function searchInArray(array, value, propertyName) {
    	    	var propertyNameFound = angular.isDefined(propertyName) && propertyName != '';
    	    	
    	    	if (angular.isDefined(array) && angular.isArray(array) && array.length > 0) {
    	    		for (var index = 0; index < array.length; index++) {
    	    			if (propertyNameFound) {
	    	    			if (angular.lowercase(array[index][propertyName]).indexOf(angular.lowercase(value) || '') !== -1) {
	    	    				return true;
	    	    			}
    	    			} else {
    	    				if (angular.lowercase(array[index]).indexOf(angular.lowercase(value) || '') !== -1) {
	    	    				return true;
	    	    			}
    	    			}
    	    		}
    	    	}    	    	
    	    	return false;
    	    }
    }];
	
		

AccountSummaryCtrl.resolve ={
    content:['ContentService',
        function(ContentService){
            return ContentService.fetchContent("Common,Account_Summary");
        }]
    
};
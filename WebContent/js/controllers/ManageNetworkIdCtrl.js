/**
 * Created by vk0014941 on 11/12/2015.
 */

//Controller for Manage Account Section

var ManageNetworkIdCtrl = [
    '$scope',
    '$rootScope',
    '$log',
    'MonikerService',
    'OAuthServices',
    'OrderManagementService',
    '$location',
    '$window',
    'oauthDomainConfig',
    '$q',
    'content',
    '$filter',
    '$timeout',
    function (
        $scope,
        $rootScope,
        $log,
        MonikerService,
        OAuthServices,
        OrderManagementService,
        $location,
        $window,
        oauthDomainConfig,
        $q,
        content,
        $filter,
        $timeout
    ) {
    	
        $log.info("ManageNetworkIdCtrl invoked");
        $scope.content = content;
        $scope.page = 'mainPage';
        $scope.networkIdSearch = '';
     // Pagination	
    	$scope.mainPage = {
        		'pagination':{
        			'enabled': false,
        			'currentPage': 1,
        			'startPage': 1,
        			'itemPerPage': 10,
        			'totalPages': 0
        		}        		
        };
    	$scope.sortData ={
    		sortType: "networkId",
    		sortReverse: false
    	};
    	$scope.apiCount = 0;
    	$scope.stopSpinner = function(){  
        	$scope.apiCount = $scope.apiCount -1;
        	if($scope.apiCount === 0){
        		$rootScope.$broadcast("inProgressEnd");
        	}
        };
    	
      //get account info data
        var accountInfoData = OAuthServices.getAccountDataFromCache("accountInfoData");
        
        if(angular.isDefined(accountInfoData.groups) && accountInfoData.groups.indexOf('network') != -1){
        	$scope.page = 'NoAccessPage';
        }
     
        $scope.networkIdsList = OrderManagementService.getOrderMgmtDataCache("networkidUserData");
        /*$scope.networkIds = [];	
        $scope.ButtonText = [];*/ 
        if(angular.isUndefined($scope.networkIdsList) || !($scope.networkIdsList)) {
        	$scope.networkIdsList = [];
        	$rootScope.$broadcast("inProgressStart");
    		$scope.apiCount = $scope.apiCount + 1;
			// Fetch NetworkId List
    		$scope.networkIdUserData =[];
    		 var requestList = [];
    		 // Get Pending Users List 
    		 var inputData = {};
    		 inputData.domain = "consumer";
    		 requestList.push(OAuthServices.getNetworkIdList());        		 
    		 requestList.push(OAuthServices.getAllNetworkIdUsers(inputData));
    		 requestList.push(OAuthServices.getPendingInvitedUsers(inputData));
            		 
    		 asyncCall(requestList,
	       				function error(results) {
	       					$log.info('AccountSummaryCtrl getUsersByNetworkId Some error occurred', angular.toJson(results));
	       					$scope.stopSpinner();
	       				},
	                    function success(results) {
	       					$log.info('AccountSummaryCtrl getUsersByNetworkId Success', angular.toJson(results));
	       					var networkIdList = results[0];
	       					var allUserList = results[1];
	       					var pendingUserList = results[2];
	       					for(var i=0; i<networkIdList.length; i++){
	       						var networkId = networkIdList[i];
	       						var users =[];
	       						for(var j=0; j<allUserList.length; j++){
	       							if(angular.isDefined(allUserList[j].networkIds) && networkId === allUserList[j].networkIds[0]){
	       								allUserList[j].status="Active";
	       								users.push(allUserList[j]); 	       								
	       							}
	       						}
	       						for(var k=0; k<pendingUserList.length; k++){
       							if(angular.isDefined(pendingUserList[k].networkIds) && networkId === pendingUserList[k].networkIds[0]){
       								pendingUserList[k].status="Pending";
       								users.push(pendingUserList[k]); 	       								
       							}
       						}
	       						networkId.activeUserCount = ''+getNetworkUserCount(networkId);
	       						$scope.networkIdUserData.push({"networkId": networkId, "Users" :users, "status":'active', "seatId":[]});
	       					}
	       					OrderManagementService.setOrderMgmtDataCache("networkidUserData", $scope.networkIdUserData);
	       					$scope.networkIdsList = $scope.networkIdUserData;
	       					$scope.stopSpinner();
	       				}
	                 )
     	}else{
     		var networkIds = $scope.networkIdsList;
     		$scope.networkIdsList = [];
     		
     		for (i = 0; i < networkIds.length; i++) {
     			if (networkIds[i].status === 'active') {    				
     				networkIds[i].activeUserCount = ''+getNetworkUserCount(networkIds[i]);
     				$scope.networkIdsList.push(networkIds[i])
     			}
     		}     	
     	} 
        
        //Fetch Users Count based on NetworkId
		function getNetworkUserCount(networkId){
			if(angular.isDefined(networkId) && angular.isDefined(networkId.Users) && networkId.Users.length >0) {
				var activeUserArr = $filter('filter')(networkId.Users, {status: 'Active'}, true);
				return activeUserArr.length;
			}
			else{
				return 0;
			}			
		};
		
		 // Go to Manage User Page
        $scope.gotoManageUser = function(networkId){        	   	
        	$location.path('/usersServices/' + networkId);
        };
     
     /* $scope.index = 0;
      $scope.networkUsers = [];
      $scope.networkuserdetails = false;*/
      
     /* $scope.selectNetworkId = function(networkId, index){
    	  if($scope.ButtonText[index] == 'E'){
    		  $scope.ButtonText[index] = "C";
    	  }else{
    		  $scope.ButtonText[index] = "E";
    	  }
    	  $scope.selectedNetworkId = networkId;
    	  angular.forEach($scope.networkIds, function(nwObj, index){
      		if(nwObj.name == networkId){
      			$scope.index = index;
      			$scope.networkUsers = nwObj.value;
      		}
      	  })
    	  if($scope.ButtonText[index] == "E" && (angular.isUndefined($scope.networkUsers) || $scope.networkUsers.length <= 0 || $scope.networkUsers =='')){
    		$rootScope.$broadcast("inProgressStart");
  	      	var inputData = {};
  	      	inputData.networkId = networkId;
  	      	nwUserResponse = OAuthServices.getUsersByNetworkId(inputData);
  	      	nwUserResponse.then(
  						function(successData) {
  							$log.info('ManageNetworkIdCtrl::getUsersByNetworkId method:: Success ');
  							$scope.networkIds[$scope.index].value = successData;
  							OAuthServices.setAccountDataInCache("networkIdsUserList", $scope.networkIds);
  		                },
  		                function(error) {
  		                    $log.info('ManageNetworkIdCtrl::getUsersByNetworkId method:: Error '+angular.toJson(error));
  		                    if (angular.isDefined(error.data)) {
  		                    	$scope.setMessages([{"type":2, "message":error.data.responseMessage}]);
  		                    } else {
  		                    	$scope.setMessages([{"type":2, "message":"Failed to get user for selected Network ID."}]);
  		                    }
  		                }
  	      		)['finally'](function(){
  	      			$rootScope.$broadcast("inProgressEnd");
  	      		});

    	  }
    	
      };*/
      
      /*$scope.hideDetails = function(){
    	  for(var i=0; i<$scope.ButtonText.length; i++){
    		  $scope.ButtonText[i] = "C";
    	  }
    	}*/
        
     
      /*function updateNetworkIdCache(networkId, networkUser){
    	  $log.info("networkId="+networkId+" networkUser="+angular.toJson(networkUser));
    	  angular.forEach($scope.networkIds, function(nwObj, index){
        		if(nwObj.name == networkId){
        			var networkUsersArray = nwObj.value;
        			$log.info("networkUsersArray="+angular.toJson(networkUsersArray));
        			angular.forEach(networkUsersArray, function(userObject, itemIndex){
        				if(userObject.username == networkUser.username){
        					networkUsersArray.splice(itemIndex, 1);
        					$scope.networkIds[index].value = networkUsersArray;
            				$log.info(itemIndex+": "+userObject.username+"===== "+networkUser.username);
            				OAuthServices.setAccountDataInCache("networkIdsUserList", $scope.networkIds);
            				$log.info("cache updated: "+angular.toJson($scope.networkIds));
        				}
        			});
        		 }
    	  });
    	  
      }*/
      
      /*$scope.deleteNetworkUser = function(user){
    	    $rootScope.$broadcast("inProgressStart");
	      	var inputData = {};
	      	inputData.username = user.username;
	      	deleteResponse = OAuthServices.deleteUser(inputData);
	      	deleteResponse.then(
						function(successData) {
							$log.info('ManageNetworkIdCtrl::deleteNetworkId method:: Success ');
							$scope.deleteNetworkIdSuccessMsg = successData;
							updateNetworkIdCache($scope.selectedNetworkId, user);
							//deleteUserFromSeat();
							$scope.setMessages([{"type":0, "message":"Network User Deleted successfully."}]);
		                },
		                function(errorData) {
		                    $log.info('ManageNetworkIdCtrl::deleteNetworkId method:: Error ');
		                    if (angular.isDefined(errorData)) {
		                    	$scope.setMessages([{"type":2, "message":errorData.responseMessage}]);
		                    } else {
		                    	$scope.setMessages([{"type":2, "message":"Failed to delete Network User."}]);
		                    }
		                }
	      		)['finally'](function(){
	      			$rootScope.$broadcast("inProgressEnd");
	      		});
      	};*/
      
      /*function deleteUserFromSeat(){
			var inputData = {};			
			inputData.SeatId = $scope.networkIdObj.SeatId;
			inputData.NetworkId = $scope.networkIdObj.NetworkID;
			OrderManagementService.deleteNetworkId(inputData).then(function(success){
				$log.info("ManageNetworkIdCtrl:  OrderManagementService Delete user successful");
	    	    OrderManagementService.readAccount().then(function(success) {
					$log.info("ManageNetworkIdCtrl: delete user from seat successfull: ");
					$scope.accountSummaryData = success;
					populateNWUserData();
				});
	    	   }, function(errorData) {
	    		   $log.info("ManageNetworkIdCtrl: delete user from seat failed: ");
	    		   if (angular.isDefined(errorData)) {
                    	$scope.setMessages([{"type":2, "message":"Failed to remove the association between user and seat [" + errorData.responseMessage + "]."}]);
                    } else {
                    	$scope.setMessages([{"type":2, "message":"Failed to remove the association between user and seat."}]);
                    }
	    		   populateNWUserData();
	    	   });
		};*/
		
		$scope.clickNext = function(){
			$scope.hideDetails();
			$scope.mainPage.pagination.startPage=$scope.mainPage.pagination.startPage+ 5;
			$scope.mainPage.pagination.currentPage = $scope.mainPage.pagination.startPage;
			
		};
		
		$scope.clickPrevious = function(){
			$scope.mainPage.pagination.startPage=$scope.mainPage.pagination.startPage- 5;
			$scope.mainPage.pagination.currentPage = $scope.mainPage.pagination.startPage;
			
		};
		
        $scope.setPage = function(pageId){        	
        	$scope.page = pageId;
      	};

		/*$scope.allUsers = [];
		$scope.allUsersList = [];
		$scope.currentUsers = [];
		$scope.currentUsersList = [];*/
		
		/* 
		 * This function will fetch the users lists 
		 * 1. All the network users based on accountName
		 * 2. Users from the selected network id.
		 */		
		/*$scope.editNetworkUser = function(networkId, pageId){
			var requestList = [];
			$scope.resetAlertMessage();
			$scope.setPage(pageId);
			$scope.selectedNetwork = networkId;
			$rootScope.isAnyUserMoved = true;
			
			var inputData = {};    		
    		inputData.domain = 'network';    		
			requestList.push(OAuthServices.getAllNetworkIdUsers(inputData));
			
			var inputData1 = {};
    		inputData1.networkId = networkId;
  	      	requestList.push(OAuthServices.getUsersByNetworkId(inputData1));
  	      	$rootScope.$broadcast("inProgressStart");
			asyncCall(requestList,
				function error(results) {
					$log.info('Some error occurred', angular.toJson(results));
				},
                function success(results) {
                     $log.info('Results (final):', angular.toJson(results));
             		 $scope.allUsersList = results[0];
             		 $scope.currentUsersList = results[1];

             		 if($scope.allUsersList.length > 0){
             			$scope.allUsers = $scope.allUsersList.slice();
             		 }             		 
             		 if($scope.currentUsersList.length > 0){
                 		 $scope.currentUsers = $scope.currentUsersList.slice();
              		 }
             		 
         			//Removing current(selected) network users from all network users list			
         			for( var i=$scope.allUsers.length - 1; i>=0; i--){
         			 	for( var j=0; j<$scope.currentUsers.length; j++){
         			 	    if($scope.allUsers[i] && ($scope.allUsers[i].username === $scope.currentUsers[j].username)){
         			 	    	$scope.allUsers.splice(i, 1);
         			    	}
         			    }
         			}
                 }
                )['finally'](function(){
                	$rootScope.$broadcast("inProgressEnd");
                });
	    };*/
	    
        /*function asyncCall(listOfPromises, onErrorCallback, finalCallback) {

            listOfPromises  = listOfPromises  || [];
            onErrorCallback = onErrorCallback || angular.noop;
            finalCallback   = finalCallback   || angular.noop;

            // Create a new list of promises
            // that can "recover" from rejection
            var newListOfPromises = listOfPromises.map(function (promise) {
              return promise['catch'](function (reason) {

                // First call the `onErrroCallback`
                onErrorCallback(reason);
                return reason;

              });
            });

            return $q.all(newListOfPromises).then(finalCallback);
        };

        $scope.usersToBeAdded = [];
        $scope.usersToBeRemoved = [];
        
        
         * User will be added into add or remove user list based on the direction (right or left)
         
		$scope.moveItem = function(user, from, to, direction) {
	        var idx=from.indexOf(user);
	        if (idx != -1) {
	            from.splice(idx, 1);
	            to.push(user);
	            if(direction === 'moveRight'){
		            if($scope.currentUsersList.indexOf(user) == -1) {
		            	$scope.usersToBeAdded.push(user.username);
		            	//$scope.usersToBeAdded.push(user);
		            }
		            var id = $scope.usersToBeRemoved.indexOf(user.username); 
	            	if(id != -1)  {
	            		$scope.usersToBeRemoved.splice(id, 1);
	            	}
	            }else if(direction === 'moveLeft'){
		            if($scope.currentUsersList.indexOf(user) != -1){
		            	$scope.usersToBeRemoved.push(user.username);
		            }
		          	var id = $scope.usersToBeAdded.indexOf(user.username); 
		           	if(id != -1){
		           		$scope.usersToBeAdded.splice(id, 1);
		           	}
	            }
	        }
	        enableDisableUserUpdateBtn();	        
	    };

	 
          * All users will be added into add or remove user list based on the direction (right or left)
          
	    $scope.moveAll = function(from, to, direction) {
	        angular.forEach(from, function(user) {
	            to.push(user);
	            if(direction === 'moverightall'){
	            	if($scope.currentUsersList.indexOf(user) == -1) {
		            	$scope.usersToBeAdded.push(user.username);
		            	//$scope.usersToBeAdded.push(user);
		            }	
		            var id = $scope.usersToBeRemoved.indexOf(user.username); 
	            	if(id != -1)  {
	            		$scope.usersToBeRemoved.splice(id, 1);
	            	}
	            }else if(direction === 'moveleftall'){
	            	if($scope.currentUsersList.indexOf(user) != -1){
		            	$scope.usersToBeRemoved.push(user.username);
		            }
		          	var id = $scope.usersToBeAdded.indexOf(user.username); 
		           	if(id != -1){
		           		$scope.usersToBeAdded.splice(id, 1);
		           	}
	            }
	        });
	        from.length = 0;
	        enableDisableUserUpdateBtn();
	    }; 
	    
	    
	     * It will enable Submit button, when user is moved to add remove list.
	     
	    function enableDisableUserUpdateBtn(){
	    	if($scope.usersToBeAdded.length > 0 || $scope.usersToBeRemoved.length > 0){
	    		$rootScope.isAnyUserMoved = false;
	    	}else{
	    		$rootScope.isAnyUserMoved = true;
	    	}
	    };
	    
	    
	     * Added or removed users with selected network id will be send to API.
	     * Whereas added user will be removed from the Existing network Id
	     * subaccountUserListUpdates api accepts networkId, addUsers[] and removeUsers[]
	     
	    $scope.updateNetworkUser = function(){
	    	var inputData = {};
	    	var addUsers = [];
	    	inputData.networkId = $scope.selectedNetwork;
	    	$scope.apiCount = $scope.apiCount +1;

	    	if($scope.usersToBeAdded.length > 0) {
	    		inputData.addUsers = $scope.usersToBeAdded;
	    	}
	    	if($scope.usersToBeRemoved.length > 0) { 
	    		inputData.removeUsers = $scope.usersToBeRemoved;
	    	}
	    	
	    	if($scope.usersToBeAdded.length > 0 || $scope.usersToBeRemoved.length > 0){
	     		$rootScope.$broadcast("inProgressStart");
		    	OAuthServices.subaccountUserListUpdates(inputData).then(
		    	function(success){
					$log.info('ManageNetworkIdCtrl : updateNetworkUser :: successfull ' + angular.toJson(success));
				//	$scope.setMessages([ {"type" : 0,"message" : 'User updated successfully'} ]);
					//Updating networkId user List in cache
					var isLoopConitnue = true;
					angular.forEach($scope.networkIds, function(nwObj, index){
						if(isLoopConitnue){
			        		if(nwObj.name == $scope.selectedNetwork){
			        			$scope.networkIds[index].value = [];
			        			$scope.ButtonText[index] = "C";
			        			isLoopConitnue = false;
			        		}							
						}
		    	   });
				   //Fetching users data and removing it from their existing network id 
					if($scope.usersToBeAdded.length > 0) {
						//removeUsersfromNetworkId($scope.usersToBeAdded)
						readRemoveUserFromNetworkId($scope.usersToBeAdded);
					}
				   //Clearing data from lists	
				   clearEditUserData();	
	    	    	
				}, function(errorData) {
	    		   $log.info("ManageNetworkIdCtrl : updateNetworkUser :: add / remove user failed: ");
	    		   if (angular.isDefined(errorData)) {
	    			   $scope.setMessages([{"type":2, "message":errorData.responseMessage}]);
	                } else {
	                	$scope.setMessages([{"type":2, "message":"Failed to move or remove user from NetworkId."}]);
	                }
	    	    }
	    	    )['finally'](function(){
		     	//	$rootScope.$broadcast("inProgressEnd");
	    	    		$scope.stopSpinner();
	    	    	//	$scope.setPage('mainPage');
				});
	    	}
	    };
	    
	    
	     * Fetching users data from readUser api
	     
	    function readRemoveUserFromNetworkId(users) {
	    	var requestList = [];
	    	$scope.apiCount = $scope.apiCount +1;
	    	
	    	for(var i=0; i<users.length; i++) {
	    		var inputData = {};
	    		inputData.userName = users[i];
	    		requestList.push(OAuthServices.readUser(inputData, false));
	    	}
	    	asyncCall(requestList,
	    		function error(results) {
	    			$log.info('Some error occurred in getUsersData ', angular.toJson(results));	    			
	    		},
	    		function success(results) {
	    			//Removing users from existing networkIds
	    			removeUsersfromNetworkId(results);
	    		}
	    	)['finally'](function(){
	    		$scope.stopSpinner();
	    	});
	    };
	    
	    
	     * Removing users from the existing network id
	     
	    function removeUsersfromNetworkId(removeUsers) {
	    	var requestList = [];
	    	var networkIds = [];
	    	var networkIdsClearFromCache = [];
	    	$scope.apiCount = $scope.apiCount +1;

	    	for(var i=0; i<removeUsers.length; i++) {
	    		networkIds = removeUsers[i].networkIds;
	    		for(var j=0; j<networkIds.length; j++){
	    			if(networkIds[j] != $scope.selectedNetwork){
	    				var inputData = {};
			    		inputData.networkId = networkIds[j];
			    		var username = [];
			    		username.push(removeUsers[i].username);
			    		inputData.removeUsers = username;
			    		requestList.push(OAuthServices.subaccountUserListUpdates(inputData));
			    		networkIdsClearFromCache.push(networkIds[j]);
	    			}
	    		}
	    	}
	    	
	    	asyncCall(requestList,
	    		function error(results) {
	    			$log.info('Some error occurred in removeUsersfromNetworkId ', angular.toJson(results));
	    		},
	    		function success(results) {
	    			//Clearing networkId users from cache
	    			$scope.setMessages([ {"type" : 0,"message" : 'User updated successfully'} ]);
	    			for(var i=0; i<networkIdsClearFromCache.length; i++){
	    				var isLoopConitnue = true;
						angular.forEach(networkIdsClearFromCache[i], function(nwObj, index){
							if(isLoopConitnue){
				        		if(nwObj.name == networkIdsClearFromCache[i]){
				        			$scope.networkIds[index].value = [];
				        			$scope.ButtonText[index] = "C";
				        			isLoopConitnue = false;
				        		}							
							}
			    	   });	    				
	    			}
	    		}
	    	)['finally'](function(){
	    		$scope.stopSpinner();
	    		$scope.setPage('mainPage');
	    	});
	    };*/
	    
	    /*
	     * Clearing variables values
	     */
	    function clearEditUserData(){
			$scope.allUsers = [];
			$scope.allUsersList = [];
	    	$scope.currentUsers = [];
			$scope.currentUsersList = [];
			$scope.usersToBeAdded = [];
	        $scope.usersToBeRemoved = [];   
			$scope.editNetworkId = "";
	    };
	    
	    /*
	     * This function will clear all the scope variable values.
	     *  It will be redirected to manage user page 
	     */  
	    /*$scope.cancelUpdateUser = function(page){
	    	//Clear values from current and all users list
	    	clearEditUserData();
			$scope.resetAlertMessage();
	    	$scope.page = page;
	    };*/
	    
	    $scope.filter = function() {
		    $timeout(function() { //wait for 'filtered' to be changed
		        $scope.mainPage.pagination.totalPages = Math.ceil($scope.filtered.length/$scope.mainPage.pagination.itemPerPage);
		        if($scope.mainPage.pagination.currentPage > $scope.mainPage.pagination.totalPages){
		        	$scope.mainPage.pagination.currentPage = 1;
		        	$scope.mainPage.pagination.startPage = 1;
		        }
		    }, 10);
		};
		
		$scope.search = function (row) {
	        return (angular.lowercase(row.networkId).indexOf(angular.lowercase($scope.networkIdSearch) || '') !== -1 ||
	                angular.lowercase(row.activeUserCount).indexOf(angular.lowercase($scope.networkIdSearch) || '') !== -1);
	    };
	    
		function asyncCall(listOfPromises, onErrorCallback, finalCallback) {

            listOfPromises  = listOfPromises  || [];
            onErrorCallback = onErrorCallback || angular.noop;
            finalCallback   = finalCallback   || angular.noop;

            // Create a new list of promises
            // that can "recover" from rejection
            var newListOfPromises = listOfPromises.map(function (promise) {
              return promise['catch'](function (reason) {

                // First call the `onErrroCallback`
                onErrorCallback(reason);
                return reason;

              });
            });

            return $q.all(newListOfPromises).then(finalCallback);
        };
    }
];

ManageNetworkIdCtrl.resolve ={
		content:['ContentService',
		         function(ContentService){
		             return ContentService.fetchContent("Common,Manage_User");
		         }]
	};

var ManageUsersCtrl = [
    '$scope',
    '$rootScope',
    '$log',
    'OAuthServices',
    'OrderManagementService',
    '$q',
    'content',
    '$routeParams',
    '$filter',
    '$timeout',
    'SplunkService',
    'splunkConfig',
    'envConfig',
    function (
        $scope,
        $rootScope,
        $log,
        OAuthServices,
        OrderManagementService,
        $q,
        content,
        $routeParams,
        $filter,
        $timeout,
        SplunkService,
        splunkConfig,
        envConfig
    ) {
    	 $scope.selectedNetworkId = $routeParams.networkId;
    	 $scope.content = content;
         $scope.tab = 'membersData';
         $scope.mainPage = {
         		'pagination':{
         			'enabled': false,
         			'currentPage': 1,
         			'startPage': 1,
         			'itemPerPage': 10,
         			'totalPages': 0
         		}        		
         };
         $scope.useSplunk= envConfig.useSplunk;
         $scope.usersToAdd = [];
         $scope.usersToRemove = [];
         $scope.networkUserList = [];
         
         $rootScope.breadcrumbs = [{ label:'Groups', url: '#/manageNetworkId'}]; // Setting Breadcrumbs
         $rootScope.pageTitle = $scope.selectedNetworkId;
         
         $scope.apiCount = 0;
     	 $scope.stopSpinner = function(){  
         	$scope.apiCount = $scope.apiCount -1;
         	if($scope.apiCount <= 0) {
        		$scope.apiCount = 0;
        		$rootScope.$broadcast("inProgressEnd");
        	}
         };
         $scope.startSpinner = function(){ 
        	$rootScope.$broadcast("inProgressStart");
          	$scope.apiCount = $scope.apiCount +1;
          };
         
         $scope.setTab = function(tabId){
        	 if(angular.isDefined(tabId)){
        		 $scope.tab = tabId;
        		 if($scope.tab === 'manageMembers'){
        			 getUserData();
        	         getAllUsers();
        		 }
        		 if($scope.useSplunk && $scope.tab === 'usageData'){
                	 try{
                    	 var loginRes = SplunkService.login();	// Login to Splunk Service  
                    	 $log.info("Splunk Login "+angular.toJson(loginRes));
                    	 generateGraph(); 
                	 }catch(err) {
                		 $scope.chartError = true;
                		 $scope.stopSpinner();
        	          }

                 }
        	 }       	 
         };
                              	
         getUserData();
         //getAllUsers();
         
               
        
         
         // Fetch User List
         function getUserData(){
        	 $scope.startSpinner();
        	 var inputData = {};    		
        	 inputData.networkId = $scope.selectedNetworkId;  
        	 OAuthServices.getUsersByNetworkId(inputData).then(
       				function(successData) {
     	        		   $log.info('ManageUsersCtrl::_getUserList :: Success '+angular.toJson(successData));
     	        		  $scope.networkUserList = successData;
     	        		  $scope.networkUserListEdit = angular.copy(successData);
     	        		  updateListForDisplay();
     	        		  if(OrderManagementService.getOrderMgmtDataCache("networkidUserData")){
     	        			  var networkIdUserData = OrderManagementService.getOrderMgmtDataCache("networkidUserData");
     	        			  
     	        			  for (var index = 0; index < networkIdUserData.length; index++) {
     	        				  if (networkIdUserData[index].networkId == $scope.selectedNetworkId) {
     	        					  networkIdUserData[index].Users = [];
     	        					  for (i = 0; i < $scope.networkUserList.length; i++) {
     	        						 $scope.networkUserList[i].status = 'Active';
     	        						 networkIdUserData[index].Users.push($scope.networkUserList[i]);
     	        					  }
     	        					 break;
     	        				  }
     	        			  }
     	        			 OrderManagementService.setOrderMgmtDataCache("networkidUserData", networkIdUserData);
     	        		  }
     	        	   },
     	        	   function(error) {
     	        		   $log.info('ManageUsersCtrl::accountSummaryData method:: Error '+angular.toJson(error.data));
     	        		   	if (angular.isDefined(error.data.responseMessage)
     									&& (error.data.responseMessage != null || error.data.responseMessage != "")) {
     	        		   		//$scope.setMessages([ {"type" : 2,"message" : error.data.responseMessage} ]);
     							} else {
     								//$scope.setMessages([ {"type" : 2,"message" : "Authorization Denied"} ]);
     							}
     	        		   $scope.networkUserList = [];
     	        	   }		       		
       					)['finally'](function(){
       						$scope.stopSpinner();
     				});
         };
        	 
         // Fetch All Users for Account
         function getAllUsers(){
        	 $scope.startSpinner();
        	 var inputData = {};    		
        	 inputData.domain = "consumer";  
        	 OAuthServices.getAllNetworkIdUsers(inputData).then(
       				function(successData) {
     	        		   $log.info('ManageUsersCtrl::_getAllUserList :: Success '+angular.toJson(successData));
     	        		   $scope.allUserList = successData;
     	        		   $scope.allUserListEdit = angular.copy(successData);     	        		   
     	        		   updateListForDisplay();
     	        	   },
     	        	   function(error) {
     	        		   $log.info('ManageUsersCtrl::_getAllUserList method:: Error '+angular.toJson(error.data));
     	        		   	if (angular.isDefined(error.data.responseMessage)
     									&& (error.data.responseMessage != null || error.data.responseMessage != "")) {
     	        		   		$scope.setMessages([ {"type" : 2,"message" : error.data.responseMessage} ]);
     							} else {
     								$scope.setMessages([ {"type" : 2,"message" : "Authorization Denied"} ]);
     							}
     	        	   }		       		
       					)['finally'](function(){
       						$scope.stopSpinner();
     				}); 
         };
         
         // START Members TAB //
         // Delete User
    	 $scope.deleteNetworkUser = function(networkId){
    		$scope.startSpinner();
	      	var inputData = {};
	      	inputData.username = networkId.username;
	      	deleteResponse = OAuthServices.deleteUser(inputData);
	      	deleteResponse.then(
						function(successData) {
							$log.info('ManageUsersCtrl::deleteNetworkId method:: Success ');
							$scope.deleteNetworkIdSuccessMsg = successData;
							$scope.setMessages([{"type":0, "message":"Network User Deleted successfully."}]);
							getUserData();
		                },
		                function(errorData) {
		                    $log.info('ManageUsersCtrl::deleteNetworkId method:: Error ');
		                    if (angular.isDefined(errorData)) {
		                    	$scope.setMessages([{"type":2, "message":errorData.responseMessage}]);
		                    } else {
		                    	$scope.setMessages([{"type":2, "message":"Failed to delete Network User."}]);
		                    }
		                }
	      		)['finally'](function(){
	      			$scope.stopSpinner();
	      		});
      	};
      	
      	// Manage User Screen- Move User from All Users TO Current Group
      	$scope.addUserInGroup = function(user){
      		if(angular.isDefined(user)){
                $scope.usersToAdd.push(user);
                $scope.networkUserListEdit.push(user);
                $scope.allUserListEdit.splice($scope.allUserListEdit.indexOf(user),1);
      		}
      	};
        // Manage User Screen- Move User from Current Group TO All Users
      	$scope.removeUserFromGroup = function(user){
      		if(angular.isDefined(user)){
	      		$scope.usersToRemove.push(user);
	      		$scope.allUserListEdit.push(user);
	      		$scope.networkUserListEdit.splice($scope.networkUserListEdit.indexOf(user),1);      		
      		}
      	};
      	
      	// Remove User from existing group - API is not handling it so temporary code
      	function removeUserFromOldGroup(removeUsers){
      		var requestList = [];
	    	$scope.startSpinner();

	    	for(var i=0; i<removeUsers.length; i++) {
	    		if(angular.isDefined(removeUsers[i].networkIds)){
	    			var networkIds = removeUsers[i].networkIds;
		    		for(var j=0; j<networkIds.length; j++){
		    			if(networkIds[j] != $scope.selectedNetworkId){
		    				var inputData = {};
				    		inputData.networkId = networkIds[j];
				    		var username = [];
				    		username.push(removeUsers[i].username);
				    		inputData.removeUsers = username;
				    		requestList.push(OAuthServices.subaccountUserListUpdates(inputData));
		    			}
		    		}
	    		}	    		
	    	}
	    	
	    	asyncCall(requestList,
	    		function error(results) {
	    			$log.info('Some error occurred in removeUsersfromNetworkId ', angular.toJson(results));
	    		},
	    		function success(results) {
	    			//$scope.setMessages([ {"type" : 0,"message" : 'User updated successfully'} ]);
	    		}
	    	)['finally'](function(){
	    		$scope.stopSpinner();
	    	});
      	};
      	
      	// Handle multiple API calls [ Lost of Promises] and return response once
        function asyncCall(listOfPromises, onErrorCallback, finalCallback) {

            listOfPromises  = listOfPromises  || [];
            onErrorCallback = onErrorCallback || angular.noop;
            finalCallback   = finalCallback   || angular.noop;

            // Create a new list of promises
            // that can "recover" from rejection
            var newListOfPromises = listOfPromises.map(function (promise) {
              return promise['catch'](function (reason) {

                // First call the `onErrroCallback`
                onErrorCallback(reason);
                return reason;

              });
            });

            return $q.all(newListOfPromises).then(finalCallback);
        };
        
        // Manage Users - Update movement of User between Current Group and All Users
      	$scope.updateUsers = function(){
     		
	    	if($scope.usersToAdd.length > 0 || $scope.usersToRemove.length > 0){
	    		$scope.startSpinner();
	     		var inputData = {};
		    	inputData.networkId = $scope.selectedNetworkId;
		    	if($scope.usersToAdd.length > 0) {
		    		inputData.addUsers = [];
		    		for(var i=0; i<$scope.usersToAdd.length; i++){
		    			inputData.addUsers.push($scope.usersToAdd[i].username);
		    		}
		    	}
		    	if($scope.usersToRemove.length > 0) { 
		    		inputData.removeUsers = [];
		    		for(var i=0; i<$scope.usersToRemove.length; i++){
		    			inputData.removeUsers.push($scope.usersToRemove[i].username);
		    		}
		    	}
	     		
		    	OAuthServices.subaccountUserListUpdates(inputData).then(
		    	function(success){
		    		$log.info('ManageNetworkIdCtrl : updateUsers :: successfull ' + angular.toJson(success));
		            getUserData();
		            getAllUsers();		            
		            removeUserFromOldGroup($scope.usersToAdd);	// This function will be removed Once API does it in backend 
		            $scope.usersToAdd = [];
		            $scope.usersToRemove = [];
		            $scope.setMessages([{"type":0, "message":"Sub Account user list updated successfully."}]);
				}, function(errorData) {
	    		   $log.info("ManageNetworkIdCtrl : updateUsers :: add / remove user failed: ");
	    		   if (angular.isDefined(errorData)) {
	    			   $scope.setMessages([{"type":2, "message":errorData.responseMessage}]);
	                } else {
	                	$scope.setMessages([{"type":2, "message":"Failed to move or remove user from NetworkId."}]);
	                }
	    	    }
	    	    )['finally'](function(){
	    	    	$scope.stopSpinner();
				});
	    	}
      	};

      	function updateListForDisplay(){
			 $scope.networkUserListEdit = angular.copy($scope.networkUserList);
			 $scope.allUserListEdit = angular.copy($scope.allUserList);
			 
			//Removing current(selected) network users from all network users list			
  			if (angular.isDefined($scope.allUserListEdit) && angular.isArray($scope.allUserListEdit) 
  					&& angular.isDefined($scope.networkUserListEdit) && angular.isArray($scope.networkUserListEdit)) {
				for( var i=$scope.allUserListEdit.length - 1; i>=0; i--){
	  			 	for( var j=0; j<$scope.networkUserListEdit.length; j++){
	  			 	    if($scope.allUserListEdit[i] && ($scope.allUserListEdit[i].username === $scope.networkUserListEdit[j].username)){
	  			 	    	$scope.allUserListEdit.splice(i, 1);
	  			    	}
	  			    }
	  			}
				
				$scope.allUserListEdit = $scope.allUserListEdit.sort(
								function(a, b) {
									if (a.username.toLowerCase() < b.username.toLowerCase())
									    return -1;
									if (a.username.toLowerCase() > b.username.toLowerCase())
									    return 1;
									return 0;
								}
							);
				$scope.networkUserList = $scope.networkUserList.sort(
								function(a, b) {
									if (a.username.toLowerCase() < b.username.toLowerCase())
									    return -1;
									if (a.username.toLowerCase() > b.username.toLowerCase())
									    return 1;
									return 0;
								}
							);
  			}
      	};
		// Reset ManageUser screen variables 
		$scope.resetManageUsers = function(){
	         $scope.usersToAdd = [];
	         $scope.usersToRemove = [];
	         $scope.allUserListEdit = [];
	         $scope.networkUserListEdit = [];
		};
		
		// END MEMEBRS TAB //
	    
		// START USAGE DATA TAB //
		
		function generateGraph(){
			$scope.startSpinner();
   		    var query="search index=sdf source= "+splunkConfig.dashBoardLogFile
		    	+" Endpoint=Billing earliest="+splunkConfig.spanPeriod+" AccountName="+$rootScope.accountNameForSplunk
		    	+" NetworkId="+$scope.selectedNetworkId+" | timechart span="+splunkConfig.span+" count | where count  > 0 | rename count as \"SMS Count\" | rename _time as Days | eval Days=strftime(Days,\"%d-%b\")";
		    var divId = '#displayChart';	
		    
		    var results = SplunkService.getResult(query, $scope.selectedNetworkId, divId);    	   
				results.then(
	  			   function(success){  					 
	  					if(success.response.fields.length>0 ){
   						   var chart = null; 
	  			    	   var chartToken = splunkjs.UI.loadCharting(splunkConfig.chartJSPath, function() {
	  					   // Once we have the charting code, create a chart and update it.      	  	   					
	  					    		chart = new splunkjs.UI.Charting.Chart($(divId), splunkjs.UI.Charting.ChartType.COLUMN , false);  	  	   			    		
	  					    		splunkjs.UI.ready(chartToken, function() {  	  	   			    			
	  			          			chart.setData(success.response, { "chart.stackMode": "stacked"}); 
	  			          			chart.draw();  	          				
	  			     			});
	  			   			});
	  					}else{  
	  						$scope.chartError = true;
	  	   				}
	  			   },
	  			   function(error){
	  				 $scope.chartError = true;
	  			   })['finally'](function(){
	  				 $scope.stopSpinner();
	  			   });
		};
		
		// END USAGE DATA TAB //
		
      	// Pagination - Next 
      	$scope.clickNext = function(){
			$scope.mainPage.pagination.startPage=$scope.mainPage.pagination.startPage+ 5;
			$scope.mainPage.pagination.currentPage = $scope.mainPage.pagination.startPage;
			
		};
		// Pagination - Previous
		$scope.clickPrevious = function(){
			$scope.mainPage.pagination.startPage=$scope.mainPage.pagination.startPage- 5;
			$scope.mainPage.pagination.currentPage = $scope.mainPage.pagination.startPage;
		};
		// Filter Input for Members Screen
	    $scope.filter = function() {
		    $timeout(function() { //wait for 'filtered' to be changed
		        $scope.mainPage.pagination.totalPages = Math.ceil($scope.filtered.length/$scope.mainPage.pagination.itemPerPage);
		        if($scope.mainPage.pagination.currentPage > $scope.mainPage.pagination.totalPages){
		        	$scope.mainPage.pagination.currentPage = 1;
		        	$scope.mainPage.pagination.startPage = 1;
		        }
		    }, 10);
		};
		
		$scope.displayDeleteUserToolTip = true;
		
		$scope.showDeleteUserToolTip = function() {
			$scope.displayDeleteUserToolTip = true;
		}
		
		$scope.hideDeleteUserToolTip = function() {
			$scope.displayDeleteUserToolTip = false;
		}
    }];
ManageUsersCtrl.resolve ={
		content:['ContentService',
		         function(ContentService){
		             return ContentService.fetchContent("Common,Manage_User");
		         }]
	};
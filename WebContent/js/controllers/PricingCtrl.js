/**
 * Created by vk0014941 on 11/12/2015.
 */

// Controller for Account Summary Section

var PricingCtrl = [
    '$scope',
    '$rootScope',
    '$log',
    'content',
    'pricingConfig',
    'CourierService',
    '$mdDialog', 
    '$mdMedia',
    function (
        $scope,
        $rootScope,
        $log,
        content,
        pricingConfig,
        CourierService,
        $mdDialog, 
        $mdMedia
    ) {
    	$scope.content = content;
    	$scope.pricingConfig=pricingConfig;
        
       
        // if(angular.isDefined($scope.previousPath) && $scope.previousPath ===
		// '/addSeat'){
        $rootScope.breadcrumbs = [{ label:'Add a seat', url: '#/addSeat'}]; // Setting
																			// Breadcrumbs;
        // }
  
        $scope.showBillingRateData = function(ev, group) {        	
        	CourierService.getBillingRates().then(
      				function(successData) {
      					$log.info('PricingCtrl::getBillingRates method:: Success ');
      					
      					
      					/* opening popup on success */
      					var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
      		            $mdDialog.show({
      		              controller: ['$scope', '$mdDialog', function($scope, $mdDialog) {
      		            	  	$scope.billingData = successData.rateList;    		            	  
      		            	    $scope.selectedGroup = group;
      		            	  	
      			            	$scope.answer = function(answer) {
      			            		    $mdDialog.hide(answer);
      			            		  };
      		            		}],
      		              templateUrl: 'partials/fragmentTemplates/pricing/internationalSMS.tmpl.html',
      		              parent: angular.element(document.body),
      		              targetEvent: ev,
      		              clickOutsideToClose:false,
      		              fullscreen: useFullScreen
      		            })
      		            .then(function(answer) {

      		            }, function() {

      		            });
      		            $scope.$watch(function() {
      		              return $mdMedia('xs') || $mdMedia('sm');
      		            }, function(wantsFullScreen) {
      		              $scope.customFullscreen = (wantsFullScreen === true);
      		            });
   	        	   },
   	        	   function(errorData) {
   	        		   	$log.info('PricingCtrl::getBillingRates method:: Error ');
   	        		   	$scope.errors = {};
   	        		   	if(angular.isDefined(errorData.responseMessage) && errorData.responseMessage != null && errorData.responseMessage != ""){
   	        			   $scope.errors.errorMsg = errorData.responseMessage;
   	        		   	}else{
   	        			   $scope.errors.errorMsg = "Failed to retrieve SMS Charges.";   
   	        		   	}
   	        		   	$scope.setMessages([{"type":2, "message":$scope.errors.errorMsg}]);     	        		  
   	        	   }		       		
      		)['finally'](function() {
      			$rootScope.$broadcast("inProgressEnd");
  			});	
        }
    }
];

PricingCtrl.resolve ={
	    content:['ContentService',
	        function(ContentService){
	            return ContentService.fetchContent("Common,Pricing");
	        }]	    
	};
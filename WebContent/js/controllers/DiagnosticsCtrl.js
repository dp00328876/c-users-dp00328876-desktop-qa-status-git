/**
 * Created by vk0014941 on 11/12/2015.
 */

//Controller for Diagnostics Section

var DiagnosticsCtrl = [
    '$scope',
    '$rootScope',
    '$log',
    '$location',
    'MonikerService',
    'OAuthServices',
    'CourierService',
    'apiConfig',
    function (
        $scope,
        $rootScope,
        $log,
        $location,
        MonikerService,
        OAuthServices,
        CourierService,
        apiConfig
    ) {

        $log.info(" Inside DiagnosticsCtrl");
        var accountInfoData = OAuthServices.getAccountDataFromCache("accountInfoData");
	$scope.isNetworkUser = (angular.isDefined(accountInfoData.groups) && accountInfoData.groups.indexOf('network') != -1);
        
        $scope.courierServiceStatusData = {};
        $scope.courierServiceStatusData.headers = {
	        	  "Accept": "application/json",
	        	  "CorrelationID": apiConfig.headers.CorrelationId,
	        	  "AccountName": apiConfig.headers.AccountName
	          };
      
       
        $scope.displayResultSection = false;
        $scope.phoneNumberToTest = false;
        
        $scope.setProductFields = function(productname){
        	if(productname == 'Courier-Basic'){
        		$scope.phoneNumberToTest = true;
        	}
        }
        
        $scope.showTestResultSection = function(){
    		$scope.displayResultSection = true;
        }
        
        $scope.testCourierService = function(){
           $rootScope.$broadcast("inProgressStart");
   	   	   var testCourierData = {};
   	   	   testCourierData.phoneNumber = $scope.courierServiceStatusData.phoneNumber;
 	           $log.info("testCourierService phoneNumber:"+$scope.courierServiceStatusData.phoneNumber);
 	           var requestString = angular.toJson(testCourierData);
 	           //$scope.jsonRequest = JSON.stringify(requestString, null,"    ")
 	           $scope.jsonRequest =  requestString.replace(/,/g, ',\n');
 	           testCourierData.headers = $scope.courierServiceStatusData.headers;
 	           var courierServiceStatusResponse = CourierService.getCourierServiceStatus(testCourierData);
 	           courierServiceStatusResponse.then(
 	                  function(success) {
 	                      $scope.response = success;
 	                      $scope.showTestResultSection();
 	                  },
 	                  function(error) {
 	                	  if (angular.isUndefined($scope.response)){
 	                		  $scope.response = {};
 	                	  }
 	                	  $scope.response.responseStatus = error.status;
 	                      $log.info('ProductCtrl::sendMessage method:: Error ');
 	                    
 	                      if(error.responseMessage!=null && error.responseMessage!=""){	                    	  
 	                    	  $scope.setMessages([{"type":2, "message":error.responseMessage}]);
 	                      }else{
 	                    	  $scope.setMessages([{"type":2, "message":'Diagnostics failed.'}]);	                    	   
 	                      }
 	                  }
 	              )['finally'](function(){
 	                 	$rootScope.$broadcast("inProgressEnd");
                  });
      		};
     		
       		// code to toggle API console (request and response)
       		$scope.showAPIDetailsSection = false;
            	$scope.toggleAPIDetailsSection = function() {
               		$scope.showAPIDetailsSection = !$scope.showAPIDetailsSection;
            	}
      		
     		$scope.go = function (page) {
                $location.path('/' + page);
                
     		};
     	 	// ToDo Fetch this from JSON using OM APIs
			$scope.accountSummaryData = [ {
				title : 'Courier-Basic',
				titleTotal : '4',
				titlePending : '1',
				configureDetails : 'true',
				content : [ {
					SeatName : 'Courier 1',
					SeatID : 'testpub1',
					ProductName : 'courier-basic',
					Status : 'Completed',
					moniker : '3035554590',
					networkData : [ {
						NtworkID : 'RG78999',
						NetworkName : 'Richard Parker1',
						NetworkEmail : 'richard@domain.com',
					}, {
						NtworkID : 'VG78222',
						NetworkName : 'Voris Li1',
						NetworkEmail : 'voli@domain.com',
					}, {
						NtworkID : 'JB22999',
						NetworkName : 'James Bond1',
						NetworkEmail : 'jb@domain.com',
					} ],
					monikerData : [ {
						Moniker : '989-000-9999',
						NetworkID : 'RG55662',
						Status : 'Working',
					}, {
						Moniker : '989-000-9999',
						NetworkID : 'RG55662',
						Status : 'Working',
					}, {
						Moniker : '989-000-9999',
						NetworkID : 'RG55662',
						Status : 'Working',
					} ]
				}],
			}];
    }
];

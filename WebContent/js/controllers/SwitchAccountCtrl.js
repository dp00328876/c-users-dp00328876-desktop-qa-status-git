var SwitchAccountCtrl = [
        '$route',
        '$rootScope',
        '$scope',
        '$log',
        '$location',
        '$routeParams',
        'OAuthServices',
        'PlatformService',
        '$window',
        function(
            $route,
            $rootScope,
            $scope,
            $log,
            $location,
            $routeParams,
            OAuthServices,
            PlatformService,
            $window
        ){
            $log.info("SwitchAccountCtrl.js:: Entering inside");         
            $scope.passThruAccount = $routeParams.accountName;
            
            var inputData = {'accountName':$scope.passThruAccount};
            
            $rootScope.$broadcast("inProgressStart");
            
            PlatformService.switchAccount(inputData)
			.then(
				function (successResponse) {
					/* updating the account information in cache. */
					var tempTokenData = JSON.parse(sessionStorage.getItem('ctl_auth'));
					tempTokenData.AccountName = $scope.passThruAccount;
					sessionStorage.setItem('ctl_auth', JSON.stringify(tempTokenData));
					
					var accountInfoData = OAuthServices.getAccountDataFromCache("accountInfoData");
					accountInfoData.accountName = $scope.passThruAccount;
					OAuthServices.setAccountDataInCache("accountInfoData", accountInfoData);
					
					/* Fetching branding information for the selected Account. */
					PlatformService.getAccount()
						.then(
							function(successData) {
								$log.info("Account Information loaded from platform.");
								$rootScope.$broadcast("inProgressEnd");
								// loading Branding information
					            $rootScope.loadBrandingInfo();
								$location.path('/accountSummary');
								/* reload to update the branding information */
								//setTimeout( function() {$window.location.reload()}, 500);
							},
							function(errorData) {
								$log.info("Failed to load the Account Information from platform.");
								$rootScope.$broadcast("inProgressEnd");
								$location.path('/accountSummary');
							}); 
				},
				function (errorResponse) {
					
				});                	       				
        }
    ];
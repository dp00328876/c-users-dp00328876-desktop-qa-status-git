/**
 * Created by PB0083784 on 11/24/2015.
 */

//Controller for Products Section

'use strict';

var ProductsCtrl = [
    '$scope',
    '$q',
    '$rootScope',
    '$log',
    '$location',
    '$route',
    '$window',
    '$http',
    '$routeParams',
    'CourierService',
    'MonikerService',
    'DumpsterService',
    'OrderManagementService',
    'OAuthServices',
    'AngularCacheService',
    'envConfig',
    'apiConfig',
    'ctlUtils',
    //'allocatedMonikers',
    //'accountSummaryData',
    'content',
    'supportedCountryCallingCodes',
    function (
        $scope,
        $q,
        $rootScope,
        $log,
        $location,
        $route,
        $window,
        $http,
        $routeParams,
        CourierService,
        MonikerService,
        DumpsterService,
        OrderManagementService,
        OAuthServices,
        AngularCacheService,
        envConfig,
        apiConfig,
        ctlUtils,
       // allocatedMonikers,
       // accountSummaryData,
        content,
        supportedCountryCallingCodes
       
    ) {
    	  $scope.errLbl=true;
    	  $scope.seatIdforMonikers = $routeParams.seatId;
    	  $scope.content = content;
    	  $scope.active = false;
          $scope.active1 = true;
          $scope.currentPage = $routeParams.pageId;
          $scope.apiCount = 0;
          $scope.SMSRate=0.0065;
          $scope.IneternationSMSRate=0.0065;
          $scope.productRate=0.99;
          $scope.TNRate=0.99;
          $scope.NationalSMSSent=0;
          $scope.InternationalSMSSent=0;
          $log.info(" Inside ProductsCtrl");          
          
          var productMetaData = [{
                            		"id" : "1",
                            		"name" : "Courier-Basic",
                            		"version" : "1.0",
                            		"legalText" : ""
                            	}                            ];
          
         
          $scope.ShowCountryCodes=false;
          
          $scope.ShowCountryCodesData=function(){
        	      $scope.ShowCountryCodes=true;
        	      $scope.orderSummarySection=false;
          }
          
          $scope.hideCountryCodesData=function(){
	      	  $scope.ShowCountryCodes=false;
	      	  $scope.orderSummarySection=true;
		  }

        if( $location.path() === '/productSendMessage/'+$scope.seatIdforMonikers ||
        		$location.path() === '/productReadMessage/'+$scope.seatIdforMonikers ||
        		$location.path() === '/productDeleteMessage/'+$scope.seatIdforMonikers){
        	$rootScope.breadcrumbs = [{ label:'Seat Details', url: '#/accountSummary/'+$scope.seatIdforMonikers}]; // Setting Breadcrumbs
        	$rootScope.pageTitle = '';
            $rootScope.pageDescription = '';
        }
        
        var OrderMgmtDataCache = AngularCacheService.OrderMgmtDataCache;    //Initializing cache service
        var accountInfoData = OAuthServices.getAccountDataFromCache("accountInfoData");
        $scope.isNetworkUser = (angular.isDefined(accountInfoData.groups) && accountInfoData.groups.indexOf('network') != -1);
        $scope.subscribeToolTipText = $scope.isNetworkUser ? "Login as Administrator to use this feature" : "Manually setup a product";
        $scope.quickstartToolTipText = $scope.isNetworkUser ? "Login as Administrator to use this feature" : "Automatically Setup a product with defaults";
        $scope.consumptionServiceToolTipText = $scope.isNetworkUser ? "Go to Communication Services tab to use this feature" : "Login as Consumer to use this feature";
        
        /**
         * Set cache
         * @param {String} key
         * @param {Object|String} val
         * @returns {Boolean}
         */
        var setOrderMgmtDataCache=function(key,val) {
            if(angular.isDefined(key) && angular.isDefined(val) && key!=="") {
            	OrderMgmtDataCache.put(key,val);
                return(true);
            }
            return(false);
        };

        /**
         * Getting Cache data
         * @param {String} key
         * @returns {Boolean|Object|String}
         */
        var _getOrderMgmtDataCache=function(key) {
            return(OrderMgmtDataCache.get(key) || false);
        };
        
        $scope.stopSpinner = function(){  
        	$scope.apiCount = $scope.apiCount -1;
        	if($scope.apiCount <= 0){
        		$rootScope.$broadcast("inProgressEnd");
        	}
        };
        $scope.startSpinner = function(){ 
        	$rootScope.$broadcast("inProgressStart");
          	$scope.apiCount = $scope.apiCount +1;
          };
        
        $scope.sendMsgData = {};
        $scope.readMsgData = {};
        $scope.deleteMsgData = {};
        $scope.buyProductInput = {};
        $scope.courierServiceStatusData = {};
        $scope.phoneNumberRegex = "/^[0-9]*$/";
        $scope.myDate = new Date();
        $scope.submitOrderButtonEnabled = false;
        $scope.response = {};
        $scope.errors = {};
        $scope.sendMessageSuccessMesssage = true;
  	  	$scope.sendMessageErrorMesssage = false;
  	  	$scope.readMessageSuccessMesssage = true;
	  	$scope.readMessageErrorMesssage = false;
	  	$scope.deleteMessageSuccessMesssage = true;
  	  	$scope.deleteMessageErrorMesssage = false;
  	  	$scope.productshoppingcart = true;
  	  	$scope.orderSuccessMsgSection = false;
  	  	$scope.orderErrorMsgSection = false;
  	  	$scope.orderSummarySection = true;
  	  	$scope.partialOrderSuccessMsgSection = false;
  	  	$scope.buyProductInput.prodQuantity = 1;
  	  	$scope.cartDetails = {};
  	  	$scope.cartDetails.totalQuantity = 0;
  	  	$scope.cartDetails.Products = [];
  	  	$scope.seatDetails = {};
  	  	$scope.seatDetails.Seats = [];
  	  	$scope.index = 0;
     	$scope.updateFlag = false;
     	
  	  	$scope.cartDetails = _getOrderMgmtDataCache("cartdetails");
  	  	$scope.seatDetails = _getOrderMgmtDataCache("seatdetails");
  	  	$log.info("_getOrderMgmtDataCache:"+$scope.cartDetails);
  	  	$log.info("_getOrderMgmtDataCache seatDetails:"+$scope.seatDetails);
  	  	
  	  	if(angular.isUndefined($scope.cartDetails) || !($scope.cartDetails)){
  	  		
  	  		$log.info("_getOrderMgmtDataCache failed:"+$scope.cartDetails);
	  	  	$scope.cartDetails = {};
	  	  	$scope.cartDetails.Products = [];
	  	  	$scope.cartDetails.totalQuantity = 0;
  	  	}
  	  	
  	  	if(angular.isUndefined($scope.seatDetails) || !($scope.seatDetails)){
	  		
	  		$log.info("_getOrderMgmtDataCache seatDetails failed:"+$scope.seatDetails);
	  	  	$scope.seatDetails = {};
	  	  	$scope.seatDetails.Seats = [];
	  	}
  	  	
  	  	if($scope.cartDetails.totalQuantity == 0){
     		$scope.productshoppingcart = false;
     	}
  	  	
        //Send Message
        $scope.sendMsgData.headers = {
	        	  "Content-Type": "application/json", 
	        	  "Accept": apiConfig.headers.Accept, 
	        	  /*"CorrelationId": apiConfig.headers.CorrelationId,  
	        	  "AccountName": accountInfoData.accountName,*/
	        	  "SeatID": "",
	        	  /*"networkId":accountInfoData.network_id*/
	          };
        $scope.sendMsgData.method = 'POST';
        var baseURL = CTL.Service.Helper.getBaseApiURL();
        var defaultSDKURL = CTL.Service.Defaults;
        var sendMessageRequestUrlFromSDK=defaultSDKURL.apiUrls.sendSMS.split(':'); 
        $scope.sendMsgData.requestUrl = baseURL + sendMessageRequestUrlFromSDK[0];        
        //Read Message
        $scope.readMsgData.method = 'GET';
        $scope.readMsgData.requestUrl = baseURL + defaultSDKURL.apiUrls.readSMS;
        $scope.readMsgData.queryParam = {};
        $scope.readMsgData.headers = {
	        	  "Accept": apiConfig.headers.Accept,
	        	/*  "AccountName": accountInfoData.accountName,*/
	        	  "SeatId" : "",
	        	  /*"CorrelationId": ctlUtils.getRandomFiveDigitNumber()*/
	          };
               
        //Delete Message
        $scope.deleteMsgData.method = 'DELETE';
        $scope.deleteMsgData.requestUrl = baseURL + defaultSDKURL.apiUrls.deleteSMS;
        $scope.deleteMsgData.queryParam = {};
        $scope.deleteMsgData.headers = {
	        	  "Accept": "application/json", 
	        	/*  "AccountName": accountInfoData.accountName,
	        	  "CorrelationId": ctlUtils.getRandomFiveDigitNumber(),*/
	        	  "seatId":""
	          };
       
        
        //Courier Service Status
        var testStatusRequestUrlFromSDK=defaultSDKURL.apiUrls.smsStatus.split(':'); 
        $scope.courierServiceStatusData.method = 'GET';
        $scope.courierServiceStatusData.headers = {
        		"Accept": "application/json",
        		/*"CorrelationId": apiConfig.headers.CorrelationId,
	        	"AccountName": accountInfoData.accountName */
	          };
        $scope.courierServiceStatusData.requestUrl = baseURL + testStatusRequestUrlFromSDK[0];
        
       
        /*$scope.getAllocatedMonikers = allocatedMonikers();
        $scope.SeatsForAccount = getAllocatedSeats();*/
        
        if (angular.isDefined($scope.seatIdforMonikers) && $scope.seatIdforMonikers != '') {
        	readSeatData($scope.seatIdforMonikers);
        } else {        
        	getAllocatedMonikers();
        }
        //getSeats();
        
     // Fetch Seat Data and update in Cache
    	//Reload Seat data when seat gets modified
    	function readSeatData(seatId){
    		$scope.startSpinner();
			
			var inputData = {};
			inputData.seatId = seatId;
			OrderManagementService.readSeat(inputData).then(
					function(successData) {
						$log.info('ProductCtrl::readSeat:: Success');
						$scope.allocatedSeats = []
               			$scope.allocatedSeats.push(successData);
						$scope.sendMsgData.selectedMoniker = successData.moniker;
					}, function(errorData) {
						$log.info('ProductsCtrl::readSeat method:: Error');
						}
					)['finally'](function() {
		    			$scope.stopSpinner();
					});
    	}
        
        function getAllocatedMonikers(){
        	$scope.startSpinner();
        	var allocatedMonikerResponse = MonikerService.getAllocatedMonikers({});
        	allocatedMonikerResponse.then(
        	   function(successData) {
        		   $log.info('ProductsCtrl::getAllocatedMonikers method:: Success ');
        		   $scope.allocatedMonikers = successData.phoneNumbers;
        		   
        		   var accountSummaryData = OrderManagementService.readAccount({});
               		accountSummaryData.then(
                   		function(successData) {
                   			$log.info('ProductCtrl::readAccount:: Success');
                   			$scope.allocatedSeats = successData[0].seats;
                   		},
                   		function(errorData) {
                   			$log.info('AccountDataCtrl::readAccount method:: Error');
                   		})
        	   },
        	   function(errorData) {
        		   $log.info('ProductsCtrl::getAllocatedMonikers method:: Error ');
        		   if(errorData.responseMessage!=null || errorData.responseMessage!=""){
        			   $scope.errors.errorMsg = errorData.responseMessage;
        		   }else{
        			   $scope.errors.errorMsg = "Unfortunately some error has occurred, please retry after sometime";   
        		   }
        	   }
        	)['finally'](function() {
    			$scope.stopSpinner();
			});        
        };
        
        /*function getSeats(){
        	$scope.startSpinner();
        	var accountSummaryData = OrderManagementService.readAccount({});
        	accountSummaryData.then(
            		function(successData) {
            			$log.info('ProductCtrl::getSeatIds method:: Success');
            			$scope.allocatedSeats = successData[0].seats;
            		},
            		function(errorData) {
            			$log.info('AccountDataCtrl::getAccountSummaryData method:: Error');
            		})['finally'](function() {
            			$scope.stopSpinner();
        			});
        };*/
        /*if (typeof accountSummaryData.then == 'function') {
        	accountSummaryData.then(
        		function(successData) {
        			$log.info('ProductCtrl::getSeatIds method:: Success');
        			$scope.allocatedSeats = successData[0].seats;
        		},
        		function(errorData) {
        			$log.info('AccountDataCtrl::getAccountSummaryData method:: Error');
        		});
        	} else {
        		if (angular.isDefined(accountSummaryData) && angular.isArray(accountSummaryData)) {
        			$scope.allocatedSeats = accountSummaryData[0].seats;
        		}
        	}*/
        
        /* Populating the allocated Seat Data. START */
       /* $scope.allocatedSeats = [];
        var accountSummaryData = OrderManagementService.getOrderMgmtDataCache("networkidsummarydata");
    	for (var index = 0; index < accountSummaryData.length; index++) {
        	for (var index1 = 0; index1 < accountSummaryData[index].seats.length; index1++) {
    			$scope.allocatedSeats.push(accountSummaryData[index].seats[index1]);
    		}
    	}*/
    	/* Populating the allocated Seat Data. END */
        
        
        if (typeof supportedCountryCallingCodes.then == 'function') {
	        supportedCountryCallingCodes.then(
	        		function(successData) {
	        			$log.info('ProductCtrl::getSupportedCountryCallingCodes method:: Success');
	        			$scope.supportedCountryCallingCodes = successData.callingCodes;
	        		},
	        		function(error) {
	        			$log.info('ProductCtrl::getSupportedCountryCallingCodes method:: failed');
	        			$scope.supportedCountryCallingCodes = [];
	        		}
	        );
        } else {
        	$scope.supportedCountryCallingCodes = supportedCountryCallingCodes.callingCodes;
        }
        
        $scope.userDetails = {};
        var workingPhoneNumbers = {};
        if(angular.isUndefined($rootScope.authResponse)){
            $scope.userDetails.Seat ={};
            $scope.userDetails.Service ={};
            $scope.userDetails.Seat.Read ='1';
            $scope.userDetails.Service.Read ='1';
            $log.info("ProductsCtrl AuthResponse Test Setup: ")
        }
        else{
            $scope.userDetails = $rootScope.authResponse;
        }
        
       // $scope.currentPage = "products_landing";
       $scope.addClient = false;
       $scope.go = function (page) {
                $location.path('/' + page);
                $scope.currentPage = "0";
                $scope.addClient = false;
                $scope.tabId = 1;
                $scope.response = {};
                $scope.errors = {};
               	$scope.orderSuccessMsgSection = false;
               	$scope.orderSummarySection = true;
               	$scope.orderErrorMsgSection = false;
               	$scope.partialOrderSuccessMsgSection = false;
               	$scope.productshoppingcart = true;
               	if($scope.cartDetails.totalQuantity == 0){
               		$scope.productshoppingcart = false;
               	}
               	resetForms();
        };
        
        function resetForms(){
        	$log.info("resetForms:");
        	//reset send message 
        	delete $scope.sendMsgData.selectedMoniker;
        	$scope.sendMsgData.headers.SeatID = "";
        	$scope.sendMsgData.headers.networkId = accountInfoData.network_id;
        	$scope.sendMsgData.selectedMoniker="";
        	$scope.sendMsgData.countryCode="";
        	$scope.sendMsgData.to="";
        	$scope.sendMsgData.messageText="";
	        
	        //reset read message 
        	$scope.readMsgData.selectedSeat="";
	        $scope.readMsgData.messageId ="";
	        $scope.readMsgData.fromPhoneNumber = "";
            $scope.readMsgData.toPhoneNumber = "";
            $scope.readMsgData.messageText = "";
            $scope.readMsgData.startDate = "";
            $scope.readMsgData.endDate = "";
            $scope.readMsgData.queryParam.offset = "";
	      	$scope.readMsgData.queryParam.limit = "";
	      	$scope.readMsgData.queryParam.Pretty = "";
	      	$scope.readMsgData.queryParam.Sort = "";
	      	$scope.readMsgData.queryParam.query = "";
                        
            //reset delete message 
            $scope.deleteMsgData.selectedSeat="";
            $scope.deleteMsgData.messageId = "";
	        $scope.deleteMsgData.fromPhoneNumber = "";
            $scope.deleteMsgData.to = "";
            $scope.deleteMsgData.messageText = "";
            $scope.deleteMsgData.startDate = "";
            $scope.deleteMsgData.endDate = "";
            $scope.deleteMsgData.queryParam.query="";
            
            //reset buy product 
            $scope.buyProductInput.prodQuantity = 1;
    		$scope.buyProductInput.customerId = "";
        }
        //For setting API send/read/delete message pages
        $scope.page = "products_landing";
        // Passthrough to other Products page here - Subscribe/Edit/OrderSummary
        if($scope.currentPage){
        	$scope.page = $scope.currentPage;
        	$scope.active = false;
            $scope.active1 = true;
        	$scope.addClient = true;
        	if($scope.currentPage == 'products_ordersummary'){
        		$scope.orderSuccessMsgSection = false;
               	$scope.orderSummarySection = true;
               	$scope.orderErrorMsgSection = false;
               	$scope.partialOrderSuccessMsgSection = false;
        	}
        }
        
    	//set default values for send and read console
        $scope.sendMsgData.toCountryCode = '+1'
    	$scope.readMsgData.queryParam.offset = "";
    	$scope.readMsgData.queryParam.limit = 10;
    	$scope.readMsgData.queryParam.Pretty = 'true';
    	//$scope.readMsgData.queryParam.Sort = 'createDate';
    	 
        $scope.setIncludePage = function(currentPage){
        	$location.path('/products/'+currentPage);
        }
        
        $scope.setIncludePageInProductService = function(currentPage){
        	$location.path('/product_services/'+currentPage);
        }
        
        if($route.current.params.pageId && $route.current.params.pageId === 'products_buy'){
        	$scope.setIncludePage("products_buyproducts");
        }
       //focus on selected quantity if updating manually
        $scope.onTextClick = function ($event) {
            $event.target.select();
        };

        $scope.increaseQuantity = function(){
            if($scope.buyProductInput.prodQuantity >= 0){
                $scope.buyProductInput.prodQuantity = Number($scope.buyProductInput.prodQuantity) + 1;
            }
        }
        
        //$scope.productQuantity = 0;
        $scope.increaseQuantityForUpdate = function(index){
        	if(angular.isDefined($scope.editProduct.productQuantity)){
            	$scope.editProduct.productQuantity = Number($scope.editProduct.productQuantity) + 1;
            }
        	
        }
        
        $scope.increaseQunatityFromOrderPage = function(index) {
        	$scope.cartDetails.Products[index].Quantity = Number($scope.cartDetails.Products[index].Quantity) + 1;
        	$scope.quantityChangeOnOrderSummary();
        }

        $scope.decreaseQuantity = function(){
            if($scope.buyProductInput.prodQuantity > 0) {
                $scope.buyProductInput.prodQuantity = Number($scope.buyProductInput.prodQuantity) - 1;
            }
            
        }
        
        $scope.decreaseQuantityForUpdate = function(index){
        	if(angular.isDefined($scope.editProduct.productQuantity) && $scope.editProduct.productQuantity > 0){
        		$scope.editProduct.productQuantity = Number($scope.editProduct.productQuantity) - 1;
            }       	
        }
        
        $scope.decreaseQunatityFromOrderPage = function(index) {
        	if (Number($scope.cartDetails.Products[index].Quantity) > 0) {
        		$scope.cartDetails.Products[index].Quantity = Number($scope.cartDetails.Products[index].Quantity) - 1;
        		$scope.quantityChangeOnOrderSummary();
        	}
        }
        
        $scope.addToCart = function(productName){
        	$scope.page = 'products_ordersummary';
        	$scope.addClient = true;
        	
        	$scope.cartDetails.Products.ProductDisplayLabel = productName;
        	$scope.cartDetails.Products.ProductName = "courier-basic";
        	$scope.cartDetails.Products.Quantity = Number($scope.buyProductInput.prodQuantity);
        	$scope.cartDetails.Products.Service = "SMS Service";
        	$scope.cartDetails.Products.legalAckStatus = $scope.buyProductInput.legalAckStatus;
        	var length = $scope.cartDetails.Products.length;
        	if(angular.isUndefined($scope.cartDetails.Products.length)){
        		length = 0;
        	}
        	$log.info("addToCart : length:"+length);
    		$scope.cartDetails.Products.push({
    									"ProductName":"courier-basic",
						 				"CustomerID" : $scope.buyProductInput.customerId,
						 				"Quantity":$scope.buyProductInput.prodQuantity,
						 				"Service":"SMS Service",
						 				"legalAckStatus" : $scope.buyProductInput.legalAckStatus
    				});
    		$scope.cartDetails.Products[length].ProductDisplayLabel = productName;
    		$scope.cartDetails.Products[length].ProductName = "courier-basic";
        	$scope.cartDetails.Products[length].CustomerID = $scope.buyProductInput.customerId;
        	$scope.cartDetails.Products[length].Quantity = Number($scope.buyProductInput.prodQuantity);
        	$scope.cartDetails.Products[length].Service = "SMS Service";        	
        	$scope.cartDetails.Products[length].legalAckStatus = $scope.buyProductInput.legalAckStatus;
        	$scope.cartDetails.totalQuantity = $scope.cartDetails.totalQuantity + Number($scope.buyProductInput.prodQuantity);
    		$scope.buyProductInput.prodQuantity = 0;
    		$scope.buyProductInput.customerId = "";
    		$scope.buyProductInput.legalAckStatus = false;
        	$scope.productshoppingcart = true;
        	$log.info("addToCart : Total Quantity:"+$scope.cartDetails.totalQuantity);
        	$log.info("addToCart : JSON:"+angular.toJson($scope.cartDetails));
        	setOrderMgmtDataCache("cartdetails", $scope.cartDetails);
        }
                
        $scope.editProduct = function(index){
        	//var tempCustId = $scope.cartDetails.Products[index].CustomerID;
        	//var tempQuantity = $scope.cartDetails.Products[index].Quantity;
        	$scope.editProduct.productQuantity = $scope.cartDetails.Products[index].Quantity;
        	$scope.editProduct.customerId = $scope.cartDetails.Products[index].CustomerID;
        	$scope.page = 'products_editproducts';
        	$scope.addClient = true;
        	$scope.index = index;
        	$log.info("editProduct: index:"+index);
        }
        
        $scope.deleteProduct = function(index){
        	
	        	$scope.cartDetails.totalQuantity = 0;
	        	$scope.cartDetails.Products.splice(index, 1);        	
	        	angular.forEach($scope.cartDetails.Products, function(item){
	        		$scope.cartDetails.totalQuantity = $scope.cartDetails.totalQuantity + parseInt(item.Quantity); 
	            })
	        	setOrderMgmtDataCache("cartdetails", $scope.cartDetails);
        }
        
        $scope.updateCart = function(){
        	if($scope.editProduct.productQuantity == 0){
        		$scope.cartDetails.Products.splice($scope.index, 1);   
        	}else{
        		$scope.cartDetails.Products[$scope.index].Quantity = Number($scope.editProduct.productQuantity);
        		$scope.cartDetails.Products[$scope.index].CustomerID = $scope.editProduct.customerId;
        	}
        	$scope.cartDetails.totalQuantity = 0;
        	angular.forEach($scope.cartDetails.Products, function(item){
        		$scope.cartDetails.totalQuantity = $scope.cartDetails.totalQuantity + parseInt(item.Quantity); 
            })
            $log.info("updateCart : totalQuantity ="+$scope.cartDetails.totalQuantity);
        	$scope.page = 'products_ordersummary';
        	$scope.addClient = true;
        	setOrderMgmtDataCache("cartdetails", $scope.cartDetails);
        }
        
        $scope.quantityChangeOnOrderSummary = function() {
        	$scope.cartDetails.totalQuantity = 0;
        	angular.forEach($scope.cartDetails.Products, function(item){
        		$scope.cartDetails.totalQuantity = $scope.cartDetails.totalQuantity + parseInt(item.Quantity); 
            })
        }
        
        
        $scope.enableSubmitOrderButton = function(){
        	$log.info("enableSubmitOrderButton:"+$scope.submitOrderButtonEnabled);
        	$scope.submitOrderButtonEnabled = true;
        };
        
        
        $scope.buyProduct = function(){
            $scope.successOrders = [];
            if($scope.cartDetails.totalQuantity > 0){
                $rootScope.$broadcast("inProgressStart");
                var inputData = {};
                var requestList = [];
                var customerIdList = [];
                var productList = [];
                var successResponse = {};
                // Loop for Products 
                angular.forEach($scope.cartDetails.Products, function(item, index){
                	inputData.customerId = item.CustomerID;
                    inputData.productName = item.ProductName;
                    inputData.legalAckValue = productMetaData[0].name + "_" + productMetaData[0].version + "_" + accountInfoData.userName;    
                    inputData.legalAckStatus = item.legalAckStatus;
                    var count = item.Quantity;
                    successResponse.customerId = item.CustomerID;
                    
                    while (count> 0){
                        requestList.push(OrderManagementService.createSeat(inputData));
                        customerIdList.push(inputData.customerId);
                        if($scope.content.Common.Products[0].Product.Key === inputData.productName){
                            productList.push($scope.content.Common.Products[0].Product.Value);
                        }else{
                            productList.push(inputData.productName);
                        }
                        count--;
                    }
                });
                
                var responseList = [];
                asyncCall(requestList,
                    function error(results) {
                         $log.info('Some error occurred', angular.toJson(results));
                        
                     },
                     function success(results) {
                         $log.info('Results (final):', angular.toJson(results));
                         var successCount = 0;
                         responseList = results;
                         for(var i=0; i< customerIdList.length; i++){
         	        		 successResponse = {};
                        	 successResponse.customerId = customerIdList[i];
                        	 successResponse.productName = productList[i];
        	        		 successResponse.response = responseList[i];
        	        		 $scope.successOrders.push(successResponse);
         	        		 if(angular.isDefined(successResponse.response.seatId)){
         	        			successCount = successCount+1;
         	        			for(var j=0; j<$scope.cartDetails.Products.length;j++){
         	        				if($scope.cartDetails.Products[j].CustomerID == successResponse.customerId){
         	        					$scope.cartDetails.Products[j].Quantity = $scope.cartDetails.Products[j].Quantity - 1;
         	        					if($scope.cartDetails.Products[j].Quantity == 0){
         	        						$scope.cartDetails.Products.splice(j, 1);
         	        					}
         	        				}
         	        			}
         	        		 }
                         }
	                     $scope.cartDetails.totalQuantity = $scope.cartDetails.totalQuantity - successCount;
	                     setOrderMgmtDataCache("cartdetails", $scope.cartDetails);
                         $scope.orderSuccessMsgSection = true;
	                     $scope.orderSummarySection = false;
	                     $scope.partialOrderSuccessMsgSection = false;
	            		 
                     }
                    )['finally'](function(){
                    	$rootScope.$broadcast("inProgressEnd");
                    });
                
                
              //  $rootScope.$broadcast("inProgressEnd");
            }
        };
        
        
        function asyncCall(listOfPromises, onErrorCallback, finalCallback) {

            listOfPromises  = listOfPromises  || [];
            onErrorCallback = onErrorCallback || angular.noop;
            finalCallback   = finalCallback   || angular.noop;

            // Create a new list of promises
            // that can "recover" from rejection
            var newListOfPromises = listOfPromises.map(function (promise) {
              return promise['catch'](function (reason) {

                // First call the `onErrroCallback`
                onErrorCallback(reason);
                return reason;

              });
            });

            return $q.all(newListOfPromises).then(finalCallback);
          };
        
        
        $scope.showOrderSuccessMsgSection = function(){
        	$log.info("showOrderSuccessMsgSection"+$scope.submitOrderButtonEnabled);
        	//if($scope.submitOrderButtonEnabled){
        		 $scope.orderSuccessMsgSection = true;
                 $scope.orderSummarySection = false;
        	//}
           
        }
	
              
       $scope.setMonikerValue = function (phoneNumber) { 
	       	$scope.sendMsgData.fromPhoneNumber = phoneNumber;
	       	var associatedSeatId = getAssociatedSeatId($scope.sendMsgData.fromPhoneNumber);
	       	$scope.sendMsgData.headers.SeatID = !angular.isUndefined(associatedSeatId) ? associatedSeatId : "";
	        $log.info("Selected Value: " + phoneNumber);
       }
       
       $scope.setSeatIdValueForRead = function(seatId){
    	   $scope.readMsgData.headers.SeatId = seatId;
    	   //$scope.readMsgData.seatId = seatId;
           $log.info("Selected Value: " + seatId);
       }
          
       $scope.setSeatIdValueForDelete = function(seatId){
    	   $scope.deleteMsgData.headers.seatId = seatId;
           $log.info("Selected Value: " + seatId);
       }
       
       $scope.sendMessage = function(){ 
    	 
           $scope.active1 = false;
    	  // var target = angular.element('#SendMesageBtn');
    	   
    	   var sendMessageData = {};
    	   $scope.response = {};
    	   if(angular.isDefined($scope.sendMsgData.fromPhoneNumber)){
    		   sendMessageData.fromPhoneNumber = $scope.sendMsgData.fromPhoneNumber;
    	   }
           if(angular.isDefined($scope.sendMsgData.to)){
        	   $log.info("sendMessage to:"+($scope.sendMsgData.to).length);
               sendMessageData.to = $scope.sendMsgData.toCountryCode + $scope.sendMsgData.to;
           }
           if(angular.isDefined($scope.sendMsgData.messageText)){
        	   sendMessageData.messageText = $scope.sendMsgData.messageText;
           }
           var requestString = angular.toJson(sendMessageData);
           //$scope.jsonRequest = JSON.stringify(requestString, null,"    ")
           $scope.jsonRequest =  requestString.replace(/,/g, ',\n');
           sendMessageData.headers = $scope.sendMsgData.headers;
           var associatedSeatId = getAssociatedSeatId($scope.sendMsgData.fromPhoneNumber);
           if (!angular.isUndefined(associatedSeatId)) {
        	   sendMessageData.headers.SeatID = associatedSeatId;
           }
           sendMessageData.networkId = accountInfoData.network_id;
           if(angular.isDefined(sendMessageData.fromPhoneNumber) && angular.isDefined(sendMessageData.to) && angular.isDefined(sendMessageData.messageText)){
        	 
        	   $rootScope.$broadcast("inProgressStart");
        	   var courierSendMsgResponse = CourierService.sendSMS(sendMessageData);
	     	 courierSendMsgResponse.then(
	              function(success) {
	                  $scope.response.data = success.data;
	                  $scope.response.StatusCode = success.StatusCode;
	                  if($scope.response.data==null){
	                	  $scope.response.data = "Unfortunately some error has occurred, please retry after sometime";
	                  }
	                  $scope.response.responseStatus = success.status;
	                  $scope.sendMessageSuccessMesssage = true;
	            	  $scope.sendMessageErrorMesssage = false;
	            	  
	            	  $scope.setMessages([{"type":0, "message":"Message sent."}]);
	            	  
	            	 //$window.scrollTo(0, 530);
	            	 /* $scope.sendMsgData.selectedMoniker="";
	            	  $scope.sendMsgData.countryCode="";
	            	  $scope.sendMsgData.to="";
	            	  $scope.sendMsgData.messageText="";*/
	            	 },
	              function(error) {
	            	  $scope.errLbl=false;
	            	  $scope.sendMessageSuccessMesssage = false;
	            	  $scope.sendMessageErrorMesssage = true;
	            	  $scope.response.errorMsg = error.data;
	            	  $scope.response.StatusCode = error.StatusCode;
	            	  $scope.setMessages([{"type":2, "message":error.data.responseMessage}]);
	                  $log.info('ProductCtrl::sendMessage method:: Error ');
	              }
	          )['finally'](function(){
              	$rootScope.$broadcast("inProgressEnd");
              });
           }
   		};
   
   		// code to toggle API console (request and response)
   		$scope.showAPIDetailsSection = false;
        $scope.toggleAPIDetailsSection = function() {
            $scope.showAPIDetailsSection = !$scope.showAPIDetailsSection;
        }
  
   		$scope.readMessage = function(){  
   			$rootScope.$broadcast("inProgressStart");
              $scope.active1 = false;
              $scope.response = {};
              $scope.errors = {};
              var readMessageData = {};
              var queryString ='';
              var postData = {};           
             
              // setting seatId
              postData.seatId = $scope.readMsgData.headers.SeatId;
             
              if(!(angular.isUndefined($scope.readMsgData.queryParam.offset) || $scope.readMsgData.queryParam.offset == "")) {
            	  readMessageData.offset=$scope.readMsgData.queryParam.offset;
            	  postData.offset = $scope.readMsgData.queryParam.offset;
              }
              
              if(!(angular.isUndefined($scope.readMsgData.queryParam.limit) || $scope.readMsgData.queryParam.limit == "")) {
            	  readMessageData.limit=$scope.readMsgData.queryParam.limit;
            	  postData.limit = $scope.readMsgData.queryParam.limit;
              }
              
              if(!(angular.isUndefined($scope.readMsgData.queryParam.Pretty) || $scope.readMsgData.queryParam.Pretty == "")) {
            	  readMessageData.Pretty=$scope.readMsgData.queryParam.Pretty;
            	  postData.pretty = $scope.readMsgData.queryParam.Pretty;
              }
              
              if(!(angular.isUndefined($scope.readMsgData.queryParam.Sort) || $scope.readMsgData.queryParam.Sort == "")) {
            	  readMessageData.Sort=$scope.readMsgData.queryParam.Sort;
            	  postData.sort = $scope.readMsgData.queryParam.Sort;
              }              
              
              /* MessageId */
              if (!(angular.isUndefined($scope.readMsgData.messageId) || $scope.readMsgData.messageId == "")) {
            	  readMessageData.messageId = $scope.readMsgData.messageId;
            	  queryString = 'key'+ encodeURI(':') + readMessageData.messageId;            	 
              }
              
              /*  fromPhoneNumber*/
              if (!(angular.isUndefined($scope.readMsgData.fromPhoneNumber) || $scope.readMsgData.fromPhoneNumber == "")) {
            	  readMessageData.fromPhoneNumber = $scope.readMsgData.fromPhoneNumber;
            	  queryString = queryString+ ' AND from' + encodeURI(':') + readMessageData.fromPhoneNumber;            	 
              }
              
              /*  toPhoneNumber*/              
              if (!(angular.isUndefined($scope.readMsgData.toPhoneNumber) || $scope.readMsgData.toPhoneNumber == "")) {
            	  readMessageData.toPhoneNumber = $scope.readMsgData.toPhoneNumber;
            	  queryString = queryString+' AND to'+ encodeURI(':') + readMessageData.toPhoneNumber;            
              }
              
              /*  messageText*/       
              if (!(angular.isUndefined( $scope.readMsgData.messageText) || $scope.readMsgData.messageText == "")) {
            	  readMessageData.messageText = $scope.readMsgData.messageText;
            	  queryString=queryString+' AND message' + encodeURI(':') + readMessageData.messageText.replace(/:/g, "?");            
              }
              
              /* startDate*/ 
              if (!(angular.isUndefined($scope.readMsgData.startDate) || $scope.readMsgData.startDate == "")) {
            	  readMessageData.startDate = $scope.readMsgData.startDate; 
            	  var date=$scope.readMsgData.startDate;
            	  $log.info($scope.readMsgData.startDate);
                  var year=date.slice(6,10);
                  var month=date.slice(0,2);
                  var day=date.slice(3,5);
                  var newCreateDate=year+'/'+month+'/'+day;
                  readMessageData.crateDateMs = Date.parse(newCreateDate);
            	  queryString = queryString+' AND createDate ['+ readMessageData.crateDateMs +' TO *]';            	
              }
              
              /* endDate*/ 
              if (!(angular.isUndefined($scope.readMsgData.endDate) || $scope.readMsgData.endDate == "")) {
            	  readMessageData.endDate = $scope.readMsgData.endDate;
            	  var Edate=$scope.readMsgData.endDate;
            	  $log.info($scope.readMsgData.endDate);
                  var year=Edate.slice(6,10);
                  var month=Edate.slice(0,2);
                  var day=Edate.slice(3,5);
                  var newEndDate=year+'/'+month+'/'+day;
                  readMessageData.newEndDate = Date.parse(newEndDate);
            	  queryString = queryString+' AND createDate [* TO '+ readMessageData.newEndDate + ']';             
              }
           
              if($scope.readMsgData.startDate > $scope.readMsgData.endDate){
            	  $scope.ShowDateError=true;            	  
              }
              
              var requestString = angular.toJson(readMessageData);
              $scope.jsonRequest =  requestString.replace(/,/g, ',\n');
              readMessageData.headers = $scope.readMsgData.headers;
              
              if(!(angular.isUndefined(queryString) || queryString == "")) {             
            	  queryString=queryString.replace(/^ AND\s+/i,'');
            	  postData.query = '(' + queryString + ')';              	 
              }
             
             readMessageData.readMessageUri=$scope.readMeassageSolrUrl;
          
        	 var courierReadMsgResponse = DumpsterService.readMessage(postData);
        	 courierReadMsgResponse.then(
        		 function(successData) {
                      $scope.response.data = successData.data;
                      $scope.response.StatusCode = successData.StatusCode;
                      $scope.readMessageSuccessMesssage = true;
                	  $scope.readMessageErrorMesssage = false;
                	  
                	  //$window.scrollTo(0, 995);
                	 /* $scope.readMsgData.messageId="";
                	  $scope.readMsgData.fromPhoneNumber="";
                	  $scope.readMsgData.to="";
                	  $scope.readMsgData.messageText="";
                	  $scope.readMsgData.queryParam.offset = "";
                	  $scope.readMsgData.queryParam.limit = "";
                	  $scope.readMsgData.queryParam.Pretty = "";
                	  $scope.readMsgData.queryParam.Sort = "";
                	  $scope.readMsgData.queryParam.query = "";
                	  $scope.readMsgData.startDate="";
                	  $scope.readMsgData.endDate="";
                	  */
                	  
                  },
                  function(error) {
                	  $scope.readMessageSuccessMesssage = false;
                	  $scope.readMessageErrorMesssage = true;
                	  $scope.errors.errorMsg = error.data;
                	  $scope.response.StatusCode = error.StatusCode;
                      $log.info('ProductCtrl::readMessage method:: Error ');
                                           /*if(error.statusText!=null || error.statusText!=""){
                    	  $scope.errors.errorMsg = error.statusText;
                      }else{
                    	  $scope.errors.errorMsg = "Unfortunately some error has occurred, please retry after sometime";   
                      }*/
                  }
             )['finally'](function(){
               	$rootScope.$broadcast("inProgressEnd");
             });
         };
          
             $scope.deleteMessage = function(){
            	 $rootScope.$broadcast("inProgressStart");
            	 $scope.active1 = false;
                 var queryString='';                 
                 var deleteMessageData={};
                 $scope.response = {};
                 $scope.errors = {};
                 
                 deleteMessageData.seatId = $scope.deleteMsgData.headers.seatId
                 
                 if (!(angular.isUndefined($scope.deleteMsgData.messageId) || $scope.deleteMsgData.messageId == "")) {
                	 queryString = 'key' + encodeURI(':') + $scope.deleteMsgData.messageId;
                 }
                 if (!(angular.isUndefined($scope.deleteMsgData.fromPhoneNumber) || $scope.deleteMsgData.fromPhoneNumber == "")) {
                	 queryString = queryString+' And from' + encodeURI(':') + $scope.deleteMsgData.fromPhoneNumber;
                 }
                 if (!(angular.isUndefined($scope.deleteMsgData.to) || $scope.deleteMsgData.to=="")) {
                 	 queryString = queryString+' AND to'+ encodeURI(':') + $scope.deleteMsgData.to;
                 }
                 if (!(angular.isUndefined($scope.deleteMsgData.messageText) || $scope.deleteMsgData.messageText == "")) {
                	 queryString=queryString+' AND message' + encodeURI(':') + $scope.deleteMsgData.messageText;                	  
                 }
                 if (!(angular.isUndefined($scope.deleteMsgData.startDate) || $scope.deleteMsgData.startDate=="")) {
                	 var date=$scope.deleteMsgData.startDate;
               	     $log.info($scope.readMsgData.startDate);
                     var year=date.slice(6,10);
                     var month=date.slice(0,2);
                     var day=date.slice(3,5);
                     var newCreateDate=year+'/'+month+'/'+day;
                     queryString = queryString+' AND createDate ['+ Date.parse(newCreateDate) +' TO *]';
                 }
                 if (!(angular.isUndefined($scope.deleteMsgData.endDate) || $scope.deleteMsgData.endDate == "")) {
                	 var Edate=$scope.deleteMsgData.endDate;
               	     $log.info($scope.readMsgData.endDate);
                     var year=Edate.slice(6,10);
                     var month=Edate.slice(0,2);
                     var day=Edate.slice(3,5);
                     var newEndDate=year+'/'+month+'/'+day;
                     queryString = queryString+' AND createDate [* TO '+ Date.parse(newEndDate) + ']';
                 }        	
                 	
             	 if (!(angular.isUndefined(queryString) || queryString == "")) {
                 	deleteMessageData.query= '(' + queryString.replace(/^ AND\s+/i,'') + ')';	                 	 
             	 }
                 
               	 var courierReadMsgResponse = DumpsterService.deleteMessage(deleteMessageData);
               	 courierReadMsgResponse.then(
               			function(success) {
               				  $scope.response.data = success;
       	                      $scope.response.StatusCode = success.StatusCode;
     	                      $scope.deleteMessageSuccessMesssage = true;
     	                	  $scope.deleteMessageErrorMesssage = false;
     	                	  //$window.scrollTo(0, 730);
     	                	  $scope.setMessages([{"type":0, "message":"Messages deleted successfully."}]);
     	                	 /* $scope.deleteMsgData.messageId="";
     	                	  $scope.deleteMsgData.fromPhoneNumber="";
     	                	  $scope.deleteMsgData.to="";
     	                	  $scope.deleteMsgData.messageText="";
     	                	  $scope.deleteMsgData.startDate="";
     	                	  $scope.deleteMsgData.endDate="";
     	                	  $scope.deleteMsgData.queryParam.query="";*/
     	                  },
     	                  function(error) {
     	                	  $scope.deleteMessageSuccessMesssage = false;
     	                	  $scope.deleteMessageErrorMesssage = true;
     	                	  $scope.errors.errorMsg = error.data;
     	                	  $scope.response.StatusCode = error.StatusCode;
     	                      $log.info('ProductCtrl::deleteMessage method:: Error ');
     	                      $scope.setMessages([{"type":2, "message":error.data.responseMessage}]);
     	                     /* if(error.statusText!=null || error.statusText!=""){
     	                    	  $scope.errors.errorMsg = error.statusText;
     	                      }else{
     	                    	  $scope.errors.errorMsg = "Unfortunately some error has occurred, please retry after sometime";   
     	                      }*/
     	                  }
                    )['finally'](function(){
                      	$rootScope.$broadcast("inProgressEnd");
                    });
                };
                
                $scope.testServiceSuccessMesssage = true;
                $scope.testServiceErrorMesssage = false;
                 
                $scope.testCourierService = function(){
                   $rootScope.$broadcast("inProgressStart");
                   $scope.response = {};
                   $scope.errors = {};
                   $scope.active1 = false;
         	   	   var testCourierData = {};
         	   	   testCourierData.phoneNumber = $scope.courierServiceStatusData.phoneNumber;
     	           $log.info("testCourierService phoneNumber:"+$scope.courierServiceStatusData.phoneNumber);
     	           var requestString = angular.toJson(testCourierData);
     	           //$scope.jsonRequest = JSON.stringify(requestString, null,"    ")
     	           $scope.jsonRequest =  requestString.replace(/,/g, ',\n');
     	           testCourierData.headers = $scope.courierServiceStatusData.headers;
     	           var courierServiceStatusResponse = CourierService.getCourierServiceStatus(testCourierData);
     	           courierServiceStatusResponse.then(
     	                  function(success) {
     	                      $scope.response.data = success;
     	                      $scope.response.StatusCode = $rootScope.apiResponse.StatusCode;
     	                      $scope.testServiceSuccessMesssage = true;
     	                      $scope.testServiceErrorMesssage = false;
     	                     //$window.scrollTo(0,380); 
     	                  },
     	                  function(error) {
     	                	  $scope.errors.errorMsg = error.data;
     	                	  $scope.response.StatusCode = error.StatusCode;
     	                      $log.info('ProductCtrl::sendMessage method:: Error ');
     	                      $scope.testServiceSuccessMesssage = false;
     	                      $scope.testServiceErrorMesssage = true;
     	                  }
     	              )['finally'](function(){
     	               	$rootScope.$broadcast("inProgressEnd");
     	              });
            		};
          
			$scope.displayJsonRequestSection = true;
	        $scope.displayJsonResponseSection = false;
	        
	        $scope.showRequestTab = function () {
	            $scope.displayJsonRequestSection = true;
	            $scope.displayJsonResponseSection = false;
	        };
			
	        $scope.showResponseTab = function () {
	            $scope.displayJsonRequestSection = false;
	            $scope.displayJsonResponseSection = true;
	       
	        };
		  
	        $scope.tabId = 1;
	        $scope.showTab = function(tabId){
	            $scope.tabId = tabId;
	        };
	        
	        $scope.isSet = function (tabId) {
	            return $scope.tabId === tabId;
	        };
	        
	        $scope.moreproductdetails = false;
        	$scope.lessproductdetails = true;
        	
	        $scope.showMoreProductDescription = function(){
	        	$scope.moreproductdetails = true;
	        	$scope.lessproductdetails = false;
	        }
	        
	        $scope.showLessProductDescription = function(){
	        	$scope.moreproductdetails = false;
	        	$scope.lessproductdetails = true;
	        }
	      
	        
	        $scope.evaluteReadQuery = function() {	        	
	        	var queryString = "";
	        	/* MessageId */
	              if (!(angular.isUndefined($scope.readMsgData.messageId) || $scope.readMsgData.messageId == "")) {
	            	  queryString = 'key:'+ $scope.readMsgData.messageId;            	 
	              }
	              
	              /*  fromPhoneNumber*/
	              if (!(angular.isUndefined($scope.readMsgData.fromPhoneNumber) || $scope.readMsgData.fromPhoneNumber == "")) {
	            	  queryString = queryString+ ' AND from:' + $scope.readMsgData.fromPhoneNumber;            	 
	              }
	              
	              /*  toPhoneNumber*/              
	              if (!(angular.isUndefined($scope.readMsgData.toPhoneNumber) || $scope.readMsgData.toPhoneNumber == "")) {
	            	  queryString = queryString+' AND to:'+ $scope.readMsgData.toPhoneNumber;            
	              }
	              
	              /*  messageText*/       
	              if (!(angular.isUndefined($scope.readMsgData.messageText) || $scope.readMsgData.messageText == "")) {
	            	  queryString=queryString+' AND message:' + $scope.readMsgData.messageText;            
	              }
	              
	              /* startDate*/ 
	              if (!(angular.isUndefined($scope.readMsgData.startDate) || $scope.readMsgData.startDate == "")) {
	            	  var date=$scope.readMsgData.startDate;
	            	  $log.info($scope.readMsgData.startDate);
	                  var year=date.slice(6,10);
	                  var month=date.slice(0,2);
	                  var day=date.slice(3,5);
	                  var newCreateDate=year+'/'+month+'/'+day;
	            	  queryString = queryString+' AND createDate ['+ Date.parse(newCreateDate) +' TO *]';            	
	              }
	              
	              /* endDate*/ 
	              if (!(angular.isUndefined($scope.readMsgData.endDate) || $scope.readMsgData.endDate == "")) {
	            	  var Edate=$scope.readMsgData.endDate;
	            	  $log.info($scope.readMsgData.endDate);
	            	   /* Checking for start date and end date    
	            	  if(angular.isDefined($scope.readMsgData.endDate) && $scope.readMsgData.endDate != null &&
	              			  angular.isDefined($scope.readMsgData.startDate) && $scope.readMsgData.startDate != null){
	              	  var startDate=convertToDate($scope.readMsgData.startDate);
	        		  var endDate=convertToDate($scope.readMsgData.endDate);
	          		   var diff=endDate.diff(startDate);
	          		   $log.info("the difference between start and end dates is [days " + diff.days+"] [hour "+ diff.hours+"]");
	          		  if( diff.days < 0){
	          			$scope.dateErrorMessage= true;
	          			$scope.dateError='formfield-error';
	          		 }else if ( diff.days >= 0) {
	          			$scope.dateErrorMessage= false;
	          			$scope.dateError='';
	          		 }
	          		  }
	          		 Checking for start date and end date finish*/ 
	            	  
	                  var year=Edate.slice(6,10);
	                  var month=Edate.slice(0,2);
	                  var day=Edate.slice(3,5);
	                  var newEndDate=year+'/'+month+'/'+day;
	            	  queryString = queryString+' AND createDate [* TO '+ Date.parse(newEndDate) + ']';             
	              }
	           
	              if($scope.readMsgData.startDate > $scope.readMsgData.endDate){
	            	  $scope.ShowDateError=true;            	  
	              }
	              	              
	              if(!(angular.isUndefined(queryString) || queryString == "")) {             
	            	  $scope.readMsgData.queryParam.query = queryString.replace(/^ AND\s+/i,'');
	            	  $scope.readMsgData.queryParam.query = '(' + $scope.readMsgData.queryParam.query + ')';
	              } else {
	            	  $scope.readMsgData.queryParam.query = "";
	              }	              
	        };	              
	        
	        $scope.evaluateDeleteQuery = function() {
	        	var queryString = "";
	        	
	        	if (!(angular.isUndefined($scope.deleteMsgData.messageId) || $scope.deleteMsgData.messageId == "")) {
               	 queryString = 'key:' + $scope.deleteMsgData.messageId;
                }
	        	
                if (!(angular.isUndefined($scope.deleteMsgData.fromPhoneNumber) || $scope.deleteMsgData.fromPhoneNumber == "")) {
               	 queryString = queryString+' And from:' + $scope.deleteMsgData.fromPhoneNumber;
                }
                
                if (!(angular.isUndefined($scope.deleteMsgData.to) || $scope.deleteMsgData.to=="")) {
                	 queryString = queryString+' AND to:' + $scope.deleteMsgData.to;
                }
                
                if (!(angular.isUndefined($scope.deleteMsgData.messageText) || $scope.deleteMsgData.messageText == "")) {
               	 queryString=queryString+' AND message:' + $scope.deleteMsgData.messageText;                	  
                }
                
                if (!(angular.isUndefined($scope.deleteMsgData.startDate) || $scope.deleteMsgData.startDate == "")) {  
                	var date=$scope.deleteMsgData.startDate;
              	    $log.info($scope.readMsgData.startDate);
                    var year=date.slice(6,10);
                    var month=date.slice(0,2);
                    var day=date.slice(3,5);
                    var newCreateDate=year+'/'+month+'/'+day;
                    queryString = queryString+' AND createDate ['+ Date.parse(newCreateDate) +' TO *]';
                }
                
                if (!(angular.isUndefined($scope.deleteMsgData.endDate) || $scope.deleteMsgData.endDate == "")) {
               	 	var Edate=$scope.deleteMsgData.endDate;
              	    $log.info($scope.readMsgData.endDate);
              	  
               	   /* Checking for start date and end date*/  
              	  if(angular.isDefined($scope.deleteMsgData.endDate) && $scope.deleteMsgData.endDate != null &&
              			  angular.isDefined($scope.deleteMsgData.startDate) && $scope.deleteMsgData.startDate != null){
              	  var startDate=convertToDate($scope.deleteMsgData.startDate);
        		  var endDate=convertToDate($scope.deleteMsgData.endDate);
          		   var diff=endDate.diff(startDate);
          		   $log.info("the difference between start and end dates is [days " + diff.days+"] [hour "+ diff.hours+"]");
          		 /* if( diff.days < 0){
          			$scope.dateErrorMessage= true;
          			$scope.dateError='formfield-error';
          		 }else if ( diff.days >= 0) {
          			$scope.dateErrorMessage= false;
          			$scope.dateError='';
          		 }*/
              	  }
          		/* Checking for start date and end date finish*/ 
                    var year=Edate.slice(6,10);
                    var month=Edate.slice(0,2);
                    var day=Edate.slice(3,5);
                    var newEndDate=year+'/'+month+'/'+day;
                    queryString = queryString+' AND createDate [* TO '+ Date.parse(newEndDate) + ']';
                }                
                	
            	if (!(angular.isUndefined(queryString) || queryString == "")) {
	                 $scope.deleteMsgData.queryParam.query = queryString.replace(/^ AND\s+/i,'');
	                 $scope.deleteMsgData.queryParam.query = '(' + $scope.deleteMsgData.queryParam.query + ')';
	              } else {
	            	 $scope.deleteMsgData.queryParam.query = "";
	              }	    
            };
            
            $scope.isLegalAccepted = function(key) {
            	var accountSummaryData = OrderManagementService.getOrderMgmtDataCache("accountsummarydata");
            	var accepted = false;
            	for (var index1 = 0; index1 < accountSummaryData[0].seats.length; index1++) {
            		if (accountSummaryData[0].seats[index1].legalAckValue === productMetaData[key-1].name + "_" + productMetaData[key-1].version + "_" + accountInfoData.userName) {
            			accepted = true;
            			$scope.buyProductInput.legalAckStatus = true;
            			break;
            		}
            	}
            	return accepted;
            }
            
            $scope.getCustomerId = function() {
            	return (angular.isDefined($scope.editProduct.customerId) ? $scope.editProduct.customerId : 'This value is for use to assign a meaningful label to this seat');
            }
	        
          function makeButtonDisable(buttonName) {
              $log.info("Disable Button "+buttonName);
              $(buttonName).addClass('btn-lg-product-white');
              $(buttonName).removeClass('btn-lg-product');
              $(buttonName).attr('disabled', true);
          }
          
          var buttonArray = ['.JSONRequest-toggle', '.JSONRespose-toggle'];
          function makeOtherEnable(buttonName) {
              for (var i = 0; i < buttonArray.length; i++) {
                  if (buttonArray[i] != buttonName) {
                      $(buttonArray[i]).removeClass('btn-lg-product-white');
                      $(buttonArray[i]).addClass('btn-lg-product');
                      $(buttonArray[i]).attr('disabled', false);
                  }
              }
          }
          
          function getAssociatedSeatId(phoneNumber) { 
        	  var associatedSeatId;
        	  if (!angular.isUndefined($scope.allocatedSeats)) {
        		  for (var index = 0; index < $scope.allocatedSeats.length; index++) {
        			  if (phoneNumber == $scope.allocatedSeats[index].moniker) {
        				  associatedSeatId = $scope.allocatedSeats[index].seatId;
        				  break;
        			  }
        		  }
        	  }        	  
        	  return associatedSeatId;
          }
          
          //go to quickStart
          $scope.goToQuickStart=function(){        	  		
          	$location.path('/quickStart');      	
          }
        }	
  
];

ProductsCtrl.resolve ={
    /*allocatedMonikers:['MonikerService',
             function getAllocatedMonikers(MonikerService){
		        var getInputData = {};		
		        var allocatedMonikerResponse = MonikerService.getMonikerDataFromCache("monikersData");
		        if(angular.isUndefined(allocatedMonikerResponse) || !(allocatedMonikerResponse)){
		     	   allocatedMonikerResponse = MonikerService.getAllocatedMonikers(getInputData);			       
		        }		        
		        return allocatedMonikerResponse;
		    }],
     accountSummaryData:['OrderManagementService',
            function getAllocatedSeats(OrderManagementService){
				var inputData = {};
				
				var accountSummaryData = OrderManagementService.getOrderMgmtDataCache("accountsummarydata");
				if(angular.isUndefined(accountSummaryData) || !(accountSummaryData)) {
				    accountSummaryData = OrderManagementService.readAccount(inputData);
				};
				return accountSummaryData;
     	}],*/
     supportedCountryCallingCodes:['CourierService',
             function(CourierService){
    	 		return 	CourierService.readCountryCallingCodes();    	 
     	}],
     	content:['ContentService',
     	        function(ContentService){
     	            return ContentService.fetchContent("Common,Products");
     	        }]
	
};
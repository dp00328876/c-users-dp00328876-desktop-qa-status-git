'use strict';
/**
 * Created by vk0014941 on 11/20/2015.
 */

// Filter for
onBoardingApp.filter('startFrom', [ '$log',
    function($log) {
        return function(input, start) {
            //    $log.info("Filter: "+input+":"+start);
            if(angular.isUndefined(input) || angular.isUndefined(start)){
                return;
            }
            start = +start; //parse to int
            return input.slice(start);
        }
    }
]);

onBoardingApp.filter('range', [function() {
	  return function(input, start, end, totalCount, itemsPerPage) {
		  start = parseInt(start);
		  end = parseInt(end);
		  totalCount = parseInt(totalCount);
		  itemsPerPage = parseInt(itemsPerPage);
		  var noOfPages = Math.ceil(totalCount/itemsPerPage);
		  if(end >= noOfPages){
			  end = noOfPages;
		  }
	    for (var i=start; i<=end; i++) {
	      input.push(i);
	    }

	    return input;
	  };
	}]);


onBoardingApp.filter('arrayToCSV', [function() {
	  return function(input) {
		  var result = '';
		  if(input.length >0){
			  for(var i=0; i<input.length; i++){
				  result = result + ", " + input[i];
			  }			  
			  result = result.slice(1);
		  }
		  return result;
	  };
	}]);
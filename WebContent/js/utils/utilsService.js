/**
 * Created by vk0014941 on 11/20/2015.
 */

var utilsServices = angular.module('ctl.common.utils.service', []);

utilsServices.factory('ctlUtils', ['$log', function($log){

        // Return 5 digit random number
        var _getRandomFiveDigitNumber = function getRandomFiveDigitNumber(){
            return Math.floor((Math.random()*89999)+10000);
        };
        
        // Decode JWT Token 
        var _decodeJWT= function (jwt_token, position) {
            var jsonPayload ={};
            try{
                if(jwt_token){
                    var jwt_token_array = jwt_token.split('.');
                    var jwtPayLoadStr = jwt_token_array[position];
                    var output = jwtPayLoadStr.replace(/-/g, '+').replace(/_/g, '/');
                    switch (output.length % 4) {
                      case 0:
                        break;
                      case 2:
                        output += '==';
                        break;
                      case 3:
                        output += '=';
                        break;
                      default:
                    	$log.error('Illegal base64url string!');
                        //throw 'Illegal base64url string!';
                    }
                    jsonPayload = JSON.parse(atob(output));	                    
                }
            }catch(e){
            	$log.error("Decoding of JWTToken throws an error "+e);
            }
            return jsonPayload;
        };

    return {
        getRandomFiveDigitNumber: _getRandomFiveDigitNumber,
        decodeJWT: _decodeJWT
    };
    }]);

'use strict';

var conf = angular.module("onboarding.configuration.service",
    [
    ]);
conf.constant("envConfig",
    {
        "env": "DEV",
        "appVersion": '%APPVERSION%',
        "build":'%BUILDNUMBER%',
        "date": '%BUILDDATE%',
        "useLoginStub": false,
        "useMonikerStub": false,
        "useCourierStub": false,
        "useDumpsterStub":false,
        "useOrderManagementStub":false,
        "useContentStub": true,
        "contentFallBack": true,
        "defaultLocale": "en",
        "sessionTimeOut": 900,
        "useSplunk":true
    });

conf.constant("apiUrls",
    {
        "MONIKER_URL": "http://64.15.188.228",
        "DUMPSTER_URL": ""
    });

conf.constant("errorMessageConfig",
    {
        defaultErrorMessage: {
            "English": "Unfortunately, we are unable to process your request at this time. Please try again later.",
            "Spanish": "Desafortunadamente no es posible procesar tu solicitud en este momento. Lamentamos el inconveniente. Vuelve a intentar m&#225;s tarde."
        },
        buttonLabel: {
            "English": "Done",
            "Spanish": "Listo"
        }
    }
);

conf.constant("apiConfig",
    {
        "headers": {
          //  "X-Requested-By": "CTL",
           // "Pragma": "no-cache",
          //  "Cache-Control": "no-cache=set-cookie",
          //  "Expires": "-1",
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        "tokenHeaders": {
            "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded"
           // "charset": "UTF-8"
        }
    });
conf.constant("footerLinkURL",
    {

    });

conf.constant("cacheConfig",
    {
        "cacheSourceSession": "sessionStorage",
        "cacheSourceLocal": "localStorage"
    });

conf.constant("splunkConfig",
	    {
			"chartJSPath" : "lib/splunk/splunk.ui.charting.js",
			"dashBoardLogFile" : "*/dashboard.log",
			"span":"1day",
			"spanPeriod":"-7d@d"
	    });
conf.constant("quickStartConfig",
		{
			"searchMoniker_State" : "CO",
			"searchMoniker_City" : "Denver",
			"searchMoniker_NPA" : "",
			"searchMoniker_NPANXX" : "",
			"monikerIndex" : "1",
			"customerId" : "myseat_",
			"productName": "courier-basic",
			"consumerName" : "quickstart",
			"consumerPassword" : "Password1234#",
			"consumerNetworkId" : "quickstart"
		});
conf.constant("oauthDomainConfig",
		{
			'webapplication':'@webapplication',
			'platform':'@admin',
			'network':'@consumer'
		});
conf.constant("pricingConfig",
		{
			'seatRental':'0.99',
			'phoneNumberRental':'0.99',
			'domesticSMSRental':'0.0065',
			'internationalSMSRental':'0.0065',
			'otherInternationalSMSSMSRental':'variable',
			'data200MbMessageSafestoreRental':'Free',
			'data10GbMessageSafestore':'0.10'			
		});
conf.constant("accountSummaryConfig",
		{
			'monikerSearchCount':'10'	
		});
conf.constant("platformConfig",
		{
			'platformURL':'https://control.dev.ctl.io'	
		});
conf.constant("platformBrandingConfig",
		{
			'portalLogoUrl':'images/logo-centurylink_withText.png',
			'loginLogoUrl':'images/logo-centurylink_withText.png',
			'faviconUrl':'images/ctlLogo.png',
			'mobileIconUrl':'images/logo-centurylink_withoutText.png',
			'satelliteCSSUrl':'',
			'controlUrl':'',
			'headerBackgroundColor':'#262626',
			'navTextColor':'#ffffff',
			'controlName':'Control Portal',
			'controlLogoutUrl':'https://control.dev.ctl.io/auth/logout',
			'controlUserProfileUrl':''
		});
conf.constant("platformSupportConfig",
		{
			'supportEmail':'',
	        'supportPhone':'',
	        'supportRequestUrl':'',
	        'featureRequestUrl':'',
	        'knowledgebaseUrl':'',
	        'knowledgebaseSearchUrl':'',
	        'chatServiceUrl':''
		});
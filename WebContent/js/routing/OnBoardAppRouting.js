'use strict';
/**
 * Created by vk0014941 on 11/8/2015.
 */




onBoardingApp.config(['$routeProvider','$locationProvider', function($routeProvider, $locationProvider) {
    // Routing Configuration Change - Page URL change

    $routeProvider.when('/accountSummary',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/acctSummary/accountSummary.html',
        isAuthRequired: true,
        contentPageId: 'Account_Summary',
        controller: AccountSummaryCtrl,
        resolve: AccountSummaryCtrl.resolve
    });   
    $routeProvider.when('/seatDetails',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/acctSummary/seatDetails.html',
        isAuthRequired: true,
        contentPageId: 'Seat_Details',
        controller: AccountSummaryCtrl,
        resolve: AccountSummaryCtrl.resolve
    });
    $routeProvider.when('/addSeat',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/acctSummary/addSeat.html',
        isAuthRequired: false,
        contentPageId: 'AddSeat',
        controller: AddSeatCtrl,
        resolve: AddSeatCtrl.resolve
    });
    $routeProvider.when('/accountSummary/pendingInvites',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/acctSummary/managePendingInvites.html',
        isAuthRequired: true,
        contentPageId: 'Manage_Invitations',
        controller: AccountSummaryCtrl,
        resolve: AccountSummaryCtrl.resolve
    });
    $routeProvider.when('/accountSummary/:seatId',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/acctSummary/configureSeat.html',
        isAuthRequired: true,
        contentPageId: 'Manage_Seat',
        controller: AccountSummaryCtrl,
        resolve: AccountSummaryCtrl.resolve
    });
    $routeProvider.when('/products',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/products/products_landing.html',
        isAuthRequired: true,
        contentPageId: 'Products',
        controller: ProductsCtrl,
        resolve: ProductsCtrl.resolve
    });
    $routeProvider.when('/products/:pageId',{
        title: 'Product Buy',
        templateUrl: 'partials/products/products_landing.html',
        isAuthRequired: true,
        contentPageId: 'Products_Subscribe',
        controller: ProductsCtrl,
        resolve: ProductsCtrl.resolve
    });
    $routeProvider.when('/product_services/:pageId',{
        title: 'Product Buy',
        templateUrl: 'partials/products/product_services.html',
        isAuthRequired: true,
        contentPageId: 'Product_Services',
        controller: ProductsCtrl,
        resolve: ProductsCtrl.resolve
    });
    $routeProvider.when('/order_summary/:prodQuantity',{
        title: 'Order Summary',
        templateUrl: 'partials/products/products_ordersummary.html',
        isAuthRequired: true,
        controller: ProductsCtrl,
        resolve: ProductsCtrl.resolve
    });
    $routeProvider.when('/usageStatistics',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/usageStatistics/usageStatistics.html',
        isAuthRequired: true,
        contentPageId: 'Usage_Statistics',
        controller: UsageStatisticsCtrl,
        resolve: UsageStatisticsCtrl.resolve
    });
    $routeProvider.when('/usersServices/:networkId',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/manageNetwork/groupUserTabs.html',
        isAuthRequired: false,
        contentPageId: 'Manage_User',
        controller: ManageUsersCtrl,
        resolve: ManageUsersCtrl.resolve
    });
    $routeProvider.when('/manageNetworkId',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/manageNetwork/manageNetworkId.html',
        isAuthRequired: true,
        contentPageId: 'Manage_NetworkId',
        controller: ManageNetworkIdCtrl,
        resolve: ManageNetworkIdCtrl.resolve
    });
    $routeProvider.when('/editNetworkId',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/manageNetwork/editNetworkId.html',
        isAuthRequired: true,
        contentPageId: 'Manage_User',
        controller: ManageNetworkIdCtrl,
        resolve: ManageNetworkIdCtrl.resolve
    });
    $routeProvider.when('/apiDocs/:backLink',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/apiDocs/apiDocs.html',
        isAuthRequired: false,
        contentPageId: 'API_Docs',
        controller: ApiDocsCtrl,
        resolve: ApiDocsCtrl.resolve
    });
    $routeProvider.when('/apiDocs',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/apiDocs/apiDocs.html',
        isAuthRequired: false,
        contentPageId: 'API_Docs',
        controller: ApiDocsCtrl,
        resolve: ApiDocsCtrl.resolve
    });
    $routeProvider.when('/restAPIDocs',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/apiDocs/restAPIDocs.html',
        isAuthRequired: true,
        contentPageId: 'API_Docs',
        controller: ApiDocsCtrl,
        resolve: ApiDocsCtrl.resolve
    });
    $routeProvider.when('/sdkAPIDocs',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/apiDocs/sdkAPIDocs.html',
        isAuthRequired: true,
        contentPageId: 'API_Docs',
        controller: ApiDocsCtrl,
        resolve: ApiDocsCtrl.resolve
    });
    $routeProvider.when('/OauthAPIDocs',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/apiDocs/OauthAPIDocs.html',
        isAuthRequired: true,
        contentPageId: 'API_Docs',
        controller: ApiDocsCtrl,
        resolve: ApiDocsCtrl.resolve
    });
    $routeProvider.when('/diagnostics',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/diagnostics/diagnostics.html',
        isAuthRequired: true,
        contentPageId: 'Diagnostics',
        controller: DiagnosticsCtrl
    });
    $routeProvider.when('/manageAccount',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/manageAccount/manageAccount.html',
        isAuthRequired: true,
        contentPageId: 'My_Profile',
        controller: ManageAccountCtrl,
        resolve:ManageAccountCtrl.resolve
        
    });
    $routeProvider.when('/pricing',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/pricing/pricingDetails.html',
        isAuthRequired: false,
        contentPageId: 'Pricing',
        controller: PricingCtrl,
        resolve: PricingCtrl.resolve
    });    
    $routeProvider.when('/footer',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/global/footer.html',
        controller: FooterCtrl,
        resolve: FooterCtrl.resolve
    });
    $routeProvider.when('/Feedback',{
        title: 'Feedback',
        templateUrl: 'partials/Feedback/feedback.html',
        controller: FooterCtrl,
        resolve: FooterCtrl.resolve
    });
    $routeProvider.when('/contactUS',{
        title: 'Contact US',
        templateUrl: 'partials/contactus/contactus.html',
        controller: FooterCtrl,
        resolve: FooterCtrl.resolve
    });
    $routeProvider.when('/TermsConditions',{
        title: 'Terms of Conditions',
        templateUrl: 'partials/termsConditions/termsConditions.html',
        controller: FooterCtrl,
        resolve: FooterCtrl.resolve
    });
    $routeProvider.when('/Legal',{
        title: 'Legal',
        templateUrl: 'partials/legal/Legal.html',
        controller: FooterCtrl,
        resolve: FooterCtrl.resolve
    });
    $routeProvider.when('/login/:pageId',{
        title: 'SDP Landing',
        templateUrl: 'partials/login/landing.html',
        controller: LoginCtrl,
        resolve: LoginCtrl.resolve
    });
    $routeProvider.when('/faq',{
        title: 'SDP FAQ',
        templateUrl: 'partials/login/faq.html',
        isAuthRequired: false,
        controller: LoginCtrl,
        resolve: LoginCtrl.resolve
    });
    $routeProvider.when('/userInvitation/answer',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/userInvitation/confirmUser.html',
        isAuthRequired: false,
        controller: UserInvitationCtrl,
        resolve: UserInvitationCtrl.resolve
    });
    $routeProvider.otherwise({
        title: 'SDP Landing',
        templateUrl: 'partials/login/landing.html',
        controller: LoginCtrl,
        contentPageId: 'Login', 
        resolve: LoginCtrl.resolve
    });
    $routeProvider.when('/quickStart',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/quickStart/quickStart.html',
        isAuthRequired: false,
        contentPageId: 'QuickStart',
        controller: QuickStartCtrl,
        resolve: QuickStartCtrl.resolve
    });
    $routeProvider.when('/productServices',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/products/product_services.html',
        isAuthRequired: true,
        contentPageId: 'Product_Services',
        controller: ProductsCtrl,
        resolve: ProductsCtrl.resolve
    });
    $routeProvider.when('/platform/sso',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/login/ssoLogin.html',
        isAuthRequired: false,
        controller: PlatformSSOCtrl
    });
    $routeProvider.when('/productSendMessage/:seatId',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/products/products_sendmessage.html',
        isAuthRequired: true,
        contentPageId: 'Products',
        controller: ProductsCtrl,
        resolve: ProductsCtrl.resolve
    });
    $routeProvider.when('/productReadMessage/:seatId',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/products/products_readmessage.html',
        isAuthRequired: true,
        contentPageId: 'Products',
        controller: ProductsCtrl,
        resolve: ProductsCtrl.resolve
    });
    $routeProvider.when('/productDeleteMessage/:seatId',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/products/products_deletemessage.html',
        isAuthRequired: true,
        contentPageId: 'Products',
        controller: ProductsCtrl,
        resolve: ProductsCtrl.resolve
    });
    $routeProvider.when('/switchAccount/:accountName',{
        title: 'SDP DashBoard',
        templateUrl: 'partials/acctSummary/switchAccount.html',
        isAuthRequired: true,
        controller: SwitchAccountCtrl
    });
    $routeProvider.when('/logout',{
        title: 'SDP Landing',
        templateUrl: 'partials/login/logout.html',
        controller: LogoutCtrl,
        resolve: LogoutCtrl.resolve
    });
    //TODO - Check below line - Its not advisable to enable HTML5 Mode
    //$locationProvider.html5Mode(true);
}]).run(function(){
    // Code for any operation when routing is invoked - Any page URL change
});
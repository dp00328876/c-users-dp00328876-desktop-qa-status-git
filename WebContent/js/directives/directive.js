


onBoardingApp.directive("navMainMenuHeader",[
    'OAuthServices',
    '$log',
    '$rootScope',
    function(
        OAuthServices,
        $log,
        $rootScope
    ){
       return{
           restrict: 'EA',
           templateUrl: 'partials/templates/navMainMenu.html',
           link : function(scope, element, attrs){
               $rootScope.$on("startDisplayMenu", function () {
                   scope.displayMenu = true;
                   $rootScope.menuItemsInfo = OAuthServices.getAccountDataFromCache("menuItemsInfo");
                   //$log.info("Menu Items Directive:"+angular.toJson(scope.menuItemsInfo));
               });
               $rootScope.$on("endDisplayMenu", function () {
                   scope.displayMenu = false;
               });
           }
       };
}]);

onBoardingApp.directive("pageTitleMenu",['OAuthServices',
            '$log',
             '$rootScope',
             function(
          	 OAuthServices,
          	 $log,
          	 $rootScope
           ){
             return{
            	 restrict: 'EA',
                 templateUrl: 'partials/templates/pageTitleTemplate.html',
                 link : function(scope, element, attrs){
                 $rootScope.$on("startDisplayMenu", function () {
                 scope.displayMenu = true;    
                 $rootScope.menuItemsInfo = OAuthServices.getAccountDataFromCache("menuItemsInfo");
                 //$log.info("Menu Items Directive:"+angular.toJson(scope.menuItemsInfo));
                 });
                 $rootScope.$on("endDisplayMenu", function () {
                 scope.displayMenu = false;
                });
              }
              };
      }]);

onBoardingApp.directive("navMenu",['OAuthServices',
        '$log',
        '$rootScope',
         function(OAuthServices,
        		 $log,
                 $rootScope
            ){
		return{
			restrict: 'EA',
            templateUrl: 'partials/templates/navMenu.html',
            link : function(scope, element, attrs){
            $rootScope.$on("startDisplayMenu", function () {
            	scope.displayMenu = true;
            	$rootScope.menuItemsInfo = OAuthServices.getAccountDataFromCache("menuItemsInfo");
               //$log.info("Menu Items Directive:"+angular.toJson(scope.menuItemsInfo));
             });
            $rootScope.$on("endDisplayMenu", function () {
        	 scope.displayMenu = false;
         	});
           },
           controller : ['$scope', function($scope){
        	   $scope.selectMenu = function(index){
                   for(var i=0; i<$scope.menuItemsInfo.menuData.length; i++){
                	   if(i === index){
                		   $scope.menuItemsInfo.menuData[i].Selected = true;
                	   }
                	   else{
                		   $scope.menuItemsInfo.menuData[i].Selected = false;
                	   }
                   }                   
               };
           }]
 	   };
}]);

onBoardingApp.directive('onLongPress', ['$timeout', function($timeout) {
    return {
        restrict: 'A',
        link: function($scope, $elm, $attrs) {
            $elm.bind('touchstart mousedown', function(evt) {
                // Locally scoped variable that will keep track of the long press
                $scope.press = true;
                $scope.pressStartTime = new Date().getTime();

                // We'll set a timeout for 600 ms for a long press
                /*$timeout(function() {
                    if ($scope.longPress) {
                        // If the touchend event hasn't fired,
                        // apply the function given in on the element's on-long-press attribute
                        $scope.$apply(function() {
                            $scope.$eval($attrs.onLongPress)
                        });
                    }
                }, 600);*/
            });

            $elm.bind('touchend mouseup', function(evt) {
                // Prevent the onLongPress event from firing
                $scope.press = false;
                if($scope.pressStartTime){
                    $scope.pressEndTime = new Date().getTime() - $scope.pressStartTime;

                    if($scope.pressEndTime >600){
                        // Long Press
                        $scope.$apply(function() {
                            $scope.$eval($attrs.onLongPress)
                        });
                    }
                    else{
                        // Short Press
                        $scope.$apply(function() {
                            $scope.$eval($attrs.onShortPress)
                        });
                    }
                }


                // If there is an on-touch-end function attached to this element, apply it
                if ($attrs.onTouchEnd) {
                    $scope.$apply(function() {
                        $scope.$eval($attrs.onTouchEnd)
                    });
                }
            });
        }
    };
}]);

// underscore.js/underscore.min.js is used for this directive
/*onBoardingApp.directive('dropdownMultiSelect', ['$log', function($log){
    return {
        restrict: 'E',
        scope:{
            model: '=',
            options: '=',
            header: '@',
            pre_selected: '=preSelected'
        },
        template: "<div class='btn-group column100 listboxes'  data-ng-class='{open: open}'>"+
        "<button class='btn TxtBoxControl dropdown-toggle pull-left column75 listboxes' data-ng-click='open=!open;openDropdown()'>{{header}}</button>"+
        "<button class='btn TxtBoxArrow dropdown-toggle pull-left column25' data-ng-click='open=!open;openDropdown()'><span class='caret'></span></button>"+
        "<ul class='dropdown-menu' aria-labelledby='dropdownMenu'>" +
        "<li><a data-ng-click='selectAll()'><i class='fa fa-check-circle'></i>  Check All</a></li>" +
        "<li><a data-ng-click='deselectAll();'><i class='fa fa-times-circle'></i>  Uncheck All</a></li>" +
        "<li class='divider'></li>" +
        "<li data-ng-repeat='option in options'> " +
        "<a data-ng-click='setSelectedItem(option.name)'>{{option.name}}<span data-ng-class='isChecked(option.name)'></span></a>" +
        "</li>" +
        "</ul>" +

        "</div>"
        ,
        controller: function($scope){

            $scope.openDropdown = function(){
                $scope.selected_items = [];
                for(var i=0; i<$scope.pre_selected.length; i++){
                    $scope.selected_items.push($scope.pre_selected[i].id);
                }
            };

            $scope.selectAll = function () {
                $scope.model = _.pluck($scope.options, 'name');
              //  console.log($scope.model);
            };
            $scope.deselectAll = function() {
                $scope.model=[];
               // console.log($scope.model);
            };
            $scope.setSelectedItem = function(id){
                var id = id;

                if (_.contains($scope.model, id)) {
                    $scope.model = _.without($scope.model, id);
                } else {
                    $scope.model.push(id);
                }
               // console.log($scope.model);
                return false;
            };
            $scope.isChecked = function (id) {
                if (_.contains($scope.model, id)) {
                    return 'fa fa-check pull-right';
                }
                return false;
            };
        }
    }
}]);*/

/*
 data-slide-toggle="displaySignInSection" data-slide-toggle-duration="500"
 */

onBoardingApp.directive('slideToggle', [function() {
    return {
        restrict: 'A',
        scope:{
            isOpen: "=slideToggle" // 'data-slide-toggle' in our html
        },
        link: function(scope, element, attr) {
            var slideDuration = parseInt(attr.slideToggleDuration, 10) || 500;

            // Watch for when the value bound to isOpen changes
            // When it changes trigger a slideToggle
            scope.$watch('isOpen', function(newIsOpenVal, oldIsOpenVal){
                if(newIsOpenVal !== oldIsOpenVal){
                    element.stop().slideToggle(slideDuration);
                }
            });

        }
    };
}]);


onBoardingApp.directive('compareTo', [function() {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    }
}]);


// Disable and Anchor Tag
onBoardingApp.directive('aDisabled', [function() {
    return {
        compile: function(tElement, tAttrs, transclude) {
            //Disable ngClick
            tAttrs["ngClick"] = "!("+tAttrs["aDisabled"]+") && ("+tAttrs["ngClick"]+")";

            //return a link function
            return function (scope, iElement, iAttrs) {

                //Toggle "disabled" to class when aDisabled becomes true
                /*scope.$watch(iAttrs["aDisabled"], function(newValue) {
                    if (newValue !== undefined) {
                        iElement.toggleClass("disabled", newValue);
                    }
                });*/

                //Disable href on click
                iElement.on("click", function(e) {
                    if (scope.$eval(iAttrs["aDisabled"])) {
                        e.preventDefault();
                    }
                });
            };
        }
    };
}]);



onBoardingApp.directive("wait", ['$rootScope', '$log','$timeout','$q', function ($rootScope, $log, $timeout, $q) {
    //style="background-image: url(../images/ajax-loader2.gif);>"
return {
    restrict: "EAC",
    transclude: true,
    template: 	'<div ng-show="isRouting" class="loader-wait" >' +
        '<div class="spinner-background pl35"><svg class="cyclops-icon cyclops-spin md"><use xlink:href="#icon-spinner"></svg></div>'+
        '<div class="ajax-loader" >' +
        '</div>'+
        '</div>',


    link: function (scope,element) {
        var timers = [],
            cancelling = false;
        /*$rootScope.spinnerPositionCalculation();
            scope.SpinnerTopPos=$rootScope.SpinnerTopPos;
            scope.SpinnerLeftPos=$rootScope.SpinnerLeftPos;*/
        $rootScope.$on("inProgressStart", function () {

            scope.isRouting = true;
            /*if (!cancelling) {
                var timer = $timeout(function(){ },
                    10);
                timers.push(timer);
                timer.then(function() {
                    // $rootScope.loginPageisHide=true;
                    scope.isRouting = true;
                }, function() {
                    scope.isRouting = false;
                    //$rootScope.loginPageisHide=false;
                });
            }*/
            $log.info("Wait.js :: inProgressStart");
        });


        $rootScope.$on("inProgressEnd", function () {
            scope.isRouting = false;
            /*if (!cancelling) {
                cancelling = true;
                for(var i=0; i<timers.length; i++) {
                    $timeout.cancel(timers[i]); // probably a better way to do this... :)
                }
                timers = []; //clear the timers we are watching
            }*/
            $log.info("Wait.js :: inProgressEnd");
            /*$rootScope.loginLoader=true;
            cancelling = false;*/

        });
    }
}
}]);

onBoardingApp.directive('pwCheck', [function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
          var firstPassword = '#' + attrs.pwCheck;
          elem.add(firstPassword).on('keyup', function () {
            scope.$apply(function () {            	
            	var v = elem.val()===$(firstPassword).val();
            	if(!v){
            		elem.css('border', '2px solid red');
            	}else{
            		elem.css('border', '');
            	}            	
            	ctrl.$setValidity('pwmatch', v);
            });
          });
        }
      }
    }]);


onBoardingApp.directive('calendar', [function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                // call $apply to bring stuff to angular model
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var options = {
                dateFormat: "mm/dd/yy",
                maxDate:'today',
                // handle jquery date change
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };

            // jqueryfy the element
            elem.datepicker(options);
        }
    }
}]);

onBoardingApp.directive("messageDisplay", [function() {
	return {
		restrict : "E",
		templateUrl : "partials/templates/messageTemplate.html",
		controller : ['$scope', '$timeout', function($scope, $timeout) {

			/* Method to determine if there is any error. */
			$scope.isError = function() {
				var messages = $scope.getMessages();
				if (messages != null) {
					for ( var i = 0; i < messages.length; i++) {
						if (messages[i].type == 2) {
							return true;
						}
					}
				}
				return false;
			}
			
			/* Method to determine if there is an info messages. */
			$scope.isInfo = function() {
				var messages = $scope.getMessages();
				var infoFound = false;				
				if (messages != null) {
					for ( var i = 0; i < messages.length; i++) {
						if (messages[i].type == 3) {
							infoFound = true;
						}
						if (messages[i].type == 2) {
							return false;
						}
					}
				}
				return infoFound;
			}
			
			/* */
			$scope.hasMessage = function() {
				var messages = $scope.getMessages();
				if (messages != null) {
					if (messages.length > 0) {
						$timeout(function() {
							$scope.setMessages([]);
						}, 15000)
						return true;
					}
				}
				return false;
			}

			
			/* Method to get all the messages. */
			$scope.messages = function() {
				return $scope.getMessages();
			}
		}]
	}
}]);

/*onBoardingApp.directive('ngConfirmAction', ['$uibModal',
      function($uibModal) {
        var ModalInstanceCtrl = function($scope, $uibModalInstance) {
          $scope.ok = function() {
         	 $uibModalInstance.close();
          };

          $scope.cancel = function() {
         	 $uibModalInstance.dismiss('cancel');
          };
        };

        return {
          restrict: 'A',
          scope:{
            ngConfirmAction:"&",
            item:"="
          },
          link: function(scope, element, attrs) {
            element.bind('click', function() {
              var message = attrs.ngConfirmMessage || "Are you sure ?";

              //*This doesn't works
              var modalHtml = '<div class="modal-body">' + message + '</div>';
              modalHtml += '<div class="modal-footer"><button class="btn " ng-click="ok()">OK</button><button class="btn " ng-click="cancel()">Cancel</button></div>';

              var modalInstance = $uibModal.open({
                template: modalHtml,
                controller: ModalInstanceCtrl,
                size: 'sm',
                backdrop: 'static'
              });

              modalInstance.result.then(function() {
                scope.ngConfirmAction({item:scope.item}); //raise an error : $digest already in progress
              }, function() {
                //Modal dismissed
              });       
            });

          }
        }
      }]);*/


onBoardingApp.provider('toggleSwitchConfig', [function() {
    this.onLabel = 'On';
    this.offLabel = 'Off';
    this.knobLabel = '\u00a0';

    var self = this;
    this.$get = function() {
      return {
        onLabel: self.onLabel,
        offLabel: self.offLabel,
        knobLabel: self.knobLabel
      };
    };
  }]);

onBoardingApp.directive('toggleSwitch',['toggleSwitchConfig', function (toggleSwitchConfig) {
    return {
      restrict: 'EA',
      replace: true,
      require:'ngModel',
      scope: {
        disabled: '@',
        onLabel: '@',
        offLabel: '@',
        knobLabel: '@'
      },
      template: '<div role="radio" class="toggle-switch" ng-class="{ \'disabled\': disabled }">' +
          '<div class="toggle-switch-animate" ng-class="{\'switch-off\': !model, \'switch-on\': model}">' +
          '<span class="switch-left" ng-bind="onLabel"></span>' +
          '<span class="knob" ng-bind="knobLabel"></span>' +
          '<span class="switch-right" ng-bind="offLabel"></span>' +
          '</div>' +
          '</div>',
      compile: function(element, attrs) {
        if (!attrs.onLabel) { attrs.onLabel = toggleSwitchConfig.onLabel; }
        if (!attrs.offLabel) { attrs.offLabel = toggleSwitchConfig.offLabel; }
        if (!attrs.knobLabel) { attrs.knobLabel = toggleSwitchConfig.knobLabel; }

        return this.link;
      },
      link: function(scope, element, attrs, ngModelCtrl){
        var KEY_SPACE = 32;

        element.on('click', function() {
          scope.$apply(scope.toggle);
        });

        element.on('keydown', function(e) {
          var key = e.which ? e.which : e.keyCode;
          if (key === KEY_SPACE) {
            scope.$apply(scope.toggle);
            $event.preventDefault();
          }
        });

        ngModelCtrl.$formatters.push(function(modelValue){
          return modelValue;
        });

        ngModelCtrl.$parsers.push(function(viewValue){
          return viewValue;
        });

        ngModelCtrl.$viewChangeListeners.push(function() {
          scope.$eval(attrs.ngChange);
        });

        ngModelCtrl.$render = function(){
            scope.model = ngModelCtrl.$viewValue;
        };

        scope.toggle = function toggle() {
          if(!scope.disabled) {
            scope.model = !scope.model;
            ngModelCtrl.$setViewValue(scope.model);
          }
        };
      }
    };
  }]);


onBoardingApp.directive('inlineConfirm', ['$compile', 'ReadFileService', function($compile, ReadFileService) {
	return{
		restrict: 'EA',
		scope:{
			"confirmClass": "@confirmClass",
			"confirm": "&",
			"eventPageId": "@eventPageId",
			"preConfirm": "&",
			"postConfirm": "&"
		},
		transclude: true,
		template:
			'<button ng-hide="show" class="btn" ng-class="confirmClass" ng-click="show=true; performPreConfirm(preConfirm);"><ng-transclude/></button>' +
			'<div ng-show="show" class="pa0">' +
			'<span class="message">Are you sure? </span>' +
			'<button class="btn btn-danger btn-mini" ng-click="show=false; performPostConfirm(postConfirm);">No</button>&nbsp;' +
			'<button class="btn btn-primary btn-mini" ng-click="confirm(); reportEvent(eventPageId);  performPostConfirm(postConfirm);">Yes</button> ' +			
			'</div>',
		link: function(scope, element, attrs){
			
		},
		controller : ['$scope', function($scope) {
			$scope.reportEvent = function(eventPageId){
				$scope.show = false;
				var reportJSON = ReadFileService.getReportingDataCache('reportingConfig');					
				if(angular.isDefined(eventPageId) && reportJSON && angular.isDefined(reportJSON.Events[eventPageId])){						
					var rptParams = reportJSON.Events[eventPageId];
					console.log('PageEvent : '+angular.toJson(rptParams));
					// --------- Captured after login in OAUTH ---------
					//ga('set', 'userId', 'UserId');	// User ID
					//ga('set', 'language', 'en-us');	// Language
					//ga('set', 'dimension1', 'AccountName');	// Account NAme
					//ga('set', 'dimension2', 'NetworkID');	// Network ID
					// --------- Move to One Place ---------
			    	ga('set', 'page', rptParams.url);	// Page URL
					ga('set', 'title', rptParams.title);	// Page Title
			    	//ga('send', 'pageview');
			    	ga('send', 'event', rptParams.category, rptParams.action, rptParams.eventLabel);
				}
			}
			
			$scope.performPreConfirm = function(preConfirm) {
				if (angular.isDefined(preConfirm)) {
					preConfirm();
				}
			}
			
			$scope.performPostConfirm = function(postConfirm) {
				if (angular.isDefined(postConfirm)) {
					postConfirm();
				}
			}
		}]
	}

}]);

onBoardingApp.directive('inlineConfirmLink', ['$compile', 'ReadFileService', function($compile, ReadFileService) {
	return{
		restrict: 'EA',
		scope:{
			"confirmClass": "@confirmClass",
			"confirm": "&",
			"eventPageId": "@eventPageId"
		},
		transclude: true,
		template:
			'<a ng-hide="show" ng-class="confirmClass" ng-click="show=true"><ng-transclude/></a>' +
			'<div ng-show="show" class="pa0">' +
			'<span class="message">Are you sure? </span>' +
			'<a class="btn-mini" ng-click="show=false">No</a>&nbsp;' +
			'<a class="btn-mini" ng-click="confirm(); reportEvent(eventPageId);">Yes</a> ' +			
			'</div>',
		link: function(scope, element, attrs){
			
		},
		controller : ['$scope', function($scope) {
			$scope.reportEvent = function(eventPageId){
				$scope.show = false;
				var reportJSON = ReadFileService.getReportingDataCache('reportingConfig');					
				if(angular.isDefined(eventPageId) && reportJSON && angular.isDefined(reportJSON.Events[eventPageId])){						
					var rptParams = reportJSON.Events[eventPageId];
					console.log('PageEvent : '+angular.toJson(rptParams));
					// --------- Captured after login in OAUTH ---------
					//ga('set', 'userId', 'UserId');	// User ID
					//ga('set', 'language', 'en-us');	// Language
					//ga('set', 'dimension1', 'AccountName');	// Account NAme
					//ga('set', 'dimension2', 'NetworkID');	// Network ID
					// --------- Move to One Place ---------
			    	ga('set', 'page', rptParams.url);	// Page URL
					ga('set', 'title', rptParams.title);	// Page Title
			    	//ga('send', 'pageview');
			    	ga('send', 'event', rptParams.category, rptParams.action, rptParams.eventLabel);
				}
			}
		}]
	}

}]);

onBoardingApp.directive('pageEvent', ['$compile', '$timeout','ReadFileService', function($compile, $timeout, ReadFileService){
	return{
		restrict: 'EA',
		scope:{
			"pageId":"@pageId"
		},
		link: function(scope, element, attrs){
			scope.$watch(attrs['pageId'], function() {
				var report = function(){
					var reportJSON = ReadFileService.getReportingDataCache('reportingConfig');					
					if(angular.isDefined(attrs.pageId) && reportJSON && angular.isDefined(reportJSON.Events[attrs.pageId])){						
						var rptParams = reportJSON.Events[attrs.pageId];
						console.log('PageEvent : '+angular.toJson(rptParams));
						// --------- Captured after login in OAUTH ---------
						//ga('set', 'userId', 'UserId');	// User ID
						//ga('set', 'language', 'en-us');	// Language
						//ga('set', 'dimension1', 'AccountName');	// Account NAme
						//ga('set', 'dimension2', 'NetworkID');	// Network ID
						// --------- Move to One Place ---------
				    	ga('set', 'page', rptParams.url);	// Page URL
						ga('set', 'title', rptParams.title);	// Page Title
				    	ga('send', 'pageview');
					}
				}
				$timeout(report, 0);
           });
		}
	};
}]);

onBoardingApp.directive('apiEvent', ['$compile', '$timeout','ReadFileService', function($compile, $timeout, ReadFileService){
	return{
		restrict: 'EA',
		scope:{
			"pageId":"@pageId"
		},
		link: function(scope, element, attrs){
			element.bind('click', function(e){
				var report = function(){
					var reportJSON = ReadFileService.getReportingDataCache('reportingConfig');					
					if(angular.isDefined(attrs.apiEvent) && reportJSON && angular.isDefined(reportJSON.Events[attrs.apiEvent])){						
						var rptParams = reportJSON.Events[attrs.apiEvent];
						console.log('APIEvent : '+angular.toJson(rptParams));
						// --------- Captured after login in OAUTH ---------
						//ga('set', 'userId', 'UserId');	// User ID
						//ga('set', 'language', 'en-us');	// Language
						//ga('set', 'dimension1', 'AccountName');	// Account NAme
						//ga('set', 'dimension2', 'NetworkID');	// Network ID
						// --------- Move to One Place ---------
				    	ga('set', 'page', rptParams.url);	// Page URL
						ga('set', 'title', rptParams.title);	// Page Title
						//TODO Adding custom Parameters
				    	//ga('send', 'pageview');	// TODO Change to another event
				    	
				    	ga('send', 'event', rptParams.category, rptParams.action, rptParams.eventLabel);
					}
				}
				$timeout(report, 0);
			})
		}
	};
}]);

// Scroll DIV on top once user clicks on it
onBoardingApp.directive('scrollOnClick', [function() {
	  return {
	    restrict: 'A',
	    link: function(scope, $elm) {
	      $elm.on('click', function() {
	        $("body").animate({scrollTop: $elm.offset().top}, "slow");
	      });
	    }
	  }
	}]);

onBoardingApp.directive('stickyFooter', [
                                         '$timeout',
                                         function ($timeout) {
                                        	 return {
                                        		 restrict: 'A',
                                        		 link: function (scope, iElement, iAttrs) {
                                        			 var stickyFooterWrapper = $(iAttrs.stickyFooter);

                                        			 // Quite often you will occur a few wrapping `<div>`s in the
                                        			 // top level of your DOM, so we need to set the height
                                        			 // to be 100% on each of those. This will also set it on
                                        			 // the `<html>` and `<body>`.
                                        			 stickyFooterWrapper.parents().css('height', '100%');
                                        			 stickyFooterWrapper.css({
                                        				 'min-height': '100%',
                                        				 'height': 'auto'
                                        				// 'background-color': '#fff'
                                        			 });

                                        			 // Append a pushing div to the stickyFooterWrapper.
                                        			 var stickyFooterPush = $('<div class="push"></div>');
                                        			 stickyFooterWrapper.append(stickyFooterPush);

                                        			 var setHeights = function () {
                                        				 var height = iElement.outerHeight();
                                        				 stickyFooterPush.height(height);
                                        				 stickyFooterWrapper.css('margin-bottom', -(height));
                                        			 };

                                        			 $timeout(setHeights, 0);
                                        			 $(window).on('resize', setHeights);
                                        		 }
                                        	 };
                                         }
                                         ]);

onBoardingApp.directive('starRating', [function () {
    return {
        scope: {
            rating: '=',
            maxRating: '@',
            readOnly: '@',
            click: "&",
            mouseHover: "&",
            mouseLeave: "&"
        },
        restrict: 'EA',
        template:
            "<div style='display: inline-block; margin: 0px; padding: 0px; cursor:pointer;' ng-repeat='idx in maxRatings track by $index'> \
                    <img ng-src='{{((hoverValue + _rating) <= $index) && \"images/star-empty-lg.png\" || \"images/star-fill-lg.png\"}}' \
                    ng-Click='isolatedClick($index + 1)' \
                    ng-mouseenter='isolatedMouseHover($index + 1)' \
                    ng-mouseleave='isolatedMouseLeave($index + 1)'></img> \
            </div>",
        compile: function (element, attrs) {
            if (!attrs.maxRating || (Number(attrs.maxRating) <= 0)) {
                attrs.maxRating = '5';
            };
        },
        controller: ['$scope', '$element', '$attrs', function ($scope, $element, $attrs) {
            $scope.maxRatings = [];

            for (var i = 1; i <= $scope.maxRating; i++) {
                $scope.maxRatings.push({});
            };

            $scope._rating = $scope.rating;
			
			$scope.isolatedClick = function (param) {
				if ($scope.readOnly == 'true') return;

				$scope.rating = $scope._rating = param;
				$scope.hoverValue = 0;
				$scope.click({
					param: param
				});
			};
        }]
    };
}]);

onBoardingApp.directive('apiDocsSchema', ['$compile','$timeout', function($compile,$timeout) {
	return{
		restrict: 'EA',
		scope:{
			"schema": "@schema",
			"code": "@code",
			"source": "="
		},
		transclude: true,
		template:'<div><pre>{{output | json}}</pre><div/>',
		link: function(scope, element, attrs){
			scope.$watch(attrs['code'], function() {
				var process = function(){
					if(scope.schema.indexOf('#/definitions') === 0 ){
						var schArr = scope.schema.split('/');
						if(schArr.length === 3){
							scope.output = scope.source[schArr[2]];
						}						
					}
				};
				$timeout(process, 0);
           });
		},
		controller : ['$scope',function($scope) {

		}]
	}

}]);


onBoardingApp.directive("compareDate", [function() {
	return {
        require: 'ngModel',
        link: function (scope, elem, attrs,ctrl) {
          var StartDate = '#'+attrs.compareDate;
          elem.add(StartDate).on('mouseover', function () {
            scope.$apply(function () {            	
            	var endDate = convertToDate(elem.val());
            	var var2=convertToDate($(StartDate).val());
            	
            	var diff=endDate.diff(var2);
            	console.log("the difference between dates is :"+diff.days+"days");
            	var v;
            	if(diff.days < 0){
            		elem.css('border', '2px solid red');
            		v=false;
            	}else{
            		elem.css('border', '');
            		v=true;
            	} 
            	
            	 ctrl.$setValidity('compareDate', v);
            });
          });
        }
      }
}]);

onBoardingApp.directive('sameAs', [function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ngModel) {
          ngModel.$parsers.unshift(validate);
          
          // Force-trigger the parsing pipeline.
          scope.$watch(attrs.sameAs, function () {
            ngModel.$setViewValue(ngModel.$viewValue);
          });
          
          function validate(value) {
        	  var isValid = scope.$eval(attrs.sameAs) != value;
        	  if(!isValid){
        		  elem.css('border', '2px solid red');
        	  }
        	  else{
        		  elem.css('border', '');
        	  }
        	  ngModel.$setValidity('sameas', isValid);      
        	  return isValid ? value : undefined;
          }
        }
      };
    }]);

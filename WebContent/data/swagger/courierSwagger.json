{
	"swagger": "2.0",
	"info": {
		"description": "<p>These APIs can be used to manage all aspects of your SMS service.   Some things to keep in mind about our service: </p><p><ul><li>The international input character set for an SMS message is UTF-8.  If we receive a character outside that set, we will map it to a “?”.</li><li>The max size of an input message is 999 characters.Input messages larger than 999 will be rejected by Courier.</li><li> Messages larger than 160 characters will automatically be broken up and delivered into segments no larger than 160 characters. An input message of size 999 characters will be sent as 7 separate messages.We cannot guarantee that 7 segmented messages will be received by the destination in the correct order.A message size of 1001 characters will be rejected.</li><li> A Country code (E.164) is required for all destination phone numbers. CenturyLink does not validate the destination number is either working or capable of receiving SMS. You will be charged for SMS as long as a valid telephone number is defined as the destination.</li><li> You may be billed for any message that returns a 202 response from Courier. Other HTTP responses will neither be billed nor appear in Dumpster.</li><li> SMS destinations include all U.S. numbers and destinations around the world that do not charge an additional termination fee.  Refer to the website for a complete list of valid destinations.</li><li>A voice call to your SMS-only number will not be completed. You may have an accompanying CenturyLink voice service if you want voice calls to be supported with that number(s).If you want a voice service, you must order that service through the existing CenturyLink sales channels.At a later date, voice services will support as part of CenturyLink cloud.</li><li> Only SMS send is available.SMS receive will be supported at a later date.</li><li> Messages sent to 911 will not be completed. 911 is not a supported destination. This will be supported at a later date.</li><li> A Courier seat may be automatically suspended if the service usage follows our SPAM or solicitation profiles. A CTL suspended phone number will not allow SMS messages to be sent. Seat and TN charges will still apply. A customer care ticket is required to reset a number to working status. Another carrier may also “black-list” a number from sending any messages to their subscribers if the traffic profile meets their SPAM or solicitation profile. In this case, you will STILL be billed by CTL messages if their carrier does not notify us of the ban.</li><li>In most cases, SMS message will arrive within 5 seconds of being sent. SMS is not a guaranteed message delivery or message timelines protocol. If a message was unable to be sent after it was accepted (202), a failure message will be sent to your dumpster. This message will not be billed. This message is only saved if you have enabled storage during product configuration.</li><li>Do you want to check if our network and services are currently available? A diagnostics call is provided report the health of the network. There is no usage charges generated with the use of this API. The health check status is systematically updated every five minutes and executes live test messages.</li></ul></p>",
		"version": "10.9",
		"title": "Send SMS messages",
		"contact": {},
		"license": {
			"name": "©2015, CenturyLink. All Rights Reserved."
		}
	},
	"host": "piicou01:7091",
	"basePath": "/",
	"tags": [{
		"name": "courier-service-controller",
		"description": "Courier Service Controller"
	}],
	"paths": {
		"/courier/v1/messages/{phoneNumber}": {
			"post": {
				"tags": ["courier-service-controller"],
				"summary": "Send a message",
				"description": "Send a message using the Courier Service",
				"operationId": "sendMessagesUsingPOST",
				"consumes": ["application/json"],
				"produces": ["application/json"],
				"parameters": [{
					"name": "Authorization",
					"in": "header",
					"description": "Access token from authentication. Must be in format 'Bearer {accessToken}'",
					"required": true,
					"type": "string"
				}, {
					"name": "NetworkID",
					"in": "header",
					"description": "networkId",
					"required": false,
					"type": "string"
				}, {
					"name": "phoneNumber",
					"in": "path",
					"description": "phoneNumber",
					"required": true,
					"type": "string"
				}, {
					"in": "body",
					"name": "requestBody",
					"description": "requestBody",
					"required": true,
					"schema": {
						"$ref": "#/definitions/SendMessageRequest"
					}
				}],
				"responses": {
					"200": {
						"description": "OK",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"400": {
						"description": "The data was invalid.  Confirm that your 'to' telephone number is valid and the message is entered",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"202": {
						"description": "The message will be sent as quickly as possible",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"403": {
						"description": "The service is not provisioned for this account",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"502": {
						"description": "There is a problem with a backend system and we are unable to process the request",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					}
				},
				"security": [{
					"OAuth2": []
				}]
			}
		},
		"/courier/v1/serviceprofile/{phoneNumber}": {
			"get": {
				"tags": ["courier-service-controller"],
				"summary": "Read a Courier Service",
				"description": "Read an existing Courier Service",
				"operationId": "readCourierServiceUsingGET",
				"consumes": ["application/json"],
				"produces": ["application/json"],
				"parameters": [{
					"name": "Authorization",
					"in": "header",
					"description": "Access token from authentication. Must be in format 'Bearer {accessToken}'",
					"required": true,
					"type": "string"
				}, {
					"name": "NetworkID",
					"in": "header",
					"description": "networkId",
					"required": false,
					"type": "string"
				}, {
					"name": "phoneNumber",
					"in": "path",
					"description": "phoneNumber",
					"required": true,
					"type": "string"
				}],
				"responses": {
					"200": {
						"description": "The service data was retrieved",
						"schema": {
							"$ref": "#/definitions/ReadCourierServicePayload"
						}
					},
					"403": {
						"description": "The service is not provisioned for this account",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"502": {
						"description": "There is a problem with a backend system and we are unable to process the request",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"404": {
						"description": "The service does not exist in our system",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					}
				},
				"security": [{
					"OAuth2": []
				}]
			},
			"post": {
				"tags": ["courier-service-controller"],
				"summary": "Create a Courier Service",
				"description": "<p>Create a Courier Service for your account</p>",
				"operationId": "createCourierServiceUsingPOST",
				"consumes": ["application/json"],
				"produces": ["application/json"],
				"parameters": [{
					"name": "Authorization",
					"in": "header",
					"description": "Access token from authentication. Must be in format 'Bearer {accessToken}'",
					"required": true,
					"type": "string"
				}, {
					"name": "SeatID",
					"in": "header",
					"description": "seatId",
					"required": false,
					"type": "string"
				}, {
					"name": "NetworkID",
					"in": "header",
					"description": "networkId",
					"required": false,
					"type": "string"
				}, {
					"name": "PlatformUser",
					"in": "header",
					"description": "platformUser",
					"required": false,
					"type": "string"
				}, {
					"name": "NetworkUser",
					"in": "header",
					"description": "networkUser",
					"required": false,
					"type": "string"
				}, {
					"name": "phoneNumber",
					"in": "path",
					"description": "phoneNumber",
					"required": true,
					"type": "string"
				}, {
					"in": "body",
					"name": "requestBody",
					"description": "requestBody",
					"required": true,
					"schema": {
						"$ref": "#/definitions/CreateCourierServiceRequest"
					}
				}],
				"responses": {
					"200": {
						"description": "OK",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"400": {
						"description": "The data you provided did not allow the request to complete validation.",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"202": {
						"description": "There service will be created as quickly as possible",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"502": {
						"description": "There is a problem with a backend system and we are unable to process the request",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"409": {
						"description": "The data you provided did not allow the request to complete validation.",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					}
				},
				"security": [{
					"OAuth2": []
				}]
			}
		},
		"/courier/v1/serviceprofile/{phoneNumber}/{productName}": {
			"put": {
				"tags": ["courier-service-controller"],
				"summary": "Update the courier service",
				"description": "Change flags in the Courier Service which affect how it works.",
				"operationId": "updateCourierServiceUsingPUT",
				"consumes": ["application/json"],
				"produces": ["application/json"],
				"parameters": [{
					"name": "Authorization",
					"in": "header",
					"description": "Access token from authentication. Must be in format 'Bearer {accessToken}'",
					"required": true,
					"type": "string"
				}, {
					"name": "NetworkID",
					"in": "header",
					"description": "networkId",
					"required": false,
					"type": "string"
				}, {
					"name": "phoneNumber",
					"in": "path",
					"description": "phoneNumber",
					"required": true,
					"type": "string"
				}, {
					"name": "productName",
					"in": "path",
					"description": "productName",
					"required": true,
					"type": "string"
				}, {
					"in": "body",
					"name": "requestBody",
					"description": "requestBody",
					"required": true,
					"schema": {
						"$ref": "#/definitions/UpdateCourierServiceRequest"
					}
				}],
				"responses": {
					"200": {
						"description": "OK",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"400": {
						"description": "The data you provided did not allow the request to complete validation.",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"403": {
						"description": "The service is not provisioned for this account",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"502": {
						"description": "There is a problem with a backend system and we are unable to process the request",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"404": {
						"description": "The service does not exist in our system",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					}
				},
				"security": [{
					"OAuth2": []
				}]
			},
			"delete": {
				"tags": ["courier-service-controller"],
				"summary": "Delete a Courier Service",
				"description": "Remove the Courier Service from your account.",
				"operationId": "deleteCourierServiceUsingDELETE",
				"consumes": ["application/json"],
				"produces": ["application/json"],
				"parameters": [{
					"name": "Authorization",
					"in": "header",
					"description": "Access token from authentication. Must be in format 'Bearer {accessToken}'",
					"required": true,
					"type": "string"
				}, {
					"name": "NetworkID",
					"in": "header",
					"description": "networkId",
					"required": false,
					"type": "string"
				}, {
					"name": "PlatformUser",
					"in": "header",
					"description": "platformUser",
					"required": false,
					"type": "string"
				}, {
					"name": "NetworkUser",
					"in": "header",
					"description": "networkUser",
					"required": false,
					"type": "string"
				}, {
					"name": "phoneNumber",
					"in": "path",
					"description": "phoneNumber",
					"required": true,
					"type": "string"
				}, {
					"name": "productName",
					"in": "path",
					"description": "productName",
					"required": true,
					"type": "string"
				}],
				"responses": {
					"200": {
						"description": "OK",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"400": {
						"description": "The data you provided did not allow the request to complete validation.",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"403": {
						"description": "The service is not provisioned for this account",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"502": {
						"description": "There is a problem with a backend system and we are unable to process the request",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					},
					"404": {
						"description": "The service does not exist in our system",
						"schema": {
							"$ref": "#/definitions/CourierServiceResponse"
						}
					}
				},
				"security": [{
					"OAuth2": []
				}]
			}
		},
		"/courier/v1/status/{phoneNumber}": {
			"get": {
				"tags": ["courier-service-controller"],
				"summary": "Retrieve the Courier Service status",
				"description": "This call checks the results of a recently-executed test correlation to ensure that the Courier service is functioning properly.",
				"operationId": "getCourierStatusUsingGET",
				"consumes": ["application/json"],
				"produces": ["application/json"],
				"parameters": [{
					"name": "Authorization",
					"in": "header",
					"description": "Access token from authentication. Must be in format 'Bearer {accessToken}'",
					"required": true,
					"type": "string"
				}, {
					"name": "NetworkID",
					"in": "header",
					"description": "networkId",
					"required": false,
					"type": "string"
				}, {
					"name": "phoneNumber",
					"in": "path",
					"description": "phoneNumber",
					"required": true,
					"type": "string"
				}],
				"responses": {
					"200": {
						"description": "OK",
						"schema": {
							"$ref": "#/definitions/GetCourierStatusResponse"
						}
					}
				},
				"security": [{
					"OAuth2": []
				}]
			}
		}
	},
	"definitions": {
		"GetCourierStatusResponse": {
			"required": ["correlationId", "diagnosticSummary", "resultCode", "timestamp"],
			"properties": {
				"correlationId": {
					"type": "string",
					"description": "The correlation id of the request"
				},
				"diagnosticSummary": {
					"type": "string",
					"description": "A text description of the GetCourierStatus result"
				},
				"resultCode": {
					"type": "integer",
					"format": "int32",
					"description": "Result code of the request [0=success]"
				},
				"timestamp": {
					"type": "string",
					"format": "date-time",
					"description": "The time the message was sent"
				}
			},
			"description": "The data for the requested message"
		},
		"UpdateCourierServiceRequest": {
			"required": ["enabled"],
			"properties": {
				"enabled": {
					"type": "boolean",
					"description": "A flag which designates whether the service is active (allowing messages to be sent)"
				},
				"messageSaveStore": {
					"type": "boolean",
					"description": "A flag controlling the persistence of the SMS messages.   If true, sent messages are saved in our system"
				}
			},
			"description": "The request body sent to Update a service"
		},
		"CourierServiceResponse": {
			"required": ["correlationId", "responseMessage"],
			"properties": {
				"correlationId": {
					"type": "string",
					"description": "The correlation id of the request"
				},
				"responseMessage": {
					"type": "string",
					"description": "&quot;SUCCESS&quot; or an error message in the case of failure"
				}
			},
			"description": "A general response sent by the service"
		},
		"ReadCourierServicePayload": {
			"required": ["enabled", "messageSaveStore"],
			"properties": {
				"enabled": {
					"type": "boolean",
					"description": "A flag which designates whether the service is active (allowing messages to be sent)"
				},
				"messageSaveStore": {
					"type": "boolean",
					"description": "A flag controlling the persistence of the SMS messages.   If true, sent messages are saved in our system"
				}
			},
			"description": "A read courier service payload"
		},
		"CreateCourierServiceRequest": {
			"required": ["enabled", "productName"],
			"properties": {
				"enabled": {
					"type": "boolean",
					"description": "A flag which designates whether the service is active (allowing messages to be sent)"
				},
				"messageSaveStore": {
					"type": "boolean",
					"description": "A flag controlling the persistence of the SMS messages.   If true, sent messages are saved in our system"
				},
				"productName": {
					"type": "string",
					"description": "The product/service offering name for the call"
				}
			},
			"description": "The request body sent to create a service"
		},
		"SendMessageRequest": {
			"required": ["messageText", "to"],
			"properties": {
				"messageText": {
					"type": "string",
					"description": "The text being sent through the SMS service.  This can be a maximum of 1000 characters"
				},
				"to": {
					"type": "string",
					"description": "The destination for the SMS message.The phone number must be in <a href='https://en.wikipedia.org/wiki/E.164'>E.164 format</a>. At this time, SMS is allowed only to countries that do not charge a termination fee."
				}
			},
			"description": "Send an SMS message"
		}
	}
}

{
    "swagger": "2.0",
    "info": {
        "description": "<p>These APIs can be used to shop for CenturyLink provided Telephone Numbers (TNs).  TNs are available in almost every geography within the United States.   Both individual and groups of sequential numbers are available.   The number of TNs that may be assigned or reserved for a seat is dependent of the service.  By reserving a TN, it guarantees the number you have selected will be available to you when you are ready to use it.   A number will remain reserved for 3-days but the reservation may be renewed after that time if the number is still available.  After 3-days, the number is available for reservation on a first come basis.  In most cases, there are no charges associated with reserved numbers.   A CenturyLink provided TN is required for many services in Connected Communications, such as Courier-Basic.   Through a process called Number Porting, if you should leave CenturyLink and want to take your CenturyLink provided number(s) with you, you may request your new provider to initiate a process called Port-Out.  In this process, CenturyLink will move your numbers to your new provider.</p><p> If you currently have telecommunications service with an entity other than CenturyLink and want to bring your numbers with you, you may make a request to CenturyLink to request your number comes with you.   In a future release, we will allow numbers to be Ported-In to CenturyLink over the web.   At present, if you want to bring your numbers with you, you must order an accompanying voice service and call CenturyLink at 866-476-9909 or visit our web site at www.centurylink.com.</p><p>Other types of Monikers include:<ul><li>A VoIP domain.   If you are looking for a custom branded VoIP domain, let us know!</li><li>An Email domain.  If you are looking for a custom branded email domain, let us know!</li><li>An SMS short code.  If you are looking for a business branded SMS short code, let us know!</li><li>An Email name.  If you are looking for an outbound email name for an email relay, let us know!</li></ul></p> ",
        "version": "10.6",
        "title": "Moniker API"
    },
    "host": "10.122.65.17",
    "basePath": "/v1",
    "schemes": [
        "http"
    ],
    "consumes": [
        "application/json"
    ],
    "produces": [
        "application/json",
        "plain/text"
    ],
    "paths": {               
        "/available/phoneNumbers": {
            "get": {
                "tags": [
                    "Moniker Basic Operations"
                ],
                "summary": "Available PhoneNumbers",
                "description": "Provides Avaialble Phone Numbers based on NPA(Area Code Of Telephone Number), NPANXX(Area Code And Central Swithcing Office Designation Of Telephone Number), City and State.",
                "parameters": [
					{                
						"name": "Authorization",
						"in": "header",
						"description": "Access token from authentication. Must be in format 'Bearer {accessToken}'",
						"required": true,
						"type": "string"
					},
                    {
                        "name": "city",
                        "in": "query",
                        "description": "City And State Of Telephone Number .",
                        "required": false,
                        "type": "string"
                    },
                    {
                        "name": "state",
                        "in": "query",
                        "description": "City And State Of Telephone Number . State should be 2 characters.",
                        "required": false,
                        "type": "string"
                    },
                    {
                        "name": "npa",
                        "in": "query",
                        "description": "Area Code of Telephone Number . Should be 3 digits only.",
                        "required": false,
                        "type": "number",
                        "format": "int3"
                    },
                    {
                        "name": "npanxx",
                        "in": "query",
                        "description": "Area Code And exchange Of Telephone Number . Should be 6 digits only.",
                        "required": false,
                        "type": "number",
                        "format": "int6"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "An array of available Phone Numbers.",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/AvailablePhoneNumbers"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/BadRequest"
                        }
                    },
                    "401": {
                        "description": "UnAuthorized User",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/UnauthorizedUser"
                        }
                    },
                    "502": {
                        "description": "Bad Gateway",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/BadGateway"
                        }
                    },
                    "503": {
                        "description": "Service Unavailable - If Moniker database is unavailable.",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/ServiceUnavailable"
                        }
                    }
                }
            }
        },
        "/phoneNumbers": {
            "get": {
                "tags": [
                    "Moniker Basic Operations"
                ],
                "summary": "User PhoneNumbers",
                "description": "Provides user Phone Numbers based on NPA(Area Code Of Telephone Number), NPANXX(Area Code And Central Swithcing Office Designation Of Telephone Number), City and State.",
                "parameters": [
                ],
                "responses": {
                    "200": {
                        "description": "An array of User Phone Numbers.",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/MyPhoneNumbers"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/BadRequest"
                        }
                    },
                    "401": {
                        "description": "Unauthorized User",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/UnauthorizedUser"
                        }
                    },
                    "502": {
                        "description": "Bad Gateway",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/BadGateway"
                        }
                    },
                    "503": {
                        "description": "Service Unavailable - If Moniker database is unavailable.",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/ServiceUnavailable"
                        }
                    }
                }
            }
        },
        "/phoneNumbers/{phoneNum}": {           
            "post": {
                "tags": [
                    "Moniker Operations By Key"
                ],
                "summary": "Add User Phone Number",
                "description": "To Add user Phone Number",
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
					{                
						"name": "Authorization",
						"in": "header",
						"description": "Access token from authentication. Must be in format 'Bearer {accessToken}'",
						"required": true,
						"type": "string"
					},
                    {
                        "name": "SeatId",
                        "in": "header",
                        "description": "User Seat Id",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "phoneNum",
                        "in": "path",
                        "description": "Phone Number To Add",
                        "required": true,
                        "type": "string",
                        "format": "int10"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Phone Number Assigned To User, again tried then response with 200 and lastUpdate time will not change.",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/MyPhoneNumber"
                        }
                    },
                    "201": {
                        "description": "Phone Number Assigned To User.",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/MyPhoneNumber"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/BadRequest"
                        }
                    },
                    "401": {
                        "description": "Unauthorized User",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/UnauthorizedUser"
                        }
                    },
                    "404": {
                        "description": "Not Found",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/NotFound"
                        }
                    },
                    "502": {
                        "description": "Bad Gateway",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/BadGateway"
                        }
                    },
                    "503": {
                        "description": "Service Unavailable - If Moniker database is unavailable.",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/ServiceUnavailable"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "Version": {
            "properties": {
                "Version": {
                    "type": "array",
                    "minimum": 0,
                    "maximum": 200,
                    "items": {
                        "type": "object",
                        "properties": {
                            "productName": {
                                "type": "string",
                                "format": "int10",
                                "description": "Provides the product name of current version."
                            },
                            "productVersion": {
                                "type": "string",
                                "format": "int10",
                                "description": "Provides the product Version of current version."
                            },
                            "productBuild": {
                                "type": "string",
                                "format": "int10",
                                "description": "Provides the product Build of current version."
                            },
                            "productCommitId": {
                                "type": "string",
                                "format": "int10",
                                "description": "Provides the product Commit ID of current version."
                            }
                        }
                    }
                }
            }
        },
        "Metrics": {
            "properties": {
                "Metrics": {
                    "type": "array",
                    "minimum": 0,
                    "maximum": 200,
                    "items": {
                        "type": "object",
                        "properties": {
                            "connect-requests": {
                                "type": "string",
                                "format": "int10",
                                "description": "Provides the current Moniker connect-requests metrics."
                            },
                            "bytes-written": {
                                "type": "string",
                                "format": "int10",
                                "description": "Provides the current Moniker bytes-written metrics."
                            },
                            "put-requests": {
                                "type": "string",
                                "format": "int10",
                                "description": "Provides the current Moniker put-requests metrics."
                            },
                            "messages-sent": {
                                "type": "string",
                                "format": "int10",
                                "description": "Provides the current Moniker messages-sent metrics."
                            },
                            "metrics-other": {
                                "type": "string",
                                "format": "int10",
                                "description": "Provides other Moniker metrics."
                            }
                        }
                    }
                }
            }
        },
        "AvailablePhoneNumbers": {
            "properties": {
                "availablePhonenumbers": {
                    "type": "array",
                    "minimum": 0,
                    "maximum": 200,
                    "items": {
                        "type": "object",
                        "format": "int10",
                        "description": "Provides Avaialble Phone Numbers based on NPA(Area Code Of Telephone Number), NPANXX(Area Code And Central Swithcing Office Designation Of Telephone Number), City and State."
                    }
                }
            }
        },
        "Domains": {
            "properties": {
                "domains": {
                    "type": "array",
                    "minimum": 0,
                    "items": {
                        "type": "object",
                        "properties": {
                            "domainName": {
                                "type": "string",
                                "description": "Provides the the domain name."
                            }
                        }
                    }
                }
            }
        },
        "DomainName": {
            "properties": {
                "domainName": {
                    "type": "string"
                }
            }
        },
        "UserName": {
            "properties": {
                "userName": {
                    "type": "string"
                }
            }
        },
        "UserNames": {
            "properties": {
                "userNames": {
                    "type": "array",
                    "minimum": 0,
                    "items": {
                        "type": "object",
                        "properties": {
                            "userName": {
                                "type": "string",
                                "description": "Provides the the user name."
                            }
                        }
                    }
                }
            }
        },
        "BadRequest": {
            "type": "object",
            "properties": {
                "developerMessage": {
                    "type": "string",
                    "description": "You have have provided an invalid body "
                },
                "moreInfo": {
                    "type": "string",
                    "description": "For More Information Refer Developers Document.",
                    "default": "http://localhost:8080/v1/phoneNumbers/docs/api/query/"
                }
            }
        },
        "UnauthorizedUser": {
            "properties": {
                "developerMessage": {
                    "type": "string",
                    "description": "User Not Authorized "
                },
                "moreInfo": {
                    "type": "string",
                    "description": "For More Information Refer Developers Document.",
                    "default": "http://localhost:8080/v1/phoneNumbers/docs/api/query/"
                }
            }
        },
        "NotFound": {
            "properties": {
                "developerMessage": {
                    "type": "string",
                    "description": "Phone Number is not assigned to you, or you meant to POST to acquire the phone number "
                },
                "moreInfo": {
                    "type": "string",
                    "description": "For More Information Refer Developers Document.",
                    "default": "http://localhost:8080/v1/phoneNumbers/docs/api/query/"
                }
            }
        },
        "Conflict": {
            "properties": {
                "developerMessage": {
                    "type": "string",
                    "description": "illegal state change.  Attempt to change from Working to Assigned.  Someone may have already reserved one of more of your intended numbers."
                },
                "moreInfo": {
                    "type": "string",
                    "description": "For More Information Refer Developers Document.",
                    "default": "http://localhost:8080/v1/phoneNumbers/docs/api/query/"
                }
            }
        },
        "PreconditionFailed": {
            "properties": {
                "developerMessage": {
                    "type": "string",
                    "description": "The UserName@Domain is not unique."
                },
                "moreInfo": {
                    "type": "string",
                    "description": "For More Information Refer Developers Document.",
                    "default": "http://localhost:8080/v1/phoneNumbers/docs/api/query/"
                },
                "suggestedAlternatives": {
                    "type": "string",
                    "description": "UserName@Domain2 OR UserName@Domain3 OR UserName123@Domain OR 123UserName@Domain."
                }
            }
        },
        "BadGateway": {
            "properties": {
                "developerMessage": {
                    "type": "string",
                    "description": "Upstream service is unavilable"
                },
                "moreInfo": {
                    "type": "string",
                    "description": "For More Information Refer Developers Document.",
                    "default": "http://localhost:8080/v1/phoneNumbers/docs/api/query/"
                }
            }
        },
        "ServiceUnavailable": {
            "properties": {
                "developerMessage": {
                    "type": "string",
                    "description": "Moniker service is unavailable.  Please try again later "
                },
                "moreInfo": {
                    "type": "string",
                    "description": "For More Information Refer Developers Document.",
                    "default": "http://localhost:8080/v1/phoneNumbers/docs/api/query/"
                }
            }
        },
        "MyPhoneNumbers": {
            "properties": {
                "phoneNumbers": {
                    "type": "array",
                    "minimum": 0,
                    "maximum": 9,
                    "items": {
                        "type": "object",
                        "properties": {
                            "phoneNumber": {
                                "type": "string",
                                "format": "int10",
                                "description": "Phone Number of the user."
                            },
                            "currentState": {
                                "type": "string",
                                "description": "Current status.(working , aging and available)."
                            },
                            "lastUpdate": {
                                "type": "string",
                                "format": "date",
                                "description": "Last updated time stamp."
                            }
                        }
                    }
                }
            }
        },
        "MyPhoneNumber": {
            "properties": {
                "phoneNumber": {
                    "type": "string",
                    "format": "int10",
                    "description": "Phone Number of the user."
                },
                "currentState": {
                    "type": "string",
                    "description": "Current status.(assigned,working , aging and available)."
                },
                "lastUpdate": {
                    "type": "string",
                    "format": "date",
                    "description": "Last updated time stamp."
                }
            }
        }
    }
}
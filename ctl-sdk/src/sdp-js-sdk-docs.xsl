<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" indent="yes" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"/>
<xsl:template match="sdp-js-sdk-docs">
<xsl:variable name="space" select="' '"/>
<html>
	<head>
		<title>CTL JS-SDK</title>
		<link href="styles/bootstrap.css" rel="stylesheet" />
		<link href="styles/bootstrap-theme.css" rel="stylesheet" />		
		<script src="scripts/jquery.min.js"></script>
		<script src="scripts/bootstrap.min.js"></script>
	</head>
	<body>
		<header class="navbar navbar-inverse">
			<div class="navbar-header">
				<div class="navbar-brand"><xsl:value-of select="title"/></div>
			</div>
			<div class="collapse navbar-collapse">
				<nav id="bs-navbar" class="collapse navbar-collapse"> 
					<ul class="nav navbar-nav">						
						<li class="active"> <a href="#">API</a> </li>						
					</ul>					
				</nav>
			</div>			
		</header>
		
		<!-- Left Menu START -->
		<div data-spy="affix" class="panel panel-default">
			<div class="panel-heading"><strong>Available Services</strong></div>
			<div class="panel-body" role="complementary"> 
				<nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm"> 
					<ul class="nav bs-docs-sidenav">
						<xsl:for-each select="services/service">
						<li class="list-group"> <a href='#{namewospace}' data-toggle="collapse" data-target='#{namewospace}Panel'><strong><xsl:value-of select="name"/></strong></a>
							<ul id='{namewospace}Panel' class="nav bs-docs-sidenav collapse"> 
							<xsl:for-each select="methods/method">						
								<li> 
									<a href="#{name}"><xsl:value-of select="name"/>
									</a>
								</li>												
							</xsl:for-each>
							</ul>
						</li> 
						</xsl:for-each>
					</ul>
				</nav>
			</div>		
		</div>
		<!-- Left Menu END -->
		
		<!-- Contents START -->
		<div class="container bs-docs-container">			
			<div class="row"> 
				<div class="col-md-10 pull-right" role="main">
					<xsl:for-each select="services/service">
					<div id="{namewospace}" class="bs-docs-section"> 
						<h1 class="page-header">
							<xsl:value-of select="name"/>
						</h1>
						<p><xsl:value-of select="description"/></p>
						<h3 id="{namewospace}-methods">Available Methods</h3>
						<!-- Method Details START-->
						<xsl:for-each select="methods/method">
						<div id="{name}" class="panel panel-default">
						  <div class="panel-heading" style="padding:5px"><strong><xsl:value-of select="position()"/>.<xsl:value-of select="$space"/><xsl:value-of select="name"/></strong></div>
							  <div class="panel-body">
								<p class="text-justify"><xsl:value-of select="description"/></p>								
								<table class="table table-bordered table-condensed table-responsive">
									<thead>
									  <tr class="row active">
										<th>Parameter Name</th>
										<th>Type</th>
										<th>Description</th>
									  </tr>
									</thead>
									<tbody>
									  <xsl:for-each select="parameters/parameter">
									  <tr class="row">
										<td><xsl:value-of select="name"/></td>
										<td><xsl:value-of select="direction"/></td>
										<td>				
											<xsl:value-of select="description"/>
											<xsl:if test="paramDef/param">
												<div class="m-t"></div>
												<table class="table table-condensed table-bordered table-responsive">
													<thead>
													  <tr class="info">
														<th>Name</th>
														<th>Description</th>
													  </tr>
													</thead>
													<tbody>
													<xsl:for-each select="paramDef/param">
														<tr>
															<td><xsl:value-of select="name"/></td>
															<td><xsl:value-of select="description"/></td>
														</tr>
													</xsl:for-each>
													</tbody>
												</table>
											</xsl:if>
										</td>
									  </tr>
									  </xsl:for-each>
									</tbody>								
								  </table>

								<div class="panel panel-default">
									<div class="panel-heading" style="padding:5px;">
										<div class="panel-title">
											<a data-toggle="collapse" href="#{name}Sample">
											<strong>Sample Code</strong>
											</a>
										</div>
									</div>
									<div id="{name}Sample" class="panel-collapse collapse">
										<div class="panel-body">
											<ul class="nav nav-tabs" role="tablist">
												<!--li role="presentation" class="active"><a href="#{name}html" aria-controls="{name}html" role="tab" data-toggle="tab">HTML</a></li-->
												<li class="active" role="presentation"><a href="#{name}js" aria-controls="{name}js" role="tab" data-toggle="tab">Javascript</a></li>
											</ul>
											<div class="tab-content">
												<!--div role="tabpanel" class="tab-pane active" id="{name}html">
													<figure class="highlight">
														<pre style="padding:0px;line-height:1">
															<code class="language-html" data-lang="html">
															<xsl:value-of select="htmlexample"/>
															</code>
														</pre>
													</figure>
												</div-->
												<div role="tabpanel" class="tab-pane active" id="{name}js">
													<figure class="highlight">
														<pre style="padding:0px;line-height:1">
															<code class="language-html" data-lang="html">
															<xsl:value-of select="jsexample"/>
															</code>
														</pre>
													</figure>
												</div>
											</div>
										</div>
									</div>
								</div>								  
							</div>
						</div>
						</xsl:for-each>
						<!-- Method Details END-->												
					</div>
					</xsl:for-each>
				</div>
			</div>			
		</div>		
		<!-- Contents END -->		
	</body>
</html>
</xsl:template>
</xsl:stylesheet>
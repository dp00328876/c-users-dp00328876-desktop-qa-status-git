// Generated on 2013-07-19 using generator-angular 0.3.0
'use strict';
/*var LIVERELOAD_PORT = 35729;
var lrSnippet = require('connect-livereload')({port: LIVERELOAD_PORT});
var mountFolder = function(connect, dir) {
    return connect.static(require('path').resolve(dir));
};*/

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'


module.exports = function(grunt) {
	
	var SRC_DIR = grunt.option('SRC_DIR');
	var DEST_DIR = grunt.option('DEST_DIR');
	var CTL_ENV = grunt.option('CTLENV');

    // load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    // configurable paths
    /* var yeomanConfig = {
     app: 'app',
     dist: 'dist'
     };

     /*try {
     yeomanConfig.app = require('./bower.json').appPath || yeomanConfig.app;
     } catch (e) {}*/

    grunt.initConfig({

      //  yeoman: grunt.file.readJSON('grunt-properties.json'),
        
       // watch: {
       //     livereload: {
        //        options: {
        //            livereload: LIVERELOAD_PORT
        //        },
         //       files: [
         //           '<%= yeoman.app %>/{,*/}*.html',
         //           '{.tmp,<%= yeoman.app %>}/css/{,*/}*.css',
         //           '{.tmp,<%= yeoman.app %>}/js/{,*/}*.js',
         //          '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
          //      ]
          //  }
       // },
       /* ver : {
            internallib: {
                forceVersion: '<%= yeoman.version %>',
                phases: [
                    {
                        files: ['<%= yeoman.dist %>/lib/internal/*.js'],
                        references: ['<%= yeoman.dist %>/index.html']
                    }
                ],
                versionFile: 'version.json',
                baseDir: '<%= yeoman.dist %>/lib/internal'
            }
        },*/
       /* connect: {
            options: {
                port: 9000,
                // Change this to '0.0.0.0' to access the server from outside.
                hostname: 'localhost'
            },
            livereload: {
                options: {
                    middleware: function(connect) {
                        return [
                            lrSnippet,
                            mountFolder(connect, '.tmp'),
                            mountFolder(connect, yeomanConfig.app)
                        ];
                    }
                }
            },
            test: {
                options: {
                    middleware: function(connect) {
                        return [
                            mountFolder(connect, '.tmp'),
                            mountFolder(connect, 'test')
                        ];
                    }
                }
            },
            dist: {
                options: {
                    middleware: function(connect) {
                        return [
                            mountFolder(connect, yeomanConfig.dist)
                        ];
                    }
                }
            }
        },*/
        /*open: {
            server: {
                url: 'http://localhost:<%= connect.options.port %>'
            }
        },*/
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        DEST_DIR+'/*',
                        !DEST_DIR+'/.git*'
                    ]
                }]
            },
            cleanjs: {
                src: [SRC_DIR+'/js/configuration/configuration.js']
            },
            server: '.tmp'
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            all: [
                'Gruntfile.js',
                SRC_DIR+'/js/{,*/}*.js'
            ]
        },
        coffee: {
            dist: {
                files: [{
                    expand: true,
                    cwd: SRC_DIR+'/js',
                    src: '{,*/}*.coffee',
                    dest: '.tmp/js',
                    ext: '.js'
                }]
            },
            test: {
                files: [{
                    expand: true,
                    cwd: 'test/spec',
                    src: '{,*/}*.coffee',
                    dest: '.tmp/spec',
                    ext: '.js'
                }]
            }
        },
        // not used since Uglify task does concat,
        // but still available if needed
        /*concat: {
         dist: {}
         },*/
        concat: {
            js: {
                options: {
                    separator: ';',
                },
            	//src: SRC_DIR+'/js/**/*.js',
            	src: [
            	      	SRC_DIR+'/js/**/*.js',
            	      	'!**/configuration.js'
            	      ],
                dest: DEST_DIR+'/js/scripts.js',
               /* dist: {
                    //src: ['src/intro.js', 'src/project.js', 'src/outro.js'],
                	src: '<%= yeoman.app %>/js/controllers/*.js',
                    dest: '<%= yeoman.dist %>/js/scripts.js',
                  },*/               
            },
        },
        
        rev: {
            dist: {
                files: {
                    src: [
                        DEST_DIR+'/js/**/*.js',
                        !DEST_DIR+'/js/configuration/**',
                        DEST_DIR+'/css/{,*/}*.css',
                        DEST_DIR+'/images/{,*/}*.{png,jpg,jpeg,gif,webp}',
                        DEST_DIR+'/css/fonts/*'
                    ]
                }
            }
        },
        useminPrepare: {
            html: SRC_DIR+'/index.html',
            options: {
                dest: DEST_DIR
            }
        },
        usemin: {
            html: [DEST_DIR+'/**/*.html'],
            css: [DEST_DIR+'/css/{,*/}*.css'],
            options: {
                basedir: DEST_DIR,
                dirs: [DEST_DIR]
            }
        },
        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: SRC_DIR+'/images',
                    src: '{,*/}*.{png,jpg,jpeg}',
                    dest: DEST_DIR+'/images'
                }]
            }
        },
        cssmin: {
            // By default, your `index.html` <!-- Usemin Block --> will take care of
            // minification. This option is pre-configured if you do not wish to use
            // Usemin blocks.
            // dist: {
            //   files: {
            //     '<%= yeoman.dist %>/css/main.css': [
            //       '.tmp/css/{,*/}*.css',
            //       '<%= yeoman.app %>/css/{,*/}*.css'
            //     ]
            //   }
            // }
        },
        htmlmin: {
            dist: {
                options: {
                    /*removeCommentsFromCDATA: true,
                     // https://github.com/yeoman/grunt-usemin/issues/44
                     //collapseWhitespace: true,
                     collapseBooleanAttributes: true,
                     removeAttributeQuotes: true,
                     removeRedundantAttributes: true,
                     useShortDoctype: true,
                     removeEmptyAttributes: true,
                     removeOptionalTags: true*/
                },
                files: [{
                    expand: true,
                    cwd: SRC_DIR ,
                    src: ['*.html', 'partials/*.html'],
                    dest: DEST_DIR
                }]
            }
        },
		/*html2js: {
			options: {
				 base: '<%= yeoman.app %>/'
			},
			files: {
                src: ['<%= yeoman.app %>/partials/xx/*.html','<%= yeoman.app %>/partials/yy/*.html'],
				dest: '<%= yeoman.dist %>/templates.js'
			}
		},*/
        
        config: {
            src: SRC_DIR,
            dist: DEST_DIR
          },
        
        'string-replace': {
            all: {
              files: {
            	  '<%= config.dist %>/lib/sdk/ctl-sdk.js' : '<%= config.dist %>/lib/sdk/ctl-sdk.js',
            	  '<%= config.dist %>/js/configuration/configuration.js' : '<%= config.dist %>/js/configuration/configuration.js',
            	  '<%= config.dist %>/index.html' : '<%= config.dist %>/index.html'
              },
              options: {
                replacements: [{
                  pattern: '%CTLENV%',
                  replacement: grunt.option('CTLENV') //'<%= yeoman.env %>'
                },
                {
             	   pattern: '%BUILDDATE%',
             	   replacement: grunt.option('BUILDDATE') 
                },
                {
             	   pattern: '%APPVERSION%',
             	   replacement: grunt.option('APPVERSION') 
                	},
                 {
                     pattern: '%BUILDNUMBER%',
                     replacement: grunt.option('BUILDNUMBER') 
                 },
                 {
                     pattern: '%GA-ID%',
                     replacement: grunt.option('GA-ID') //'<%= yeoman.env %>'
                   }
                ]
              }
            }            
          },
        
        
        // Put files not handled in other tasks here
        copy: {
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: SRC_DIR ,
                    dest: DEST_DIR,
                    src: [
                        '*.{ico,png,txt}',
                        'data/**/*',
                        'css/**/*',
                        'partials/**/*',
                        'stubs/**/*',
                        'fonts/*',
                        'lib/**/*',
                        'js/configuration/**/*.js',
                        'images/{,*/}*.{gif,webp,svg}'
                    ]
                }, {
                        expand: true,
                        cwd: '.tmp/images',
                        dest: DEST_DIR+'/images',
                        src: [
                            'generated/*'
                        ]
                    },
                    {
                        expand: true,
                        cwd: './',
                        dest: DEST_DIR+'/WEB-INF/',
                        src: [
                            'web.xml'
                        ]
                    }

                ]
            },
            configurationjs: {
                files: [{
                        expand: true,
                        cwd: SRC_DIR+'/js/configuration',
                        dest: SRC_DIR+'/js/configuration/',
                        src: [
                            'configuration.js.'+ CTL_ENV
                        ],
                        rename: function(dest, src) {
                            return dest + 'configuration.js';
                        }
                    }
                ]
            }
        },
        concurrent: {
            dist: [
                'imagemin',
                'htmlmin'
            ]
        },
        /*karma: {
            unit: {
                configFile: 'karma.conf.js',
                singleRun: true
            }
        },
        cdnify: {
            dist: {
                html: ['<%= yeoman.dist %>/*.html']
            }
        },*/
        //ngmin: { dist : { src : [<%=concat.output%>.js], dest: '<%=concat.output%>.annotated.js'} }
        //ngmin: { dist : { src : '*.js', dest: '*.js'} },
        ngmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: DEST_DIR+'/js',
                    src: '*.js',
                    dest: DEST_DIR+'/js'
                }]
            }
        },
        uglify: {
            options: {
                mangle: false
            },
            dist: {
                files: [{
                	src: DEST_DIR+'/js/scripts.js' ,
                	 dest:  DEST_DIR+'/js/scripts.js'
                    
                },
                {
                	src: DEST_DIR + '/lib/sdk/ctl-sdk.js' ,
                	dest: DEST_DIR + '/lib/sdk/ctl-sdk.js'
                }
                ]
            }

        }
    });


    /*grunt.registerTask('server', function(target) {
        if (target === 'dist') {
            return grunt.task.run(['build', 'open', 'connect:dist:keepalive']);
        }

        grunt.task.run([
            'clean:server',
            'connect:livereload',
            'open',
            'watch'
        ]);
    });*/


    grunt.registerTask('test', [
        'clean:server',
        'connect:test'
        // 'karma'
    ]);

    grunt.registerTask('build', [
        'clean:dist',
        ///'clean:cleanjs', //Task to delete old configuration.js file
        'copy:configurationjs', //Task to rename configuration js file based on env flag
        'useminPrepare',
        'concurrent:dist',        
        'concat:js',
        'copy:dist',
        'string-replace:all',
        ///'cdnify',
        'ngmin',
        ///'cssmin',
		///'html2js',
        'uglify',
        'rev',
        'usemin'
       
    ]);

    grunt.registerTask('default', [
        //'jshint',
       // 'test',
		//'clean:cleanjs', //Task to delete old configuration.js file
		//'copy:configurationjs', //Task to rename configuration js file based on env flag
        'build'
    ]);
};
